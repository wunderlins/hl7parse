set(MINGW64_BASE "C:/msys64/mingw64" CACHE STRING "Default path to mingw installation, Windows only (-mINGW64_BASE=<path>)" )

# gnu default locations
set(CMAKE_INSTALL_INCLUDEDIR include)
set(CMAKE_INSTALL_LIBDIR lib)
set(CMAKE_INSTALL_BINDIR bin)
set(CMAKE_INSTALL_SYSCONFDIR etc)
set(CMAKE_INSTALL_DATAROOTDIR share)
if(WIN32 OR APPLE)
  set(CMAKE_INSTALL_LIBDIR "./")
  set(CMAKE_INSTALL_BINDIR "./")
  set(CMAKE_INSTALL_SYSCONFDIR "./")
else()
    include(GNUInstallDirs)
endif()

# These will broadly follow the same layout, but we define the new 
# INSTALL_LIBDIR, INSTALL_BINDIR, INSTALL_INCLUDEDIR, and 
# INSTALL_CMAKEDIR variables, which the users can override, 
# if they are so inclined:
#[[
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY
  ${PROJECT_BINARY_DIR}/${CMAKE_INSTALL_LIBDIR})
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY
  ${PROJECT_BINARY_DIR}/${CMAKE_INSTALL_LIBDIR})
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY
  ${PROJECT_BINARY_DIR}/${CMAKE_INSTALL_BINDIR})
]]

# make sure to use environment variables if set
if(DEFINED ENV{QT_DIR})
    set(QT_DIR $ENV{QT_DIR})
    set(Qt5_DIR $ENV{QT_DIR})
endif()
if(DEFINED ENV{Qt5_DIR})
    set(QT_DIR $ENV{Qt5_DIR})
    set(Qt5_DIR $ENV{Qt5_DIR})
endif()