# create / truncate file
set(output-file ../generated/lib7.h)
file(WRITE ${output-file} "")

set(incl "")
set(buffer "")
foreach(f ${lib7-export-headers})
    # set marker
    get_filename_component(filename ${f} NAME)
    string(APPEND buffer "// ${filename} ///////////////////\n")

    file(READ ${f} content)
    set(content "${content}\n")
    string(JOIN "\;" content ${content}) # make string from list

    # match all includes
    string(REGEX MATCHALL "#include[\t ]+<[^>]+>" incl_file ${content})
    list(APPEND incl ${incl_file})
    #message(STATUS "${incl}")

    # clean guards
    string(REGEX REPLACE "#ifndef[^\n]+H_?\n" "" content "${content}")
    string(REGEX REPLACE "#define[^\n]+H_?\n" "" content "${content}")
    string(REGEX REPLACE "#endif[^\n]+H_?\n" "" content "${content}")
    string(REGEX REPLACE "#pragma[ \t]+once[ \t]*\n" "" content "${content}")

    # remove includes
    string(REGEX REPLACE "#include[^\n]+\n" "" content "${content}")

    # remove externs
    string(REGEX REPLACE "#ifdef __cplusplus *\nextern \"C\" *{ *\n#endif *\n" "" content "${content}")
    string(REGEX REPLACE "#ifdef __cplusplus *\n *} *\n#endif *\n" "" content "${content}")

    #file(APPEND ${output-file} ${content})
    string(APPEND buffer ${content})
    #message(STATUS "f=${f}")
    #break()
endforeach()

# append all headers and guard
list(REMOVE_DUPLICATES incl)
string(JOIN "\n" incl_str ${incl}) # make string from list
#message(STATUS "${incl_str}")
file(WRITE ${output-file} "#ifndef LIB7_H\n#define LIB7_H\n\n${incl_str}\n\n${buffer}\n#endif // LIB7_H\n")

