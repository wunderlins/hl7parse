# do this after all install(...) commands so that all targets are finalized. 
# Essentially, the last thing included at the end of the top-level CMakeLists.txt

set(_fmt TGZ)
if(WIN32)
  set(_fmt ZIP)
endif()

set(CPACK_PACKAGE_NAME "${PACKAGE_NAME}")
set(CPACK_PACKAGE_VENDOR "${PACKAGE_VENDOR}")
set(CPACK_PACKAGE_CONTACT "${PACKAGE_AUTHOR}")

set(CPACK_PACKAGE_VERSION "${VERSION_VIEWER}")
list(GET VERSION_VIEWER_LIST 0 MAJOR)
list(GET VERSION_VIEWER_LIST 1 MINOR)
list(GET VERSION_VIEWER_LIST 2 PATCH)
set(CPACK_PACKAGE_VERSION_MAJOR "${MAJOR}")
set(CPACK_PACKAGE_VERSION_MINOR "${MINOR}")
set(CPACK_PACKAGE_VERSION_PATCH "${PATCH}")

set(CPACK_PACKAGE_DESCRIPTION ${PACKAGE_NAME})
set(CPACK_PACKAGE_CONTACT "${PACKAGE_AUTHOR} ${APP_EMAIL}")

set(CPACK_RESOURCE_FILE_LICENSE "${CMAKE_CURRENT_SOURCE_DIR}/LICENSE.txt")
set(CPACK_RESOURCE_FILE_README "${CMAKE_CURRENT_SOURCE_DIR}/README.md")
set(CPACK_OUTPUT_FILE_PREFIX "${CMAKE_CURRENT_SOURCE_DIR}/release")
set(CPACK_PACKAGE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})

# should we build a debian package?
# https://cmake.org/cmake/help/v3.3/module/CPackDeb.html
find_program(APT_AVAILABLE "apt")
if(APT_AVAILABLE AND NOT APPLE)
  list(APPEND _fmt "DEB")
  set(CPACK_DEBIAN_PACKAGE_DEPENDS, "libqt5gui5,libqt5widgets5")
  set(CPACK_DEBIAN_PACKAGE_SHLIBDEPS ON)
  set(CPACK_DEBIAN_PACKAGE_DESCRIPTION "${CPACK_PACKAGE_DESCRIPTION}")
  install(FILES 
    ${CMAKE_CURRENT_LIST_DIR}/../generated/7view.desktop
    DESTINATION ${CMAKE_INSTALL_DATAROOTDIR}/applications/
  )
endif()

# windows NSIS installer
if(WIN32)
  list(APPEND CPACK_GENERATOR "NSIS")
  set(CPACK_PACKAGE_INSTALL_DIRECTORY "${PACKAGE_NAME}")
  set(CPACK_NSIS_INSTALLED_ICON_NAME "${CMAKE_CURRENT_SOURCE_DIR}/resources/app2.ico")
  set(CPACK_NSIS_MUI_ICON "${CMAKE_CURRENT_SOURCE_DIR}/resources/app2.ico")
  set(CPACK_NSIS_HELP_LINK ${APP_URL})
  set(CPACK_NSIS_URL_INFO_ABOUT ${APP_URL})
  set(CPACK_NSIS_CONTACT ${APP_EMAIL})
  set(CPACK_NSIS_DISPLAY_NAME ${PACKAGE_NAME})
  set(CPACK_NSIS_PACKAGE_NAME ${PACKAGE_NAME})
  set(CPACK_NSIS_MANIFEST_DPI_AWARE true)
  
  # add a link directly into the start menu
  set(CPACK_NSIS_CREATE_ICONS_EXTRA "CreateShortCut '$SMPROGRAMS\\\\$START_MENU\\\\7view.lnk' '$INSTDIR\\\\7view.exe'")
  set(CPACK_NSIS_DELETE_ICONS_EXTRA "Delete '$SMPROGRAMS\\\\7view.lnk'" )
  
  # add explorer.exe context menu entries
  # https://docs.microsoft.com/en-us/windows/win32/shell/context-menu-handlers
  #[[
    Windows Registry Editor Version 5.00

    [HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\CommandStore\shell\hl7.view]
    "Icon"="\"C:\\Program Files\\7view\\7view.exe\""
    @="Open with &7View"

    [HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\CommandStore\shell\hl7.view\command]
    @="\"C:\\Program Files\\7view\\7view.exe\" \"%1\""

    [HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\CommandStore\shell\hl7.pdf]
    "Icon"="\"C:\\Program Files\\7view\\7pdf.exe\""
    @="Display &PDF from MDM Message"

    [HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\CommandStore\shell\hl7.pdf\command]
    @="\"C:\\Program Files\\7view\\7pdf.exe\" \"%1\""

    [HKEY_CLASSES_ROOT\*\shell\7tool]
    "MUIVerb"="&7 Tools"
    "SubCommands"="hl7.view;hl7.pdf"
    "Icon"="\"C:\\Program Files\\7view\\7view.exe\""
  ]]
  SET(CPACK_NSIS_EXTRA_INSTALL_COMMANDS "
    WriteRegStr HKLM 'SOFTWARE\\\\Microsoft\\\\Windows\\\\CurrentVersion\\\\Explorer\\\\CommandStore\\\\shell\\\\7tool.view' '' 'Open with &7View'
    WriteRegStr HKLM 'SOFTWARE\\\\Microsoft\\\\Windows\\\\CurrentVersion\\\\Explorer\\\\CommandStore\\\\shell\\\\7tool.view' 'Icon' '\\\"$INSTDIR\\\\7view.exe\\\"'
    WriteRegStr HKLM 'SOFTWARE\\\\Microsoft\\\\Windows\\\\CurrentVersion\\\\Explorer\\\\CommandStore\\\\shell\\\\7tool.view\\\\command' '' '\\\"$INSTDIR\\\\7view.exe\\\" \\\"%1\\\"'

    WriteRegStr HKLM 'SOFTWARE\\\\Microsoft\\\\Windows\\\\CurrentVersion\\\\Explorer\\\\CommandStore\\\\shell\\\\7tool.pdf' '' 'Open &PDF from HL7-MDM'
    WriteRegStr HKLM 'SOFTWARE\\\\Microsoft\\\\Windows\\\\CurrentVersion\\\\Explorer\\\\CommandStore\\\\shell\\\\7tool.pdf' 'Icon' '\\\"$INSTDIR\\\\7pdf.exe\\\"'
    WriteRegStr HKLM 'SOFTWARE\\\\Microsoft\\\\Windows\\\\CurrentVersion\\\\Explorer\\\\CommandStore\\\\shell\\\\7tool.pdf\\\\command' '' '\\\"$INSTDIR\\\\7pdf.exe\\\" \\\"%1\\\"'

    WriteRegStr HKCR '*\\\\shell\\\\7tool' 'MUIVerb' '&7 Tools'
    WriteRegStr HKCR '*\\\\shell\\\\7tool' 'SubCommands' '7tool.view;7tool.pdf'
    WriteRegStr HKCR '*\\\\shell\\\\7tool' 'Icon' '\\\"$INSTDIR\\\\7view.exe\\\"'
  ")
  SET(CPACK_NSIS_EXTRA_UNINSTALL_COMMANDS "
    DeleteRegKey HKLM 'SOFTWARE\\\\Microsoft\\\\Windows\\\\CurrentVersion\\\\Explorer\\\\CommandStore\\\\shell\\\\7tool.view'
    DeleteRegKey HKLM 'SOFTWARE\\\\Microsoft\\\\Windows\\\\CurrentVersion\\\\Explorer\\\\CommandStore\\\\shell\\\\7tool.pdf'
    DeleteRegKey HKCR '*\\\\shell\\\\7tool'
  ")

  if (NOT QT_STATIC)
    #"${Qt5Core_DIR}/../../../bin/windeployqt.exe" --dir qt_deps --no-quick-import --no-system-d3d-compiler --no-angle --no-opengl-sw "${CMAKE_CURRENT_BINARY_DIR}/viewer/7view.exe"
    #"${Qt5Core_DIR}/../../../bin/windeployqt64releaseonly.exe" --release --dir qt_deps --no-quick-import --no-system-d3d-compiler --no-angle --no-opengl-sw "${CMAKE_CURRENT_BINARY_DIR}/uploader/uploader.exe"
    find_program(WINDEPLOYQT_EXECUTABLE windeployqt HINTS "${QT_DIR}/../../../bin")
    add_custom_target(Qt5deps ALL
      "${WINDEPLOYQT_EXECUTABLE}" --release --dir qt_deps --no-quick-import --no-system-d3d-compiler --no-angle --no-opengl-sw "${CMAKE_CURRENT_BINARY_DIR}/viewer/7view.exe"
      DEPENDS 7view
      COMMENT "Finding Qt5 shared libraries"
      WORKING_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}"
    )

    #[[
    file(MAKE_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/qt_deps/")
    file(GET_RUNTIME_DEPENDENCIES
      #EXECUTABLES "${CMAKE_BINARY_DIR}/lib/lib7.dll"
      RESOLVED_DEPENDENCIES_VAR found_deps
      UNRESOLVED_DEPENDENCIES_VAR unfound_deps
    )

    if (unfound_deps MATCHES ".*WINDOWS.*" OR unfound_deps MATCHES ".*windows.*")
      # not all required libraries found, probably error
      #message(ERROR "Not all dependencies of libcurl-4.dll found.")
      #continue()
    endif ()

    foreach (found_dep IN LISTS found_deps)
      message(STATUS "lib7.dll dep: ${found_dep}")
      # Determine if the dep library should be copied.
      #if (dep_not_wanted)
      if (found_dep MATCHES ".*WINDOWS.*" OR found_dep MATCHES ".*windows.*")
        continue()
      endif ()
      configure_file("${found_dep}" "${CMAKE_CURRENT_BINARY_DIR}/qt_deps/" COPYONLY)
    endforeach ()
    ]]
    
    # Copy all Qt5 assets to windows bin directory
    install(DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/qt_deps/" DESTINATION "./")
  endif()
endif()

# OSX app bundle
# FIXME: may be better to use qtmacdeploy to build Bundle, cpack Bundle 
#        seems not to be well documented
if(APPLE)
  list(APPEND CPACK_GENERATOR "BUNDLE")
  set(CPACK_BUNDLE_NAME ${PACKAGE_NAME}) #: The bundle name
  set(CPACK_BUNDLE_ICON "${CMAKE_CURRENT_SOURCE_DIR}/resources/logo.icns") #: The bundle icon
  set(CPACK_BUNDLE_PLIST "${CMAKE_CURRENT_SOURCE_DIR}/generated/info.plist") #: The bundle plist

  # generated info plist
  #install(CODE "
  #  include(BundleUtilities)
  #  fixup_bundle(/Users/wus/Projects/hl7parse/build/_CPack_Packages/Darwin/Bundle/7view-darwin-0.4.1/7view.app \"\" \"\")
  #" COMPONENT Runtime)
endif()

# not .gitignore as its regex syntax is distinct
file(READ ${CMAKE_CURRENT_LIST_DIR}/cpack_ignore.txt _cpack_ignore)
string(REGEX REPLACE "\n" ";" _cpack_ignore ${_cpack_ignore})
set(CPACK_SOURCE_IGNORE_FILES "${_cpack_ignore}")
message("${CPACK_SOURCE_IGNORE_FILES}")

set(CPACK_GENERATOR ${_fmt})
set(CPACK_SOURCE_GENERATOR ${_fmt})
string(TOLOWER ${CMAKE_SYSTEM_NAME} _sys)
string(TOLOWER ${PROJECT_NAME} _project_lower)
set(CPACK_PACKAGE_FILE_NAME "${CPACK_PACKAGE_NAME}-${_sys}-${CPACK_PACKAGE_VERSION}")
set(CPACK_SOURCE_PACKAGE_FILE_NAME "${CPACK_PACKAGE_NAME}-${_sys}-${CPACK_PACKAGE_VERSION}-source")

install(FILES ${CPACK_RESOURCE_FILE_README} ${CPACK_RESOURCE_FILE_LICENSE}
  DESTINATION ${CMAKE_INSTALL_DATAROOTDIR}/docs/${PROJECT_NAME})

# icons
install(FILES 
  "${CMAKE_CURRENT_SOURCE_DIR}/resources/logo2.png"
  "${CMAKE_CURRENT_SOURCE_DIR}/resources/logo2_cmd.png"
  DESTINATION ${CMAKE_INSTALL_DATAROOTDIR}/${PROJECT_NAME})

if(WIN32)
  install(FILES 
    "${CMAKE_CURRENT_SOURCE_DIR}/resources/app2.ico" 
    "${CMAKE_CURRENT_SOURCE_DIR}/resources/app2_cmd.ico"
    DESTINATION ${CMAKE_INSTALL_DATAROOTDIR}/${PROJECT_NAME})
endif()

if(APPLE)
  install(FILES 
    "${CMAKE_CURRENT_SOURCE_DIR}/resources/logo2.icns"
    DESTINATION ${CMAKE_INSTALL_DATAROOTDIR}/${PROJECT_NAME})
endif()

include(CPack)