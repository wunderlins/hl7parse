/*
    treemodel.cpp

    Provides a simple tree model to show how to create and use hierarchical
    models.
*/

#include <QDebug>
#include <QTextBlock>
#include <QTextEdit>
#include "treeitem.h"
#include "treemodel.h"
#include "decode.h"
#include "parsercb.h"

TreeModel::TreeModel(TreeItem *root, QObject *parent)
    : QAbstractItemModel(parent)
    , watcher(new QFutureWatcher<int>)
    , future(new QFuture<int>)
{
    rootItem = root;
}

//! [0]
TreeModel::TreeModel(const QString &filename, QObject *parent)
    : QAbstractItemModel(parent)
    , watcher(new QFutureWatcher<int>)
    , future(new QFuture<int>)
{
    QList<QVariant> rootData;
    rootData << "Segment" << "#" << "Data";
    rootItem = new TreeItem(rootData);

    w = (MainWindow*) parent; // getMainWindow();
    connect(this, &TreeModel::showAlert, w, &MainWindow::alert);
    connect(watcher, SIGNAL(finished()), this, SLOT(finishedParsing()));

    // pass window instance to static callback class
    parserCb::init(this);
    m_filename = filename;
}

void TreeModel::loadFile() {
    // HL7 File descriptor
    if ((fd = fopen(m_filename.toUtf8(), "rb")) == nullptr) {
        qDebug() << "Failed to open: " << m_filename;
        w->log_line("Failed to open: " + m_filename);
    } else {
        w->log_line("Opening HL7 file: " + m_filename);

        // initialize separator data structure
        hl7_meta = init_hl7_meta_t();

        // check if this file has a bom;
        // this will move df at the first byte after the BOM
        hl7_meta->bom = detect_bom(fd);

        // parse hl7 file
        this->message = create_message_t(hl7_meta);

        // register callbacks
        this->message->state->cb_start    = parserCb::start;
        this->message->state->cb_end      = parserCb::end;
        this->message->state->cb_progress = parserCb::progress;
        this->message->state->cb_segment  = parserCb::segment;

        // show progress bar
        //w->cb_start(this->message);

        // use QConcurrent for threading
        //QThreadPool pool;
        *future = QtConcurrent::run(hl7_decode, fd, &message);
        watcher->setFuture(*future);

        // close file handle in finishedParsing
        //fclose(fd);
    }

    w->getTreeMain()->setModel(this);
    //w->getTreeMain()->setColumnHidden(1, true);
    w->getTreeMain()->setColumnWidth(0, 75);
    connect(w->getTreeMain()->selectionModel(),
            SIGNAL(w->selectionChanged(const QItemSelection &, const QItemSelection &)),
            this,
            SLOT(w->on_treeMainSelectionChanged(const QItemSelection &, const QItemSelection &)));

}

void TreeModel::finishedParsing() {

    // future.result() blocks, use FutureWatcher to make this function
    // run in a separate, non-blocking thread
    //
    // Example:
    // https://codejourneys.blogspot.com/2008/06/qt-simple-example-of-use-of.html
    int ret = future->result();

    // try to decocde message
    //ret = hl7_decode(fd, &message);
    qDebug() << "parsed file, return code: " << ret;

    if (ret != 0) {
        // log_error("We failed to parse message at line %d, exiting.", message->num_children);
        qDebug() << "cannot parse file, exit code of parser: " << ret;
        QString message("Internal parser error");
        if (ret == 1)
            message = "General parser Error";
        if (ret == 2)
            message = "Delimiter Detection failed";
        if (ret == 3)
            message ="First segment is not MSH";
        emit showAlert("Parser Failure", QString("%1: %2").arg(message).arg(ret));
    } else {
        char* meta_string = hl7_meta_string(hl7_meta);
        log_info("%s", meta_string);
        w->log_line(meta_string);
        std::free(meta_string);

        // initialize model
        //setupModelData(rootItem);

        // setup trees
        emit parsed();
    }

}
//! [0]

void TreeModel::free() {
    free_message_t(message);
    //free_hl7_meta(hl7_meta);
    delete rootItem;
}

//! [1]
TreeModel::~TreeModel()
{
    /*
    free_message_t(message);
    free(hl7_meta);
    delete rootItem;
    */
}
//! [1]

//! [2]
int TreeModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return static_cast<TreeItem*>(parent.internalPointer())->columnCount();
    else
        return rootItem->columnCount();
}
//! [2]

//! [3]
QVariant TreeModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (role != Qt::DisplayRole)
        return QVariant();

    TreeItem *item = static_cast<TreeItem*>(index.internalPointer());

    return item->data(index.column());
}
//! [3]

//! [4]
Qt::ItemFlags TreeModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        //return NULL;
        return Qt::NoItemFlags;

    return QAbstractItemModel::flags(index);
}
//! [4]

//! [5]
QVariant TreeModel::headerData(int section, Qt::Orientation orientation,
                               int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
        return rootItem->data(section);

    return QVariant();
}
//! [5]

//! [6]
QModelIndex TreeModel::index(int row, int column, const QModelIndex &parent)
            const
{
    if (!hasIndex(row, column, parent)) {
        qDebug() << "no index " << row << " " << column;
        return QModelIndex();
    }

    TreeItem *parentItem;

    if (!parent.isValid())
        parentItem = rootItem;
    else
        parentItem = static_cast<TreeItem*>(parent.internalPointer());

    TreeItem *childItem = parentItem->child(row);
    if (childItem)
        return createIndex(row, column, childItem);
    else
        return QModelIndex();
}
//! [6]

//! [7]
QModelIndex TreeModel::parent(const QModelIndex &index) const
{
    if (!index.isValid()) {
        qDebug() << "no parent";
        return QModelIndex();
    }

    TreeItem *childItem = static_cast<TreeItem*>(index.internalPointer());
    TreeItem *parentItem = childItem->parentItem();

    if (parentItem == rootItem)
        return QModelIndex();

    return createIndex(parentItem->row(), 0, parentItem);
}
//! [7]

//! [8]
int TreeModel::rowCount(const QModelIndex &parent) const
{
    TreeItem *parentItem;
    if (parent.column() > 0)
        return 0;

    if (!parent.isValid())
        parentItem = rootItem;
    else
        parentItem = static_cast<TreeItem*>(parent.internalPointer());

    return parentItem->childCount();
}
//! [8]

QModelIndex TreeModel::findNode(TreeItemMeta *meta, bool subTree) {

    // bounds check
    if (subTree == false && rootItem->childCount() <= meta->segment) {
        QModelIndex r;
        return r;
    }

    // find the segment
    TreeItem *segmentItem = nullptr;
    QModelIndex rootIndex = index(meta->segment, 0);
    if (subTree) {
        segmentItem = rootItem;
    } else {
        segmentItem = static_cast<TreeItem*>(rootIndex.internalPointer());
    }


    // find fieldlist or field
    for(int fli=0; fli<segmentItem->childCount(); fli++) {
        QModelIndex fieldlist;
        if (subTree) {
            fieldlist = index(fli, 0);
        } else {
            fieldlist = index(fli, 0, rootIndex);
        }
        TreeItem *fieldlistItem = static_cast<TreeItem*>(fieldlist.internalPointer());

        if (fieldlistItem->meta->fieldlist == meta->fieldlist &&
            fieldlistItem->meta->field     == meta->field) {
            qDebug() << "found node: " << QString::number(fli);

            // check if we have to find a component
            if (meta->comp == -1)
                return fieldlist;

            QModelIndex component = index(meta->comp, 0, fieldlist);
            //TreeItem *componentItem = static_cast<TreeItem*>(component.internalPointer());

            // check if we have to find a sub component
            if (meta->subcmp == -1)
                return component;


            QModelIndex subcomponent = index(meta->subcmp, 0, component);
            //TreeItem *subcomponentItem = static_cast<TreeItem*>(subcomponent.internalPointer());

            return subcomponent;

        }
    }

    return rootIndex;
}

QString TreeModel::editorText(node_t *node) {
    MainWindow *w = getMainWindow();
    if (w->lineLength > 0 && node->length > (size_t) w->lineLength) {
        QString line(reinterpret_cast<const char*>(node->data));
        line = line.mid(0, w->lineLength);
        line.replace('\n', ' ');
        line.replace('\r', ' ');
        return "<span>" + line.toHtmlEscaped() + "<span class='continued'>…</span></span>";
    }

    return QString(reinterpret_cast<const char*>(node->data)).toHtmlEscaped();
}

bool TreeModel::firstFieldIsSequence(node_t *segment) {
    QString buffer;
    if (segment->num_children > 0)
        if (segment->children[0]->num_children > 0 &&
            segment->children[0]->children[0]->length < 4)
            buffer = reinterpret_cast<const char*>(segment->children[0]->children[0]->data);

    if (buffer.length()) {
        int number = buffer.toInt();
        QString newNumber = QString::number(number);
        if (newNumber == buffer)
            return true;
    }
    return false;
}

void TreeModel::setupModelData(TreeItem *parent, int start_line) {
    // reference to our main window which holds the textEditor
    MainWindow *w = getMainWindow();

    // color palette
    QString color_text("#FFF");

    QString sep_field  = QString(hl7_meta->sep_field).toHtmlEscaped();
    QString sep_rep    = QString(hl7_meta->sep_rep).toHtmlEscaped();
    QString sep_comp   = QString(hl7_meta->sep_comp).toHtmlEscaped();
    QString sep_subcmp = QString(hl7_meta->sep_subcmp).toHtmlEscaped();

    // disable all signals on textEdit, treeMain and TreeSub
    w->getTreeMain()->blockSignals(true);
    w->getTreeSub()->blockSignals(true);

    // TODO: try to load version from MSH-12
    
    // load schema information
    hl7types = get_version_by_str(w->defaultVersion.toStdString().c_str());

    QString v = w->defaultVersion;
    hl7type_handle *hl7types = version;
    while (hl7types != NULL) {
        //versions.append(hl7types->version);
        if (strcmp(v.toStdString().c_str(), hl7types->version) == 0)
            break;
        hl7types = hl7types->next;
    }
    Segment *seg = NULL;
    Field *field = NULL;

    // loop over all messages
    qDebug() << "Message: " << message->num_children;
    for (int i=start_line; i<message->num_children; i++) {
        QString segment_name(reinterpret_cast<const char*>(message->segments[i]->data));
        // qDebug() << "Item: " << segment_name;

        seg = get_segment((const char*) message->segments[i]->data, hl7types->id);
        if (seg != NULL) {
            qDebug() << "SEG " << seg->id;
        }

        QString segment_buffer("");
        segment_buffer.append("<b style='color: "+w->color_segment+"'>" + segment_name + "</b>");

        hl7_addr_t *seg_addr = create_addr();
        seg_addr->seg_count = i;
        seg_addr->segment = (char*) segment_name.toStdString().c_str();

        TreeItem *p;
        p = new TreeItem(message->segments[i], seg_addr, parent);
        p->segment = seg;
        if (seg != NULL) {
            p->descr = QString(seg->name);
        }

        // loop over all Field lists in Segment
        for (int ii=0; ii<message->segments[i]->num_children; ii++) {
            node_t *n = message->segments[i]->children[ii];
            qDebug() << "n  " << i << " " << ii << "(" << n->num_children << ") " << n->type << " " << reinterpret_cast<const char*>(n->data);
            if (segment_name == "MSH" || segment_name == "MSA") {
                if (ii > 1)
                    segment_buffer.append("<b style='color: "+w->color_delimiter+"'>"+ sep_field +"</b>");
            } else {
                segment_buffer.append("<b style='color: "+w->color_delimiter+"'>"+ sep_field +"</b>");
            }

            field = NULL;
            if (seg != NULL && seg->childCount >= ii) {
                field = &seg->fields[ii];
            }

            // field
            for (int iii=0; iii<n->num_children; iii++) {
                if (iii > 0) {
                    segment_buffer.append("<b style='color: "+w->color_delimiter+"'>"+ sep_rep +"</b>");
                }
                node_t *n1 = n->children[iii];
                //qDebug() << "n1 " << i << " " << ii << " " << iii << "(" << n1->num_children << ") " << n1->type << " " << reinterpret_cast<const char*>(n->data);

                if (field != NULL) {
                    qDebug() << "field type: " << ii << " -> " << field->type->id
                            << ", " << field->name;
                }

                TreeItem *t1;
                hl7_addr_t *flist_addr = clone_addr(seg_addr);
                flist_addr->fieldlist = ii;
                flist_addr->field     = iii;
                t1 = new TreeItem(n1, flist_addr, p);
                Field *f1 = field;
                t1->field = f1;
                if (field != NULL) {
                    t1->hl7Type = f1->type;
                    t1->descr = QString(field->name);
                }

                //cursor.insertHtml(editorText(n1));
                segment_buffer.append(editorText(n1));

                // component
                for (int iiii=0; iiii<n1->num_children; iiii++) {
                    if (iiii > 0) {
                        //cursor.insertHtml("<b style='color: "+w->color_delimiter+"'>"+ sep_comp +"</b>");
                        segment_buffer.append("<b style='color: "+w->color_delimiter+"'>"+ sep_comp +"</b>");
                    }

                    node_t *n2 = n1->children[iiii];
                    TreeItem *t2 = nullptr;
                    hl7_addr_t *comp_addr = clone_addr(flist_addr);
                    comp_addr->comp = iiii;
                    t2 = new TreeItem(n2, comp_addr, t1);

                    // check if Field has a type with length greater or 
                    // equal to iiii
                    if (field && field->type->childCount > iiii) {
                        t2->component = &field->type->component[iiii];
                        t2->hl7Type = t2->component->type;
                        t2->descr = QString(t2->component->name);
                    }
                    segment_buffer.append(editorText(n2));

                    // subcomponent
                    for (int iiiii=0; iiiii<n2->num_children; iiiii++) {

                        if (iiiii > 0) {
                            segment_buffer.append("<b style='color: "+w->color_delimiter+"'>"+ sep_subcmp +"</b>");
                        }

                        node_t *n3 = n2->children[iiiii];
                        //qDebug() << "n2 " << i << " " << ii << " " << iii << " " << iiii << "(" << n2->num_children << ") " << n2->type << " " << (const char*) n2->data;

                        TreeItem *t3 = nullptr;
                        hl7_addr_t *subcomp_addr = clone_addr(comp_addr);
                        subcomp_addr->subcmp = iiiii;
                        t2 = new TreeItem(n3, subcomp_addr, t2);

                        // check if Field has a type with length greater or 
                        // equal to iiii
                        if (t2->component && t2->component->type->childCount >= iiiii) {
                            t3->component = &t2->component->type->component[iiiii];
                            t3->hl7Type = t3->component->type;
                            t3->descr = QString(t3->component->name);
                        }

                        segment_buffer.append(editorText(n3));

                        //qDebug() << segment_name << ": " << QString::number(i) << "-" << QString::number(ii) << "-" << QString::number(iii) << "-" << QString::number(iiii) << "-" << QString::number(iiiii) << ": " << QString(reinterpret_cast<const char*>(n3->data)).toHtmlEscaped();
                    }

                }

            }
        }
        emit add_text_editor_line(segment_buffer, i);
    }

    // re-enable signals
    w->getTreeMain()->blockSignals(false);
    w->getTreeSub()->blockSignals(false);

    emit loading_segment(start_line, (char *) message->segments[start_line]->data);
}

// hl7parse callbacks
void TreeModel::cb_progress(message_t *message, unsigned long total, unsigned long current) {
    int percent = current*100/total;
    //qDebug() << "Progress: " << percent;
    emit loading_progress(percent);
    //log_line("Progress: " + QString::number(percent));
    //m_statusProgressBar->setValue(percent);
}

void TreeModel::cb_start(message_t *message) {
    //m_statusProgressBar->show();
    emit loading_start();
}

void TreeModel::cb_end(message_t *message, unsigned long max, unsigned long current, int exit_code) {
    //m_statusProgressBar->hide();
    emit loading_end(exit_code);

    // close file handle in finishedParsing
    fclose(fd);
}

void TreeModel::cb_segment(message_t *message, unsigned long num, char *name) {
    qDebug() << "cb_segment " << name;
    setupModelData(rootItem, num);
    //emit loading_segment(num, name);
}
