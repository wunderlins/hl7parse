#include "view_util.h"
#include "base64.h"

view_util::view_util(node_t *node, hl7_meta_t *meta, int chunk_size, QObject *parent): QObject(parent)
{
    m_node = node;
    m_meta = meta;
    m_chunk_size = chunk_size;

    // analise data
    //(void) get_mime();
}

view_util::~view_util() {}

QByteArray view_util::get_stub() {
    // make sure the stub is not empty
    if (m_stub != nullptr) {
        return m_stub;
    }

    if (m_node->length > m_chunk_size)
        m_stub = QByteArray((char *) m_node->data, m_chunk_size);
    else if (m_node->length > 0)
        m_stub = QByteArray((char *) m_node->data, m_node->length - 1);
    else 
        m_stub = QByteArray("");

    return m_stub;
}

bool view_util::is_hl7_encoded() {
    if (m_is_hl7_initialized)
        return m_is_hl7;

    QByteArray data;
    if(is_base64_encoded() != NO)
        data = get_stub_decoded();
    else
        data = get_stub();

    // check if we find an escape character in the node
    for (int i=0; i<data.length(); i++) {
        if (data.at(i) == m_meta->sep_escape) {
            m_is_hl7_initialized = true;
            m_is_hl7 = true;
            return true;
        }
    }

    m_is_hl7_initialized = true;
    m_is_hl7 = false;
    return false;
}

probability view_util::is_base64_encoded() {
    if (m_is_base64_initialized)
        return m_is_base64;

    if (m_node->length < 2) {
        if (!m_node->length) {
            m_is_base64 = NO;
            m_is_base64_initialized = true;
            return NO;
        }

        if (m_node->data[0] == 0) {
            m_is_base64 = NO;
            m_is_base64_initialized = true;
            return NO;
        }
    }
    
    QByteArray stub = get_stub();

    // minimal cost check
    //
    // check if the length is devideable by 4, if not, it can't 
    // be base64 decoded
    if (stub.length() > 0 && stub.length() % 4 != 0) {
        m_is_base64_initialized = true;
        m_is_base64 = NO;
        return NO;
    }

    // use the base64 decode routine and check the return value. If it fails
    // it can't be base 64 encoded (invalid char or length)
    //
    // this will use some processing power on long data but we have limited
    // it to chunck_size
    char *in  = (char*) malloc(m_chunk_size);
    unsigned char *out = (unsigned char*) malloc(m_chunk_size);
    size_t inl = m_stub.length();
    size_t outl = m_chunk_size;
    int ret = 0;

    memcpy(in, m_stub.data(), m_stub.length());
    ret = hl7_64decode(in, inl, out, &outl);

    // free uneeded resources
    m_stub_decoded = QByteArray((const char*) out, outl); // will copy, we can free
    free(in);
    free(out);

    if (ret != 0) {
        m_is_base64_initialized = true;
        m_is_base64 = NO;
        return NO;
    }

    // only partially checked, see if the byte array ends in '='
    if (m_stub.size() < m_node->length) {
        if (m_node->data[m_node->length - 2] != '=') {
            m_is_base64 = MAYBE;
            m_is_base64_initialized = true;
            return MAYBE;
        }
    } else {
        if (m_stub.at(m_stub.size() - 1) != '=') {
            m_is_base64 = MAYBE;
            m_is_base64_initialized = true;
            return MAYBE;
        }
    }
    
    // ends in '=' and is decodeable, we are pretty certain
    m_is_base64 = YES;
    m_is_base64_initialized = true;
    return YES;
}

QString view_util::get_file_suffix() {
    if (m_is_base64_initialized == false)
        get_mime();

    return m_file_suffix;
}

QString view_util::get_mime() {
    if (m_is_base64_initialized == false)
        is_base64_encoded();
    
    if (m_mime_type != nullptr)
        return m_mime_type;
    
    QMimeDatabase db;
    QMimeType mime;
    if (m_is_base64 != NO)
        mime = db.mimeTypeForData(m_stub_decoded);
    else
        mime = db.mimeTypeForData(m_stub);

    m_mime_type = mime.name();
    m_file_suffix = mime.preferredSuffix();

    m_is_base64_initialized = true;
    return m_mime_type;
}

int view_util::get_chunk_size() {
    return m_chunk_size;
}

QByteArray view_util::get_stub_decoded() {
    if (!m_is_base64_initialized)
        is_base64_encoded();
    
    if (m_stub_decoded == nullptr)
        return QByteArray("");
    
    return m_stub_decoded;
}
