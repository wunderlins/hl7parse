#ifndef VIEW_UTIL_H
#define VIEW_UTIL_H

#include <QObject>
#include <QMimeDatabase>
#include <QMimeType>

#include "node.h"

typedef enum probability {
    NO=0,
    MAYBE,
    YES
} probability;

class view_util : public QObject
{
    Q_OBJECT
    
public:
    explicit view_util(node_t *node, hl7_meta_t *meta, int chunk_size = 500, QObject *parent = nullptr);
    ~view_util();

    QByteArray get_stub();
    QByteArray get_stub_decoded();
    probability is_base64_encoded();
    bool is_hl7_encoded();
    QString get_mime();
    QString get_file_suffix();
    int get_chunk_size();

signals:

private:
    hl7_meta_t *m_meta = nullptr;
    node_t *m_node = nullptr;
    QByteArray m_stub = nullptr;
    QByteArray m_stub_decoded = nullptr;
    int m_chunk_size = 0; ///! defaults to 500
    bool m_is_base64_initialized = false;
    probability m_is_base64 = NO;
    bool m_is_hl7_initialized = false;
    bool m_is_hl7 = false;

    QString m_mime_type = nullptr;
    QString m_file_suffix = nullptr;
};

#endif // VIEW_UTIL_H
