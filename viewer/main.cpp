#include "mainwindow.h"
#include "treemodel.h"
#include "osxapplication.h"

#include <QApplication>
#include <QTreeView>
#include <QObject>
#include <QString>
#include <QVariant>

#include <QSettings>

int main(int argc, char *argv[])
{
    OsxApplication a(argc, argv);

    MainWindow w;
    w.show();

    return a.exec();
}
