#include "parsercb.h"

TreeModel *parserCb::m_obj = nullptr;

void parserCb::init(TreeModel *obj) {
    parserCb::m_obj = obj;
}

void parserCb::progress(message_t *message, unsigned long total, unsigned long current) {
    if (m_obj == nullptr)
        return;
    parserCb::m_obj->cb_progress(message, total, current);
}

void parserCb::start(message_t *message) {
    if (m_obj == nullptr)
        return;

    parserCb::m_obj->cb_start(message);
}

void parserCb::end(message_t *message, unsigned long max, unsigned long current, int exit_code) {
    if (m_obj == nullptr)
        return;
    
    parserCb::m_obj->cb_end(message, max, current, exit_code);
}

void parserCb::segment(message_t *message, unsigned long num, char *name) {
    if (m_obj == nullptr)
        return;
    //qDebug() << "SEGMET (parsercb) " << name;

    parserCb::m_obj->cb_segment(message, num, name);
}

