#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include <QDialog>

namespace Ui {
class Configuration;
}

class Configuration : public QDialog
{
    Q_OBJECT

public:
    explicit Configuration(QWidget *parent = nullptr);
    ~Configuration();

private slots:
    void on_buttonBox_accepted();
    void on_btnColorSegment_clicked();

    void on_btnColorDelimiter_clicked();

    void on_btnColorText_clicked();

    void on_btnColorBackground_clicked();

    void on_btnColorHighlight_clicked();

    void on_btnColorSearch_clicked();

    void on_btnColorHighlightfg_clicked();

private:
    Ui::Configuration *ui;
    QString toRgb(QColor color);
    QString pickColor(QString color);
};

#endif // CONFIGURATION_H
