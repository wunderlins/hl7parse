#ifndef PARSERCB_H
#define PARSERCB_H

#include "treemodel.h"
#include "message_state.h"

class parserCb {

    
public:
    //explicit parserCb(QObject *parent = nullptr);
    static void init(TreeModel *obj);
    static void progress(message_t *message, unsigned long total, unsigned long current);
    static void start(message_t *message);
    static void end(message_t *message, unsigned long max, unsigned long current, int exit_code);
    static void segment(message_t *message, unsigned long num, char *name);

private:
    static TreeModel *m_obj; // = nullptr;
    
};

#endif // PARSERCB_H