#include <QTextDocument>
#include <QDebug>

#include "mainwindow.h"
#include "textfinder.h"

textFinder::textFinder(QObject *parent)
  : QObject(parent)
  , lastResults(new QList<QTextCursor>())
{

}

void textFinder::reset() {
    // reset statusbar
    MainWindow *w = getMainWindow();
    w->setStatusbar("");

    // reset format
    QTextCharFormat oldFormat;
    oldFormat.setForeground(Qt::black);
    oldFormat.setFontWeight(0);

    // reset last search
    for(int i=0; i<lastResults->length(); i++) {
        QTextCursor pos = lastResults->at(i);
        //qDebug() << "resetting: " << pos.position();
        pos.movePosition(QTextCursor::WordRight, QTextCursor::KeepAnchor);
        pos.setPosition(pos.position()-1, QTextCursor::KeepAnchor);
        pos.mergeCharFormat(oldFormat);
    }

    // empty list of results
    while (lastResults->length())
        lastResults->removeLast();

    // last search string
    lastSearchString = "";
}

int textFinder::find(QString searchString) {
    qDebug() << "textFinder::find(" << searchString << ")";

    lastSearchString = searchString;
    int length = searchString.length();
    qDebug() << "Search String length: " << length;
    int count = 0;
    MainWindow *w = getMainWindow();
    auto textEdit = w->getTextEdit();
    QTextDocument *document = textEdit->document();
    //bool found = false;

    // remove old searchterm
    reset();

    if (searchString.isEmpty()) {
        count = 0;
    } else {
        QTextCursor highlightCursor(document);
        QTextCursor cursor(document);

        cursor.beginEditBlock();

        QTextCharFormat plainFormat(highlightCursor.charFormat());
        QTextCharFormat colorFormat = plainFormat;
        QBrush brush = Qt::red;
        brush.setColor(w->color_search);
        colorFormat.setForeground(brush);

        while (!highlightCursor.isNull() && !highlightCursor.atEnd()) {
            highlightCursor = document->find(searchString, highlightCursor);
            lastResults->append(highlightCursor);

            if (!highlightCursor.isNull()) {
                //found = true;
                highlightCursor.setPosition(highlightCursor.position()-length,
                                             QTextCursor::MoveAnchor);
                highlightCursor.setPosition(highlightCursor.position()+length,
                                             QTextCursor::KeepAnchor);
                //highlightCursor.setPosition(highlightCursor.position()-1, QTextCursor::KeepAnchor);
                highlightCursor.mergeCharFormat(colorFormat);
                count++;
            }

            // move at least one character
            highlightCursor.setPosition(highlightCursor.position()+1);
        }

        cursor.endEditBlock();

    }

    QString status;
    status = "Found " + QString::number(count) + " items";
    w->setStatusbar(status);
    return count;
}
