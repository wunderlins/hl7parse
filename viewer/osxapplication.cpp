#include "mainwindow.h"
#include "osxapplication.h"
#include <QApplication>
#include <QFileOpenEvent>
#include <QtDebug>
#include <QMessageBox>

OsxApplication::OsxApplication(int &argc, char **argv)
        : QApplication(argc, argv)
{
}

bool OsxApplication::event(QEvent *event)
{
    if (event->type() == QEvent::FileOpen) {
        QFileOpenEvent *openEvent = static_cast<QFileOpenEvent *>(event);
        qDebug() << "Open file" << openEvent->file();

        /*
        QMessageBox msgBox ;
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setText("File Dropped: " + openEvent->file());
        msgBox.setWindowModality(Qt::ApplicationModal);
        msgBox.exec();
        */

        MainWindow *w = getMainWindow();
        w->load_file(openEvent->file());
    }

    return QApplication::event(event);
}

