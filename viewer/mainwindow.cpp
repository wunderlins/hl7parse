#include <QTextDocument>
#include <QScrollBar>
#include <QTextBlock>
#include <QDebug>
#include <QFileDialog>
#include <QMimeData>
#include <QMessageBox>
#include <QMimeDatabase>
#include <QMimeType>

#include "mainwindow.h"
#include "treemodel.h"
#include "treeitem.h"
#include "ui_mainwindow.h"
#include "configuration.h"
#include "cfg.h"
#include "node_util.h"

#include "../generated/version_viewer.h"
#include "../QHexView/document/buffer/qmemorybuffer.h"

MainWindow* getMainWindow()
{
    foreach (QWidget *w, qApp->topLevelWidgets())
        if (MainWindow* mainWin = qobject_cast<MainWindow*>(w))
            return mainWin;
    return nullptr;
}

QList<QString> MainWindow::get_supported_versions() {
    return versions;
}

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , rownum(new QLabel(""))
    , colnum(new QLabel(""))
    , flength(new QLabel(""))
    , ui(new Ui::MainWindow)
    , lastUsedFiles(new QList<QString>)
    , m_qhexview(new QHexView())
{

    // initialize list of supported version from hl7types
    hl7type_handle *v = version;
    while (v != NULL) {
        versions.append(v->version);
        v = v->next;
    }
    qDebug() << versions;

    defaultVersion = cfgGet("hl7.defaultVersion").toString();
    if (defaultVersion == "") defaultVersion = "2.5.1";

    // read config
    fontName = cfgGet("font.family").toString();
    fontSize = cfgGet("font.size").toInt();
    lineLength = cfgGet("line.length").toInt();

    // set defaults
    if (fontName == "")  fontName = "Monospace";
    qDebug() << "font.family: " << fontName;
    if (fontSize == 0)   fontSize = 10;
    qDebug() << "font.size: " << fontSize;
    if (lineLength < 1) lineLength = max_displayable_text_length;
    qDebug() << "line.length: " << lineLength;

    // initialize UI
    ui->setupUi(this);

    // for first run, collapse detail
    /*
    QList<int> detailSizes{16000, 0};
    ui->detailSplitter->setSizes(detailSizes);
    */

    // restore state
    restoreGeometry(cfgGet("state/geometry").toByteArray());
    restoreState(cfgGet("state/windowState").toByteArray());
    ui->treeMainSplitter->restoreState(cfgGet("state/treeMainSplitterState").toByteArray());
    ui->treeMainSplitter->restoreGeometry(cfgGet("state/treeMainSplitterGeometry").toByteArray());
    ui->consoleSplitter->restoreState(cfgGet("state/consoleSplitterState").toByteArray());
    ui->consoleSplitter->restoreGeometry(cfgGet("state/consoleSplitterGeometry").toByteArray());
    ui->detailSplitter->restoreState(cfgGet("state/detailSplitterState").toByteArray());
    ui->detailSplitter->restoreGeometry(cfgGet("state/detailSplitterGeometry").toByteArray());

    // get color settings
    color_segment     = cfgGet("color/segment", color_segment).toString();
    color_delimiter   = cfgGet("color/delimiter", color_delimiter).toString();
    color_text        = cfgGet("color/text", color_text).toString();
    color_background  = cfgGet("color/background", color_background).toString();
    color_highlight   = cfgGet("color/highlight", color_highlight).toString();
    color_highlightfg = cfgGet("color/highlightfg", color_highlightfg).toString();
    color_search      = cfgGet("color/search", color_search).toString();

    // visible ?
    if (cfgGet("state/treeMainVisibleState").toInt() == 0) {
        ui->treeMain->hide();
        ui->actionTree->setChecked(false);
    } else {
        ui->treeMain->show();
        ui->actionTree->setChecked(true);
    }

    if (cfgGet("state/treeSubVisibleState").toInt() == 0) {
        ui->treeSub->hide();
        ui->actionSegment->setChecked(false);
    } else {
        ui->treeSub->show();
        ui->actionSegment->setChecked(true);
    }

    if (cfgGet("state/treeConsoleVisibleState").toInt() == 0) {
        ui->console->hide();
        ui->actionLog->setChecked(false);
    } else {
        ui->console->show();
        ui->actionLog->setChecked(true);
    }

    if (isVisibleDetail())
        ui->actionField_Details->setChecked(true);

    // default options
    if (cfgGet("state/firstRun") != "1") {
        // show sub tree
        ui->treeSub->show();
        ui->actionSegment->setChecked(true);

        // show detail pane
        ui->actionField_Details->setChecked(true);
        on_actionField_Details_triggered(true);

        // re-adjust vertical splitter position
        QList<int> sizes{450, 16000};
        ui->treeMainSplitter->setSizes(sizes);

        // remember setting
        cfgSet("state/firstRun", "1");
    }

    // initialize Tree
    model = nullptr;
    modelTreeSub = nullptr;

    // initialize text editor
    editorDocument = ui->textEdit->document();
    QBrush brush = Qt::lightGray;
    brush.setColor(QColor(color_highlight));
    highlight_format.setBackground(brush);
    QBrush fg = Qt::lightGray;
    fg.setColor(QColor(color_highlightfg));
    highlight_format.setForeground(fg);

    // default font
    editorFont = QFont(fontName);
    editorFont.setStyleHint(QFont::TypeWriter);
    editorFont.setPointSize(fontSize);
    ui->textEdit->setFont(editorFont);
    //ui->fieldValue->setFont(editorFont);
    ui->textEdit->setLineWrapMode(QTextEdit::NoWrap);

    // make sure all white space is displayed in editor
    QTextDocument *document = ui->textEdit->document();
    /*
    QTextOption option = document->defaultTextOption();
    option.setFlags(QTextOption::ShowTabsAndSpaces | QTextOption::IncludeTrailingSpaces);
    document->setDefaultTextOption(option);
    */
    document->setDefaultStyleSheet("p, span, li { white-space: pre-wrap; } .continued {}");
    QPalette p = ui->textEdit->palette(); // define pallete for textEdit..
    p.setColor(QPalette::Base, QColor(color_background)); // set color "Red" for textedit base
    p.setColor(QPalette::Text, QColor(color_text)); // set text color which is selected from color pallete
    ui->textEdit->setPalette(p);

    // modify status bar
    //status->setAlignment()
    //statusBar()->addWidget(status);
    rownum->setMinimumWidth(25);
    colnum->setMinimumWidth(25);
    flength->setMinimumWidth(25);
    statusBar()->addPermanentWidget(new QLabel("Line:"));
    statusBar()->addPermanentWidget(rownum);
    statusBar()->addPermanentWidget(new QLabel("Char:"));
    statusBar()->addPermanentWidget(colnum);
    statusBar()->addPermanentWidget(new QLabel("Length:"));
    statusBar()->addPermanentWidget(flength);

    m_statusProgressBar = new QProgressBar(this);
    m_statusProgressBar->hide();
    statusBar()->addPermanentWidget(m_statusProgressBar);

    // initialize the finder class
    finder = new textFinder(this);

    // connect global keyboard short cuts
    keyCtrlF = new QShortcut(this); // Initialize the object
    keyCtrlF->setKey(Qt::CTRL + Qt::Key_F); // Set the key code
    // connect handler to keypress
    connect(keyCtrlF, SIGNAL(activated()), this, SLOT(on_actionFind_triggered()));

    // prevent console and trees from collappsing
    int index1 = ui->consoleSplitter->indexOf(ui->console);
    ui->consoleSplitter->setCollapsible(index1, false);

    /*
    int index2 = ui->treeSubSplitter->indexOf(ui->treeSub);
    ui->treeSubSplitter->setCollapsible(index2, false);
    int index3 = ui->treeSubSplitter->indexOf(ui->treeMain);
    ui->treeSubSplitter->setCollapsible(index3, false);

    int index4 = ui->treeMainSplitter->indexOf(ui->detailSplitter);
    ui->treeMainSplitter->setCollapsible(index4, false);
    */

    updateMrudMenu();

    /*
    lastUsedFiles = cfgGetStringArray("lastUsedFiles", "file");
    for (int i = 0; i < lastUsedFiles->length() ; ++i) {
        QString path = lastUsedFiles->at(i);
        // get the file name from path
        QStringList aFile = path.split(QDir::separator());
        // on windows it is possible to use "/" as path separator
        if (aFile.last() == path) {
            aFile = path.split("/");
        }
        QString file = aFile.last();

        // append to menu
        QAction* action = ui->menuFile->addAction(file);
        // lambda to pass the full path to load_file, ugly
        connect(action, &QAction::triggered, [this, path] { load_file(path); });
        qDebug() << file;
    }
    */

    // initialize Hex Viewer
    //QHexViewContainer
    //auto vLayoutTab1 = new QVBoxLayout(m_qhexview);
    m_qhexview->setReadOnly(true);
    //m_qhexview->
    ui->tabDetails->addTab(m_qhexview, "Hex");

    if (QCoreApplication::arguments().length() > 1) {
        file = QCoreApplication::arguments().at(1);

        load_file();
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::alert(QString title, QString mssage) {
    QMessageBox msgBox ;
    msgBox.setIcon(QMessageBox::Critical);
    msgBox.setWindowTitle(title);
    msgBox.setText(mssage);
    msgBox.setWindowModality(Qt::ApplicationModal);
    msgBox.exec();
}

void MainWindow::updateMrudMenu() {
    qDebug() << "updateMrudMenu()";

    // remove file entries
    if (fileMenuActions == 0) {
        qDebug() << "first run";
        fileMenuActions = ui->menuFile->actions().length();
    } else {
        qDebug() << "removing items, original: " << fileMenuActions << ", length" << ui->menuFile->actions().length();
        // remove file entries
        while (ui->menuFile->actions().length() > fileMenuActions) {
            QAction *a = ui->menuFile->actions().at(ui->menuFile->actions().length()-1);
            ui->menuFile->removeAction(a);
            qDebug() << "removing: " << ui->menuFile->actions().length();
        }
    }

    // add them again with the new order and length
    lastUsedFiles = cfgGetStringArray("lastUsedFiles", "file");
    for (int i = lastUsedFiles->length()-1; i > -1; --i) {
        QString path = lastUsedFiles->at(i);
        // get the file name from path
        QStringList aFile = path.split(QDir::separator());
        // on windows it is possible to use "/" as path separator
        if (aFile.last() == path) {
            aFile = path.split("/");
        }
        QString file = aFile.last();

        // append to menu
        QAction* action = ui->menuFile->addAction(file);
        // lambda to pass the full path to load_file, ugly
        connect(action, &QAction::triggered, [this, path] { load_file(path); });
        qDebug() << file;
    }
}

void MainWindow::on_actionClose_triggered()
{
    loaded = false;
    ui->treeMain->setModel(nullptr);
    ui->treeSub->setModel(nullptr);
    modelTreeSub = nullptr;

    // free our node structure
    if (model->message != nullptr) {
        free_message_t(model->message);
        model->message = nullptr;
    }

    /* meta is freed by message
    //FIXME: META handle meta in message_t
    if (model->hl7_meta != nullptr) {
        free(model->hl7_meta);
        model->hl7_meta = nullptr;
    }
    */

    if (model->rootItem != nullptr)
        delete model->rootItem;

    if (model != nullptr) {
        delete model;
        model = nullptr;
    }

    ui->textEdit->setHtml("");
}

void MainWindow::on_actionOpen_HL7_File_triggered()
{

    QString lastFolder = cfgGet("state/lastFolder").toString();
    if (lastFolder == "")
        lastFolder = QDir::homePath();
    file = QFileDialog::getOpenFileName(this,
        tr("Open HL7 File"), lastFolder,
        tr("HL7 Files (*.hl7 *.HL7);;All Files (*.*)"));

    if (file != nullptr)
        load_file();
}

void MainWindow::load_file(QAction *action) {
    log_line(action->text());
}

void MainWindow::load_file(QString fileToLoad)
{
    qDebug() << "void MainWindow::load_file(QString fileToLoad)";

    // FIXME: update MRUD menu list when a new file is loaded
    if (fileToLoad != nullptr) {
        file = fileToLoad;
    }

    qDebug() << "MainWindow::load_file(): " << file;
    // check if we have to free memory
    if (model != nullptr) {
        on_actionClose_triggered();
    }

    // set window title to file name
    QStringList aFile = file.split(QDir::separator());
    // on windows it is possible to use "/" as path separator
    if (aFile.last() == file) {
        aFile = file.split("/");
    }
    setWindowTitle(aFile.last());

    // store the last folder location
    QString lastFolder = file;
    int ix = lastFolder.lastIndexOf(aFile.last());
    lastFolder.replace(ix, lastFolder.length()-1);
    cfgSet("state/lastFolder", lastFolder);

    // remeber last used file
    if (lastUsedFiles->indexOf(file) == -1) {
        // save in config
        cfgSetStringArray("lastUsedFiles", "file", lastUsedFiles);
    } else {
        // remove it
        lastUsedFiles->removeAt(lastUsedFiles->indexOf(file));
    }
    // put it into the first slot
    lastUsedFiles->prepend(file);

    // make sure we have only max 10 files in the list
    while(lastUsedFiles->length() > 9)
        lastUsedFiles->removeLast();

    // update file list menu
    updateMrudMenu();

    // setup main tree
    model = new TreeModel(file, this);
    // connect signals
    connect(model, &TreeModel::loading_start,    this, &MainWindow::loading_start);
    connect(model, &TreeModel::loading_progress, this, &MainWindow::loading_progress);
    connect(model, &TreeModel::loading_end,      this, &MainWindow::loading_end);
    connect(model, &TreeModel::parsed,           this, &MainWindow::parsed);
    connect(model, &TreeModel::loading_segment,  this, &MainWindow::loading_segment);
    connect(model, &TreeModel::add_text_editor_line,  this, &MainWindow::add_text_editor_line);

    // load new file
    //m_loading_data_arrived = false;
    model->loadFile();
    //qDebug() << "LAST!!!";

    // tree will be populated when loading finishes, see loading_end() signal
}

void MainWindow::dragEnterEvent(QDragEnterEvent *e)
{
    if (e->mimeData()->hasUrls()) {
        e->acceptProposedAction();
    }
}

void MainWindow::dropEvent(QDropEvent* event)
{
    foreach (const QUrl &url, event->mimeData()->urls()) {
       QString fileName = url.toLocalFile();
       log_line("MainWindow::dropEvent(): " + fileName);
       file = fileName;
       load_file();
       return;
    }
}


bool MainWindow::isVisibleDetail() {
    QList<int> sizes = ui->detailSplitter->sizes();
    if (sizes[1] > 0)
        return true;
    return false;
}

bool MainWindow::isVisibleTreeMain() {
    return ui->treeMain->isVisible();
}

bool MainWindow::isVisibleTreeSub() {
    return ui->treeSub->isVisible();
}

void MainWindow::detailPanelHide(bool state) {
    if (state) {
        QList<int> sizes{16000, 0};
        ui->detailSplitter->setSizes(sizes);

    } else {
        QList<int> sizes{16000, 100};
        ui->detailSplitter->setSizes(sizes);
    }
}

void MainWindow::toggleLeftBar() {
    log_visitibility();

    // close if there is nothing to see
    if (!isVisibleDetail() && !isVisibleTreeSub() && !isVisibleTreeMain()) {
        log_line("sizes: "
                 + QString::number(ui->treeMainSplitter->sizes().at(0))
                 + ", "
                 + QString::number(ui->treeMainSplitter->sizes().at(1)));
        leftbarLastWidth = ui->treeMainSplitter->sizes().at(0);
        if (leftbarLastWidth > 0) {
            QList<int> sizes{0, 16000};
            ui->treeMainSplitter->setSizes(sizes);
        }
    }

    // open if there is at least one panel visable
    if ((isVisibleDetail() || isVisibleTreeSub() || isVisibleTreeMain())
        && ui->treeMainSplitter->sizes().at(0) == 0) {
        if (leftbarLastWidth == 0)
            leftbarLastWidth = 250;
        QList<int> sizes{leftbarLastWidth, ui->treeMainSplitter->sizes().at(1)-leftbarLastWidth};
        ui->treeMainSplitter->setSizes(sizes);
    }
}

void MainWindow::log_visitibility() {
    log_line("TreeMain: " + QString::number(isVisibleTreeMain()));
    log_line("Treesub:  " + QString::number(isVisibleTreeSub()));
    log_line("Detail:   " + QString::number(isVisibleDetail()));
}

void MainWindow::closeEvent(QCloseEvent *event) {
    cfgSet("state/geometry", saveGeometry());
    cfgSet("state/windowState", saveState());
    cfgSet("state/treeMainSplitterState", ui->treeMainSplitter->saveState());
    cfgSet("state/treeMainSplitterGeometry", ui->treeMainSplitter->saveGeometry());
    cfgSet("state/detailSplitterState", ui->detailSplitter->saveState());
    cfgSet("state/detailSplitterrGeometry", ui->detailSplitter->saveGeometry());
    cfgSet("state/consoleSplitterState", ui->consoleSplitter->saveState());
    cfgSet("state/consoleSplitterGeometry", ui->consoleSplitter->saveGeometry());

    // visible state of panels
    cfgSet("state/treeMainVisibleState", (int) ui->treeMain->isVisible());
    cfgSet("state/treeSubVisibleState", (int) ui->treeSub->isVisible());
    cfgSet("state/treeConsoleVisibleState", (int) ui->console->isVisible());

    QMainWindow::closeEvent(event);
}

void MainWindow::log_treeItemMeta(TreeItemMeta *meta) {
    log_line("--- selection -----");
    log_line(
        "type:      " + QString::number(meta->type) + "\n" +
        "segment:   " + QString::number(meta->segment) + "\n" +
        "fieldlist: " + QString::number(meta->fieldlist) + "\n" +
        "field      " + QString::number(meta->field) + "\n" +
        "comp:      " + QString::number(meta->comp) + "\n" +
        "subcmp:    " + QString::number(meta->subcmp)
    );
    log_line("-------------------");
}

bool MainWindow::subTreeCollapsed() {
    return (ui->treeSub->size().width() == 0) ? true : false;
}

void MainWindow::on_treeMainSelectionChanged(const QItemSelection &selected,
                                             const QItemSelection &deselected) {

    qDebug() << "MainWindow::on_treeMainSelectionChanged()";

    // get selected item meta data
    //QTextCursor cursor = ui->textEdit->textCursor();
    QModelIndex index = selected.indexes().at(0);
    TreeItem *item = static_cast<TreeItem*>(index.internalPointer());
    TreeItemMeta *meta = item->meta;
    log_treeItemMeta(meta);

    // find start and end position in block
    QTextBlock block = editorDocument->findBlockByNumber(meta->segment);
    QString line = block.text();
    log_line("Line: " + line);

    // start looping character by character
    int len = line.length();
    QChar c = 0;
    int i = 0;

    // remove last selection
    //reset_selection();

    int start = -1;
    int end = len;

    // if MSG was clicked, select the whole line
    if (meta->type == SEGMENT) {
       start = 0;
       end   = 0;
       for(; i<len; i++) {
           c = line.at(i).unicode();
           if (c == model->hl7_meta->sep_field) {
                break;
           }
           end++;
       }

    } else {
        // find nth fieldlist separator after MSG
        int fieldset_p = -1;
        // special tratement, because the first | itself is a fiedlist in MSH
        if (meta->segment == 0)
            fieldset_p++;
        for(; i<len; i++) {
            c = line.at(i).unicode();
            if (c == model->hl7_meta->sep_field) {
                fieldset_p++;
                if (fieldset_p == meta->fieldlist) {
                    start = i;
                    break;
                }
            }
        }

        // find nth field separator after fieldlist
        if (meta->field > 0) {
            int rep_p = 0;
            for(; i<len; i++) {
                c = line.at(i).unicode();
                if (c == model->hl7_meta->sep_rep) {
                    rep_p++;
                    qDebug() << "rep: " << i << ", " << rep_p;
                    if (rep_p == meta->field) {
                        start = i;
                        break;
                    }
                }
            }
        }

        // if comp is != -1 find nth comp separator
        if (meta->comp > 0) {
            int comp_p = 0;
            for(; i<len; i++) {
                c = line.at(i).unicode();
                if (c == model->hl7_meta->sep_comp) {
                    comp_p++;
                    if (comp_p == meta->comp) {
                        start = i;
                        break;
                    }
                }
            }
        }

        // if subcmp != -1 find nth subcmp separator
        if (meta->subcmp > 0) {
            int subcmp_p = 0;
            for(; i<len; i++) {
                c = line.at(i).unicode();
                if (c == model->hl7_meta->sep_subcmp) {
                    subcmp_p++;
                    if (subcmp_p == meta->subcmp) {
                        start = i;
                        break;
                    }
                }
            }
        }

        // find next separator (any) or end of line
        i++;
        for(; i<len; i++) {
            c = line.at(i).unicode();

            if (c == model->hl7_meta->sep_field || c == model->hl7_meta->sep_rep ||
                c == model->hl7_meta->sep_comp || c == model->hl7_meta->sep_subcmp ) {
                end = i;
                break;
            }
        }
    }


    // shave off the start delimiter from the selection if this is not the line start
    if (start)
        start++;

    // do select
    reset_selection();
    log_treeItemMeta(meta);
    this->select(start, end, true, meta);

    // if this was a segment, then create a new treeitem model
    // and attach it to treeSub
    TreeItem *segment = item;
    while (segment->meta->type != SEGMENT) {
        segment = segment->parentItem();
    }
    updateTreeSub(segment, item->meta);

    // update detail view
    updateDetails(meta);
}

void MainWindow::updateTreeSub(TreeItem *root, TreeItemMeta *selectedMeta) {
    if (root == nullptr) // special case for msh.2, delimiters
        return;

    // new node ?
    if (!modelTreeSub || root->meta != modelTreeSub->rootItem->meta) {

        if (modelTreeSub)
            delete modelTreeSub;

        modelTreeSub = new TreeModel(root);

        //modelTreeSub->rootItem = root;
        ui->treeSub->setModel(modelTreeSub);
        //ui->treeSub->setColumnHidden(1, true);

        // FIXME: do not reset column width after startup
        ui->treeSub->setColumnWidth(0, 65);

        // connect selection signal
        connect(ui->treeSub->selectionModel(),
                SIGNAL(selectionChanged(const QItemSelection &, const QItemSelection &)),
                this,
                SLOT(on_treeMainSelectionChanged(const QItemSelection &, const QItemSelection &)));
    }

    // segment selected? we cant show selection
    QItemSelectionModel *selection = ui->treeSub->selectionModel();
    if (selectedMeta->type == SEGMENT) {
        ui->treeSub->selectionModel()->blockSignals(true);
        selection->reset();
        ui->treeSub->selectionModel()->blockSignals(false);
        ui->treeSub->viewport()->update(); // manually force a repaint
        return;
    }

    QModelIndex selected = modelTreeSub->findNode(selectedMeta, true);
    TreeItem *selectItem = static_cast<TreeItem*>(selected.internalPointer());
    log_treeItemMeta(selectItem->meta);

    ui->treeSub->scrollTo(selected, QAbstractItemView::PositionAtCenter);
    QModelIndex endix = ui->treeSub->model()->index(selected.row(), ui->treeSub->model()->columnCount() - 1, selected.parent());
    qDebug() << "row: " << QString::number(selected.row());
    qDebug() << "updateTreeSub() " << selected << " " << endix;
    // update tree selection, block signals so it will not fire a
    // new textEdit selection event
    ui->treeSub->selectionModel()->blockSignals(true);
    selection->reset();
    selection->select(QItemSelection(selected, endix), QItemSelectionModel::Select);
    ui->treeSub->selectionModel()->blockSignals(false);
    ui->treeSub->viewport()->update(); // manually force a repaint
}

void MainWindow::log_line(QString line) {
    ui->console->insertPlainText(line + "\n");
    ui->console->verticalScrollBar()->setValue(ui->console->verticalScrollBar()->maximum());
}

void MainWindow::on_treeMain_activated(const QModelIndex &index)
{
    //auto item = index.model();
    log_line("on_treeMain_activated: " + index.data().toString());
}

void MainWindow::on_treeMain_pressed(const QModelIndex &index)
{
    log_line("on_treeMain_pressed: " + index.data().toString());

    TreeItem *item = static_cast<TreeItem*>(index.internalPointer());
    node_t *n = item->m_node;

    if (n != nullptr && n->type > 1)
        log_line(item->data(0).toString() + " " + item->data(1).toString() + " "
                 + item->data(2).toString() + ": " + QString::number(n->type));
}

void MainWindow::on_treeMain_entered(const QModelIndex &index)
{
    log_line("on_treeMain_entered: " + index.data().toString());
}

Ui::MainWindow *MainWindow::getUi() {
    return ui;
}

QTextEdit *MainWindow::getTextEdit() {
    return ui->textEdit;
}

QTreeView *MainWindow::getTreeMain() {
    return ui->treeMain;
}

QTreeView *MainWindow::getTreeSub() {
    return ui->treeSub;
}

void MainWindow::textEditScrollTop() {
    qDebug() << "Scrolling textEdit to top";
    QTextCursor cursor = ui->textEdit->textCursor();
    cursor.movePosition(QTextCursor::Start);
    ui->textEdit->setTextCursor(cursor);
}

void MainWindow::reset_selection() {

    // nothing to do
    if (highlight_start == -1)
        return;

    // default format, use msh line
    QTextCharFormat oldFormat = ui->textEdit->document()->findBlock(1).charFormat();
    QTextCharFormat newFormat;
    QFont f = oldFormat.font();
    QBrush brush = Qt::lightGray;
    if (highlight_delim_start) {
        brush.setColor(QColor(color_text));
    } else {
        brush.setColor(QColor(color_segment));
    }
    newFormat.setForeground(brush);
    newFormat.setBackground(oldFormat.background());

    // remember last position
    QTextCursor cursor = ui->textEdit->textCursor();
    int absolute_position = cursor.position();

    // reset last selected text fragment
    cursor.setPosition(highlight_start, QTextCursor::MoveAnchor);
    cursor.setPosition(highlight_end, QTextCursor::KeepAnchor);
    cursor.mergeCharFormat(newFormat);

    // reset delimiter format
    brush.setColor(QColor(color_delimiter));
    newFormat.setForeground(brush);
    //newFormat.setFontWeight(400);
    f.setWeight(QFont::Bold);
    newFormat.setFont(f);
    if (highlight_delim_start) {
        cursor.setPosition(highlight_start, QTextCursor::MoveAnchor);
        cursor.setPosition(highlight_start+1, QTextCursor::KeepAnchor);
        cursor.mergeCharFormat(newFormat);
    }
    if (highlight_delim_end) {
        cursor.setPosition(highlight_end-1, QTextCursor::MoveAnchor);
        cursor.setPosition(highlight_end, QTextCursor::KeepAnchor);
        cursor.mergeCharFormat(newFormat);
    }

    // reset cursor
    cursor.setPosition(absolute_position, QTextCursor::MoveAnchor);

    // reset state
    highlight_start = -1;
    highlight_end   = -1;
    highlight_delim_start = false;
    highlight_delim_end   = false;
}

void MainWindow::select(int start, int end, bool scroll, TreeItemMeta *meta) {
    qDebug() << "MainWindow::select(" << start << ", " << end << ")";

    QTextBlock block = editorDocument->findBlockByNumber(meta->segment);
    QTextCursor cursor = ui->textEdit->textCursor();
    int absolute_position = cursor.position();

    // select beginning and ending delimiter. This way an empty field is
    // also clearly marked
    highlight_delim_start = false;
    highlight_delim_end   = false;
    int d_start = start;
    int d_end   = end;
    if (d_start > 0) {
        d_start--;
        highlight_delim_start = true;
    }
    if (d_end < block.length()) {
        d_end ++;
        highlight_delim_end = true;
    }

    cursor.setPosition(block.position() + d_start, QTextCursor::MoveAnchor);
    //QTextBlock block = cursor.block();
    cursor.setPosition(block.position() + d_end, QTextCursor::KeepAnchor);
    cursor.mergeCharFormat(highlight_format);

    // reset cursor
    cursor.setPosition(absolute_position, QTextCursor::MoveAnchor);

    // scroll the document vertically to so that the line is centered
    // crude method, calculate line height
    QScrollBar *scrollbar = ui->textEdit->verticalScrollBar();
    int max = scrollbar->maximum();

    if (scroll && max > 0) {
        // find percentual position of line in document
        int nlines = editorDocument->blockCount();
        //if (nlines < 0) nlines = 0;
        int block_number = block.blockNumber();
        double line_pos = (double) block_number / (double) nlines;

        // percentual scrol pos
        int scroll_pos = (int) max * line_pos;
        scrollbar->setSliderPosition(scroll_pos);

        log_line(
            "nlines: " + QString::number(nlines) + ", " +
            "block_number: " + QString::number(block_number) + ", " +
            "line_pos: " + QString::number(line_pos) + ", " +
            "scroll_pos: " + QString::number(scroll_pos)
        );
    }

    // remeber state
    highlight_start = block.position() + d_start;
    highlight_end   = block.position() + d_end;

    // update statusbar
    QModelIndex node = model->findNode(meta);
    TreeItem *item = static_cast<TreeItem*>(node.internalPointer());
    int length = block.length();
    if (item && item->m_node)
        length = (int) item->m_node->length - 1;
    rownum->setNum(block.blockNumber() + 1);
    colnum->setNum(start + 1);
    flength->setNum(length);
}

void MainWindow::on_textEdit_cursorPositionChanged()
{
    if (m_loading) // do not allow selection changes during load
    //if (!m_loading_data_arrived) // do not allow selection changes during load
        return;

    QTextCursor cursor = ui->textEdit->textCursor();
    log_line("Cursor position: " + QString::number(cursor.position()) +
             ", pos in block: " + QString::number(cursor.positionInBlock()) +
             ", block number: " + QString::number(cursor.blockNumber()));

    if (!loaded)
        return;

    mutex_cursor.lock();

    // note our position
    int segment   = cursor.blockNumber();
    int position  = cursor.positionInBlock();
    //int absolute_position = cursor.position();
    QTextBlock block = cursor.block();

    QString line = block.text();
    int fieldlist_p  = -1; // |
    int field_p      = -1; // ~
    int comp_p       = -1; // ^
    int subcmp_p     = -1; // &

    int start        = 0; //cursor.position(); // 0;
    int end          = 0; //cursor.position(); // line.length()-1;

    // sub delimiters
    int start_field  =  0;
    int start_rep    = -1;
    int start_comp   = -1;
    int start_subcmp = -1;

    QChar delim_end   = 0;
    QChar delim_start = 0;

    // find the fieldlist position and start
    for (int i=position-1; i>=0; i--) {
        QChar c = line.at(i).unicode();

        if (fieldlist_p < 0) {
            start_field = position-i;
        }

        if (fieldlist_p < 0 && c == model->hl7_meta->sep_rep)
            field_p++;

        if (line.at(i).unicode() == model->hl7_meta->sep_field) {
            if (delim_start == 0)
                delim_start = c;

            if (fieldlist_p < 0)
                field_p++;

            fieldlist_p++;
        }

        if (fieldlist_p == -1 && start_rep == -1 && c == model->hl7_meta->sep_rep) {
            if (delim_start == 0)
                delim_start = c;
            start_rep = position-i;
        }

        if (fieldlist_p < 0 && start_rep < 0 && c == model->hl7_meta->sep_comp) {
            if (comp_p == -1)
                comp_p++;
            comp_p++;
        }

        if (fieldlist_p == -1 && start_rep == -1 && start_comp == -1 && c == model->hl7_meta->sep_comp) {
            if (delim_start == 0)
                delim_start = c;
            start_comp = position-i;
        }

        if (comp_p < 0 && fieldlist_p < 0 && start_rep < 0 && c == model->hl7_meta->sep_subcmp) {
            if (subcmp_p == -1)
                subcmp_p++;
            subcmp_p++;
        }

        if (fieldlist_p == -1 && start_rep == -1 && start_comp == -1 && start_subcmp == -1 && c == model->hl7_meta->sep_subcmp) {
            if (delim_start == 0)
                delim_start = c;
            start_subcmp = position-i;
        }
    }
    if (line.mid(0, 3) == "MSH" || line.mid(0, 3) == "MSA")
        fieldlist_p++;

    // find the field list end
    for (int i=position; i<line.length(); i++) {
        if (line.at(i).unicode() == model->hl7_meta->sep_field) {
            delim_end = line.at(i).unicode();
            break;
        }
        if (line.at(i).unicode() == model->hl7_meta->sep_rep) {
            delim_end = line.at(i).unicode();
            break;
        }
        if (line.at(i).unicode() == model->hl7_meta->sep_comp) {
            delim_end = line.at(i).unicode();
            break;
        }
        if (line.at(i).unicode() == model->hl7_meta->sep_subcmp) {
            delim_end = line.at(i).unicode();
            break;
        }
        end++;
    }

    // calculate actual start, end and node type
    node_type_t type = SEGMENT;
    if (start_subcmp != -1) {
        start = start_subcmp;
        type = SUBCOMP;
    } else if (start_comp != -1) {
        start = start_comp;
        type = COMP;
    } else if (start_rep != -1) {
        start = start_rep;
        type = FIELDLIST;
    } else {
        start = start_field;
        type = FIELD;
    }

    if (delim_end == model->hl7_meta->sep_subcmp) {
        type = SUBCOMP;
        if (subcmp_p == -1)
            subcmp_p++;
    } else if (delim_end == model->hl7_meta->sep_comp) {
        type = COMP;
        if (comp_p == -1)
            comp_p++;
    } else if (delim_end == model->hl7_meta->sep_rep) {
        type = FIELD;
    }

    if (position - start_field == 0)
        type = SEGMENT;

    // not in segment name, don'tselect the beginning delimiter
    if (start != cursor.positionInBlock())
        start--;

    // absolute positions
    /*
    end   = cursor.position() + end;
    start = cursor.position() - start;
    */
    end   = cursor.positionInBlock() + end;
    start = cursor.positionInBlock() - start;

    log_line(
        "delim_start:  " + QString(delim_start) + "\n" +
        "delim_end:    " + QString(delim_end) + "\n" +
        "type:         " + QString::number(type) + "\n" +
        "end:          " + QString::number(end) + "\n" +
        "start:        " + QString::number(start) + "\n" +
        "start_field:  " + QString::number(start_field) + "\n" +
        "start_rep:    " + QString::number(start_rep) + "\n" +
        "start_comp:   " + QString::number(start_comp) + "\n" +
        "start_subcmp: " + QString::number(start_subcmp)
    );

    // open and highlight the tree item
    TreeItemMeta m;
    m.type = (node_type_t) type;
    m.segment = segment;
    m.fieldlist = fieldlist_p;
    m.field = field_p;
    m.comp = comp_p;
    m.subcmp = subcmp_p;
    log_treeItemMeta(&m);

    // get node index
    QModelIndex selected = model->findNode(&m);

    // check if this node is actually within line range
    if (model->rootItem->childCount() <= m.segment) {
        // we have to stop here, because we ran over the available
        // segments in treeModel
        return;
    }

    TreeItem *selectItem = static_cast<TreeItem*>(selected.internalPointer());

    // reset old highlighting and set new text highlight
    reset_selection();
    select(start, end, false, &m);
    mutex_cursor.unlock();

    // if tree sub is opened, then do not open tree main, only scroll to segment
    QModelIndex open = selected;
    if (!subTreeCollapsed()) {
        TreeItem *current = static_cast<TreeItem*>(open.internalPointer());
        while (open.parent().isValid()) {
            open = open.parent();
            current = static_cast<TreeItem*>(open.internalPointer());
            log_treeItemMeta(current->meta);
        }

        // also, update treeSub's model
        ui->treeSub->blockSignals(true);
        updateTreeSub(current, &m);
        ui->treeSub->blockSignals(false);
    }

    // open all parent nodes
    QModelIndex p = selected;
    ui->treeMain->collapseAll(); // close all nodes
    if (subTreeCollapsed()) {
        while (true) {
            ui->treeMain->expand(p);
            p = p.parent();
            if (!p.isValid()) // at the top ?
                break;
        }
    }

    ui->treeMain->scrollTo(open, QAbstractItemView::PositionAtCenter);
    QModelIndex endix = ui->treeMain->model()->index(open.row(), ui->treeMain->model()->columnCount() - 1, open.parent());
    qDebug() << "row: " << QString::number(open.row());
    QItemSelectionModel *selection = ui->treeMain->selectionModel();

    // update tree selection, block signals so it will not fire a
    // new textEdit selection event
    ui->treeMain->selectionModel()->blockSignals(true);
    selection->reset();
    selection->select(QItemSelection(open, endix), QItemSelectionModel::Select);
    ui->treeMain->selectionModel()->blockSignals(false);

    // update detail view and status-bar
    updateDetails(&m);

}

void MainWindow::updateDetails(TreeItemMeta *meta) {
    // get node index
    futil = nullptr;
    QModelIndex select = model->findNode(meta);
    selectItem = static_cast<TreeItem*>(select.internalPointer());

    QModelIndex ix = model->index(meta->segment, 0);
    auto data = model->data(ix, Qt::DisplayRole);
    QString segment_name = data.toString();

    QString status = segment_name + "-" + QString::number(meta->fieldlist + 1);
    if (meta->field != -1)
        status.append("(" + QString::number(meta->field+1) + ")");
    if (meta->comp != -1)
        status.append("-" + QString::number(meta->comp+1));
    if (meta->subcmp != -1)
        status.append("-" + QString::number(meta->subcmp+1));

    ui->fieldB64Encoded->setText("");
    ui->fieldHl7Encoded->setText("");
    ui->fieldContentType->setText("");
    ui->fieldHl7Description->setText("");
    ui->fieldHl7Table->setText("");
    ui->fieldHl7Type->setText("");
    ui->fieldBtnOpen->setEnabled(false);

    if (selectItem == nullptr) // msh-1
        return;
    
    /*
    QString rawData("");
    if (selectItem && selectItem->node && selectItem->node->data)
        rawData.append(reinterpret_cast<const char*>(selectItem->node->data));
    */
    // std::string => QByteArray
    QByteArray rawData;
    if (selectItem->m_node != NULL && selectItem->m_node) {
        int l = selectItem->m_node->length;
        if (l) l--;
        rawData = QByteArray::fromRawData((char*) selectItem->m_node->data, l);
    } else
        rawData = QByteArray("");

    ui->fieldAddress->setText(status);

    if (selectItem->m_node != NULL) {
        futil = new view_util(selectItem->m_node, model->message->meta);
        if (futil->is_base64_encoded() == YES)
            ui->fieldB64Encoded->setText("Yes");
        else if (futil->is_base64_encoded() == MAYBE)
            ui->fieldB64Encoded->setText("Probably");
        else
            ui->fieldB64Encoded->setText("No");
            
        if (futil->is_hl7_encoded())
            ui->fieldHl7Encoded->setText("Yes");
        else
            ui->fieldHl7Encoded->setText("No");

        ui->fieldContentType->setText(futil->get_mime());

        if (selectItem->m_node->length > 1) {
            ui->fieldBtnOpen->setEnabled(true);
        }
    }

    // update detail data
    // FIXME: see how to run widget update in a separate thread
    //        https://stackoverflow.com/questions/18548048/qlistview-takes-too-long-to-update-when-given-100k-items/18579171#18579171
    /*
    ui->fieldValue->blockSignals(true);
    ui->fieldValue->setPlainText(rawData);
    ui->fieldValue->blockSignals(false);
    */

    // Load data from In-Memory Buffer...
    QHexDocument* document = nullptr;
    document = QHexDocument::fromMemory<QMemoryBuffer>(rawData);
    document->setHexLineWidth(8);
    m_qhexview->setDocument(document);

    // update status bar
    ui->statusbar->showMessage(status);

    // update HL7 meta information
    if (selectItem->segment != nullptr) {
        ui->fieldHl7Type->setText("Segment");
        ui->fieldHl7Description->setText(selectItem->segment->name);
        ui->fieldHl7Table->setText("n/a");
    } else if (selectItem->field != nullptr) {
        ui->fieldHl7Type->setText(selectItem->field->type->id);
        ui->fieldHl7Description->setText(selectItem->field->name);
        ui->fieldHl7Table->setText(selectItem->field->table);
    }  else if (selectItem->component != nullptr) {
        if (selectItem->component->type && selectItem->component->type->id)
            ui->fieldHl7Type->setText(selectItem->component->type->id);
        if (selectItem->component->name)
            ui->fieldHl7Description->setText(selectItem->component->name);
        if (selectItem->component->table)
            ui->fieldHl7Table->setText(selectItem->component->table);
    } 
        
}

void MainWindow::setStatusbar(QString text) {
    ui->statusbar->showMessage(text);
}

void MainWindow::on_textEdit_textChanged()
{
    //qDebug() << "on_textEdit_textChanged";
    //textEditScrollTop();
    ;
}

void MainWindow::on_textEdit_copyAvailable(bool b)
{
    qDebug() << "on_textEdit_copyAvailable: " + QString::number(b);
    //textEditScrollTop();
    //loaded = true;
}

void MainWindow::on_actionScroll_Top_triggered()
{
    textEditScrollTop();
}

void MainWindow::on_actionLog_triggered()
{
    if (ui->console->isVisible()) {
        ui->console->hide();
    } else {
        ui->console->show();
    }
}

void MainWindow::on_actionTree_triggered()
{
    if (ui->treeMain->isVisible()) {
        ui->treeMain->hide();
        //if (!isVisibleTreeSub())
        //    ui->treeSub->show();
    } else {
        ui->treeMain->show();
    }
    log_visitibility();
    toggleLeftBar();
}

void MainWindow::on_actionQuit_triggered()
{
    on_actionClose_triggered();
    QApplication::quit();
}

void MainWindow::on_actionSegment_triggered()
{
    if (ui->treeSub->isVisible()) {
        ui->treeSub->hide();
        //if (!isVisibleTreeMain())
        //    ui->treeMain->show();
    } else {
        ui->treeSub->show();
    }
    log_visitibility();
    toggleLeftBar();
}

void MainWindow::on_actionField_Details_triggered(bool checked)
{
    detailPanelHide((checked) ? false : true);
    log_visitibility();
    toggleLeftBar();
}

void MainWindow::on_actionConfiguration_triggered()
{
    Configuration *configuration = new Configuration();
    configuration->exec();
}

void MainWindow::on_splitter_2_splitterMoved(int pos, int index)
{

}

void MainWindow::on_actionFind_triggered()
{
    qDebug() << "on_actionFind_triggered()";
    ui->fieldSearch->selectAll();
    ui->fieldSearch->setFocus();
}

void MainWindow::on_buttonSearch_clicked()
{
    finder->find(ui->fieldSearch->text());
}

void MainWindow::on_buttonClear_clicked()
{
    finder->reset();
    ui->fieldSearch->setText("");
}

void MainWindow::on_fieldSearch_returnPressed()
{
    on_buttonSearch_clicked();
}

void MainWindow::on_action_About_triggered()
{

    QMessageBox msgBox;
    msgBox.setTextFormat(Qt::RichText);
    msgBox.setWindowTitle("About");

    QPixmap pixmap = QPixmap(":/resources/logo2.png");
    msgBox.setWindowIcon(QIcon(pixmap));

    QString text = "<table><tr><td style='padding-right: 30px; padding-top: 10px;'><img src=':/resources/logo2_64.png' width='64'></td><td style='padding-right: 20px;'>";
    text.append("<table>");
    text.append("<tr style='font-weight: bold; font-size: 16pt;'><td>7view&nbsp;&nbsp;</td><td>" + QString(VERSION_VIEWER) + " <small>(" + QString(VERSION_GIT) +")</small></td></tr>");
    text.append("<tr><td>&nbsp;</td></tr>");
    text.append("<tr><td colspan='2'>" + QSysInfo::prettyProductName() + " " + QSysInfo::currentCpuArchitecture() + "</td></tr>");
    text.append("<tr><td>Hostname</td><td>" + QSysInfo::machineHostName() + "</td></tr>");
    text.append("<tr><td>ID</td><td>" + QSysInfo::machineUniqueId() + "</td></tr>");
    text.append("<tr><td>&nbsp;</td></tr>");

    text.append("<tr><td>7parse</td><td>version: " + QString(VERSION_PARSER) + "</td></tr>");
    text.append("<tr><td>7search</td><td>version: " + QString(VERSION_SEARCH) + "</td></tr>");
    text.append("</table><br><p>© 2021, Simon Wunderlin<br><br>");
    text.append("Licensed under the <a href='https://gitlab.com/wunderlins/hl7parse/-/raw/master/LICENSE'>LGPL 3</a></p></td></tr></table>");
    //msgBox.about(this, "7view", text);
    msgBox.setText(text);
    msgBox.exec();
    //QMessageBox::about(this, "7view", text);
}

void MainWindow::on_fieldBtnOpen_clicked()
{
    ui->fieldBtnOpen->setEnabled(false);

    // we need to construct a file name from
    // <temp-folder>/<process-id>-<field-name>.<file-extension>
    QString tmp_file = QStandardPaths::writableLocation(QStandardPaths::TempLocation);
    tmp_file.append("/");
    tmp_file.append(QString::number(QCoreApplication::applicationPid()));
    tmp_file.append("-");

    // add address to string
    hl7_addr_t *addr = addr_from_node(selectItem->m_node);
    char *addr_str = addr_to_string(addr);
    // replace brackets with underscore
    QString qaddr(addr_str);
    qaddr = qaddr.replace("(", "_");
    qaddr = qaddr.replace(")", "_");
    tmp_file.append(qaddr);
    free(addr_str);
    free_addr(addr);

    // add file ending
    tmp_file.append(".");
    tmp_file.append(futil->get_file_suffix());

    qDebug() << "Temp File Name: " << tmp_file;

    // if the file is base64 encoded, we use the decoder from lib7 to write 
    // the node's content to a file.
    //
    // This ism ore fficient on large files than doing it on qt, hl7 encoded 
    // base64 must first be hl7 decoded (i.e. \.br\) and then bas64 decoded.
    // 
    // hl7_64decode_fd does while streaming a ndoe's content to a file.
    bool export_success = false;
    if (futil->is_base64_encoded() != NO) {
        int ret = -1;
        FILE *fd = fopen(tmp_file.toStdString().c_str(), "wb");

        if (fd != NULL) {
            ret = hl7_64decode_fd((char*) selectItem->m_node->data, selectItem->m_node->length, fd);
            fclose(fd);
        } else {
            alert("Export Error", "Failed to open temp file.");
            return;
        }

        // TODO: handle file open and decode errors -> show dialog on error
        if (ret == 0) {
            export_success = true;
        } else {
            alert("Export Error", "Failed to Base64 decode file.\n\nCorrupt Base64 encoding, opening anyway.");
            export_success = true;
            //return;
        }
    }

    // otherwise 
    //
    // we have to write the raw data to a file ourself
    else {
        QByteArray bytes{(char*) selectItem->m_node->data, (int) selectItem->m_node->length - 1};
        QFile out(tmp_file);
        bool success = out.open(QIODevice::WriteOnly);
        if (success == true) {
            out.write(bytes);
            out.close();
            export_success = true;
        } else {
            alert("Export Error", "Failed to write field content to file.");
            return;
        }
    }

    // open the file if export was successful
    //
    // https://doc.qt.io/qt-5/qdesktopservices.html
    // QDesktopServices::openUrl(QUrl("file:///C:/Documents and Settings/All Users/Desktop", QUrl::TolerantMode));
    if (export_success) {
        QUrl u{"file:///" + tmp_file};
        bool launched = QDesktopServices::openUrl(u);
        ui->fieldBtnOpen->setEnabled(true);

        if (!launched) {
            alert("Launch Error", "Unable to find launcher for file:\n" + tmp_file);
        }

        return;
    }

    alert("Export Error", "Unhandled preview error.");
}

void MainWindow::loading_start() {
    m_loading = true;
    m_statusProgressBar->show();

    // disable search
    ui->fieldSearch->setDisabled(true);
    ui->buttonSearch->setDisabled(true);
    ui->buttonClear->setDisabled(true);
}

void MainWindow::loading_progress(int percent) {
    qDebug() << "Loading: " << percent << "%";

    // FIXME: might need mutex here for progres ??
    //mutex_progress.lock();
    m_statusProgressBar->setValue(percent);
    //mutex_progress.unlock();
}

void MainWindow::loading_end(int exit_code) {
    qDebug() << "loading_end";
    m_statusProgressBar->hide();
    m_loading = false;
    //m_loading_data_arrived = true;

    // enable search
    ui->fieldSearch->setDisabled(false);
    ui->buttonSearch->setDisabled(false);
    ui->buttonClear->setDisabled(false);
}

void MainWindow::loading_segment(unsigned long num, char name[3]) {
    qDebug() << "SEGMENT " << name;
    //m_loading_data_arrived = true;
}

void MainWindow::parsed() {
    qDebug() << "parsed";
    if (model->rootItem->childCount() == 0) {
        /*
        QMessageBox msgBox ;
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setText("Failed to parse Document.");
        msgBox.setWindowModality(Qt::ApplicationModal);
        msgBox.exec();
        */

        //alert("Error", "Failed to parse Document.");

        return;
    }
    //loaded = true;

    /*
    ui->treeMain->setModel(model);
    ui->treeMain->setColumnHidden(1, true);
    ui->treeMain->setColumnWidth(0, 75);
    */

    // check if the last char is a newline character
    QTextCursor cursor = ui->textEdit->textCursor();
    /*
    QChar lastChar = ui->textEdit->toPlainText().at(ui->textEdit->toPlainText().length()-1);
    //qDebug() << "Last Char: " << lastChar;
    if (lastChar != '\r' && lastChar != '\n')
        cursor.insertText(QString('\n'));
    */
    
    //cursor.insertText("\n");
    /*
    // remove first newline
    cursor.movePosition(QTextCursor::Start);
    cursor.movePosition(QTextCursor::Down, QTextCursor::MoveAnchor, 0);
    cursor.select(QTextCursor::LineUnderCursor);
    cursor.removeSelectedText();
    cursor.deleteChar(); // clean up new line
    textEditScrollTop();
    */

}

void MainWindow::on_textEdit_selectionChanged()
{
    if (m_loading == true)
        return;
    
    // check if the hex tab is open
    if (ui->tabDetails->currentIndex() != 1) // HEX tab not visable
        return;

    // hex tab is visible, so get the selected string and show 
    // it in the widget
    QTextCursor cursor = ui->textEdit->textCursor();
    QString text = cursor.selectedText();

    if (!text.length())
        return;
    
    QHexDocument* document = nullptr;
    document = QHexDocument::fromMemory<QMemoryBuffer>(text.toUtf8());
    document->setHexLineWidth(8);
    m_qhexview->setDocument(document);
}

void MainWindow::add_text_editor_line(QString html_line, int line_num) {

    mutex_cursor.lock();
    QTextCursor cursor = ui->textEdit->textCursor();

    ui->textEdit->blockSignals(true);
    cursor.movePosition(QTextCursor::End);
    cursor.beginEditBlock();
    cursor.insertBlock();
    cursor.insertHtml(html_line);
    cursor.endEditBlock();

    ui->textEdit->blockSignals(false);

    if (line_num == 0) {
        cursor.movePosition(QTextCursor::Start);
        cursor.movePosition(QTextCursor::Down, QTextCursor::MoveAnchor, 0);
        cursor.select(QTextCursor::LineUnderCursor);
        cursor.removeSelectedText();
        cursor.deleteChar(); // clean up new line
        cursor.movePosition(QTextCursor::End);
        ui->textEdit->setTextCursor(cursor);
        loaded = true;
    }
    mutex_cursor.unlock();
}

/*
void MainWindow::loading_data_arrived(bool state) {
    m_loading_data_arrived = state;
}
*/