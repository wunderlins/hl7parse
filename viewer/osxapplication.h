#ifndef OSXAPPLICATION_H
#define OSXAPPLICATION_H

#include <QApplication>

class OsxApplication : public QApplication
{
    Q_OBJECT
public:
    OsxApplication(int &argc, char **argv);
    bool event(QEvent *event);
};

#endif // OSXAPPLICATION_H
