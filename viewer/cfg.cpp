#include <QDebug>
#include "cfg.h"

void cfgInit() {
    // initialize namespaces for our config object to be used various system
    // config subsystems.
    QCoreApplication::setOrganizationName("WunderlinSoft");
    QCoreApplication::setOrganizationDomain("wunderlin.net");
    QCoreApplication::setApplicationName("7view");
}

QVariant cfgGet(QString name)
{
    return cfg.value(name);
}

QVariant cfgGet(QString name, QString defaultValue)
{
    return cfg.value(name, defaultValue);
}

void cfgSet(QString name, QVariant value)
{
    cfg.setValue(name, value);
}

QList<QString> *cfgGetStringArray(QString name, QString itemName) {
    QList<QString> *data = new QList<QString>();
    int size = cfg.beginReadArray(name);
    //qDebug() << "cfgGetStringArray(), " << size;

    for (int i = 0; i < size ; ++i) {
        cfg.setArrayIndex(i);
        QString item = cfg.value(itemName).toString();
        //qDebug() << item;
        data->append(item);
    }
    cfg.endArray();
    return data;
}

void cfgSetStringArray(QString name, QString key, QList<QString> *value) {
    // save in config
    cfg.beginWriteArray(name);
    for (int i = 0; i < value->size(); ++i) {
        cfg.setArrayIndex(i);
        cfg.setValue(key, value->at(i));
    }
    cfg.endArray();
}
