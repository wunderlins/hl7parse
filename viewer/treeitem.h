#ifndef TREEITEM_H
#define TREEITEM_H

#include <QList>
#include <QVariant>
#include "decode.h"
#include "lib7types.h"
//#include "address.h"

class TreeItemMeta {
public:
    explicit TreeItemMeta();
    ~TreeItemMeta();

    int segment = -1;
    int fieldlist = -1;
    int field = -1;
    int comp = -1;
    int subcmp = -1;
    node_type_t type = SEGMENT;

    friend bool operator== (const TreeItemMeta &tm1, const TreeItemMeta &tm2);

};

//! [0]
class TreeItem
{
public:
    explicit TreeItem(const QList<QVariant> &data, TreeItem *parentItem = nullptr);
    explicit TreeItem(node_t *node, hl7_addr_t *addr, TreeItem *parentItem = nullptr);
    ~TreeItem();

    void appendChild(TreeItem *child);

    TreeItem *child(int row);
    int childCount() const;
    int columnCount() const;
    QVariant data(int column) const;
    int row() const;
    TreeItem *parentItem();

    node_t *m_node;
    node_t *get_node();

    TreeItemMeta *meta;
    hl7_addr_t *addr();

    // schema information
    Segment *segment = nullptr;
    Field *field = nullptr;
    DataTypeComponent *component = nullptr;
    DataType *hl7Type = nullptr;
    QString descr = "";

private:
    QList<TreeItem*> m_childItems;
    QList<QVariant> m_itemData;
    TreeItem *m_parentItem;
    
    // cache addr for quicker lookups
    hl7_addr_t *m_addr;
};
//! [0]

#endif // TREEITEM_H
