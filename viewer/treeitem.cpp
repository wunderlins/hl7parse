/*
    treeitem.cpp

    A container for items of data supplied by the simple tree model.
*/

#include <QStringList>

#include "treeitem.h"

TreeItemMeta::TreeItemMeta() {}
TreeItemMeta::~TreeItemMeta() {}
bool operator== (const TreeItemMeta &tm1, const TreeItemMeta &tm2)
{
    if (tm1.type != tm2.type)           return false;
    if (tm1.segment != tm2.segment)     return false;
    if (tm1.fieldlist != tm2.fieldlist) return false;
    if (tm1.field != tm2.field)         return false;
    if (tm1.comp != tm2.comp)           return false;
    if (tm1.subcmp != tm2.subcmp)       return false;

    return true;
}


//! [0]
TreeItem::TreeItem(const QList<QVariant> &data, TreeItem *parent)
    : meta(new TreeItemMeta())
{
    m_parentItem = parent;
    m_itemData = data;
    m_node = nullptr;
    m_addr = nullptr;
}

TreeItem::TreeItem(node_t *node, hl7_addr_t *addr, TreeItem *parent) 
    : meta(new TreeItemMeta())
{
    m_parentItem = parent;
    //m_itemData = data;
    m_node = node;
    m_addr = addr;

    meta->segment = addr->seg_count;
    meta->fieldlist = addr->fieldlist;
    meta->field = addr->field;
    meta->comp = addr->comp;
    meta->subcmp = addr->subcmp;
    meta->type = node->type;

    if (m_parentItem != nullptr)
        m_parentItem->appendChild(this);

    // setup data array: seq, children, data
    QList<QVariant> tmp_data{"", "", ""};
    if (node->type == SEGMENT)         tmp_data[0] = m_addr->segment;
    else if (node->type == FIELDLIST)  tmp_data[0] = QVariant::fromValue(m_addr->fieldlist+1);
    else if (node->type == FIELD && node->parent->num_children <= 1)
        tmp_data[0] = QString::number(m_addr->fieldlist+1);
    else if (node->type == FIELD && node->parent->num_children > 1)
        tmp_data[0] = QString::number(m_addr->fieldlist+1) + "(" + QString::number(m_addr->field+1) + ")";
    //else if (node->type == FIELD)      tmp_data[0] = QString::number(m_addr->field);
    else if (node->type == COMP)       tmp_data[0] = QVariant::fromValue(m_addr->comp+1);
    else if (node->type == SUBCOMP)    tmp_data[0] = QVariant::fromValue(m_addr->subcmp+1);

    QString d{""};
    if (node->data != NULL) {
        if (node->length > 20) {
            QString tmp(reinterpret_cast<const char*>(node->data));
            d.append(tmp.mid(0, 19));
            d.append("…");
        } else {
            d += new QString(reinterpret_cast<const char*>(node->data));
        }

        // replace all newlines with space
        d.replace('\n', ' ');
        d.replace('\r', ' ');
    }
    tmp_data[2] = d;

    m_itemData = tmp_data;

}
//! [0]

//! [1]
TreeItem::~TreeItem()
{
    qDeleteAll(m_childItems);
}
//! [1]

//! [2]
void TreeItem::appendChild(TreeItem *item)
{
    m_childItems.append(item);
}
//! [2]

//! [3]
TreeItem *TreeItem::child(int row)
{
    return m_childItems.value(row);
}
//! [3]

//! [4]
int TreeItem::childCount() const
{
    return m_childItems.count();
}
//! [4]

//! [5]
int TreeItem::columnCount() const
{
    return m_itemData.count();
}
//! [5]

//! [6]
QVariant TreeItem::data(int column) const
{
    QVariant ret{""};

    if (column == 0)
        return m_itemData.value(column);

    if (column == 2) {
        QString r{};
        r.append(m_itemData.value(column).toString());
        r.append("\n");
        r.append(descr);
        return r;
    }

    if (m_node == nullptr)
        return ret;

    if (m_node->type == SEGMENT) {
        ret = QVariant::fromValue(m_node->num_children);
    } else /* if (m_node->type == FIELD) {
        if (field && field->type)
            ret = field->type->id;
    } else if (m_node->type == FIELDLIST) {
        if (field && field->type)
            ret = field->type->id;
    } */

    {
        if (hl7Type != NULL)
            ret = hl7Type->id;

    }

    return ret;

}
//! [6]

//! [7]
TreeItem *TreeItem::parentItem()
{
    return m_parentItem;
}
//! [7]

//! [8]
int TreeItem::row() const
{
    if (m_parentItem)
        return m_parentItem->m_childItems.indexOf(const_cast<TreeItem*>(this));

    return 0;
}
//! [8]

node_t *TreeItem::get_node() {
    return m_node;
}

hl7_addr_t *TreeItem::addr() {
    if (m_node == nullptr)
        return nullptr;

    if (m_addr == nullptr) {
        m_addr = addr_from_node(m_node);
    }

    return m_addr;

}