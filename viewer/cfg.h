#ifndef SETTINGS_H
#define SETTINGS_H

#include <QObject>
#include <QSettings>
#include <QCoreApplication>
#include <QVariant>
#include <QString>
#include <QList>

static QSettings cfg("WunderlinSoft", "7view");
void cfgInit();

// single values
QVariant cfgGet(QString name);
QVariant cfgGet(QString name, QString defaultValue);
void cfgSet(QString name, QVariant value);

// arrays
QList<QString> *cfgGetStringArray(QString name, QString itemName);
void cfgSetStringArray(QString name, QString key, QList<QString> *value);

#endif // SETTINGS_H
