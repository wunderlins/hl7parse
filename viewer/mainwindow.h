#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QItemSelection>
#include <QMainWindow>
#include <QTextDocument>
#include <QTextEdit>
#include <QTextBlock>
#include <QLabel>
#include <QShortcut>
#include <QTreeView>
#include <QStandardPaths>
#include <QDesktopServices>
#include <QUrl>
#include <QProgressBar>
#include <QMutex>
#include <QMetaType>

#include "treemodel.h"
#include "treeitem.h"
#include "textfinder.h"
#include "view_util.h"
#include "base64.h"
#include "../QHexView/qhexview.h"
#include "../build/lib7types.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

extern const char *VERSION_PARSER;
extern const char *VERSION_SEARCH;
extern const char *VERSION_GIT;

class TreeModel; // forward declaration

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    QList<QString> get_supported_versions();
    void closeEvent(QCloseEvent *event);

    void log_line(QString line);
    Ui::MainWindow *getUi();
    QTextEdit *getTextEdit();
    QTreeView *getTreeMain();
    QTreeView *getTreeSub();

    void textEditScrollTop();
    void updateTreeSub(TreeItem *root, TreeItemMeta *selected);
    void detailPanelHide(bool state);
    int max_displayable_text_length = 80; // bytes, after that add '...' in editor

    // status bar information
    QLabel *rownum;
    QLabel *colnum;
    QLabel *flength;
    QFont editorFont;

    // configuration
    QString defaultVersion = nullptr;
    QString fontName = nullptr;
    int fontSize     = 0;
    int lineLength   = 0;
    void setStatusbar(QString text);
    void load_file(QString fileToLoad=nullptr);
    void load_file(QAction *action);

    QString color_segment     = "#00AA7F";
    QString color_delimiter   = "#00AAFF";
    QString color_text        = "#000";
    QString color_background  = "#FFF";
    QString color_highlight   = "#DDD";
    QString color_highlightfg = "#000";
    QString color_search      = "#FF0000";

    //void loading_data_arrived(bool state);

public slots:
    void alert(QString title, QString mssage);
    void loading_start();
    void loading_progress(int percent);
    void loading_end(int exit_code);
    void loading_segment(unsigned long num, char name[3]);
    void parsed();

private slots:
    void on_actionOpen_HL7_File_triggered();
    void on_treeMain_activated(const QModelIndex &index);
    void on_treeMain_pressed(const QModelIndex &index);
    void on_treeMain_entered(const QModelIndex &index);

    void on_textEdit_cursorPositionChanged();
    void on_textEdit_textChanged();
    void on_textEdit_copyAvailable(bool b);

    void on_actionScroll_Top_triggered();
    void on_actionLog_triggered();
    void on_actionTree_triggered();
    void on_actionClose_triggered();
    void on_actionQuit_triggered();

    void on_treeMainSelectionChanged(const QItemSelection &selected,
                                     const QItemSelection &deselected);
    void reset_selection();
    void select(int start, int end, bool scroll = true, TreeItemMeta *meta = nullptr);
    void updateDetails(TreeItemMeta *meta);
    void dropEvent(QDropEvent* event);
    void dragEnterEvent(QDragEnterEvent *e);
    void on_actionSegment_triggered();
    void on_actionConfiguration_triggered();
    void on_splitter_2_splitterMoved(int pos, int index);
    bool subTreeCollapsed();
    void on_actionField_Details_triggered(bool checked);
    void on_actionFind_triggered();
    void on_buttonSearch_clicked();
    void on_buttonClear_clicked();
    void on_fieldSearch_returnPressed();
    void on_action_About_triggered();
    bool isVisibleDetail();
    bool isVisibleTreeMain();
    bool isVisibleTreeSub();
    void log_visitibility();
    void toggleLeftBar();

    void on_fieldBtnOpen_clicked();

    void on_textEdit_selectionChanged();

    void add_text_editor_line(QString html_line, int line_num);

private:
    Ui::MainWindow *ui;
    TreeModel *model;
    TreeModel *modelTreeSub;
    QTextDocument *editorDocument;
    bool loaded = false;
    QString file = nullptr;
    QTextCharFormat highlight_format;

    // highlighting position
    int highlight_start = -1;
    int highlight_end   = -1;
    bool highlight_delim_start = false;
    bool highlight_delim_end   = false;

    void log_treeItemMeta(TreeItemMeta *meta);
    QShortcut *keyCtrlF;  // Entity of Ctrl + F hotkeys
    textFinder *finder = nullptr;

    int leftbarLastWidth = 0;
    QList<QString> *lastUsedFiles;
    void updateMrudMenu();
    int fileMenuActions = 0;

    QHexView *m_qhexview = nullptr;
    TreeItem *selectItem = nullptr;
    view_util *futil = nullptr;

    QProgressBar *m_statusProgressBar = nullptr;
    bool m_loading = false;
    //bool m_loading_data_arrived = false;

    QList<QString> versions = {};
    QMutex mutex_cursor;
};

MainWindow* getMainWindow();
#endif // MAINWINDOW_H
