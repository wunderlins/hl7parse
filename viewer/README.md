# simple HL7 viewer

## TODO

VIEWER_0_1_0 aka "the first usable"
- [x] select text when tree node is selected
- [x] file open
    - [x] dialog on ctrl-o/menu
    - [x] argv[1]
- [x] add position information to tree model
- [x] automatically open and select tree node when field is selected in textEdit
- [x] window title (file name)
- [x] application icon
- [x] truncate long lines in textEdit and treeMain
- [x] scroll to selection in textEdit
- [x] file open
    - [x] drag&drop

VIEWER_0_2_0 aka "the configurable"
- [x] fix memory leaks in libparser.dll
- [x] clicking on node with type SEGMENT does not scroll textEdit
- [x] make selection viewable even if there is an empty field
- [ ] ~~remove node_t from tree model (add all metadata to treeitem->meta)~~
- [x] Add QConfig
- [x] save state
    - [x] remember last folder location
    - [x] remember window state
    - [x] splitter positions
    - [x] fix open/close panels by using window state
- [x] configuration dialog
    - [x] max displayable line length
    - [x] font

VIEWER_0_3_0 aka "the shipable"
- [x] fix SEGFAULT with msh_minial.hl7
- [x] search: only selected search term, not whole word.
- [x] make sure all white space is displayed in textEdit
- [x] mrud
    - [x] display list of files in file menu
    - [x] update list upon load
    - [x] limit size to 10
    - [x] upon load, remove existing files and append them to the top
- [x] add segment sequence information to tree
- [x] treeSub
    - [x] implement treeSub to remove a lot of scrolling in the main tree
    - [x] automatic opening and scrolling
- [x] implement search text in textEdit: see https://doc.qt.io/qt-5/qtextedit.html#setExtraSelections, https://doc.qt.io/qt-5/qtuitools-textfinder-example.html
- [x] check if 0 length does not truncate long lines.

VIEWER_0_4_0 aka "the presentable"
- [ ] investigate memory managmenet
- [ ] distribution
    - [ ] deb build
    - [x] dmg build
    - [ ] setup exe
- [ ] add toolbar with open/collapse panel buttons
- [ ] maybe: add collapse all button to treeMain
- [ ] detail dialog
    - [ ] field representation as 
        - [x] String
        - [ ] decimal
        - [ ] hex (QHexView)[https://github.com/Dax89/QHexView]
        - [ ] date format
    - [ ] export long lines as file
        - [ ] add base64 decode
- [ ] a more consistent handling of resizes and open/close of panels
- [ ] show line numbers in textEdit
- [ ] statusbar: display metadata (encoding, hl7 version, type, sub-type)
- [ ] make a nicer icon
- [ ] about dialog
    - [x] ressource file for parser
    - [x] ressource file for viewer
    - [x] windows executable icon for viewer
    - [x] logo
    - [x] copyrights
    - [ ] version information
- [ ] configuration dialog
    - [x] colors
    - [ ] dark mode: https://github.com/Jorgen-VikingGod/Qt-Frameless-Window-DarkStyle/tree/master/darkstyle, https://github.com/KDE/breeze/blob/master/colors/BreezeDark.colors
    - [ ] menu icons, see: https://stackoverflow.com/questions/39749874/use-a-system-icon-in-the-seticon-method-of-qwidget

VIWER_0_5_0 aka "the informative"
- [ ] include xsd parser
- [ ] add type info browser
- [ ] add type info to tree, maybe? consider performance
