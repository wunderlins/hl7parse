#include <QString>
#include <QDebug>
#include <QColorDialog>
#include <QDebug>

#include "configuration.h"
#include "ui_configuration.h"
#include "mainwindow.h"
#include "cfg.h"

Configuration::Configuration(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Configuration)
{
    ui->setupUi(this);

    // set defaults
    MainWindow *w = getMainWindow();

    // set ui fields
    ui->fontComboBox->setCurrentFont(w->fontName);
    ui->fontSize->setCurrentText(QString::number(w->fontSize));
    ui->lineLength->setText(QString::number(w->lineLength));

    ui->lblDisplaySegment->setStyleSheet("background-color: " + getMainWindow()->color_segment);
    ui->lblColorSegment->setText(getMainWindow()->color_segment);

    ui->lblDisplayDelimiter->setStyleSheet("background-color: " + getMainWindow()->color_delimiter);
    ui->lblColorDelimiter->setText(getMainWindow()->color_delimiter);

    ui->lblDisplayText->setStyleSheet("background-color: " + getMainWindow()->color_text);
    ui->lblColorText->setText(getMainWindow()->color_text);

    ui->lblDisplayBackground->setStyleSheet("background-color: " + getMainWindow()->color_background);
    ui->lblColorBackground->setText(getMainWindow()->color_background);

    ui->lblDisplayHighlight->setStyleSheet("background-color: " + getMainWindow()->color_highlight);
    ui->lblColorHighlight->setText(getMainWindow()->color_highlight);

    ui->lblDisplayHighlightfg->setStyleSheet("background-color: " + getMainWindow()->color_highlightfg);
    ui->lblColorHighlightfg->setText(getMainWindow()->color_highlightfg);

    ui->lblDisplaySearch->setStyleSheet("background-color: " + getMainWindow()->color_search);
    ui->lblColorSearch->setText(getMainWindow()->color_search);

    QList<QString> v = w->get_supported_versions();
    QString selectedVersion = getMainWindow()->defaultVersion;
    ui->versionComboBox->addItems(v);
    for (int i=0; i<v.length(); i++) {
        if (selectedVersion == v.at(i)) {
            ui->versionComboBox->setCurrentIndex(i);
            break;
        }   
    }


    // accept a number range only for character length
    ui->lineLength->setValidator(new QIntValidator(1, w->max_displayable_text_length, this));
}

Configuration::~Configuration()
{
    delete ui;
}

void Configuration::on_buttonBox_accepted()
{
    // get configuration
    QString fontComboBox = ui->fontComboBox->currentFont().family();
    int fontSize = ui->fontSize->currentText().toInt();
    int lineLength = ui->lineLength->text().toInt();

    // save configuration
    MainWindow *w = getMainWindow();
    w->fontName = fontComboBox;
    cfgSet("font.family", fontComboBox);

    if (fontSize) {
        cfgSet("font.size", QString::number(fontSize));
        w->fontSize = fontSize;
        qDebug() << "font size: " << fontSize;
    }
    
    if (lineLength > 0 && lineLength <= w->max_displayable_text_length ) {
        cfgSet("line.length", QString::number(lineLength));
        w->lineLength = lineLength;
    }

    qDebug() << "saving:\nfont.family: " << fontComboBox << "\nfont.size: " << fontSize << "\nline.length: " << lineLength;

    // apply configuration
    w->editorFont.setFamily(fontComboBox);
    w->editorFont.setPointSize(fontSize);
    //w->lineLength = lineLength;

    // save colors
    cfgSet("color/segment", ui->lblColorSegment->text());
    w->color_segment = ui->lblColorSegment->text();
    cfgSet("color/delimiter", ui->lblColorDelimiter->text());
    w->color_delimiter = ui->lblColorDelimiter->text();
    cfgSet("color/text", ui->lblColorText->text());
    w->color_text = ui->lblColorText->text();
    cfgSet("color/background", ui->lblColorBackground->text());
    w->color_background = ui->lblColorBackground->text();
    cfgSet("color/highlight", ui->lblColorHighlight->text());
    w->color_highlight = ui->lblColorHighlight->text();
    cfgSet("color/highlightfg", ui->lblColorHighlightfg->text());
    w->color_highlightfg = ui->lblColorHighlightfg->text();
    cfgSet("color/search", ui->lblColorSearch->text());
    w->color_search = ui->lblColorSearch->text();

    cfgSet("hl7.defaultVersion", ui->versionComboBox->currentText());
    w->defaultVersion = ui->versionComboBox->currentText();
}

QString Configuration::toRgb(QColor color) {
    return QString("#%1%2%3").arg(color.red(), 2, 16, QChar('0')).arg(color.green(), 2, 16, QChar('0')).arg(color.blue(), 2, 16, QChar('0')).toUpper();
}

QString Configuration::pickColor(QString color) {
    QColor newColor = QColorDialog::getColor(color, this);
    return toRgb(newColor);
}

void Configuration::on_btnColorSegment_clicked()
{
    QString newColor = pickColor(ui->lblColorSegment->text());
    ui->lblDisplaySegment->setStyleSheet("background-color: " + newColor);
    ui->lblColorSegment->setText(newColor);
    qDebug() << ui->lblColorSegment->text() << ", rgb: " << newColor;
}

void Configuration::on_btnColorDelimiter_clicked()
{
    QString newColor = pickColor(ui->lblColorDelimiter->text());
    ui->lblDisplayDelimiter->setStyleSheet("background-color: " + newColor);
    ui->lblColorDelimiter->setText(newColor);
    qDebug() << ui->lblColorDelimiter->text() << ", rgb: " << newColor;
}

void Configuration::on_btnColorText_clicked()
{
    QString newColor = pickColor(ui->lblColorText->text());
    ui->lblDisplayText->setStyleSheet("background-color: " + newColor);
    ui->lblColorText->setText(newColor);
    qDebug() << ui->lblColorText->text() << ", rgb: " << newColor;

}

void Configuration::on_btnColorBackground_clicked()
{
    QString newColor = pickColor(ui->lblColorBackground->text());
    ui->lblDisplayBackground->setStyleSheet("background-color: " + newColor);
    ui->lblColorBackground->setText(newColor);
    qDebug() << ui->lblColorBackground->text() << ", rgb: " << newColor;
}

void Configuration::on_btnColorHighlight_clicked()
{
    QString newColor = pickColor(ui->lblColorHighlight->text());
    ui->lblDisplayHighlight->setStyleSheet("background-color: " + newColor);
    ui->lblColorHighlight->setText(newColor);
    qDebug() << ui->lblColorHighlight->text() << ", rgb: " << newColor;
}

void Configuration::on_btnColorSearch_clicked()
{
    QString newColor = pickColor(ui->lblColorSearch->text());
    ui->lblDisplaySearch->setStyleSheet("background-color: " + newColor);
    ui->lblColorSearch->setText(newColor);
    qDebug() << ui->lblColorSearch->text() << ", rgb: " << newColor;
}

void Configuration::on_btnColorHighlightfg_clicked()
{
    QString newColor = pickColor(ui->lblColorHighlightfg->text());
    ui->lblDisplayHighlightfg->setStyleSheet("background-color: " + newColor);
    ui->lblColorHighlightfg->setText(newColor);
    qDebug() << ui->lblColorHighlightfg->text() << ", rgb: " << newColor;

}
