#ifndef TEXTFINDER_H
#define TEXTFINDER_H

#include <QObject>
#include <QTextCursor>

class textFinder : public QObject
{
    Q_OBJECT
public:
    explicit textFinder(QObject *parent = nullptr);
    int find(QString searchTerm);
    QString lastSearchString;
    void reset();

signals:

public slots:

private:
    QList<QTextCursor> *lastResults;
};

#endif // TEXTFINDER_H
