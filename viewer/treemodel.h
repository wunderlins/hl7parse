#ifndef TREEMODEL_H
#define TREEMODEL_H

#include <QAbstractItemModel>
#include <QModelIndex>
#include <QVariant>
#include <QFutureWatcher>
#include <QtConcurrent>
#include <QFuture>
#include <QStringList>
#include <QScrollBar>
#include <QThreadPool>

#include "treeitem.h"
#include "decode.h"
#include "mainwindow.h"
#include "../build/lib7types.h"

//class TreeItem;

class MainWindow; // forward declaration

//! [0]
class TreeModel : public QAbstractItemModel
{
    Q_OBJECT

public:
    explicit TreeModel(const QString &filename, QObject *parent = nullptr);
    explicit TreeModel(TreeItem *root, QObject *parent = nullptr);
    ~TreeModel();
    void free();

    QVariant data(const QModelIndex &index, int role) const override;
    Qt::ItemFlags flags(const QModelIndex &index) const override;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const override;
    QModelIndex index(int row, int column,
                      const QModelIndex &parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex &index) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    hl7_meta_t* hl7_meta = nullptr;
    message_t *message = nullptr;

    QModelIndex findNode(TreeItemMeta *meta, bool subTree = false);
    TreeItem *rootItem;
    void loadFile();

public slots:
    void finishedParsing();

    // hl7parse callbacks
    void cb_progress(message_t *message, unsigned long total, unsigned long current);
    void cb_start(message_t *message);
    void cb_end(message_t *message, unsigned long max, unsigned long current, int exit_code);
    void cb_segment(message_t *message, unsigned long num, char *name);

signals:
    void showAlert(QString title, QString message);
    void loading_start();
    void loading_progress(int percent);
    void loading_end(int exit_code);
    void loading_segment(unsigned long num, char *name);
    void parsed();
    void add_text_editor_line(QString html_line, int line_num);

private:
    void setupModelData(TreeItem *parent, int start_line = 0);

    QString editorText(node_t *node);
    bool firstFieldIsSequence(node_t *segment);

    QFutureWatcher<int> *watcher = nullptr;
    QFuture<int> *future = nullptr;
    MainWindow *w = nullptr;
    QString m_filename = nullptr;
    FILE *fd;

    hl7type_handle *hl7types = nullptr;
};
//! [0]

#endif // TREEMODEL_H
