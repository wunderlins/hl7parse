![7View Logo](images/logo2_64.png)

# 7View

#### Cross-platform HL7 viewer for Linux, MacOS and Windows

![Linux Screenshot](images/screenshot_viewer_kde_0.4.1.png)

`7view` is a simple and fast hl7 viewer that can be used on Windows, MacOS 
and Linux. The main features of the viewer are:

- **Syntax highlighting**: Delemiters and segment names are colorized
- **Large file handling**: can deal with large files such as Base64 embedded MDM messages
- **Shell integration**: for gui applications (Explorer and Finder context menu)
- **Search**: search for keywords in a file
- **Base64 decode**: fields and open them in external viewer
- **HEX Viewer**: for inspecting specific fields (usefull for analyzing encoding problems)

All sources are [freely available](https://gitlab.com/wunderlins/hl7parse) and 
licensed under the LGPL. Report issues on [gitlab.com/wunderlins/hl7parse](https://gitlab.com/wunderlins/hl7parse/-/issues).

----

This project also contains a collection of hl7 utilities written in C and C++. 
All tools are available and tested on Debian, Windows and MacOS. The packaged 
tools provide the following functionality: 

- gui tools
  - `7view` - a gui viewer for HL7 files
  - `7pdf` - extract a base64 encoded string from a field and open it in a pdf viewer
- command line tools
  - `7parse` - create a command line grep-able tree representation of a hl7 file
  - `7search` - fast hl7 content search utility that knows the structure
- developer tools
  - `lib7` - the C implementation of the hl7 parser ([API Documentation][1])
  - `pylib7` - python bindings for the C parser ([API Documentation][2])

[1]: http://spliffy.tx0.org/7view/doc/lib7/
[2]: http://spliffy.tx0.org/7view/doc/pylib7/

## Download

| System | Download | System Requirements |
|---|---|---|
| Windows | [Installer][20], [ZIP][21], [Source][22] | Windows 7/10 (maybe Vista, 64 bit only) |
| MacOS | [Disk Image (dmg)][23], [Source][22] | MacOS 10.15 (Catalina) |
| Debian | [Debian Package (deb)][24], [Source][22] | Debian Buster or equivalent Ubuntu |
| Linux | [x64 binary][25], [Source][22] | |
| Python3 | [pip package][26] | Tested with Python 3.8+ (no python2 support) |

[20]: http://spliffy.tx0.org/7view/releases/0.4.1/7view-windows-0.4.1.exe
[21]: http://spliffy.tx0.org/7view/releases/0.4.1/7view-windows-0.4.1.zip
[22]: https://gitlab.com/wunderlins/hl7parse/-/tree/master
[23]: http://spliffy.tx0.org/7view/releases/0.4.1/7view-osx-0.4.1.dmg
[24]: http://spliffy.tx0.org/7view/releases/0.4.1/7view-linux-0.4.1.deb
[25]: http://spliffy.tx0.org/7view/releases/0.4.1/7view-linux-0.4.1.tar.gz
[26]: http://spliffy.tx0.org/7view/releases/0.4.1/lib7-0.5.1.tar.gz

### Disclaimer

All work product by the team is provided ​“AS IS”. Other than as provided in this agreement, the team makes no other warranties, express or implied, and hereby disclaims all implied warranties, including any warranty of merchantability and warranty of fitness for a particular purpose.

### Release 0.4.1 - moar HEX'n streams

The 0.4.1 Release is a maintanance release. with a few new features.

- Open Base64 embedded documents in external viewers.
- Render files while they are parsed line by line, this is useful for very large files
- Progress bar while loading, also useful for very large files
- HEX viewer for inspecting binary content in a field.
- Shell integration on windows (when installer is used).
- Platform specific installers (deb, exe, dmg).

A lot of infrastructure has been changed 

- gmake to cmake migration, better IDE integration and better crossplatform capabilities
- doxygen & sphinx for api documentation
- installers for all 3 supported platforms
- python setuptools package
- unit-tests for lib7 with ctest and a little bit for 7view with qtest
- experimental node editing functionality
- added CI, gitlab will now compile every commit to master
- with camke: better integration into IDEs such as Qt Creator, VS Code on linux, macos and windows (with mingw)
- added [code of conduct][3]
- added sub-project [QHexView][4] (made some  for macos font rendering, assimilated into project tree)
- new icon / logo   
- python lib now implemented with ctypes
- `hl7_meta_t` is now a member of `message_t`. Hl7 metadata is now always part of a message object.
- better error handling in viewer
- lib7 has gotten some callbacks for progress/start/end and when a new segment is available
- 7view will render line by line
- 7view loads the file in separate thread and shows a progress bar


[3]: https://gitlab.com/wunderlins/hl7parse/-/blob/master/CODE_OF_CONDUCT.md
[4]: https://github.com/Dax89/QHexView

### Older Releases

Some older releases are [available here](http://spliffy.tx0.org/7view/releases/).
## Source Code

```bash
me@there:~$ git clone https://gitlab.com/wunderlins/hl7parse.git
me@there:~$ git submodule init   # optional for doxygen theme
me@there:~$ git submodule update # optional for doxygen theme
```

You can find instructions of setting up the build environment in [README.md](https://gitlab.com/wunderlins/hl7parse/-/blob/master/README.md). The command line utilites have minimal 
dependencies (glibc), the gui utilities require Qt5 and additional tools 
are optionally required (for testing and documentation).

## Python Module

**Install**

    pip install --user lib7-X.X.X.tar.gz

**Uninstall**

    pip uninstall -y "$(pip freeze --user | grep lib7)"

**Usage**

Example from the [API Documentation][2]:

```python
import lib7

msg = lib7.open("path/to/some/file.hl7")

# check what sort of message type this is, defined in MsH-9.1
msh91 = None
msh92 = None
try:
    msh91 = msg.get("MSH-9.1")
    print(msh91) # should print the content of the FIELD
except Hl7StructureException:
    print("MSH-9.1 Not found)

try:
    msh92 = msh91.next_sibling
except Hl7StructureException:
    print("MSH-9.2 Not found)

root = msh92
while root.parent:
    root = root.parent

# we have now found the top most node which is of type MESSAGE
# This node contains N segments in children
print(root.type) # prints: Type.MESSAGE
print("%r" % root) # prints: <MESSAGE (children: 2)>
print(root.num_children) # prints: 2
assert(root == msg) # same same but different (variable name)

# every node in the structure is an iterator which will
# iterate over all child nodes.
#
# This is the preferred way, because it is faster than first
# fetching all nodes via `n.children` and then looping over them.
for seg in root:
    print("%r" % seg)
    # prints: <SEGMENT MSH(1) (children: 18), MSH>
    #         <SEGMENT PID(1) (children: 1), PID>

    print(seg.addr)
    # prints: MSH(1)
    #         PID(1)

# or you may fetch a list of child ndoes like this:
children = root.children

# print the segment names
for c in children:
    print(c.data) # prints: MSH, PID, ...
```
