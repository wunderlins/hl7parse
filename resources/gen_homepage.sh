#!/usr/bin/env bash

OUT_DIR=../generated/homepage
mkdir $OUT_DIR 2>/dev/null
cp homepage/github.css $OUT_DIR/github.css
cp -r homepage/favicon $OUT_DIR/
cp -r homepage/images $OUT_DIR/
cp -r homepage/highlightjs/ $OUT_DIR/
markdown -f +autolink -f +githubtags -f +strikethrough -f +superscript \
    -f +tables -f +fencedcode -f +image -f +header -f +toc \
    -o homepage/index.html homepage/index.md

cat <<- "EOF" > $OUT_DIR/index.html
<!doctype html>
<html>
<head>
    <meta charset="utf8">
    <meta name="viewport" content="width: device-width, initial-scale=1">

    <title>7view - HL7 Viewer</title>
    <meta name="color-scheme" content="dark light">
    <meta name="author" content="Simon Wunderlin">
    <meta name="creator" content="Simon Wunderlin">
    <meta name="description" content="HL7 Viewer for Windows, Linux and MacOS">
    <meta name="robots" content="all">
    <meta name="generator" content="markdown">
    <meta name="keywords" content="hl7, viewer, windows, osx, macos, linux, debian, health level 7">

    <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
    <link rel="manifest" href="favicon/site.webmanifest">
    <link rel="mask-icon" href="favicon/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="github.css">
    <style>
        body p {margin: 0px; padding: 0px;}
        body p img[src="images/logo2_64.png"] { 
            float: left; 
            margin-right: 1.2em;
            vertical-align: top;
            border-radius: 5pt;
        }
        body h1 {
            padding-bottom: 0em; 
            margin-bottom: 0px;
            padding-top: 0em; 
            margin-top: 0px;
            vertical-align: top;
        }
        body h4 {padding-top: 0.6em; margin-top: 0px;}
        @media only screen and (min-width: 1280px) {
            body p img[src="images/screenshot_viewer_kde_0.4.1.png"] { 
                float: right;
            }
        }
        h2 {clear: both;}
    </style>
    <link rel="stylesheet" href="highlightjs/styles/default.min.css">
</head>
<body>

EOF

cat homepage/index.html >> $OUT_DIR/index.html

cat <<- "EOF" >> $OUT_DIR/index.html

<script src="highlightjs/highlight.min.js"></script>
<script>hljs.highlightAll();</script>
</body>
</html>

EOF

# configure highlight-js
sed -i -e 's/<code class="/<code class="language-/g' $OUT_DIR/index.html
