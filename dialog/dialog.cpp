#include <QFile>
#include <QDebug>

#include "dialog.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
    this->setWindowTitle(QCoreApplication::arguments().at(1));
    //QString text;
    //text.append(QCoreApplication::arguments().at(2).toUtf8().replace("\\n", "<br>"));

    QString text;
    QString param2;
    param2.append(QCoreApplication::arguments().at(2).toUtf8());
    if (param2 == "-f") {
        // read file at param 3
        QString file = QCoreApplication::arguments().at(3).toUtf8();
        QFile f(file);
        if (!f.open(QFile::ReadOnly | QFile::Text)) {
            qDebug() << "Failed to open file: " << file;
        } else {
            text.append(f.readAll().replace("\\n", "<br>"));
        }

    } else {
        text.append(QCoreApplication::arguments().at(2).toUtf8().replace("\\n", "<br>"));
    }


    ui->label->setText(text);
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::on_pushButton_clicked()
{
    QCoreApplication::quit();
}
