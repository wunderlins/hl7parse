"""
This is a python3 interface to lib7.so

lib7.so is a fast HL7 parser library written in C and compiled as shared 
object .so (.dll on windows). Python gets access to the library trough 
the `ctypes.CDLL` interface. 


STRUCTURE 

The library provides read-only access to the C data structure via the 
`hl7` object. `Mydata = hl7("filename")` will parse a hl7 file into a tree 
structure. Every node (Message, Segment, Component and Sub-Component) exposes
an property `children` which is a list of child nodes. The `size` attribute
is indicating how large the payload size of this node is in bytes. If 
you want to know the number of children in a node, use the `num_children` 
property. A node with children has always a payload size of 0, except for 
Segments (node type 2).


DATA ACCESS

Accessing the data of a node (provided size is larger than 0) can be done by
the proeprty `data`. Since lib7 is a low level library, encoding is not 
taken care of at this level. You will always get a byte-array of data and 
will have to deal with encoding yourself.


META DATA

MSH-18 might tell you what encoding was used, however there is no guarantee 
that this information is correct and often is is not. You may conveniently 
access the encoding information from `hl7.meta.encoding`. You will find
more interesting stuff in the meta class such as delimiters that have been 
automatically detected while parsing the hl7 file.

NOTE: this library provies read only access, use the `hl7` object as 
      entry point. There is a whole bunch of low level methods 
      which can manipulate C-Pointers (see details in lib7.h). 
      
      Using them is dangerous and will most likely corrupt the data.
      Doing so will most likely burn down your house. You have been warned.


Example Usage:
```
    msgs = hl7("data/minimal.hl7")

    print(msgs.meta)
    print("type: " + str(msgs.type))
    print("num_children: " + str(msgs.num_children))

    # Example on how to loov over the whole structure of a parsed hl7 file
    for segment in msgs.segments:
        #print(f"{segment.data.decode('utf-8')}")
        print(str(segment)) # print the whole segment as string

        fc = 1
        for fieldset in segment.children:
            # no data here, every fieldset has at least one field
            # for example, PID-1 has never data, data will be found in PID-1.1
            for field in fieldset.children:
                print(f"+ {fc} {field.data.decode('utf-8')}")
                for comp in field.children:
                    print(f"+- {comp.data.decode('utf-8')}")
                    for subcomp in comp.children:
                        print(f"+-- {subcomp.data.decode('utf-8')}")
            fc += 1

```

(c) 2021, Simon Wunderlin
"""

