"""
This is a python3 interface to lib7.so

lib7.so is a fast HL7 parser library written in C and compiled as shared 
object .so (.dll on windows). Python gets access to the library trough 
the `ctypes.CDLL` interface. 


STRUCTURE 

The library provides read-only access to the C data structure via the 
`hl7` object. `Mydata = hl7("filename")` will parse a hl7 file into a tree 
structure. Every node (Message, Segment, Component and Sub-Component) exposes
an property `children` which is a list of child nodes. The `size` attribute
is indicating how large the payload size of this node is in bytes. If 
you want to know the number of children in a node, use the `num_children` 
property. A node with children has always a payload size of 0, except for 
Segments (node type 2).


DATA ACCESS

Accessing the data of a node (provided size is larger than 0) can be done by
the proeprty `data`. Since lib7 is a low level library, encoding is not 
taken care of at this level. You will always get a byte-array of data and 
will have to deal with encoding yourself.


META DATA

MSH-18 might tell you what encoding was used, however there is no guarantee 
that this information is correct and often is is not. You may conveniently 
access the encoding information from `hl7.meta.encoding`. You will find
more interesting stuff in the meta class such as delimiters that have been 
automatically detected while parsing the hl7 file.

NOTE: this library provies read only access, use the `hl7` object as 
      entry point. There is a whole bunch of low level methods 
      which can manipulate C-Pointers (see details in lib7.h). 
      
      Using them is dangerous and will most likely corrupt the data.
      Doing so will most likely burn down your house. You have been warned.


Example Usage:
```
    msgs = hl7("data/minimal.hl7")

    print(msgs.meta)
    print("type: " + str(msgs.type))
    print("num_children: " + str(msgs.num_children))

    # Example on how to loov over the whole structure of a parsed hl7 file
    for segment in msgs.segments:
        #print(f"{segment.data.decode('utf-8')}")
        print(str(segment)) # print the whole segment as string

        fc = 1
        for fieldset in segment.children:
            # no data here, every fieldset has at least one field
            # for example, PID-1 has never data, data will be found in PID-1.1
            for field in fieldset.children:
                print(f"+ {fc} {field.data.decode('utf-8')}")
                for comp in field.children:
                    print(f"+- {comp.data.decode('utf-8')}")
                    for subcomp in comp.children:
                        print(f"+-- {subcomp.data.decode('utf-8')}")
            fc += 1

```

(c) 2021, Simon Wunderlin
"""

# FIXME: let CMake handle version numbers
__version__ = '0.0.2-SNAPSHOT'

r"""Wrapper for lib7.h

Generated with:
../language_bindings/ctypesgen/run.py -l lib7.dll -o lib7_tmp.py lib7.h

Do not modify this file.
"""

__docformat__ = "restructuredtext"

# Begin preamble for Python v(3, 2)

import ctypes, os, sys
from ctypes import *

_int_types = (c_int16, c_int32)
if hasattr(ctypes, "c_int64"):
    # Some builds of ctypes apparently do not have c_int64
    # defined; it's a pretty good bet that these builds do not
    # have 64-bit pointers.
    _int_types += (c_int64,)
for t in _int_types:
    if sizeof(t) == sizeof(c_size_t):
        c_ptrdiff_t = t
del t
del _int_types


class UserString:
    def __init__(self, seq):
        if isinstance(seq, bytes):
            self.data = seq
        elif isinstance(seq, UserString):
            self.data = seq.data[:]
        else:
            self.data = str(seq).encode()

    def __bytes__(self):
        return self.data

    def __str__(self):
        return self.data.decode()

    def __repr__(self):
        return repr(self.data)

    def __int__(self):
        return int(self.data.decode())

    def __long__(self):
        return int(self.data.decode())

    def __float__(self):
        return float(self.data.decode())

    def __complex__(self):
        return complex(self.data.decode())

    def __hash__(self):
        return hash(self.data)

    def __cmp__(self, string):
        if isinstance(string, UserString):
            return cmp(self.data, string.data)
        else:
            return cmp(self.data, string)

    def __le__(self, string):
        if isinstance(string, UserString):
            return self.data <= string.data
        else:
            return self.data <= string

    def __lt__(self, string):
        if isinstance(string, UserString):
            return self.data < string.data
        else:
            return self.data < string

    def __ge__(self, string):
        if isinstance(string, UserString):
            return self.data >= string.data
        else:
            return self.data >= string

    def __gt__(self, string):
        if isinstance(string, UserString):
            return self.data > string.data
        else:
            return self.data > string

    def __eq__(self, string):
        if isinstance(string, UserString):
            return self.data == string.data
        else:
            return self.data == string

    def __ne__(self, string):
        if isinstance(string, UserString):
            return self.data != string.data
        else:
            return self.data != string

    def __contains__(self, char):
        return char in self.data

    def __len__(self):
        return len(self.data)

    def __getitem__(self, index):
        return self.__class__(self.data[index])

    def __getslice__(self, start, end):
        start = max(start, 0)
        end = max(end, 0)
        return self.__class__(self.data[start:end])

    def __add__(self, other):
        if isinstance(other, UserString):
            return self.__class__(self.data + other.data)
        elif isinstance(other, bytes):
            return self.__class__(self.data + other)
        else:
            return self.__class__(self.data + str(other).encode())

    def __radd__(self, other):
        if isinstance(other, bytes):
            return self.__class__(other + self.data)
        else:
            return self.__class__(str(other).encode() + self.data)

    def __mul__(self, n):
        return self.__class__(self.data * n)

    __rmul__ = __mul__

    def __mod__(self, args):
        return self.__class__(self.data % args)

    # the following methods are defined in alphabetical order:
    def capitalize(self):
        return self.__class__(self.data.capitalize())

    def center(self, width, *args):
        return self.__class__(self.data.center(width, *args))

    def count(self, sub, start=0, end=sys.maxsize):
        return self.data.count(sub, start, end)

    def decode(self, encoding=None, errors=None):  # XXX improve this?
        if encoding:
            if errors:
                return self.__class__(self.data.decode(encoding, errors))
            else:
                return self.__class__(self.data.decode(encoding))
        else:
            return self.__class__(self.data.decode())

    def encode(self, encoding=None, errors=None):  # XXX improve this?
        if encoding:
            if errors:
                return self.__class__(self.data.encode(encoding, errors))
            else:
                return self.__class__(self.data.encode(encoding))
        else:
            return self.__class__(self.data.encode())

    def endswith(self, suffix, start=0, end=sys.maxsize):
        return self.data.endswith(suffix, start, end)

    def expandtabs(self, tabsize=8):
        return self.__class__(self.data.expandtabs(tabsize))

    def find(self, sub, start=0, end=sys.maxsize):
        return self.data.find(sub, start, end)

    def index(self, sub, start=0, end=sys.maxsize):
        return self.data.index(sub, start, end)

    def isalpha(self):
        return self.data.isalpha()

    def isalnum(self):
        return self.data.isalnum()

    def isdecimal(self):
        return self.data.isdecimal()

    def isdigit(self):
        return self.data.isdigit()

    def islower(self):
        return self.data.islower()

    def isnumeric(self):
        return self.data.isnumeric()

    def isspace(self):
        return self.data.isspace()

    def istitle(self):
        return self.data.istitle()

    def isupper(self):
        return self.data.isupper()

    def join(self, seq):
        return self.data.join(seq)

    def ljust(self, width, *args):
        return self.__class__(self.data.ljust(width, *args))

    def lower(self):
        return self.__class__(self.data.lower())

    def lstrip(self, chars=None):
        return self.__class__(self.data.lstrip(chars))

    def partition(self, sep):
        return self.data.partition(sep)

    def replace(self, old, new, maxsplit=-1):
        return self.__class__(self.data.replace(old, new, maxsplit))

    def rfind(self, sub, start=0, end=sys.maxsize):
        return self.data.rfind(sub, start, end)

    def rindex(self, sub, start=0, end=sys.maxsize):
        return self.data.rindex(sub, start, end)

    def rjust(self, width, *args):
        return self.__class__(self.data.rjust(width, *args))

    def rpartition(self, sep):
        return self.data.rpartition(sep)

    def rstrip(self, chars=None):
        return self.__class__(self.data.rstrip(chars))

    def split(self, sep=None, maxsplit=-1):
        return self.data.split(sep, maxsplit)

    def rsplit(self, sep=None, maxsplit=-1):
        return self.data.rsplit(sep, maxsplit)

    def splitlines(self, keepends=0):
        return self.data.splitlines(keepends)

    def startswith(self, prefix, start=0, end=sys.maxsize):
        return self.data.startswith(prefix, start, end)

    def strip(self, chars=None):
        return self.__class__(self.data.strip(chars))

    def swapcase(self):
        return self.__class__(self.data.swapcase())

    def title(self):
        return self.__class__(self.data.title())

    def translate(self, *args):
        return self.__class__(self.data.translate(*args))

    def upper(self):
        return self.__class__(self.data.upper())

    def zfill(self, width):
        return self.__class__(self.data.zfill(width))


class MutableString(UserString):
    """mutable string objects

    Python strings are immutable objects.  This has the advantage, that
    strings may be used as dictionary keys.  If this property isn't needed
    and you insist on changing string values in place instead, you may cheat
    and use MutableString.

    But the purpose of this class is an educational one: to prevent
    people from inventing their own mutable string class derived
    from UserString and than forget thereby to remove (override) the
    __hash__ method inherited from UserString.  This would lead to
    errors that would be very hard to track down.

    A faster and better solution is to rewrite your program using lists."""

    def __init__(self, string=""):
        self.data = string

    def __hash__(self):
        raise TypeError("unhashable type (it is mutable)")

    def __setitem__(self, index, sub):
        if index < 0:
            index += len(self.data)
        if index < 0 or index >= len(self.data):
            raise IndexError
        self.data = self.data[:index] + sub + self.data[index + 1 :]

    def __delitem__(self, index):
        if index < 0:
            index += len(self.data)
        if index < 0 or index >= len(self.data):
            raise IndexError
        self.data = self.data[:index] + self.data[index + 1 :]

    def __setslice__(self, start, end, sub):
        start = max(start, 0)
        end = max(end, 0)
        if isinstance(sub, UserString):
            self.data = self.data[:start] + sub.data + self.data[end:]
        elif isinstance(sub, bytes):
            self.data = self.data[:start] + sub + self.data[end:]
        else:
            self.data = self.data[:start] + str(sub).encode() + self.data[end:]

    def __delslice__(self, start, end):
        start = max(start, 0)
        end = max(end, 0)
        self.data = self.data[:start] + self.data[end:]

    def immutable(self):
        return UserString(self.data)

    def __iadd__(self, other):
        if isinstance(other, UserString):
            self.data += other.data
        elif isinstance(other, bytes):
            self.data += other
        else:
            self.data += str(other).encode()
        return self

    def __imul__(self, n):
        self.data *= n
        return self


class String(MutableString, Union):

    _fields_ = [("raw", POINTER(c_char)), ("data", c_char_p)]

    def __init__(self, obj=b""):
        if isinstance(obj, (bytes, UserString)):
            self.data = bytes(obj)
        else:
            self.raw = obj

    def __len__(self):
        return self.data and len(self.data) or 0

    def from_param(cls, obj):
        # Convert None or 0
        if obj is None or obj == 0:
            return cls(POINTER(c_char)())

        # Convert from String
        elif isinstance(obj, String):
            return obj

        # Convert from bytes
        elif isinstance(obj, bytes):
            return cls(obj)

        # Convert from str
        elif isinstance(obj, str):
            return cls(obj.encode())

        # Convert from c_char_p
        elif isinstance(obj, c_char_p):
            return obj

        # Convert from POINTER(c_char)
        elif isinstance(obj, POINTER(c_char)):
            return obj

        # Convert from raw pointer
        elif isinstance(obj, int):
            return cls(cast(obj, POINTER(c_char)))

        # Convert from c_char array
        elif isinstance(obj, c_char * len(obj)):
            return obj

        # Convert from object
        else:
            return String.from_param(obj._as_parameter_)

    from_param = classmethod(from_param)


def ReturnString(obj, func=None, arguments=None):
    return String.from_param(obj)


# As of ctypes 1.0, ctypes does not support custom error-checking
# functions on callbacks, nor does it support custom datatypes on
# callbacks, so we must ensure that all callbacks return
# primitive datatypes.
#
# Non-primitive return values wrapped with UNCHECKED won't be
# typechecked, and will be converted to c_void_p.
def UNCHECKED(type):
    if hasattr(type, "_type_") and isinstance(type._type_, str) and type._type_ != "P":
        return type
    else:
        return c_void_p


# ctypes doesn't have direct support for variadic functions, so we have to write
# our own wrapper class
class _variadic_function(object):
    def __init__(self, func, restype, argtypes, errcheck):
        self.func = func
        self.func.restype = restype
        self.argtypes = argtypes
        if errcheck:
            self.func.errcheck = errcheck

    def _as_parameter_(self):
        # So we can pass this variadic function as a function pointer
        return self.func

    def __call__(self, *args):
        fixed_args = []
        i = 0
        for argtype in self.argtypes:
            # Typecheck what we can
            fixed_args.append(argtype.from_param(args[i]))
            i += 1
        return self.func(*fixed_args + list(args[i:]))


def ord_if_char(value):
    """
    Simple helper used for casts to simple builtin types:  if the argument is a
    string type, it will be converted to it's ordinal value.

    This function will raise an exception if the argument is string with more
    than one characters.
    """
    return ord(value) if (isinstance(value, bytes) or isinstance(value, str)) else value

# End preamble

_libs = {}
_libdirs = []

# Begin loader

# ----------------------------------------------------------------------------
# Copyright (c) 2008 David James
# Copyright (c) 2006-2008 Alex Holkner
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#  * Neither the name of pyglet nor the names of its
#    contributors may be used to endorse or promote products
#    derived from this software without specific prior written
#    permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
# ----------------------------------------------------------------------------

import os.path, re, sys, glob
import platform
import ctypes
import ctypes.util


def _environ_path(name):
    if name in os.environ:
        return os.environ[name].split(":")
    else:
        return []


class LibraryLoader(object):
    # library names formatted specifically for platforms
    name_formats = ["%s"]

    class Lookup(object):
        mode = ctypes.DEFAULT_MODE

        def __init__(self, path):
            super(LibraryLoader.Lookup, self).__init__()
            self.access = dict(cdecl=ctypes.CDLL(path, self.mode))

        def get(self, name, calling_convention="cdecl"):
            if calling_convention not in self.access:
                raise LookupError(
                    "Unknown calling convention '{}' for function '{}'".format(
                        calling_convention, name
                    )
                )
            return getattr(self.access[calling_convention], name)

        def has(self, name, calling_convention="cdecl"):
            if calling_convention not in self.access:
                return False
            return hasattr(self.access[calling_convention], name)

        def __getattr__(self, name):
            return getattr(self.access["cdecl"], name)

    def __init__(self):
        self.other_dirs = []

    def __call__(self, libname):
        """Given the name of a library, load it."""
        paths = self.getpaths(libname)

        for path in paths:
            try:
                return self.Lookup(path)
            except:
                pass

        raise ImportError("Could not load %s." % libname)

    def getpaths(self, libname):
        """Return a list of paths where the library might be found."""
        if os.path.isabs(libname):
            yield libname
        else:
            # search through a prioritized series of locations for the library

            # we first search any specific directories identified by user
            for dir_i in self.other_dirs:
                for fmt in self.name_formats:
                    # dir_i should be absolute already
                    yield os.path.join(dir_i, fmt % libname)

            # then we search the directory where the generated python interface is stored
            for fmt in self.name_formats:
                yield os.path.abspath(os.path.join(os.path.dirname(__file__), fmt % libname))

            # now, use the ctypes tools to try to find the library
            for fmt in self.name_formats:
                path = ctypes.util.find_library(fmt % libname)
                if path:
                    yield path

            # then we search all paths identified as platform-specific lib paths
            for path in self.getplatformpaths(libname):
                yield path

            # Finally, we'll try the users current working directory
            for fmt in self.name_formats:
                yield os.path.abspath(os.path.join(os.path.curdir, fmt % libname))

    def getplatformpaths(self, libname):
        return []


# Darwin (Mac OS X)


class DarwinLibraryLoader(LibraryLoader):
    name_formats = [
        "lib%s.dylib",
        "lib%s.so",
        "lib%s.bundle",
        "%s.dylib",
        "%s.so",
        "%s.bundle",
        "%s",
    ]

    class Lookup(LibraryLoader.Lookup):
        # Darwin requires dlopen to be called with mode RTLD_GLOBAL instead
        # of the default RTLD_LOCAL.  Without this, you end up with
        # libraries not being loadable, resulting in "Symbol not found"
        # errors
        mode = ctypes.RTLD_GLOBAL

    def getplatformpaths(self, libname):
        if os.path.pathsep in libname:
            names = [libname]
        else:
            names = [format % libname for format in self.name_formats]

        for dir in self.getdirs(libname):
            for name in names:
                yield os.path.join(dir, name)

    def getdirs(self, libname):
        """Implements the dylib search as specified in Apple documentation:

        http://developer.apple.com/documentation/DeveloperTools/Conceptual/
            DynamicLibraries/Articles/DynamicLibraryUsageGuidelines.html

        Before commencing the standard search, the method first checks
        the bundle's ``Frameworks`` directory if the application is running
        within a bundle (OS X .app).
        """

        dyld_fallback_library_path = _environ_path("DYLD_FALLBACK_LIBRARY_PATH")
        if not dyld_fallback_library_path:
            dyld_fallback_library_path = [os.path.expanduser("~/lib"), "/usr/local/lib", "/usr/lib"]

        dirs = []

        if "/" in libname:
            dirs.extend(_environ_path("DYLD_LIBRARY_PATH"))
        else:
            dirs.extend(_environ_path("LD_LIBRARY_PATH"))
            dirs.extend(_environ_path("DYLD_LIBRARY_PATH"))

        if hasattr(sys, "frozen") and sys.frozen == "macosx_app":
            dirs.append(os.path.join(os.environ["RESOURCEPATH"], "..", "Frameworks"))

        dirs.extend(dyld_fallback_library_path)

        return dirs


# Posix


class PosixLibraryLoader(LibraryLoader):
    _ld_so_cache = None

    _include = re.compile(r"^\s*include\s+(?P<pattern>.*)")

    class _Directories(dict):
        def __init__(self):
            self.order = 0

        def add(self, directory):
            if len(directory) > 1:
                directory = directory.rstrip(os.path.sep)
            # only adds and updates order if exists and not already in set
            if not os.path.exists(directory):
                return
            o = self.setdefault(directory, self.order)
            if o == self.order:
                self.order += 1

        def extend(self, directories):
            for d in directories:
                self.add(d)

        def ordered(self):
            return (i[0] for i in sorted(self.items(), key=lambda D: D[1]))

    def _get_ld_so_conf_dirs(self, conf, dirs):
        """
        Recursive funtion to help parse all ld.so.conf files, including proper
        handling of the `include` directive.
        """

        try:
            with open(conf) as f:
                for D in f:
                    D = D.strip()
                    if not D:
                        continue

                    m = self._include.match(D)
                    if not m:
                        dirs.add(D)
                    else:
                        for D2 in glob.glob(m.group("pattern")):
                            self._get_ld_so_conf_dirs(D2, dirs)
        except IOError:
            pass

    def _create_ld_so_cache(self):
        # Recreate search path followed by ld.so.  This is going to be
        # slow to build, and incorrect (ld.so uses ld.so.cache, which may
        # not be up-to-date).  Used only as fallback for distros without
        # /sbin/ldconfig.
        #
        # We assume the DT_RPATH and DT_RUNPATH binary sections are omitted.

        directories = self._Directories()
        for name in (
            "LD_LIBRARY_PATH",
            "SHLIB_PATH",  # HPUX
            "LIBPATH",  # OS/2, AIX
            "LIBRARY_PATH",  # BE/OS
        ):
            if name in os.environ:
                directories.extend(os.environ[name].split(os.pathsep))

        self._get_ld_so_conf_dirs("/etc/ld.so.conf", directories)

        bitage = platform.architecture()[0]

        unix_lib_dirs_list = []
        if bitage.startswith("64"):
            # prefer 64 bit if that is our arch
            unix_lib_dirs_list += ["/lib64", "/usr/lib64"]

        # must include standard libs, since those paths are also used by 64 bit
        # installs
        unix_lib_dirs_list += ["/lib", "/usr/lib"]
        if sys.platform.startswith("linux"):
            # Try and support multiarch work in Ubuntu
            # https://wiki.ubuntu.com/MultiarchSpec
            if bitage.startswith("32"):
                # Assume Intel/AMD x86 compat
                unix_lib_dirs_list += ["/lib/i386-linux-gnu", "/usr/lib/i386-linux-gnu"]
            elif bitage.startswith("64"):
                # Assume Intel/AMD x86 compat
                unix_lib_dirs_list += ["/lib/x86_64-linux-gnu", "/usr/lib/x86_64-linux-gnu"]
            else:
                # guess...
                unix_lib_dirs_list += glob.glob("/lib/*linux-gnu")
        directories.extend(unix_lib_dirs_list)

        cache = {}
        lib_re = re.compile(r"lib(.*)\.s[ol]")
        ext_re = re.compile(r"\.s[ol]$")
        for dir in directories.ordered():
            try:
                for path in glob.glob("%s/*.s[ol]*" % dir):
                    file = os.path.basename(path)

                    # Index by filename
                    cache_i = cache.setdefault(file, set())
                    cache_i.add(path)

                    # Index by library name
                    match = lib_re.match(file)
                    if match:
                        library = match.group(1)
                        cache_i = cache.setdefault(library, set())
                        cache_i.add(path)
            except OSError:
                pass

        self._ld_so_cache = cache

    def getplatformpaths(self, libname):
        if self._ld_so_cache is None:
            self._create_ld_so_cache()

        result = self._ld_so_cache.get(libname, set())
        for i in result:
            # we iterate through all found paths for library, since we may have
            # actually found multiple architectures or other library types that
            # may not load
            yield i


# Windows


class WindowsLibraryLoader(LibraryLoader):
    name_formats = ["%s.dll", "lib%s.dll", "%slib.dll", "%s"]

    class Lookup(LibraryLoader.Lookup):
        def __init__(self, path):
            super(WindowsLibraryLoader.Lookup, self).__init__(path)
            self.access["stdcall"] = ctypes.windll.LoadLibrary(path)


# Platform switching

# If your value of sys.platform does not appear in this dict, please contact
# the Ctypesgen maintainers.

loaderclass = {
    "darwin": DarwinLibraryLoader,
    "cygwin": WindowsLibraryLoader,
    "win32": WindowsLibraryLoader,
    "msys": WindowsLibraryLoader,
}

load_library = loaderclass.get(sys.platform, PosixLibraryLoader)()


def add_library_search_dirs(other_dirs):
    """
    Add libraries to search paths.
    If library paths are relative, convert them to absolute with respect to this
    file's directory
    """
    for F in other_dirs:
        if not os.path.isabs(F):
            F = os.path.abspath(F)
        load_library.other_dirs.append(F)


del loaderclass

# End loader

add_library_search_dirs([])

# Begin libraries
_libs["lib7.dll"] = load_library("lib7.dll")

# 1 libraries
# End libraries

# No modules

# C:/msys64/mingw64/x86_64-w64-mingw32/include/stdio.h: 24
class struct__iobuf(Structure):
    pass

struct__iobuf.__slots__ = [
    '_ptr',
    '_cnt',
    '_base',
    '_flag',
    '_file',
    '_charbuf',
    '_bufsiz',
    '_tmpfname',
]
struct__iobuf._fields_ = [
    ('_ptr', String),
    ('_cnt', c_int),
    ('_base', String),
    ('_flag', c_int),
    ('_file', c_int),
    ('_charbuf', c_int),
    ('_bufsiz', c_int),
    ('_tmpfname', String),
]

FILE = struct__iobuf# C:/msys64/mingw64/x86_64-w64-mingw32/include/stdio.h: 34

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 37
class struct_anon_10(Structure):
    pass

struct_anon_10.__slots__ = [
    'section_start',
    'section_end',
    'item_start',
    'item_end',
    'item_equal',
    'item_comment',
]
struct_anon_10._fields_ = [
    ('section_start', c_size_t),
    ('section_end', c_size_t),
    ('item_start', c_size_t),
    ('item_end', c_size_t),
    ('item_equal', c_size_t),
    ('item_comment', c_size_t),
]

last_pos_t = struct_anon_10# C:/Users/wus/Projects/hl7parse/build/lib7.h: 37

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 47
class struct_anon_11(Structure):
    pass

struct_anon_11.__slots__ = [
    'start',
    'end',
    'name',
    'value',
]
struct_anon_11._fields_ = [
    ('start', c_int),
    ('end', c_int),
    ('name', String),
    ('value', String),
]

ini_item_t = struct_anon_11# C:/Users/wus/Projects/hl7parse/build/lib7.h: 47

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 57
class struct_anon_12(Structure):
    pass

struct_anon_12.__slots__ = [
    'name',
    'length',
    'size',
    'items',
]
struct_anon_12._fields_ = [
    ('name', String),
    ('length', c_int),
    ('size', c_int),
    ('items', POINTER(POINTER(ini_item_t))),
]

ini_section_t = struct_anon_12# C:/Users/wus/Projects/hl7parse/build/lib7.h: 57

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 66
class struct_anon_13(Structure):
    pass

struct_anon_13.__slots__ = [
    'length',
    'size',
    'sections',
]
struct_anon_13._fields_ = [
    ('length', c_int),
    ('size', c_int),
    ('sections', POINTER(POINTER(ini_section_t))),
]

ini_section_list_t = struct_anon_13# C:/Users/wus/Projects/hl7parse/build/lib7.h: 66

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 69
if _libs["lib7.dll"].has("ini_parse", "cdecl"):
    ini_parse = _libs["lib7.dll"].get("ini_parse", "cdecl")
    ini_parse.argtypes = [POINTER(FILE)]
    ini_parse.restype = POINTER(ini_section_list_t)

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 70
if _libs["lib7.dll"].has("ini_free", "cdecl"):
    ini_free = _libs["lib7.dll"].get("ini_free", "cdecl")
    ini_free.argtypes = [POINTER(ini_section_list_t)]
    ini_free.restype = None

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 73
if _libs["lib7.dll"].has("ini_find_section", "cdecl"):
    ini_find_section = _libs["lib7.dll"].get("ini_find_section", "cdecl")
    ini_find_section.argtypes = [POINTER(ini_section_list_t), String]
    ini_find_section.restype = POINTER(ini_section_t)

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 75
if _libs["lib7.dll"].has("ini_find_key", "cdecl"):
    ini_find_key = _libs["lib7.dll"].get("ini_find_key", "cdecl")
    ini_find_key.argtypes = [POINTER(ini_section_t), String]
    ini_find_key.restype = POINTER(ini_item_t)

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 79
if _libs["lib7.dll"].has("ini_get_value", "cdecl"):
    ini_get_value = _libs["lib7.dll"].get("ini_get_value", "cdecl")
    ini_get_value.argtypes = [POINTER(ini_section_list_t), String, String]
    if sizeof(c_int) == sizeof(c_void_p):
        ini_get_value.restype = ReturnString
    else:
        ini_get_value.restype = String
        ini_get_value.errcheck = ReturnString

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 85
class struct_bom_t(Structure):
    pass

struct_bom_t.__slots__ = [
    'bom',
    'length',
]
struct_bom_t._fields_ = [
    ('bom', String),
    ('length', c_int),
]

bom_t = struct_bom_t# C:/Users/wus/Projects/hl7parse/build/lib7.h: 85

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 100
if _libs["lib7.dll"].has("detect_bom", "cdecl"):
    detect_bom = _libs["lib7.dll"].get("detect_bom", "cdecl")
    detect_bom.argtypes = [POINTER(FILE)]
    detect_bom.restype = POINTER(bom_t)

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 137
class struct_hl7_meta_t(Structure):
    pass

struct_hl7_meta_t.__slots__ = [
    'field_length',
    'crlf',
    'sep_message',
    'sep_field',
    'sep_comp',
    'sep_rep',
    'sep_escape',
    'sep_subcmp',
    'encoding',
    'version',
    'type',
    'subtype',
    'bom',
]
struct_hl7_meta_t._fields_ = [
    ('field_length', c_int),
    ('crlf', c_int),
    ('sep_message', c_char),
    ('sep_field', c_char),
    ('sep_comp', c_char),
    ('sep_rep', c_char),
    ('sep_escape', c_char),
    ('sep_subcmp', c_char),
    ('encoding', String),
    ('version', String),
    ('type', String),
    ('subtype', String),
    ('bom', POINTER(bom_t)),
]

hl7_meta_t = struct_hl7_meta_t# C:/Users/wus/Projects/hl7parse/build/lib7.h: 137

enum_line_delimiter_t = c_int# C:/Users/wus/Projects/hl7parse/build/lib7.h: 151

DELIM_NONE = 0# C:/Users/wus/Projects/hl7parse/build/lib7.h: 151

DELIM_CR = (DELIM_NONE + 1)# C:/Users/wus/Projects/hl7parse/build/lib7.h: 151

DELIM_LF = (DELIM_CR + 1)# C:/Users/wus/Projects/hl7parse/build/lib7.h: 151

DELIM_CRLF = (DELIM_LF + 1)# C:/Users/wus/Projects/hl7parse/build/lib7.h: 151

line_delimiter_t = enum_line_delimiter_t# C:/Users/wus/Projects/hl7parse/build/lib7.h: 151

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 158
if _libs["lib7.dll"].has("init_hl7_meta_t", "cdecl"):
    init_hl7_meta_t = _libs["lib7.dll"].get("init_hl7_meta_t", "cdecl")
    init_hl7_meta_t.argtypes = []
    init_hl7_meta_t.restype = POINTER(hl7_meta_t)

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 166
if _libs["lib7.dll"].has("hl7_meta_string", "cdecl"):
    hl7_meta_string = _libs["lib7.dll"].get("hl7_meta_string", "cdecl")
    hl7_meta_string.argtypes = [POINTER(hl7_meta_t)]
    if sizeof(c_int) == sizeof(c_void_p):
        hl7_meta_string.restype = ReturnString
    else:
        hl7_meta_string.restype = String
        hl7_meta_string.errcheck = ReturnString

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 176
if _libs["lib7.dll"].has("free_hl7_meta", "cdecl"):
    free_hl7_meta = _libs["lib7.dll"].get("free_hl7_meta", "cdecl")
    free_hl7_meta.argtypes = [POINTER(hl7_meta_t)]
    free_hl7_meta.restype = None

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 187
if _libs["lib7.dll"].has("find_line_delimiter", "cdecl"):
    find_line_delimiter = _libs["lib7.dll"].get("find_line_delimiter", "cdecl")
    find_line_delimiter.argtypes = [POINTER(FILE)]
    find_line_delimiter.restype = line_delimiter_t

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 216
if _libs["lib7.dll"].has("read_meta", "cdecl"):
    read_meta = _libs["lib7.dll"].get("read_meta", "cdecl")
    read_meta.argtypes = [POINTER(hl7_meta_t), POINTER(FILE)]
    read_meta.restype = c_int

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 225
class struct_hl7_addr_t(Structure):
    pass

struct_hl7_addr_t.__slots__ = [
    'segment',
    'fieldlist',
    'field',
    'comp',
    'subcmp',
    'seg_count',
]
struct_hl7_addr_t._fields_ = [
    ('segment', String),
    ('fieldlist', c_int),
    ('field', c_int),
    ('comp', c_int),
    ('subcmp', c_int),
    ('seg_count', c_int),
]

hl7_addr_t = struct_hl7_addr_t# C:/Users/wus/Projects/hl7parse/build/lib7.h: 225

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 232
class struct_seg_count(Structure):
    pass

struct_seg_count.__slots__ = [
    'length',
    '_allocated',
    'count',
    'segments',
]
struct_seg_count._fields_ = [
    ('length', c_uint),
    ('_allocated', c_uint),
    ('count', POINTER(c_int)),
    ('segments', POINTER(POINTER(c_char))),
]

seg_count_t = struct_seg_count# C:/Users/wus/Projects/hl7parse/build/lib7.h: 232

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 250
if _libs["lib7.dll"].has("create_addr", "cdecl"):
    create_addr = _libs["lib7.dll"].get("create_addr", "cdecl")
    create_addr.argtypes = []
    create_addr.restype = POINTER(hl7_addr_t)

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 260
if _libs["lib7.dll"].has("addr_from_string", "cdecl"):
    addr_from_string = _libs["lib7.dll"].get("addr_from_string", "cdecl")
    addr_from_string.argtypes = [String]
    addr_from_string.restype = POINTER(hl7_addr_t)

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 270
if _libs["lib7.dll"].has("addr_to_string", "cdecl"):
    addr_to_string = _libs["lib7.dll"].get("addr_to_string", "cdecl")
    addr_to_string.argtypes = [POINTER(hl7_addr_t)]
    if sizeof(c_int) == sizeof(c_void_p):
        addr_to_string.restype = ReturnString
    else:
        addr_to_string.restype = String
        addr_to_string.errcheck = ReturnString

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 272
if _libs["lib7.dll"].has("free_addr", "cdecl"):
    free_addr = _libs["lib7.dll"].get("free_addr", "cdecl")
    free_addr.argtypes = [POINTER(hl7_addr_t)]
    free_addr.restype = None

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 279
if _libs["lib7.dll"].has("addr_dump", "cdecl"):
    addr_dump = _libs["lib7.dll"].get("addr_dump", "cdecl")
    addr_dump.argtypes = [POINTER(hl7_addr_t)]
    addr_dump.restype = None

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 289
if _libs["lib7.dll"].has("clone_addr", "cdecl"):
    clone_addr = _libs["lib7.dll"].get("clone_addr", "cdecl")
    clone_addr.argtypes = [POINTER(hl7_addr_t)]
    clone_addr.restype = POINTER(hl7_addr_t)

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 294
if _libs["lib7.dll"].has("create_seg_count", "cdecl"):
    create_seg_count = _libs["lib7.dll"].get("create_seg_count", "cdecl")
    create_seg_count.argtypes = []
    create_seg_count.restype = POINTER(seg_count_t)

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 301
if _libs["lib7.dll"].has("free_seg_count", "cdecl"):
    free_seg_count = _libs["lib7.dll"].get("free_seg_count", "cdecl")
    free_seg_count.argtypes = [POINTER(seg_count_t)]
    free_seg_count.restype = None

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 314
if _libs["lib7.dll"].has("add_seg_count", "cdecl"):
    add_seg_count = _libs["lib7.dll"].get("add_seg_count", "cdecl")
    add_seg_count.argtypes = [String, POINTER(seg_count_t)]
    add_seg_count.restype = c_int

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 325
if _libs["lib7.dll"].has("get_seg_count", "cdecl"):
    get_seg_count = _libs["lib7.dll"].get("get_seg_count", "cdecl")
    get_seg_count.argtypes = [String, POINTER(seg_count_t)]
    get_seg_count.restype = c_int

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 338
if _libs["lib7.dll"].has("base64decode", "cdecl"):
    base64decode = _libs["lib7.dll"].get("base64decode", "cdecl")
    base64decode.argtypes = [String, c_size_t, POINTER(c_ubyte), POINTER(c_size_t)]
    base64decode.restype = c_int

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 339
if _libs["lib7.dll"].has("hl7_64decode", "cdecl"):
    hl7_64decode = _libs["lib7.dll"].get("hl7_64decode", "cdecl")
    hl7_64decode.argtypes = [String, c_size_t, POINTER(c_ubyte), POINTER(c_size_t)]
    hl7_64decode.restype = c_int

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 340
if _libs["lib7.dll"].has("hl7_64decode_fd", "cdecl"):
    hl7_64decode_fd = _libs["lib7.dll"].get("hl7_64decode_fd", "cdecl")
    hl7_64decode_fd.argtypes = [String, c_size_t, POINTER(FILE)]
    hl7_64decode_fd.restype = c_int

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 351
if _libs["lib7.dll"].has("base64encode", "cdecl"):
    base64encode = _libs["lib7.dll"].get("base64encode", "cdecl")
    base64encode.argtypes = [POINTER(None), c_size_t, String, c_size_t]
    base64encode.restype = c_int

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 376
class struct_raw_field_t(Structure):
    pass

struct_raw_field_t.__slots__ = [
    'field',
    'delim',
    'pos',
    'delim_l',
    'length',
]
struct_raw_field_t._fields_ = [
    ('field', POINTER(c_ubyte)),
    ('delim', c_ubyte * int(1000)),
    ('pos', c_uint * int(1000)),
    ('delim_l', c_uint),
    ('length', c_size_t),
]

raw_field_t = struct_raw_field_t# C:/Users/wus/Projects/hl7parse/build/lib7.h: 376

enum_node_type_t = c_int# C:/Users/wus/Projects/hl7parse/build/lib7.h: 389

MESSAGE = 1# C:/Users/wus/Projects/hl7parse/build/lib7.h: 389

SEGMENT = 2# C:/Users/wus/Projects/hl7parse/build/lib7.h: 389

FIELDLIST = 4# C:/Users/wus/Projects/hl7parse/build/lib7.h: 389

FIELD = 8# C:/Users/wus/Projects/hl7parse/build/lib7.h: 389

COMP = 16# C:/Users/wus/Projects/hl7parse/build/lib7.h: 389

SUBCOMP = 32# C:/Users/wus/Projects/hl7parse/build/lib7.h: 389

LEAF = 64# C:/Users/wus/Projects/hl7parse/build/lib7.h: 389

node_type_t = enum_node_type_t# C:/Users/wus/Projects/hl7parse/build/lib7.h: 389

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 391
class struct_node_t(Structure):
    pass

struct_node_t.__slots__ = [
    'type',
    'id',
    'parent',
    'children',
    'num_children',
    '_num_children_allocated',
    'data',
    'length',
    'pos',
]
struct_node_t._fields_ = [
    ('type', c_int),
    ('id', c_int),
    ('parent', POINTER(struct_node_t)),
    ('children', POINTER(POINTER(struct_node_t))),
    ('num_children', c_int),
    ('_num_children_allocated', c_int),
    ('data', POINTER(c_ubyte)),
    ('length', c_size_t),
    ('pos', c_int),
]

node_t = struct_node_t# C:/Users/wus/Projects/hl7parse/build/lib7.h: 405

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 417
class struct_message_t(Structure):
    pass

struct_message_t.__slots__ = [
    'type',
    'id',
    'parent',
    'segments',
    'num_children',
    '_num_children_allocated',
]
struct_message_t._fields_ = [
    ('type', c_int),
    ('id', c_int),
    ('parent', POINTER(struct_node_t)),
    ('segments', POINTER(POINTER(struct_node_t))),
    ('num_children', c_int),
    ('_num_children_allocated', c_int),
]

message_t = struct_message_t# C:/Users/wus/Projects/hl7parse/build/lib7.h: 417

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 422
if _libs["lib7.dll"].has("create_raw_field_t", "cdecl"):
    create_raw_field_t = _libs["lib7.dll"].get("create_raw_field_t", "cdecl")
    create_raw_field_t.argtypes = []
    create_raw_field_t.restype = POINTER(raw_field_t)

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 427
if _libs["lib7.dll"].has("free_raw_field", "cdecl"):
    free_raw_field = _libs["lib7.dll"].get("free_raw_field", "cdecl")
    free_raw_field.argtypes = [POINTER(raw_field_t)]
    free_raw_field.restype = None

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 444
if _libs["lib7.dll"].has("create_node_t", "cdecl"):
    create_node_t = _libs["lib7.dll"].get("create_node_t", "cdecl")
    create_node_t.argtypes = [node_type_t, POINTER(c_ubyte), c_size_t, c_int]
    create_node_t.restype = POINTER(node_t)

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 456
if _libs["lib7.dll"].has("free_node_t", "cdecl"):
    free_node_t = _libs["lib7.dll"].get("free_node_t", "cdecl")
    free_node_t.argtypes = [POINTER(node_t)]
    free_node_t.restype = None

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 469
if _libs["lib7.dll"].has("node_append", "cdecl"):
    node_append = _libs["lib7.dll"].get("node_append", "cdecl")
    node_append.argtypes = [POINTER(POINTER(node_t)), POINTER(node_t)]
    node_append.restype = c_int

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 482
if _libs["lib7.dll"].has("process_node", "cdecl"):
    process_node = _libs["lib7.dll"].get("process_node", "cdecl")
    process_node.argtypes = [POINTER(raw_field_t), POINTER(hl7_meta_t), c_int]
    process_node.restype = POINTER(node_t)

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 489
if _libs["lib7.dll"].has("disply_raw_node", "cdecl"):
    disply_raw_node = _libs["lib7.dll"].get("disply_raw_node", "cdecl")
    disply_raw_node.argtypes = [POINTER(raw_field_t)]
    disply_raw_node.restype = None

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 498
if _libs["lib7.dll"].has("node_in_segment", "cdecl"):
    node_in_segment = _libs["lib7.dll"].get("node_in_segment", "cdecl")
    node_in_segment.argtypes = [POINTER(node_t), POINTER(hl7_addr_t)]
    node_in_segment.restype = POINTER(node_t)

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 508
if _libs["lib7.dll"].has("node_to_string", "cdecl"):
    node_to_string = _libs["lib7.dll"].get("node_to_string", "cdecl")
    node_to_string.argtypes = [POINTER(node_t), POINTER(hl7_meta_t), POINTER(c_int)]
    if sizeof(c_int) == sizeof(c_void_p):
        node_to_string.restype = ReturnString
    else:
        node_to_string.restype = String
        node_to_string.errcheck = ReturnString

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 510
if _libs["lib7.dll"].has("node_type_to_string", "cdecl"):
    node_type_to_string = _libs["lib7.dll"].get("node_type_to_string", "cdecl")
    node_type_to_string.argtypes = [node_type_t]
    node_type_to_string.restype = c_char_p

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 512
if _libs["lib7.dll"].has("create_message_t", "cdecl"):
    create_message_t = _libs["lib7.dll"].get("create_message_t", "cdecl")
    create_message_t.argtypes = []
    create_message_t.restype = POINTER(message_t)

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 513
if _libs["lib7.dll"].has("free_message_t", "cdecl"):
    free_message_t = _libs["lib7.dll"].get("free_message_t", "cdecl")
    free_message_t.argtypes = [POINTER(message_t)]
    free_message_t.restype = None

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 514
if _libs["lib7.dll"].has("message_append", "cdecl"):
    message_append = _libs["lib7.dll"].get("message_append", "cdecl")
    message_append.argtypes = [POINTER(POINTER(message_t)), POINTER(node_t)]
    message_append.restype = c_int

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 519
if _libs["lib7.dll"].has("version_parser", "cdecl"):
    version_parser = _libs["lib7.dll"].get("version_parser", "cdecl")
    version_parser.argtypes = []
    version_parser.restype = c_char_p

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 527
if _libs["lib7.dll"].has("print_error", "cdecl"):
    print_error = _libs["lib7.dll"].get("print_error", "cdecl")
    print_error.argtypes = [c_int, String]
    print_error.restype = None

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 548
if _libs["lib7.dll"].has("extract_substr", "cdecl"):
    extract_substr = _libs["lib7.dll"].get("extract_substr", "cdecl")
    extract_substr.argtypes = [c_int, c_int, POINTER(c_ubyte)]
    extract_substr.restype = POINTER(c_ubyte)

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 583
if _libs["lib7.dll"].has("parse_segment", "cdecl"):
    parse_segment = _libs["lib7.dll"].get("parse_segment", "cdecl")
    parse_segment.argtypes = [POINTER(FILE), POINTER(hl7_meta_t), POINTER(POINTER(node_t)), POINTER(POINTER(c_char))]
    parse_segment.restype = c_int

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 599
if _libs["lib7.dll"].has("hl7_decode", "cdecl"):
    hl7_decode = _libs["lib7.dll"].get("hl7_decode", "cdecl")
    hl7_decode.argtypes = [POINTER(FILE), POINTER(hl7_meta_t), POINTER(POINTER(message_t))]
    hl7_decode.restype = c_int

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 601
if _libs["lib7.dll"].has("decode", "cdecl"):
    decode = _libs["lib7.dll"].get("decode", "cdecl")
    decode.argtypes = [POINTER(FILE), POINTER(hl7_meta_t)]
    decode.restype = POINTER(message_t)

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 608
if _libs["lib7.dll"].has("hl7_open", "cdecl"):
    hl7_open = _libs["lib7.dll"].get("hl7_open", "cdecl")
    hl7_open.argtypes = [String]
    hl7_open.restype = POINTER(FILE)

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 610
if _libs["lib7.dll"].has("hl7_close", "cdecl"):
    hl7_close = _libs["lib7.dll"].get("hl7_close", "cdecl")
    hl7_close.argtypes = [POINTER(FILE)]
    hl7_close.restype = c_int

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 618
if _libs["lib7.dll"].has("version_search", "cdecl"):
    version_search = _libs["lib7.dll"].get("version_search", "cdecl")
    version_search.argtypes = []
    version_search.restype = c_char_p

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 651
class struct_flags_t(Structure):
    pass

struct_flags_t.__slots__ = [
    'verbose',
    'search_term',
    'greedy',
    'output_json',
    'output_xml',
    'output_csv',
    'address',
    'search_term_value',
    'address_value',
    'quiet',
    'decode64',
    'output_file',
    'output_file_value',
    'output_fd',
    'case_insensitive',
]
struct_flags_t._fields_ = [
    ('verbose', c_int),
    ('search_term', c_int),
    ('greedy', c_int),
    ('output_json', c_int),
    ('output_xml', c_int),
    ('output_csv', c_int),
    ('address', c_int),
    ('search_term_value', POINTER(c_ubyte)),
    ('address_value', String),
    ('quiet', c_int),
    ('decode64', c_int),
    ('output_file', c_int),
    ('output_file_value', String),
    ('output_fd', POINTER(FILE)),
    ('case_insensitive', c_int),
]

flags_t = struct_flags_t# C:/Users/wus/Projects/hl7parse/build/lib7.h: 651

enum_search_mode_t = c_int# C:/Users/wus/Projects/hl7parse/build/lib7.h: 669

SEARCH_SUBSTRING = 0# C:/Users/wus/Projects/hl7parse/build/lib7.h: 669

SEARCH_SEGMENT = 1# C:/Users/wus/Projects/hl7parse/build/lib7.h: 669

SEARCH_NODE = 2# C:/Users/wus/Projects/hl7parse/build/lib7.h: 669

search_mode_t = enum_search_mode_t# C:/Users/wus/Projects/hl7parse/build/lib7.h: 669

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 688
class struct_result_item_t(Structure):
    pass

struct_result_item_t.__slots__ = [
    'file',
    'line_num',
    'pos',
    'addr',
    'str',
    'length',
]
struct_result_item_t._fields_ = [
    ('file', String),
    ('line_num', c_int),
    ('pos', c_int),
    ('addr', POINTER(hl7_addr_t)),
    ('str', String),
    ('length', c_int),
]

result_item_t = struct_result_item_t# C:/Users/wus/Projects/hl7parse/build/lib7.h: 688

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 701
class struct_search_res_t(Structure):
    pass

struct_search_res_t.__slots__ = [
    'file',
    'addr',
    'addr_l',
    'greedy',
    'search_term',
    'length',
    'items',
]
struct_search_res_t._fields_ = [
    ('file', String),
    ('addr', POINTER(POINTER(hl7_addr_t))),
    ('addr_l', c_int),
    ('greedy', c_int),
    ('search_term', POINTER(c_ubyte)),
    ('length', c_int),
    ('items', POINTER(POINTER(result_item_t))),
]

search_res_t = struct_search_res_t# C:/Users/wus/Projects/hl7parse/build/lib7.h: 701

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 709
if _libs["lib7.dll"].has("create_search_res", "cdecl"):
    create_search_res = _libs["lib7.dll"].get("create_search_res", "cdecl")
    create_search_res.argtypes = [POINTER(c_ubyte)]
    create_search_res.restype = POINTER(search_res_t)

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 711
if _libs["lib7.dll"].has("free_search_res", "cdecl"):
    free_search_res = _libs["lib7.dll"].get("free_search_res", "cdecl")
    free_search_res.argtypes = [POINTER(search_res_t)]
    free_search_res.restype = None

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 716
if _libs["lib7.dll"].has("append_result", "cdecl"):
    append_result = _libs["lib7.dll"].get("append_result", "cdecl")
    append_result.argtypes = [String, c_int, c_int, POINTER(hl7_addr_t), POINTER(POINTER(search_res_t)), String, c_int]
    append_result.restype = c_int

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 727
if _libs["lib7.dll"].has("parse_address", "cdecl"):
    parse_address = _libs["lib7.dll"].get("parse_address", "cdecl")
    parse_address.argtypes = [String, POINTER(c_int)]
    parse_address.restype = POINTER(POINTER(hl7_addr_t))

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 742
if _libs["lib7.dll"].has("search_file", "cdecl"):
    search_file = _libs["lib7.dll"].get("search_file", "cdecl")
    search_file.argtypes = [String, flags_t]
    search_file.restype = c_int

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 753
if _libs["lib7.dll"].has("search_substring", "cdecl"):
    search_substring = _libs["lib7.dll"].get("search_substring", "cdecl")
    search_substring.argtypes = [POINTER(FILE), POINTER(search_res_t)]
    search_substring.restype = c_int

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 783
if _libs["lib7.dll"].has("search_segment", "cdecl"):
    search_segment = _libs["lib7.dll"].get("search_segment", "cdecl")
    search_segment.argtypes = [POINTER(FILE), POINTER(search_res_t)]
    search_segment.restype = c_int

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 800
if _libs["lib7.dll"].has("search_node", "cdecl"):
    search_node = _libs["lib7.dll"].get("search_node", "cdecl")
    search_node.argtypes = [POINTER(FILE), POINTER(search_res_t)]
    search_node.restype = c_int

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 811
if _libs["lib7.dll"].has("print_json_value", "cdecl"):
    print_json_value = _libs["lib7.dll"].get("print_json_value", "cdecl")
    print_json_value.argtypes = [POINTER(result_item_t), flags_t]
    print_json_value.restype = None

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 832
if _libs["lib7.dll"].has("print_xml_value", "cdecl"):
    print_xml_value = _libs["lib7.dll"].get("print_xml_value", "cdecl")
    print_xml_value.argtypes = [POINTER(result_item_t), flags_t]
    print_xml_value.restype = None

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 841
if _libs["lib7.dll"].has("output_json", "cdecl"):
    output_json = _libs["lib7.dll"].get("output_json", "cdecl")
    output_json.argtypes = [POINTER(result_item_t), flags_t, c_int]
    output_json.restype = None

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 849
if _libs["lib7.dll"].has("output_xml", "cdecl"):
    output_xml = _libs["lib7.dll"].get("output_xml", "cdecl")
    output_xml.argtypes = [POINTER(result_item_t), flags_t]
    output_xml.restype = None

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 857
if _libs["lib7.dll"].has("output_csv", "cdecl"):
    output_csv = _libs["lib7.dll"].get("output_csv", "cdecl")
    output_csv.argtypes = [POINTER(result_item_t), flags_t]
    output_csv.restype = None

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 865
if _libs["lib7.dll"].has("output_string", "cdecl"):
    output_string = _libs["lib7.dll"].get("output_string", "cdecl")
    output_string.argtypes = [POINTER(result_item_t), flags_t]
    output_string.restype = None

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 884
if _libs["lib7.dll"].has("memdup", "cdecl"):
    memdup = _libs["lib7.dll"].get("memdup", "cdecl")
    memdup.argtypes = [POINTER(None), c_size_t]
    memdup.restype = POINTER(c_ubyte)
    memdup.errcheck = lambda v,*a : cast(v, c_void_p)

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 886
if _libs["lib7.dll"].has("dump_structure", "cdecl"):
    dump_structure = _libs["lib7.dll"].get("dump_structure", "cdecl")
    dump_structure.argtypes = [POINTER(message_t)]
    dump_structure.restype = None

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 888
if _libs["lib7.dll"].has("trim", "cdecl"):
    trim = _libs["lib7.dll"].get("trim", "cdecl")
    trim.argtypes = [String]
    if sizeof(c_int) == sizeof(c_void_p):
        trim.restype = ReturnString
    else:
        trim.restype = String
        trim.errcheck = ReturnString

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 903
if _libs["lib7.dll"].has("escape", "cdecl"):
    escape = _libs["lib7.dll"].get("escape", "cdecl")
    escape.argtypes = [String, String, c_char]
    if sizeof(c_int) == sizeof(c_void_p):
        escape.restype = ReturnString
    else:
        escape.restype = String
        escape.errcheck = ReturnString

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 917
if _libs["lib7.dll"].has("node_append_child", "cdecl"):
    node_append_child = _libs["lib7.dll"].get("node_append_child", "cdecl")
    node_append_child.argtypes = [POINTER(node_t), POINTER(node_t)]
    node_append_child.restype = c_int

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 931
if _libs["lib7.dll"].has("node_set_child", "cdecl"):
    node_set_child = _libs["lib7.dll"].get("node_set_child", "cdecl")
    node_set_child.argtypes = [POINTER(node_t), POINTER(node_t), c_int]
    node_set_child.restype = POINTER(node_t)

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 942
if _libs["lib7.dll"].has("node_remove", "cdecl"):
    node_remove = _libs["lib7.dll"].get("node_remove", "cdecl")
    node_remove.argtypes = [POINTER(node_t)]
    node_remove.restype = c_int

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 952
if _libs["lib7.dll"].has("node_remove_child", "cdecl"):
    node_remove_child = _libs["lib7.dll"].get("node_remove_child", "cdecl")
    node_remove_child.argtypes = [POINTER(node_t), POINTER(node_t), c_int]
    node_remove_child.restype = POINTER(node_t)

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 963
if _libs["lib7.dll"].has("node_append_n_create", "cdecl"):
    node_append_n_create = _libs["lib7.dll"].get("node_append_n_create", "cdecl")
    node_append_n_create.argtypes = [POINTER(node_t)]
    node_append_n_create.restype = POINTER(node_t)

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 974
if _libs["lib7.dll"].has("node_pad_children", "cdecl"):
    node_pad_children = _libs["lib7.dll"].get("node_pad_children", "cdecl")
    node_pad_children.argtypes = [POINTER(node_t), c_int]
    node_pad_children.restype = POINTER(node_t)

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 982
if _libs["lib7.dll"].has("node_create_empty", "cdecl"):
    node_create_empty = _libs["lib7.dll"].get("node_create_empty", "cdecl")
    node_create_empty.argtypes = [node_type_t]
    node_create_empty.restype = POINTER(node_t)

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 995
if _libs["lib7.dll"].has("node_set_by_addr", "cdecl"):
    node_set_by_addr = _libs["lib7.dll"].get("node_set_by_addr", "cdecl")
    node_set_by_addr.argtypes = [POINTER(message_t), POINTER(hl7_addr_t), POINTER(c_ubyte)]
    node_set_by_addr.restype = c_int

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 997
if _libs["lib7.dll"].has("node_to_string2", "cdecl"):
    node_to_string2 = _libs["lib7.dll"].get("node_to_string2", "cdecl")
    node_to_string2.argtypes = [POINTER(node_t), POINTER(hl7_meta_t), POINTER(c_int)]
    node_to_string2.restype = POINTER(c_ubyte)

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 25
try:
    ALLOC_NUM_ITEMS = 10
except:
    pass

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 353
try:
    MAX_FIELDS = 1000
except:
    pass

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 868
try:
    SIZE_T_L = '%lld'
except:
    pass

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 875
try:
    MARKER_T = '\\xC3'
except:
    pass

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 876
try:
    MARKER_L = '\\xC0'
except:
    pass

# C:/Users/wus/Projects/hl7parse/build/lib7.h: 877
try:
    MARKER_D = '\\xC4'
except:
    pass

bom_t = struct_bom_t# C:/Users/wus/Projects/hl7parse/build/lib7.h: 85

hl7_meta_t = struct_hl7_meta_t# C:/Users/wus/Projects/hl7parse/build/lib7.h: 137

hl7_addr_t = struct_hl7_addr_t# C:/Users/wus/Projects/hl7parse/build/lib7.h: 225

seg_count = struct_seg_count# C:/Users/wus/Projects/hl7parse/build/lib7.h: 232

raw_field_t = struct_raw_field_t# C:/Users/wus/Projects/hl7parse/build/lib7.h: 376

node_t = struct_node_t# C:/Users/wus/Projects/hl7parse/build/lib7.h: 391

message_t = struct_message_t# C:/Users/wus/Projects/hl7parse/build/lib7.h: 417

flags_t = struct_flags_t# C:/Users/wus/Projects/hl7parse/build/lib7.h: 651

result_item_t = struct_result_item_t# C:/Users/wus/Projects/hl7parse/build/lib7.h: 688

search_res_t = struct_search_res_t# C:/Users/wus/Projects/hl7parse/build/lib7.h: 701

# No inserted files

# No prefix-stripping

#!/usr/bin/env python3
"""
Python interface to lib7

This interface gives read only access to the internal C data structures 
of lib7.so. It's very minimal and might be extended here. All the 
relevant interfaces are exposed on mostly documented in lib7.h

Example usage see at the bottom of this file.

(c) 2021, Simon Wunderlin
"""

#from ctypes import *
#import build.lib7 as lib7

class meta:
    _meta = None

    def __init__(self, meta):
        #print(meta.contents.type)
        self._meta = meta.contents
    
    @property
    def crlf(self): return self._meta.crlf
    
    @property
    def sep_message(self): return self._meta.sep_message

    @property
    def sep_field(self): return self._meta.sep_field

    @property
    def sep_comp(self): return self._meta.sep_comp

    @property
    def sep_rep(self): return self._meta.sep_rep

    @property
    def sep_subcmp(self): return self._meta.sep_subcmp

    @property
    def encoding(self):
        if not self._meta.encoding:
            return ""
        return self._meta.encoding

    @property
    def bom(self):
        if not self._meta.bom or not self._meta.bom.contents or not self._meta.bom.contents.bom:
            return ""
        return self._meta.bom.contents.bom.value

    @property
    def subtype(self): 
        if not self._meta.subtype:
            return ""
        return self._meta.subtype

    @property
    def type(self): 
        if not self._meta.type:
            return ""
        return self._meta.type

    def _separators(self):
        return str(self.sep_message + self.sep_field + self.sep_rep + \
                   self.sep_comp + self.sep_subcmp)

    def __repr__(self):
        return "<meta: sep: " + self._separators() + \
               ", crlf: " + str(self.crlf) + \
               ", type: '" + str(self.type) + "'" + \
               ", subtype: '" + str(self.subtype) + "'" + \
               ", encoding: '" + str(self.encoding) + "'" \
               ", bom: '" + str(self.bom) + "'>"

class node:
    """Interface to all node types"""
    _length = 0
    _meta = None
    _node = None

    def __init__(self, n, meta):
        self._node = n
        self._meta = meta
        self._length = n.contents.num_children
    
    @property
    def type(self):
        """ node type

        use self.type2str() for a more verbos representation"""
        return self._node.contents.type
    
    @property
    def size(self):
        """ data length of the segments contents in bytes"""
        return self._node.contents.length
    
    @property
    def pos(self):
        """position of the start delimiter within the hl7 file"""
        return self._node.contents.pos
    
    @property
    def data(self):
        """raw data as byte array"""
        if self._node.contents.length == 0:
            return b""
        
        data = cast(self._node.contents.data, c_char_p)
        return data.value
    
    @property
    def num_children(self):
        """number of child nodes 

        all nodes except SUBCOMP can have children but may have None (0)."""
        return self._length

    @property
    def children(self):
        """array of child nodes"""
        ret = []
        for s in range(self._node.contents.num_children):
            ret.append(node(self._node.contents.children[s], self._meta))
        return ret
    
    def __repr__(self):
        """make me readable ;) """
        return "<node(" + self.type2str() + ", " + str(self._node.contents.num_children) + ", " + str(self.data) + ")>"
    
    def __str__(self):
        """to string, concatenate all sub elements back into a string """
        _length = pointer(c_int())
        data = node_to_string(self._node, self._meta, _length)
        #print(_length.contents.value)
        if data:
            return ctypes.string_at(data, _length.contents.value).decode("utf-8")

        return data
        

    def type2str(self):
        """return string representation of the node type"""
        types = {
            0: "NONE",
            1: "MESSAGE",
            2: "SEGMENT",
            4: "FIELDLIST",
            8: "FIELD",
            16: "COMP",
            32: "SUBCOMP",
            64: "LEAF"
        }

        return types[self.type]

class hl7:
    file = ""
    meta = None
    _meta = None
    _msg = None
    version = {
        "VERSION_PARSER": version_parser().decode("utf-8"),
        "VERSION_SEARCH": version_search().decode("utf-8")
    }

    def __init__(self, file):
        """ open hl7 file

        this method uses the C parse to parse an HL7 file.

        After successfull parse pass, the file is closed and data is kept in memory.

        The memory is stil allocated and needs to be freed with self.__del__().

        TODO: do this by `del obj` when you are done. It is at this poitn unclear if python will free the memory by itself.
        """
        self.file = file
        fd = hl7_open(file)

        if fd:
            self._meta = init_hl7_meta_t()
            #self._msg = decode(fd, _meta).contents
            msg = pointer(struct_message_t())
            ret = hl7_decode(fd, self._meta, pointer(msg))

            # check if parser was successfull, see lib7.h:parse_segment() 
            # for error codes basically everything unequal to 0 is an error
            if ret != 0:
                errstr = [
                    "", # 0:  ok
                    "failed to allocate memory for line buffer", 
                    "failed to allocate more memory for line buffer", 
                    "EOF was reached unexpectedly", 
                    "maximum delimiters per segment is reached, raise MAX_FIELDS", 
                    "failed to allocate raw_field @see create_raw_field_t()", 
                    "failed to allocate node_t @see create_node_t()", 
                    "failed to process sub field elements @see process_node()", 
                    "failed to append child @see node_append()", 
                    "failed to allocate memory for segment name"
                ]
                hl7_close(fd)
                raise Exception(f"Failed to parse file, error code {ret}, message '{errstr[ret]}', file: '{file}'")

            self.meta = meta(self._meta)
            self._msg = msg.contents
            hl7_close(fd)
        else:
            raise Exception("Failed to open file " + file)
    
    def __del__(self):
        if self._msg != None:
            free_message_t(self._msg)

    @property
    def type(self):
        return self._msg.type
    
    @property
    def num_children(self):
        return self._msg.num_children

    @property
    def children(self):
        return self.segments

    @property
    def segments(self):
        ret = []
        for s in range(self._msg.num_children):
            ret.append(node(self._msg.segments[s], self._meta))
        return ret

    def type2str(self):
        return "MESSAGE"

if __name__ == "__main__":
    #msgs = hl7("data/minimal.hl7")
    import sys, os

    if len(sys.argv) < 2:
        print("usage: pylib7 <filename>")
        sys.exit(1)
    
    msgs = hl7(sys.argv[1])
    print(hl7.version)

    print(msgs.meta)
    print("type: " + str(msgs.type))
    print("num_children: " + str(msgs.num_children))
    #print(msgs.children[0].num_children)
    #print(msgs.children[0].children)
    #print(msgs.segments)

    # Example on how to loov over the whole structure of a parsed hl7 file
    for segment in msgs.segments:
        #print(f"{segment.data.decode('utf-8')}")
        print(str(segment))

        fc = 1
        for fieldset in segment.children:
            # no data here, every fieldset has at least one field
            # for example, PID-1 has never data, data will be found in PID-1.1
            for field in fieldset.children:
                print(f"+ {fc} {field.data.decode('utf-8')}")
                for comp in field.children:
                    print(f"+- {comp.data.decode('utf-8')}")
                    for subcomp in comp.children:
                        print(f"+-- {subcomp.data.decode('utf-8')}")
            fc += 1
