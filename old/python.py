#!/usr/bin/env python3
"""
Python interface to lib7

This interface gives read only access to the internal C data structures 
of lib7.so. It's very minimal and might be extended here. All the 
relevant interfaces are exposed on mostly documented in lib7.h

Example usage see at the bottom of this file.

(c) 2021, Simon Wunderlin
"""

#from ctypes import *
#import build.lib7 as lib7

class meta:
    _meta = None

    def __init__(self, meta):
        #print(meta.contents.type)
        self._meta = meta.contents
    
    @property
    def crlf(self): return self._meta.crlf
    
    @property
    def sep_message(self): return self._meta.sep_message

    @property
    def sep_field(self): return self._meta.sep_field

    @property
    def sep_comp(self): return self._meta.sep_comp

    @property
    def sep_rep(self): return self._meta.sep_rep

    @property
    def sep_subcmp(self): return self._meta.sep_subcmp

    @property
    def encoding(self):
        if not self._meta.encoding:
            return ""
        return self._meta.encoding

    @property
    def bom(self):
        if not self._meta.bom or not self._meta.bom.contents or not self._meta.bom.contents.bom:
            return ""
        return self._meta.bom.contents.bom.value

    @property
    def subtype(self): 
        if not self._meta.subtype:
            return ""
        return self._meta.subtype

    @property
    def type(self): 
        if not self._meta.type:
            return ""
        return self._meta.type

    def _separators(self):
        return str(self.sep_message + self.sep_field + self.sep_rep + \
                   self.sep_comp + self.sep_subcmp)

    def __repr__(self):
        return "<meta: sep: " + self._separators() + \
               ", crlf: " + str(self.crlf) + \
               ", type: '" + str(self.type) + "'" + \
               ", subtype: '" + str(self.subtype) + "'" + \
               ", encoding: '" + str(self.encoding) + "'" \
               ", bom: '" + str(self.bom) + "'>"

class node:
    """Interface to all node types"""
    _length = 0
    _meta = None
    _node = None

    def __init__(self, n, meta):
        self._node = n
        self._meta = meta
        self._length = n.contents.num_children
    
    @property
    def type(self):
        """ node type

        use self.type2str() for a more verbos representation"""
        return self._node.contents.type
    
    @property
    def size(self):
        """ data length of the segments contents in bytes"""
        return self._node.contents.length
    
    @property
    def pos(self):
        """position of the start delimiter within the hl7 file"""
        return self._node.contents.pos
    
    @property
    def data(self):
        """raw data as byte array"""
        if self._node.contents.length == 0:
            return b""
        
        data = cast(self._node.contents.data, c_char_p)
        return data.value
    
    @property
    def num_children(self):
        """number of child nodes 

        all nodes except SUBCOMP can have children but may have None (0)."""
        return self._length

    @property
    def children(self):
        """array of child nodes"""
        ret = []
        for s in range(self._node.contents.num_children):
            ret.append(node(self._node.contents.children[s], self._meta))
        return ret
    
    def __repr__(self):
        """make me readable ;) """
        return "<node(" + self.type2str() + ", " + str(self._node.contents.num_children) + ", " + str(self.data) + ")>"
    
    def __str__(self):
        """to string, concatenate all sub elements back into a string """
        _length = pointer(c_int())
        data = node_to_string(self._node, self._meta, _length)
        #print(_length.contents.value)
        if data:
            return ctypes.string_at(data, _length.contents.value).decode("utf-8")

        return data
        

    def type2str(self):
        """return string representation of the node type"""
        types = {
            0: "NONE",
            1: "MESSAGE",
            2: "SEGMENT",
            4: "FIELDLIST",
            8: "FIELD",
            16: "COMP",
            32: "SUBCOMP",
            64: "LEAF"
        }

        return types[self.type]

class hl7:
    file = ""
    meta = None
    _meta = None
    _msg = None
    version = {
        "VERSION_PARSER": version_parser().decode("utf-8"),
        "VERSION_SEARCH": version_search().decode("utf-8")
    }

    def __init__(self, file):
        """ open hl7 file

        this method uses the C parse to parse an HL7 file.

        After successfull parse pass, the file is closed and data is kept in memory.

        The memory is stil allocated and needs to be freed with self.__del__().

        TODO: do this by `del obj` when you are done. It is at this poitn unclear if python will free the memory by itself.
        """
        self.file = file
        fd = hl7_open(file)

        if fd:
            self._meta = init_hl7_meta_t()
            #self._msg = decode(fd, _meta).contents
            msg = pointer(struct_message_t())
            ret = hl7_decode(fd, self._meta, pointer(msg))

            # check if parser was successfull, see lib7.h:parse_segment() 
            # for error codes basically everything unequal to 0 is an error
            if ret != 0:
                errstr = [
                    "", # 0:  ok
                    "failed to allocate memory for line buffer", 
                    "failed to allocate more memory for line buffer", 
                    "EOF was reached unexpectedly", 
                    "maximum delimiters per segment is reached, raise MAX_FIELDS", 
                    "failed to allocate raw_field @see create_raw_field_t()", 
                    "failed to allocate node_t @see create_node_t()", 
                    "failed to process sub field elements @see process_node()", 
                    "failed to append child @see node_append()", 
                    "failed to allocate memory for segment name"
                ]
                hl7_close(fd)
                raise Exception(f"Failed to parse file, error code {ret}, message '{errstr[ret]}', file: '{file}'")

            self.meta = meta(self._meta)
            self._msg = msg.contents
            hl7_close(fd)
        else:
            raise Exception("Failed to open file " + file)
    
    def __del__(self):
        if self._msg != None:
            free_message_t(self._msg)

    @property
    def type(self):
        return self._msg.type
    
    @property
    def num_children(self):
        return self._msg.num_children

    @property
    def children(self):
        return self.segments

    @property
    def segments(self):
        ret = []
        for s in range(self._msg.num_children):
            ret.append(node(self._msg.segments[s], self._meta))
        return ret

    def type2str(self):
        return "MESSAGE"

if __name__ == "__main__":
    #msgs = hl7("data/minimal.hl7")
    import sys, os

    if len(sys.argv) < 2:
        print("usage: pylib7 <filename>")
        sys.exit(1)
    
    msgs = hl7(sys.argv[1])
    print(hl7.version)

    print(msgs.meta)
    print("type: " + str(msgs.type))
    print("num_children: " + str(msgs.num_children))
    #print(msgs.children[0].num_children)
    #print(msgs.children[0].children)
    #print(msgs.segments)

    # Example on how to loov over the whole structure of a parsed hl7 file
    for segment in msgs.segments:
        #print(f"{segment.data.decode('utf-8')}")
        print(str(segment))

        fc = 1
        for fieldset in segment.children:
            # no data here, every fieldset has at least one field
            # for example, PID-1 has never data, data will be found in PID-1.1
            for field in fieldset.children:
                print(f"+ {fc} {field.data.decode('utf-8')}")
                for comp in field.children:
                    print(f"+- {comp.data.decode('utf-8')}")
                    for subcomp in comp.children:
                        print(f"+-- {subcomp.data.decode('utf-8')}")
            fc += 1
