.PHONY: tree parsercpp
CC = gcc
CFLAGS  += -Wall -I../include
LDFLAGS += 
SOURCES := ini.c logging.c strndup.c bom.c meta.c address.c base64.c node.c decode.c search.c util.c buffer.c node_util.c encode.c
H_TMP1  := $(SOURCES:logging.c=)
H_TMP2  := $(H_TMP1:strndup.c=)
HEADERS := $(addprefix ../include/,$(H_TMP2:.c=.h))
SONAME  := lib7.dll
RC_FILE_PARSER := ../resources/parser.res
RC_FILE_SEARCH := ../resources/search.res
RC_FILE_PDF    := ../resources/pdf.res

LDGUI :=
LDCMD := -D__USE_MINGW_ANSI_STDIO=1

ifneq ($(OS),Windows_NT)
	SOURCES := $(subst strndup.c,,$(SOURCES))
	SONAME  := lib7.so
	RC_FILE_PARSER := 
	RC_FILE_SEARCH :=
	RC_FILE_PDF    :=
else
	# MinGW has some odd warnings when using printf with hex formats, disable them
	#CFLAGS += -fPIC 
	SOURCES += getline.c

	# don't show a CMD window
	ifdef PRODUCTION
		LDGUI += -mwindows 
		LDGUI += -Wl,-subsystem,windows
	endif
endif

UNAME_S := $(shell uname -s)
ifeq ($(UNAME_S),Linux)
	CFLAGS += -fPIC
endif
ifneq ($(UNAME_S),Darwin)
	#CFLAGS += -pie
endif

OBJECTS := $(SOURCES:.c=.o)

ifdef PRODUCTION
	CFLAGS += -O2 -DPRODUCTION=1
	# -O2 prevents console output to work on mingw
	# FIXED: this problem has been fixed with `pacman -Suy`, was a bug in the 
	#        current version's terminal
	#CFLAGS += -DPRODUCTION=1
else
	CFLAGS += -g
endif

# default target
all: usage lib7 7parse 7search 7pdf

usage:
	$(MAKE) -C ../ version_header
	cd .. && ./bin/usage.sh

glob:
	$(CC) $(CFLAGS) $(LDCMD) -I/usr/include -L/usr/lib -DGLOB_MAIN -o ../build/glob glob_files.c $(LDFLAGS)

## test address library
address:
	$(CC) $(CFLAGS) $(LDCMD) -DADDR_MAIN -o ../build/address address.c $(RC_FILE_PARSER) $(LDFLAGS)

# compile a standalone parser application
7pdf: $(OBJECTS) 7pdf.o
	$(CC) $(CFLAGS) -o ../build/7pdf $(OBJECTS) 7pdf.o $(RC_FILE_PDF) $(LDFLAGS) $(LDGUI)

# compile a standalone parser application
7parse: $(OBJECTS) 7parse.o
	$(CC) $(CFLAGS) $(LDCMD) -o ../build/7parse $(OBJECTS) 7parse.o $(RC_FILE_PARSER) $(LDFLAGS)

7search: $(OBJECTS) 7search.o
	$(CC) $(CFLAGS) $(LDCMD) -o ../build/7search $(OBJECTS) 7search.o $(RC_FILE_SEARCH) $(LDFLAGS)

7compose: $(OBJECTS)
	$(CC) $(CFLAGS) $(LDCMD) -o ../build/7compose 7compose.c $(OBJECTS) $(RC_FILE_SEARCH) $(LDFLAGS)

lib7: lib7.h $(SONAME)

# compile parser library, see .dll and .so version depending on platform
lib7.h:

	# all includes
	grep -h '^#include <' $(HEADERS) | sort | uniq  > ../build/lib7_tmp.h
	echo "" >> ../build/lib7.h;

	# concatenate header files
		#echo "// $${f//..\/include\//} ///////////////////////////////////////" >> ../build/lib7_tmp.h; 
	for f in $(HEADERS); do \
		echo "" >> ../build/lib7.h; \
		echo "" >> ../build/lib7_tmp.h; \
		cat $$f | egrep -v '^#include|#pragma once' >> ../build/lib7_tmp.h; \
		echo "" >> ../build/lib7.h; \
		echo "" >> ../build/lib7.h; \
	done

	# cleanup
	sed -i -e '/^#ifdef __cplusplus/{N;N;d;}' ../build/lib7_tmp.h
	sed -i -e '/^#ifndef .*H/d; /^#define .*_H/d; /^#endif *\/\/.*/d; ' ../build/lib7_tmp.h
	sed -i -e '/#include *"getline.h"/d;' ../build/lib7_tmp.h
	sed -i -e '/^$$/N;/^\n$$/D;' ../build/lib7_tmp.h

	# add guards
	echo "#ifndef LIB7_H" > ../build/lib7.h
	echo "#define LIB7_H" >> ../build/lib7.h
	echo "" >> ../build/lib7.h
	cat ../build/lib7_tmp.h >> ../build/lib7.h
	echo "#endif // LIB7_H" >> ../build/lib7.h
	rm ../build/lib7_tmp.h

# library for the world
lib7.so: $(OBJECTS)  7parse.o
	$(CC) $(CFLAGS) -fPIC -shared \
		-o ../build/lib7.so \
		$(OBJECTS) 7parse.o $(LDFLAGS) 

# library for windows (a small world)
lib7.dll: $(OBJECTS) 7parse.o
	$(CC) $(CFLAGS) -shared \
		-o ../build/lib7.dll \
		$(OBJECTS) 7parse.o $(RC_FILE_PARSER) $(LDFLAGS)

# all objects
%.o : %.c
	$(CC) -c $(CFLAGS) $< -o $@

clean:
	rm -rf *.o || true
	rm -rf ../build/7pdf || true
	rm -rf ../build/7search || true
	rm -rf ../build/7parse || true
	rm -rf ../build/address || true
	rm -rf ../build/7compose || true
	rm -rf ../build/lib7.so || true
	rm -rf ../build/lib7.dll || true
	rm -rf ../build/lib7.h || true

# DEPRICATED: use $(MAKE) -C ../python all
pylib7:
	cd ../build && python3 ../language_bindings/ctypesgen/run.py -l $(SONAME) -o lib7_tmp.py lib7.h
	cat ../include/python_header.py ../include/version.py ../build/lib7_tmp.py python.py > ../build/pylib7.py
	rm ../build/lib7_tmp.py

tostr_test: $(OBJECTS) tostr_test.o
	$(CC) $(CFLAGS) $(LDCMD) -o ../build/tostr_test $(OBJECTS) tostr_test.o $(RC_FILE_PARSER) $(LDFLAGS)

test_encode: buffer.o encode.o test_encode.o
	$(CC) $(CFLAGS) $(LDCMD) -o ../build/test_encode $(OBJECTS) test_encode.o $(RC_FILE_PARSER) $(LDFLAGS)

# run valgrind, this is for linux, maybe osx but no easy way to run on windows
memtest_7parse: 7parse
	valkyrie --track-origins=yes ../build/7parse ../MDM_TDMS_01.hl7

memtest_7search: 7search
	#valkyrie --track-origins=yes ../build/7search -a "MSH-9.1" ../data/minimal_subnodes.hl7
	valkyrie --track-origins=yes ../build/7search -a "ADD-1" -s if -i -o=xml ../data/oru/R01.hl7

memtest_7pdf: 7pdf
	valkyrie --track-origins=yes ../build/7pdf ../MDM_TDMS_01.hl7

memtest_7compose: 7compose
	valkyrie --track-origins=yes ../build/7compose
