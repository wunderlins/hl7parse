#include <stdio.h>
#include "meta.h"
#include "logging.h"
#include "encode.h"

// tests
const int loglevel = LEVEL_DEBUG;

int main() {

    hl7_meta_t *meta = init_hl7_meta_t();
    int ret = 0;
    unsigned char *str_in = (unsigned char*) "field |, comp ^, subcmp &, escape \\, rep: ~, message \r";

    // escape
    unsigned char *str_out = NULL;
    int l = strlen((char *) str_in) + 1;
    int lout = 0;
    printf("input length: %d\n", l);
    ret = hl7_escape(str_in, &str_out, l, &lout, meta);
    if (ret != 0) {
        printf("Failed to allocate memory in buf_append\n");
    }
    printf("l: %d, '%s'\n", lout, str_out);

    // unescape
    unsigned char *str_out2 = NULL;
    int lout2 = 0;
    ret = hl7_unescape(str_out, &str_out2, lout, &lout2, meta);
    if (ret != 0) {
        printf("Failed to allocate memory in buf_append\n");
    }
    printf("l: %d, '%s'\n", lout2, str_out2);

    return 0;
}
