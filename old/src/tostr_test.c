#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "logging.h"
#include "bom.h"
#include "meta.h"
#include "node.h"
#include "decode.h"
#include "util.h"
#include "node_util.h"

const int loglevel = LEVEL_INFO;

int main(int argc, char *argv[], char *envp[]) {
    // log argc
    if (loglevel >= LEVEL_DEBUG) {
        log_debug("argc: %d", argc);

        // log argc
        for (int i=0; i<argc; i++) {
            log_debug("argv[%d]: '%s'", i, argv[i]);
        }
        
        if (loglevel >= LEVEL_TRACE) {
            // log envp
            log_trace("== Environment Variables passed into process (*envp[])");
            for (char **env = envp; *env != 0; env++) {
                char *e = *env;
                // find first '='
                char *p = strchr(e, '=');
                int equal = p - e + 1;
                char key[200] = {0};
                memcpy(key, e, equal-1);
                key[equal-1] = 0;
                log_trace("[%s]='%s'", key, e+equal);
            }
            log_trace("== end (*envp[])");
        }
    }

    int ret = 0;

    // check input arguments
	if (argc != 2) {
		printf("Usage: parse <file name>\n");
		return 1;
	}
	
    // HL7 File descriptor
	FILE *fd;
	if ((fd = fopen(argv[1], "rb")) == NULL) {
		print_error(errno, argv[1]);
		return 1;
	}

    // initialize separator data structure
    hl7_meta_t* hl7_meta = init_hl7_meta_t();

    // check if this file has a bom;
    // this will move df at the first byte after the BOM
    hl7_meta->bom = detect_bom(fd); 

    // parse hl7 file
    message_t *message = create_message_t(hl7_meta);
    ret = hl7_decode(fd, &message);

    if (ret != 0) {
        fprintf(stderr, "We failed to parse message at line %d, exiting.\n", message->num_children);
    }
    /*
    char* meta_string = hl7_meta_string(hl7_meta);
    log_info("%s", meta_string);
    free(meta_string);
    */
    //dump_structure(message);
    unsigned char *str = message_to_string(message);
    printf("%s", (char*) str);
    free(str);

    // cleanup
    if (message != NULL)
        free_message_t(message);
    fclose(fd);
    free_hl7_meta(hl7_meta);
    
    return ret;
}