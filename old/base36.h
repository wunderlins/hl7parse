#define ENCODE_TABLE_LENGTH 36
#define ENCODE_DATA_LENGTH  (36*3*2)
#define ENCODE_LOOKUP(NAME) char NAME[ENCODE_TABLE_LENGTH] = { \
    'A', /* 0, ascii -65 */ \
    'B', \
    'C', \
    'D', \
    'E', \
    'F', \
    'G', \
    'H', \
    'I', \
    'J', \
    'K', /* 10 */ \
    'L', \
    'M', \
    'N', \
    'O', \
    'P', \
    'Q', \
    'R', \
    'S', \
    'T', \
    'U', /* 20 */ \
    'V', \
    'W', \
    'X', \
    'Y', \
    'Z', /* 25 */ \
    '0', /* 26, ascii -48+26 */ \
    '1', \
    '2', \
    '3', \
    '4', /* 30 */ \
    '5', \
    '6', \
    '7', \
    '8', \
    '9'  /* 35 */ \
}

#define ENCODE_DATA(NAME) unsigned char NAME[ENCODE_DATA_LENGTH] = {0}

// inline function to calculate the index of a segment
#define ENCODE_SEG(SEG, POS) (((SEG[POS] > 64) ? SEG[POS]-65 : SEG[POS]-22) + ENCODE_TABLE_LENGTH * (POS))
#define ENCODE(SEG) (ENCODE_SEG(SEG, 0) + ENCODE_SEG(SEG, 1) + ENCODE_SEG(SEG, 2))