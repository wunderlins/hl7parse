#include <stdio.h>
#include "base36.h"

int main() {
    ENCODE_LOOKUP(lookup);
    ENCODE_DATA(segment_ix);

    char seg[3];
    unsigned char ix;

    seg[0] = 'M'; seg[1] = 'S'; seg[2] = 'H';
    ix = ENCODE(seg);
    segment_ix[ix]++;
    printf("Segment: %s, ix: %d, count: %d\n", seg, ix, segment_ix[ix]);

    seg[0] = 'Y'; seg[1] = 'Y'; seg[2] = 'Y';
    ix = ENCODE(seg);
    segment_ix[ix]++;
    printf("Segment: %s, ix: %d, count: %d\n", seg, ix, segment_ix[ix]);

    seg[0] = 'Z'; seg[1] = 'Z'; seg[2] = 'Z';
    ix = ENCODE(seg);
    segment_ix[ix]++;
    printf("Segment: %s, ix: %d, count: %d\n", seg, ix, segment_ix[ix]);

    seg[0] = 'Z'; seg[1] = 'Z'; seg[2] = '0';
    ix = ENCODE(seg);
    segment_ix[ix]++;
    printf("Segment: %s, ix: %d, count: %d\n", seg, ix, segment_ix[ix]);

    seg[0] = '0'; seg[1] = 'Z'; seg[2] = 'Z';
    ix = ENCODE(seg);
    segment_ix[ix]++;
    printf("Segment: %s, ix: %d, count: %d\n", seg, ix, segment_ix[ix]);

    seg[0] = '0'; seg[1] = '0'; seg[2] = '0';
    ix = ENCODE(seg);
    segment_ix[ix]++;
    printf("Segment: %s, ix: %d, count: %d\n", seg, ix, segment_ix[ix]);

    seg[0] = '9'; seg[1] = '9'; seg[2] = '9';
    ix = ENCODE(seg);
    segment_ix[ix]++;
    segment_ix[ix]++;
    printf("Segment: %s, ix: %d, count: %d\n", seg, ix, segment_ix[ix]);

    printf("MSH Segments? count: %d\n", segment_ix[ENCODE("MSH")]);


    printf("encode: %d %d %d\n", ENCODE_SEG(seg, 0), ENCODE_SEG(seg, 1), ENCODE_SEG(seg, 2));
    return 0;
}
