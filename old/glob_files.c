#include <stdio.h>
#include <unistd.h>
#include <glob.h>
#include "glob_files.h"

#define GLOB_MAIN

#ifdef GLOB_MAIN
int main(int argc, char **argv) {

    glob_t globbuf;

    globbuf.gl_offs = 2;
    glob("*.c", GLOB_DOOFFS, NULL, &globbuf);
    glob("../src/*.c", GLOB_DOOFFS | GLOB_APPEND, NULL, &globbuf);
    globbuf.gl_pathv[0] = "ls";
    globbuf.gl_pathv[1] = "-l";
    execvp("ls", &globbuf.gl_pathv[0]);

    return 0;
}
#endif