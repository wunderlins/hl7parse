
unsigned char msh_minimal_cr_hl7[] = {
  0x4d, 0x53, 0x48, 0x7c, 0x7c, 0x0d
};
unsigned int msh_minimal_cr_hl7_len = 6;
testfile_t *f;
f->file = msh_minimal_cr_hl7; files->length = msh_minimal_cr_hl7_len;
files[len++] = f;
// ---------------------------
unsigned char msh_minimal_crlf_hl7[] = {
  0x4d, 0x53, 0x48, 0x7c, 0x7c, 0x0d, 0x0a
};
unsigned int msh_minimal_crlf_hl7_len = 7;
files[len].file = msh_minimal_crlf_hl7; files[len++].length = msh_minimal_crlf_hl7_len;
// ---------------------------
unsigned char msh_minimal_hl7[] = {
  0x4d, 0x53, 0x48, 0x7c, 0x7c
};
unsigned int msh_minimal_hl7_len = 5;
files[len].file = msh_minimal_hl7; files[len++].length = msh_minimal_hl7_len;
// ---------------------------
unsigned char msh_minimal_lf_hl7[] = {
  0x4d, 0x53, 0x48, 0x7c, 0x7c, 0x0a
};
unsigned int msh_minimal_lf_hl7_len = 6;
files[len].file = msh_minimal_lf_hl7; files[len++].length = msh_minimal_lf_hl7_len;
// ---------------------------
