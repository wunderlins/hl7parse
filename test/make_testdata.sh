#!/usr/bin/env bash

cd data;

#cat << EOF > data.h
#typedef struct testfile_t {
#  unsigned char *file;
#  int length;
#} testfile_t;
#
#int len = 0;
#testfile_t files[100];
#
#EOF

echo "" > data.h
i=0;
for f in `ls *.hl7`; do
    file=`echo "$f" | sed -e 's/\./_/'`
    xxd -i $f >> data.h
    echo "files[len].file = $file; files[len++].length = ${file}_len;"  >> data.h
    i=`expr $i + 1`
    echo "// ---------------------------"  >> data.h
done
cd ..