#include <stdio.h>

typedef struct testfile_t {
  unsigned char *file;
  int length;
} testfile_t;

int len = 0;
testfile_t files[100] = {0};

int main(int argc, char **argv) {
	#include "data/data.h"
	
	// loop over test files
	for (int i=0; i < len; i++) {
		printf("Testing file: %d, %s\n", files[i].length, files[i].file);
	}
	
	return 0;
}