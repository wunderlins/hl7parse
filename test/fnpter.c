#include <stdio.h>

void some_boring_fn( void (*cb)(char * s) ) {
    char *str = "abcdef";
    cb(str);
}

void mycb(char *str) {
    printf("mycb: %s\n", str);
}

int main() {
    some_boring_fn(mycb);
    return 0;
}