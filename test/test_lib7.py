#!/usr/bin/env python3
import os, sys

# find library
sys.path.insert(0, "../build")

from pylib7 import *

## open an hl7 file
file = "../data/minimal_pid_CR_bas64_LF.hl7"

"""
fd = hl7_open(file)
hl7_meta = init_hl7_meta_t()
message = decode(fd, hl7_meta)

#print(dir(message))
print(message.__getattribute__)
"""

#for i in range(1, 1000):
#	msg = hl7(file)
	
msg = hl7(file)

print(msg)
for s in msg.segments:
	print(s)