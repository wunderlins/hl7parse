# Releases

## Viewer and command utilities

Windows. OSX and Linxu releases for hl7 utilities:

- `linux-*.tgz`
- `win64-*.zip`
- `OSX-*.dmg`

Releases with `-SNAPSHOT` in the name are unstable development builds.

## Python library

Python `lib7` releases can be found as setup_tools source package under

- `lib7-*.tar.gz`

Releases containing `.devN` in the name are unstable development snapshots.