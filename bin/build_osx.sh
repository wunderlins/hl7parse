#!/usr/bin/env bash

# set paths for Qt5
export QT_DIR=/opt/Qt/5.15.2/clang_64/lib/cmake/Qt5
export QT_BIN="$QT_DIR/../../../bin"
export PATH="$QT_BIN:$PATH"

# make sure on OSX we use dpi scaling
export QT_ENABLE_HIGHDPI_SCALING=1

cmake -DWITH_GUI:Bool=ON \
      -DWITH_DOC:Bool=OFF \
      -DWITH_PYTHON:Bool=ON \
      -DWITH_CMD:Bool=ON \
      -DCMAKE_BUILD_TYPE:String=Release \
      $(realpath ..)

make -j4

[ $? -ne 0 ] && exit 1

../bin/deploy-osx2.sh