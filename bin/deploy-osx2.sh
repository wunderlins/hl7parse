#!/usr/bin/env bash

# configuration, set your QT directory
export QT_DIR=/opt/Qt/5.15.2/clang_64/bin

# make sure we are in the build directory
[ -d ../build/viewer ] || { echo "Must be run in the build directory"; exit 1; };
export PATH=$QT_DIR:$PATH
BIN_DIR=7view.app/Contents/MacOS

# change linker path
cd viewer
cp ../lib7/lib7.dylib $BIN_DIR/
#cp ../build/lib7.so $BIN_DIR/
#cp ../build/lib7.*.so $BIN_DIR/
cp ../../generated/lib7.h $BIN_DIR/
cp ../7search $BIN_DIR/
cp ../7parse $BIN_DIR/
cp ../7pdf $BIN_DIR/
cp ../../LICENSE.txt $BIN_DIR/
cp ../../generated/info.plist $BIN_DIR/../
mkdir -p $BIN_DIR/../Resources
cp ../../resources/logo2.icns $BIN_DIR/../Resources/
#cp ../build/7pdf.ini $BIN_DIR/
#cp ../dialog/dialog.app/Contents/MacOS/dialog $BIN_DIR/
install_name_tool -change "@rpath/lib7.dylib" "@executable_path/lib7.dylib" $BIN_DIR/7view
VERSION=$(grep 'VERSION_VIEWER =' ../../generated/version_viewer.h | cut -f2 -d'=' | sed -e 's/^[^"]"//; s/".*//')

# create dmg
$QT_DIR/macdeployqt 7view.app -always-overwrite -dmg
cp 7view.dmg ../../release/7view-osx-$VERSION.dmg
cd ..
