#!/usr/bin/env bash

# create a version header file
#
# usage: $0 VARIABLE_NAME VERSION_STRING header_file (without .h)

file=$3.h
pragma=${1}_H

echo "#ifndef $pragma" > $file
echo "#define $pragma" >> $file
echo "" >> $file
echo "const char *$1 = \"$2\";" >> $file
echo "" >> $file
echo "#endif // $pragma" >> $file
