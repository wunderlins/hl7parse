#!/usr/bin/env bash
f=TODO.md
echo "# TODO" > $f
echo "" >> $f
egrep -n "WTF|TODO|FIXME" *.c *.h | \
    sed -e 's,:[\t ]* //,,; s,:[\t ]*\*,,; s,^,- ,' \
    >> $f