#!/usr/bin/env bash

function create_header() {

    #OUTFILE=include/search_usage.h
    FILE_NAME=$1
    EXE_NAME=$2
    OUTFILE=include/${FILE_NAME}.h
    TAG=$(echo "$FILE_NAME" | tr a-z A-Z)

    echo -e "#ifndef ${TAG}_H\n#define ${TAG}_H\n" > $OUTFILE
    echo "const char *${FILE_NAME} = " >> $OUTFILE
    sed -e 's/$0/'$EXE_NAME'/g; s/\\/\\\\/g; s/"/\\"/g; s/\t/\\t/g; s/^/"/; s/$/\\n"/' \
        src/7${FILE_NAME}.txt >> $OUTFILE
    echo ";" >> $OUTFILE
    echo "" >> $OUTFILE
    echo "#endif // ${TAG}_H" >> $OUTFILE
}

create_header search_usage 7search
create_header pdf_usage 7pdf