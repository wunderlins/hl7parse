#!/usr/bin/env bash

# find dependencies
#libraries=$(LD_LIBRARY_PATH=build ldd build/7view | grep -v linux-gnu | cut -d' ' -f3)
#for f in $libraries; do
#	echo "$f"
#	ldd "$f" | grep -v linux-gnu | cut -d' ' -f3 | grep ^/
#done | sort | uniq > /tmp/libs.txt

#cp `cat /tmp/libs.txt` build/

cd build
rm -rf DistributionKit | true
rm -rf 7util | true
/snap/cqtdeployer/current/cqtdeployer.sh -bin 7view
cp 7search DistributionKit/bin
cp 7parse DistributionKit/bin
cp dialog DistributionKit/bin
cp lib7.*.so DistributionKit/lib
cp lib7.so DistributionKit/lib
mkdir -p DistributionKit/include
cp lib7.h DistributionKit/include
cp ../LICENSE DistributionKit
mv DistributionKit 7util
rm -rf ../release/7util.tgz | true
cd 7util
mv 7view.sh 7view
cp 7view dialog
sed -i 's,/bin/7view,/bin/dialog,' dialog
chmod 700 7view dialog bin/7parse bin/7search bin/7view bin/dialog
cp ../../resources/logo2.png .
tar czf ../../release/linux-SNAPSHOT.tgz *
cd ../..
