@echo off

:: set your path to the desired ming installation
set MGW_BIN=C:/msys64/mingw64/bin

:: make sure the right Qt installation is used, 
:: explicitly set the Qt directory!
::set QT_DIR=C:/Qt/5.12.11/mingw73_64/lib/cmake/Qt5
set QT_DIR=C:/Qt/5.15.2/mingw81_64/lib/cmake/Qt5
::set QT_DIR=C:/Qt/Static/5.12.10_mingw64_static_release/lib/cmake/Qt5

set QT_BIN=%QT_DIR%/../../../bin

:: add it at the beginning of $PATH
set PATH=%MGW_BIN%;%QT_BIN%;%PATH%

:: force explicit executables, this will most likely fail, you'll have to set some other stuff too.
:: cmake.exe -LA -DWITH_GUI=ON -DCMAKE_BUILD_TYPE=Release -DCMAKE_CXX_COMPILER=g++.exe -DCMAKE_C_COMPILER=gcc.exe -DCMAKE_RC_COMPILER=windres.exe -G "MinGW Makefiles" ..

:: if rc generation fails due to mangled paths, try to worce cmake to be less clever by directly using windres.exe
::cmake.exe -DWITH_GUI=ON -DCMAKE_BUILD_TYPE=Release -DCMAKE_RC_COMPILER=windres.exe -G "MinGW Makefiles" ..

:: optimal build command
::cmake.exe -DWITH_GUI:Bool=ON -DWITH_DOC:Bool=OFF -DWITH_PYTHON:Bool=ON -DWITH_CMD:Bool=ON -DCMAKE_BUILD_TYPE:String=Release -G "MinGW Makefiles" ..
:: cmake -S . -B build -DWITH_GUI:Bool=ON -DWITH_DOC:Bool=OFF -DWITH_PYTHON:Bool=OFF -DWITH_CMD:Bool=ON -DCMAKE_BUILD_TYPE:String=Release -G "MinGW Makefiles"
cmake -S . -B build -DWITH_GUI:Bool=ON -DWITH_DOC:Bool=OFF -DWITH_PYTHON:Bool=OFF -DWITH_CMD:Bool=ON -DCMAKE_BUILD_TYPE:String=Release -G Ninja
IF NOT ERRORLEVEL 0 goto end

:: compile
:: mingw32-make.exe -j 12
:: mingw32-make.exe -j4 all package
:: mingw32-make.exe all package
cmake --build build --target all
IF NOT ERRORLEVEL 0 goto end

:: create zip and nsis installer
::cpack -G NSIS64
cmake --build build --target package 
IF NOT ERRORLEVEL 0 goto end

:: find Qt Dependencies dependencies
:: windeployqt --dir qt_deps --no-quick-import --no-system-d3d-compiler --no-angle --no-opengl-sw "viewer/7view.exe"

:end