#!/usr/bin/env sh

# prepare app image directory
rm -rf AppDir >/dev/null 2>&1 || true
make install DESTDIR=AppDir

# fix icon path in desktop file
#cp $PWD/../resources/7view.desktop AppDir/usr/share/applications/7view.desktop
sed -i 's,^Icon=.*/,Icon=,' AppDir/usr/share/applications/7view.desktop
sed -i 's,\.png,,' AppDir/usr/share/applications/7view.desktop
sed -i '/Path.*/ d' AppDir/usr/share/applications/7view.desktop

# now, build AppImage using linuxdeploy and linuxdeploy-plugin-qt
# download linuxdeploy and its Qt plugin
[ ! -f linuxdeploy-x86_64.AppImage ] && wget https://github.com/linuxdeploy/linuxdeploy/releases/download/continuous/linuxdeploy-x86_64.AppImage
[ ! -f linuxdeploy-plugin-qt-x86_64.AppImage ] && wget https://github.com/linuxdeploy/linuxdeploy-plugin-qt/releases/download/continuous/linuxdeploy-plugin-qt-x86_64.AppImage

# make them executable
chmod +x linuxdeploy*.AppImage

# build qt app image
export LD_LIBRARY_PATH=$PWD/AppDir/usr/lib/
./linuxdeploy-x86_64.AppImage -v0 --appdir AppDir/ --desktop-file=$PWD/../resources/7view.desktop --icon-file=$PWD/../resources/logo2.png --executable=viewer/7view --plugin qt --output appimage
