set PROMPT=deploy-windows$G 

@echo off
set FILE=win64-SNAPSHOT
set DST=release\%FILE%
echo ================================================================^

RUN FROM MAIN PROJECT DIRECTORY^

release should be found in %DST%^

================================================================

@echo on

: DESKTOP-0S2U6OL
set QTDIR=C:\Qt5\Qt5.12.4\5.12.4\mingw73_64\bin
set QTTOOLS_DIR=C:\Qt5\Qt5.12.4\Tools\mingw730_64\bin
set EXE=build\7view.exe

: VD
set QTDIR=C:\Qt\5.15.0\mingw81_64\bin
set QTTOOLS_DIR=C:\Qt\Tools\mingw810_64\bin
set EXE=build\7view.exe

set PATH=%QTDIR%;%QTTOOLS_DIR%;%PATH%

REM set oldpwd=%cd%
REM qtenv2.bat
REM cd "%oldpwd%"

REM windeployqt --dir "%DST%" --no-plugins --no-quick-import --no-translations --no-system-d3d-compiler --no-compiler-runtime --no-angle --no-opengl-sw  "%EXE%"
windeployqt --dir "%DST%" --no-quick-import --no-system-d3d-compiler --no-angle --no-opengl-sw "%EXE%"

@echo on
copy "%EXE%" "%DST%"
copy build\lib7.dll "%DST%"
copy build\7search.exe "%DST%"
copy build\7parse.exe "%DST%"
copy build\7pdf.exe "%DST%"
copy build\7pdf.ini "%DST%"
copy build\dialog.exe "%DST%"
copy build\lib7.h "%DST%"
REM copy build\pylib7.py "%DST%"
copy resources\7pdf.reg "%DST%"
copy LICENSE "%DST%\LICENSE.txt"
copy "%QTTOOLS_DIR%\libgcc_s_seh-1.dll" "%DST%"
copy "%QTTOOLS_DIR%\libstdc++-6.dll" "%DST%"
copy "%QTTOOLS_DIR%\libwinpthread-1.dll" "%DST%"
cd "%DST%" 
del ../%FILE%.zip
zip -R ../%FILE%.zip * */*
cd ..
rmdir /S /Q %FILE%

@echo off
echo ^

^

================================================================^

DEPLOYED FILES TO %DST%^

================================================================
