# HL7 Utilities

This is a set of hl7 tools. The goal was to create tools that can 
deal with files we get (we do not create them and have to live with 
whatever a system creates) in an efiicient way.

Perfomance has been of great priority for the parser. From the local SSD a HL7
ORU message with 127 segments takes about 0.008 seconds to parse on windows.
The biaries are compiled with MinGW, we expect them to perform better on 
a POSIX platform.

Primary goals:
- parse hl7 files in an efficient way -> `lib7.dll`/`lib7.so`/`lib7.a`
- extract or search certain parts of an HL7 file in an efficient way -> `7search`
- visualize an hl7 file structure -> `7parse` / `7view`
- extract and view  Base64 encoded documents from MDM messages

Most of the data-processing for parsing, searching and addressing is 
compiled into `lib7.so`/`lib7.dll`. It is used as backend for `7view`, 
`7search`, `7parse` and `7pdf`.

## Building

### clone 

```bash
git clone --recursive -j8 git@gitlab.com:wunderlins/hl7parse.git
```

### prerequesites

- git
- cmake (>= 3.5)
- cunit
- gcc/clang
- Qt5 for 7view (optional)
- cython and sphinx for pylib7
- Windows: use Mingw to compile (easiest install via [MSYS2](https://www.msys2.org/))

### Build environment

`lib7` and `7view` use cmake as build system and have been compiled on 
Linux, OSX and Windows (with MinGW). Dependingg on the component some 
dependencies are required:

- `lib7`: the only dependency is libc. 
  - if you want to build the API documentation `-DWITH_DOC=ON` then doxygen and optionally `plantuml` must be installed (set `PLANTUML_JAR_PATH` in CmakeLists.txt).
  - if you enable unit testing with `-DWITH_LIB7_TESTS=ON` the CUnit must be installed
- `7view` requires Qt5 (-dev) to be installed. Install it on your system, find the file `Qt5Config.cmake` and set this folder as environment variable `QT_DIR`
- `pylib7` is still build with make and requires `python3x` (including headers), `cython` and `sphinx`


**NOTE**: If the build fails, make sure to set `QT_DIR` to 
the path where yout `Qt5Config.cmake` file resides. This is typically 
something like `C:\qt_base_dir\version\mingwXX_XX\lib\cmake\Qt5`. 

#### Debian

```bash
apt install cmake build-essential # for lib7 and command line tools
apt install libcunit1 libcunit1-dev # for unit tests
apt install doxygen texlive-font-utils discount # for lib7 api documentation
apt install qtbase5-dev qt5-qmake qtbase5-dev-tools # for 7 view
apt install cython3 # for python 3 bindings
pip3 install sphinx # for python api docs
```

#### Mingw64
```bash
pacman -S mingw64/mingw-w64-x86_64-cmake \
  mingw64/mingw-w64-x86_64-nsis \
  mingw64/mingw-w64-x86_64-cunit \
  mingw64/mingw-w64-x86_64-doxygen 
```

Python dependecies for building with `-DWITH_PYTHON=ON`

```bash
pacman -S mingw64/mingw-w64-x86_64-cython \
  mingw64/mingw-w64-x86_64-python-setuptools \
  mingw64/mingw-w64-x86_64-python-sphinx \
  mingw64/mingw-w64-x86_64-python-wheel

pip install sphinx-autodoc-typehints
```

#### Release vs. Debug builds

Set `cmake ... -DCMAKE_BUILD_TYPE=Release` (default) or 
`cmake ... -DCMAKE_BUILD_TYPE=Debug` to control if debug symbols will be 
compiled into build.

### Source Configuration

- if you are parsing files with many subfields, raise `MAX_FIELDS` in `include/node.h`. Default is 1000.

### Compiling

#### User tools

1. `cd build`
   1. posix: `cmake -DWITH_GUI=ON -DCMAKE_BUILD_TYPE=Release ..`
   2. mingw: `../build_mingw.bat`
2. (optional) `make help` will list all targets
3. `make` will build static/dynamic lib7, 7view, 7search and 7pdf
4. The binaries `7parse`/`7search` or `7parse.exe`/`7search.exe` can be found in 
   the `build/` folder as well as `lib7/lib7.a`/`lib7/lib7.so`/`lib7/lib7.dll`. 
   Qt viewer resides in `viewer/7view.exe`
5. `make doc` to build the lib7 api documentation (must have been configured with `cmake -DWITH_DOC ...`)
6. `make test` to run unit tests (must have been configured with `cmake -DWITH_LI7_TESTS=ON ...`)


#### Library

If you are interested inthe library only, then build with 
`cmake -DWITH_CMD=OFF  -DCMAKE_BUILD_TYPE=Release ..`. You will find `lib7.h` 
in the same folder as `lib7.a`/`lib7.so`/`lib7.dll`.

## design goals

![7parse, Data Structures](doc/7parse.png)

### 7View

![7View Screenshot, Windows v 0.0.1](doc/screenshot-hl7viewer-0_0_1.PNG)
![7View Screenshot, OSX v 0.03.1](doc/Screenshot-hl7viewer-0_3_1-osx.png)

- [x] portable (windows, linux, osx) -> Qt5/C++
- [x] search inside the document with result highlighting
- [x] reliable (can deal with malicous input)
- [ ] value formatting (ie. dates, extract base64 content), needs to be tested in real world environment
- [ ] fast search over folder 
    - [ ] strcmp() or regex
    - [ ] time range over creation- or modification file timestamp

### schema editor (maybe)
- portable  (windows, linux, osx)
- compatible (try using standrs compatible format such as XSD)
- field info
    - standard field definition
    - description of source data
    - description of destionation data
    - remarks

### hl7 editor ("soon"[tm])
- portable  (windows, linux, osx)
- reliable (can deal with malicous input)
