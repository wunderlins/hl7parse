%module decode

%typemap(in,numinputs=0) char **message_p (char *temp) "$1 = &temp;"

%typemap(argout) char **message_p {
  %append_output(PyString_FromString(*$1));
}

%{
#include "Python.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "logging.h"
#include "bom.h"
#include "util.h"
#include "meta.h"
#include "node.h"
#include "decode.h"
%}

%include "meta.h"
%include "node.h"
%include "decode.h"
