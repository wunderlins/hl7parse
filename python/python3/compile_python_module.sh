#!/usr/bin/env bash

# compile
SONAME=_decode.so
OBJECTS="../../src/bom.o ../../src/address.o ../../src/base64.o ../../src/logging.o ../../src/node.o ../../src/util.o ../../src/decode.o ../../src/meta.o ../../src/7parse.o decode_wrap.o"
if [[ "$OS" == "Wnidows_NT" ]]; then
    OBJECTS="$OBJECTS ../../src/strndup.o"
    SONAME=_decode.pyd
fi
INCLUDES="-I../../include $(python-config --cflags)"
swig -python -I../../include -o decode_wrap.c decode.i
gcc -fPIC -c decode_wrap.c $INCLUDES
gcc -shared \
    $OBJECTS \
    $(python-config --ldflags) \
    -o _decode.so
mv _decode.so ../../build/${SONAME}
mv decode.py ../../build/

# cleanup
rm -rf *.pyd __pycache__ *.py *.o *.c
