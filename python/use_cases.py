#!/usr/bin/env python

import lib7, os, sys

msg = lib7.Message("../data/minimal.hl7")

print("getting MSH")
msh = msg.get("MSH")
print("%r" % msh.parent)
print(msh.parent.type)
children = msh.parent.children
for c in children:
    print(c.data)
    print("%r" % c)
    print(c.addr)

print("MSH: " + str(msh))
print(msh.next_sibling)
print(str(msh.last_sibling) + " " + msh.last_sibling.addr)

print("is root?: " + str(msg.is_root))
msh91 = msg.get("MSH-9.1")
print("msh91: " + str(msh91))
print("is root?: " + str(msh91.is_root))
print(msh91)
p = msh91.parent
print("parent: " + str(p))
try:
    print("next_sibling:   " + str(p.next_sibling))
except IndexError as e:
    print("next_sibling:   " + str(e))

## same, same ?
print(str(p.child_at(0).id) + " " + str(msh91.id) )
assert(p.child_at(0) == msh91)
print("position in parent's childnodes: " + str(msh91.pos_in_parent))
msh92 = msh91.next_sibling
print("next_sibling: " + str(msh92))
print("prev_sibling: " + str(msh92.previous_sibling) + ", " + msh92.previous_sibling.addr)
print(msh92.previous_sibling.data)

print(msg)
print("%r: %s" % (msh92, msh92))
print("%r" % msh92.parent.parent)

# fixme, addressing is borked
msh1 = msg.get("MSH-1") # this should contain '|' if default delimiters are used
print(msh1)

msh2 = msg.get("MSH-2") # this should contain the string of the delimiters
print(msh2)

print(msg.get("MSH-3"))

# bogous address definition
try:
    print(msg.get("MSH-(1)"))
except Exception as e:
    print(type(e).__name__, ": ", e)
