"""

"""

class Hl7Exception(Exception):
    """Base class for hl7 exceptions"""
    pass


class Hl7Exception(Exception):
    """Base class for hl7 exceptions"""
    pass

class Hl7StructureException(Hl7Exception):
    """ raised when a node is not in a structure which top most parent is a message_t node""" 

 
class Hl7SetNodeException(Hl7Exception):
    """ int node_set_by_addr(...) raised an exception
    
    Error codes:

    - `-1`: Failed to allocate fieldlist 
    - `-2`: Failed to allocate field
    - `-3`: Failed to set data on field
    - `-4`: Failed to pad children on field (Comp)
    - `-5`: Failed to set data on comp
    - `-6`: Failed to pad children on field (subcmp)
    - `-7`: Failed to set data on subcomp
    - `-21`: tryed to add a component to a field that already has data
    - `-22`: tryed to add a sub-component to a component that already has data

   """
    def __init__(self, addr: str, return_code: int):
        self.code = return_code
        self.message = "Failed set node by addr '"+addr+"', reason: " + self.code_to_str(self.code) + ", error code: " + str(self.code)
        super().__init__(self.message)

    def code_to_str(self, return_code: int):
        msg = ""
        if return_code == -1: msg = "Failed to allocate fieldlist"
        elif return_code == -2: msg = "Failed to allocate field"
        elif return_code == -3: msg = "Failed to set data on field"
        elif return_code == -4: msg = "Failed to pad children on field (Comp)"
        elif return_code == -5: msg = "Failed to set data on comp"
        elif return_code == -6: msg = "Failed to pad children on field (subcmp)"
        elif return_code == -7: msg = "Failed to set data on subcomp"
        elif return_code == -21: msg = "tryed to add a component to a field that already has data"
        elif return_code == -22: msg = "tryed to add a sub-component to a component that already has data"
        return msg


class Hl7ParseException(Hl7Exception):
    """ hl7_decode() raised an exception 
    
    return codes of lib7.hl7_parse:

    Error codes:

    - `1`: failed to allocate memory for line buffer
    - `2`: failed to allocate more memory for line buffer
    - `3`: EOF was reached unexpectedly
    - `4`: maximum delimiters per segment is reached, raise MAX_FIELDS
    - `5`: failed to allocate raw_field @see create_raw_field_t()
    - `6`: failed to allocate node_t @see create_node_t()
    - `7`: failed to process sub field elements @see process_node()
    - `8`: failed to append child @see node_append()
    - `9`: failed to allocate memory for segment name

    """
    def __init__(self, return_code: int):
        self.code = return_code
        self.message = "Failed to parse hl7 file, reason: " + self.code_to_str(self.code) + ", error code: " + str(self.code)
        super().__init__(self.message)

    def code_to_str(self, return_code: int):
        msg = ""
        if return_code == 1: msg = "failed to allocate memory for line buffer"
        elif return_code == 2: msg = "failed to allocate more memory for line buffer"
        elif return_code == 3: msg = "EOF was reached unexpectedly"
        elif return_code == 4: msg = "maximum delimiters per segment is reached, raise MAX_FIELDS"
        elif return_code == 5: msg = "failed to allocate raw_field @see create_raw_field_t()"
        elif return_code == 6: msg = "failed to allocate node_t @see create_node_t()"
        elif return_code == 7: msg = "failed to process sub field elements @see process_node()"
        elif return_code == 8: msg = "failed to append child @see node_append()"
        elif return_code == 9: msg = "failed to allocate memory for segment name"
        return msg


class Hl7AddressException(Hl7Exception):
    """Raised when an invalid address is used

    return codes from address.h: int set_addr_from_string(char \*str, hl7_addr_t \**ret_addr);

    returns 0 on succes and the following error codes otherwise:

    - `-1`: string length 0
    - `-2`: segment count not ended by ')'
    - `-3`: field address must start with '-'
    - `-4`: field address is not a digit
    - `-5`: expected '(' or '.' after field
    - `-6`: invalid field address, must at least set SEG-N
    - `-7`: field repetition '(' must be followed by digit
    - `-8`: field repetition may only contain digits between '(' and ')'
    - `-9`: premature end in field repetition (fieldlist)
    - `-10`: preamture end, expecting component address after '.'
    - `-11`: value error, only digits allowed in component address after '.'
    - `-12`: premature end, comp address is empty
    - `-13`: invalid character followed after comp
    - `-14`: '.' for subcomp must be followed by an address, end of string reached
    - `-15`: premature end, subcomp address is empty

    """
    def __init__(self, addr: str, return_code: int):
        self.code = return_code
        self.message = self.code_to_str(self.code) + " in '" + addr + "' " + str(self.code)
        super().__init__(self.message)
    
    def code_to_str(self, return_code: int):
        msg = ""
        if return_code == -1: msg = "string length 0"
        elif return_code == -2: msg = "segment count not ended by ')'"
        elif return_code == -3: msg = "field address must start with '-'"
        elif return_code == -4: msg = "field address is not a digit"
        elif return_code == -5: msg = "expected '(' or '.' after field"
        elif return_code == -6: msg = "invalid field address, must at least set SEG-N"
        elif return_code == -7: msg = "field repetition '(' must be followed by digit"
        elif return_code == -8: msg = "field repetition may only contain digits between '(' and ')'"
        elif return_code == -9: msg = "premature end in field repetition (fieldlist)"
        elif return_code == -10: msg = "preamture end, expecting component address after '.'"
        elif return_code == -11: msg = "value error, only digits allowed in component address after '.'"
        elif return_code == -12: msg = "premature end, comp address is empty"
        elif return_code == -13: msg = "invalid character followed after comp"
        elif return_code == -14: msg = "'.' for subcomp must be followed by an address, end of string reached"
        elif return_code == -15: msg = "premature end, subcomp address is empty"
        return msg
