.. Whatever documentation master file, created by
   sphinx-quickstart on Sat Jun 19 12:43:45 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

API Documentation
=================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :hidden:
   
   index
   contents
   tests/test_*

.. automodule:: lib7
   :members:
   :undoc-members:
   :show-inheritance:
   :inherited-members:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
