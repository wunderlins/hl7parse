#!/usr/bin/env python

# test

import lib7, sys
msg = lib7.read(sys.argv[1])

#msg.dump()
print("%r" % msg)
for m in msg:
    print("%r" % m)
    for fl in m:
        print("\t%r" % fl)
        for f in fl:
            print("\t\t%r" % f)
            for c in f:
                print("\t\t\t%r" % c)
                for sc in c:
                    print("\t\t\t\t%r" % sc)

# try to acces an element via child_at
msh = msg.child_at(0)
print(msh.child_at(1).data) # this should be None
delimiter = msh.child_at(1).child_at(0).data # this should get us the defined delimiter
print(delimiter)

del(msg) # deallocates all memmory


# open a file that does not exist
try:
    msg = lib7.read("a")
except IOError as e:
    print("File operation Failed with message: {}".format(e))
except:
    print(e)

# open a file that is not an hl7 file
try:
    msg = lib7.read("Makefile")
    print(msg)
except IOError as e:
    print("File operation Failed with message: {}".format(e))
except Exception as e:
    print(e)

#lib7.list()
#lib7.close()
