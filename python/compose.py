#!/usr/bin/env python

import lib7

# test composition
"""
comp = lib7.compose()
print("composed")
comp.set("PID-3", "1234")
comp.set("PID-5.1", "FämilyNäme", hl7_escape=True)
comp.set("PID-5.2", "Sürnäme")
comp.set("PID-6", "funny characters \r | ^ & ~ <-- should be escaped", hl7_escape=True)

# show data
comp.dump()
print(comp)
comp.write_file("out.hl7")

"""
# read all files from data
import os

c = lib7.compose()
directory = r'../data'
flist = os.listdir(directory)

#flist = ["min.hl7"]
for filename in flist:
    if not filename.endswith(".hl7"):
        continue
    
    print(filename)
    c.open_file(directory + "/" + filename)
    c.write_file("../test/out/" + filename)

