cimport lib7
from libc.stdio cimport FILE, fopen, fclose
from libc.stdlib cimport calloc, free
from libc.string cimport strdup
from libc.string cimport strlen
from lib7exception import *

from enum import Enum

"""
This is a python interface to lib7.so. 

It's primary purpose is to have an easy interface for rapid development 
and testing of lib7. 

Not all features are available (see lib7.h) but enough to parse a HL7
file and read data properties. This allows easy search and analysis of
HL7 files.

2021, Simon Wunderlin
"""

class Type(Enum):
    """Node types
    
    Every `Node` has a type. The type tells you at which hirarchy level 
    the node is stored. The lwoer the value, the higher in the hirarchy.

    `MESSAGE` is generally the root element. However, it ispossible to have 
    detatched nodes which have no `parent` (this happens during creation or
    when the structure is not properly managed).

    `LEAF` is a relic and is never used.
    """
    UNKNOWN     = 0
    MESSAGE     = 1
    SEGMENT     = 2
    FIELDLIST   = 4
    FIELD       = 8
    COMP        = 16
    SUBCOMP     = 32
    LEAF        = 64

cdef class MetaSeparator():
    """This object holds all delimiters for `Meta` 
    
    It should always be used together with :py:class:`Meta` and is used
    as accessor to the C Meta object seperator properties `char meta_t->sep_\*`.

    *NOTE:* this is a private class.
    """

    cdef hl7_meta_t *_c_meta

    def __cinit__(self):
        self._c_meta = NULL
        pass

    def __init__(self):
        pass

    cdef _c_set_ref(self, hl7_meta_t *meta):
        self._c_meta = meta

    @property
    def segment(self) -> str:
        """
        The segment separator, typically `\\\\r`

        If this value is set to `0x10 0x13` then :py:attr:`Meta.crlf` is
        set to `True`, otherwise it is set to `False`.

        :getter: returns the current segment spearation character
        :setter: sets a new segment separation character
        :rtype: char (2 char if CRLF)
        """
        if self._c_meta == NULL:
            raise UnboundLocalError("_c_meta is uninitialized (NULL)")
        return chr(self._c_meta.sep_message)

    @property
    def field(self) -> str:
        """
        The field separator, typically `|`

        :getter: returns the current field spearation character
        :setter: sets a new field separation character
        :rtype: char
        """
        if self._c_meta == NULL:
            raise UnboundLocalError("_c_meta is uninitialized (NULL)")
        return chr(self._c_meta.sep_field)

    @property
    def comp(self) -> str:
        """
        The component separator, typically `^`

        :getter: returns the current component spearation character
        :setter: sets a new component separation character
        :rtype: char
        """
        if self._c_meta == NULL:
            raise UnboundLocalError("_c_meta is uninitialized (NULL)")
        return chr(self._c_meta.sep_comp)

    @property
    def rep(self) -> str:
        """
        The repetition separator, typically `~`

        :getter: returns the current repetition spearation character
        :setter: sets a new repetition separation character
        :rtype: char
        """
        if self._c_meta == NULL:
            raise UnboundLocalError("_c_meta is uninitialized (NULL)")
        return chr(self._c_meta.sep_rep)

    @property
    def escape(self) -> str:
        """
        The escape character, typically `\\\\`

        :getter: returns the current escape character
        :setter: sets a new escape character
        :rtype: char
        """
        if self._c_meta == NULL:
            raise UnboundLocalError("_c_meta is uninitialized (NULL)")
        return chr(self._c_meta.sep_escape)

    @property
    def subcmp(self) -> str:
        """
        The sub-component separator, typically `&`

        :getter: returns the current sub-component spearation character
        :setter: sets a new sub-component separation character
        :rtype: char
        """
        if self._c_meta == NULL:
            raise UnboundLocalError("_c_meta is uninitialized (NULL)")
        return chr(self._c_meta.sep_subcmp)

    @segment.setter
    def segment(self, val: str):
        if self._c_meta == NULL:
            raise UnboundLocalError("_c_meta is uninitialized (NULL)")
        if len(val) > 2:
            raise ValueError("Separator must be of type char (single 8bit character)")

        # we alow CR/LF/CRLF
        if len(val) == 2:
            if val[0] != "\r" or val[1] != "\n":
                raise ValueError("Double character message delimiter. Only CRLF allowed")
            
            self._c_meta.sep_message = ord("\r")    
            self._c_meta.crlf = 1
        else:
            self._c_meta.sep_message = ord(val)
            self._c_meta.crlf = 0

    @field.setter
    def field(self, val: str):
        if self._c_meta == NULL:
            raise UnboundLocalError("_c_meta is uninitialized (NULL)")
        if len(val) != 1:
            raise ValueError("Separator must be of type char (single 8bit character)")
        self._c_meta.sep_field = ord(val)

    @comp.setter
    def comp(self, val: str):
        if self._c_meta == NULL:
            raise UnboundLocalError("_c_meta is uninitialized (NULL)")
        if len(val) != 1:
            raise ValueError("Separator must be of type char (single 8bit character)")
        self._c_meta.sep_comp = ord(val)

    @rep.setter
    def rep(self, val: str):
        if self._c_meta == NULL:
            raise UnboundLocalError("_c_meta is uninitialized (NULL)")
        if len(val) != 1:
            raise ValueError("Separator must be of type char (single 8bit character)")
        self._c_meta.sep_rep = ord(val)

    @escape.setter
    def escape(self, val: str):
        if self._c_meta == NULL:
            raise UnboundLocalError("_c_meta is uninitialized (NULL)")
        if len(val) != 1:
            raise ValueError("Separator must be of type char (single 8bit character)")
        self._c_meta.sep_escape = ord(val)

    @subcmp.setter
    def subcmp(self, val: str):
        if self._c_meta == NULL:
            raise UnboundLocalError("_c_meta is uninitialized (NULL)")
        if len(val) != 1:
            raise ValueError("Separator must be of type char (single 8bit character)")
        self._c_meta.sep_subcmp = ord(val)

cdef class Meta():
    """
    Access class to the metadata stored in `Message._c_meta`

    This object pythonifies the access to all meta properties of a 
    :py:class:`Message` object.
    """
    # this is a pointer to the actual storage
    cdef hl7_meta_t *_c_meta
    cdef MetaSeparator _c_separator

    def __cinit__(self):
        #print("Meta.__cinit__()")
        self._c_meta = NULL
        self._c_separator = MetaSeparator()
        pass

    def __dealloc__(self):
        pass

    def __init__(self):
        #print("Meta.__init__()")
        pass
    
    cdef _c_set_ref(self, hl7_meta_t *meta):
        self._c_meta = meta
        self._c_separator._c_set_ref(meta)

    @property
    def separator(self) -> MetaSeparator:
        """
        Change meta data of :py:class:`Message`. This class is used
        in :py:attr:`Message.meta` to pythonify the acces to the underlying 
        `meta_t` C object.

        :getter: returns hl7 meta separator
        :setter: sets new meta separators through the properties of :py:class:`MetaSeparator`
        :rtype: MetaSeparator
        """
        return self._c_separator

    @property
    def crlf(self) -> bool:
        """`True` if the segment separator is `CRLF`

        ... otherwise `False`. If this property is true, the Segment separator
        is 2 characters long (`0x13 0x10`), otherwise it's 1.

        When :py:attr:`Meta.separator.segment` is set to `\\\\r\\\\n` (`CRLF`)
        then this property is set to `True`, else `False`.

        :setter: set segment separator to 1 or 2 bytes.
        :getter: inidicates if segment separator is 2 or 1 byte.
        :rtype: Bool
        """
        if self._c_meta == NULL:
            raise UnboundLocalError("_c_meta is uninitialized (NULL)")

        if self._c_meta.crlf == 1:
            return True
        return False

    @crlf.setter
    def crlf(self, var: bool):
        if self._c_meta == NULL:
            raise UnboundLocalError("_c_meta is uninitialized (NULL)")

        if var:
            self._c_meta.crlf = 1
        else:
            self._c_meta.crlf = 0

cdef class _AbstractNode:
    """Common properties of Node and Message
    
    This abstract class has only `cython` methods."""

    """ iter position """
    cdef int _n 

    cdef _c_init(self):
        #print("AbstractNode._c_init()")
        self._n = 0
    
    cdef Node _create_node(self, lib7.node_t *c_node_t):
        n = Node()
        n._c_struct = c_node_t
        return n

cdef class Node(_AbstractNode):
    """Node class is the python implementation for typedef struct node_t 
    
    This class provides structural access to parent/siblings and 
    children as well as holds the actual RAW data that may be stored 
    in a node.

    Every Node can technically have childeren, however, the HL7 definition 
    does not support children below the SUB-COMPONENT (typically delimited 
    by `&`) type. Children below SUB-COMPOINENT therefore cannot be 
    serialized and are forbidden.

    """
    cdef lib7.node_t *_c_struct

    @property
    def id(self) -> int:
        """Unique node ID
        
        Internally generated node id. This id is used to compare to nodes 
        if they are equal. This id is managed b the C Code and is read only.
        
        :getter: returns id
        :rtype: int"""
        return self._c_struct.id
    
    @property
    def type(self) -> Type:
        """Node type
        
        Depending on where a node is located in the hirarchy, it has 
        a different type. See also `Understanding Addressing`_

        :getter: node type
        :rtype: Type
        """
        return Type[Type(self._c_struct.type).name]
    
    @property
    def num_children(self) -> int:
        """number of children of this `Node`

        If num_children is 0, then this is typically a data node. Check the 
        `data` property for the content. This does not apply for 
        node_type SEGMENT where data always contains the segment name (no 
        matter how many children it has).

        `Node` provides and iterator to loop over all children.

        ::

            message = lib7.read(...)
            node = message.get("PID-9.1")
            for n in node:
                print node

        see also: :py:attr:`Node.children`, :py:func:`Node.__iter__`

        :getter: get number of children
        :rtype: int
        """
        return self._c_struct.num_children

    def __cinit__(self):
        #print("Node.__cinit__()")
        self._c_struct = NULL
        self._c_init()
    
    def __init__(self):
        pass

    @property
    def data_length(self) -> int:
        """length of the data
        
        if 0, then there is no data. otherwise it will contain the 
        number of bytes of the raw data stored in :py:attr:`Node.data` including the.

        :rtype: int
        """
        if self._c_struct.length:
            return self._c_struct.length - 1
        return 0

    @property
    def data(self) -> str:
        """ unsigned char: this property holds the raw data 
        
        every LEAF node has data (nodes without children), this property holds the raw of 
        the node's content.

        Data will be converted to UTF-8 before it is returned.

        No scaping is done when a file is parsed. This attribute may 
        hold hl7 escaped data such as `\\\\.br\\\\` for newlines. However, proper decoding
        can only be done by using the separator characters from :py:attr:`Message.meta`.

        If the node has no allocated memory in the C struct, `None` will be returned.

        :getter: get raw data as UTF-8
        :rtype: String
        """
        if self._c_struct.type != SEGMENT and self._c_struct.num_children:
            return None

        if self._c_struct.length == 0:
            return None
        
        return self._c_struct.data.decode("UTF-8")

    @property
    def is_root(self) -> bool:
        """checks if this is the root node

        :getter: returns `True` if there is no parent `False` otherwise.
        :rtype: bool
        """
        if self._c_struct.parent == NULL:
            return True
        return False

    @property
    def parent(self) -> Node:
        """ get the parent node 
        
        Returns the parent :py:class:`Node` or :py:class:`Message`.

        :getter: depending on parent type, usually :py:class:`Node` is returned. however the top most parent is always :py:class:`Message`
        :rtype: Node or Message
        """
        if (self._c_struct.parent.type == MESSAGE):
            # FIXME: use global cdef to generate message "Node"
            m = Message()
            m.pass_ref(<message_t *> self._c_struct.parent)
            return m

        return self._c_parent()

    @property
    def children(self) -> list[Node]:
        """ get a list of child nodes 
        
        This property returns a list of child nodes. If you want to check
        or read child often :py:attr:`Node.__iter__` is to be preferred.

        `Node.__iter__` is faster than getting a full list of all child nodes
        when you know you might not need them all.

        :getter: get a list of all child nodes
        :rtype: list Nodes
        """
        children = []

        for i in range(0, self.num_children):
            children.append(self._create_node(self._c_struct.children[i]))

        return children

    cdef Node _c_parent(self):
        if self._c_struct.parent == NULL:
            return None
        
        return self._create_node(self._c_struct.parent)

    """
    @property
    def type_to_string(self) -> str:
        "" "string representation of the current Node's type_t
        
        :getter: returns an uppercase name of the node type
        :rtype: String"" "
        return self.type.name
    """

    cdef int _c_eq(self, other: Node):
        """ check if the underlying C Storage point to the same object """
        return (self._c_struct.id == other._c_struct.id)

    def __eq__(self, other: Node) -> bool:
        if not isinstance(other, Node):
            return False

        return self._c_eq(other)

    def __repr__(self) -> str:
        out = "<"+ self.type.name + " " + self.addr +" (children: " + str(self._c_struct.num_children) + ")"

        if self._c_struct.type != lib7.SEGMENT:
            if self._c_struct.length > 0:
                out += ", [" + str(self.data_length) + "] '" + self.data + "'"
        else:
            out += ", " + self.data

        return out + ">"

    cdef str _c_to_string(self):
        cdef int length = 0
        cdef node_t *n = self._c_struct
        while n != NULL:
            if n.type == MESSAGE:
                break
            n = n.parent
        cdef message_t *m = <message_t*> n
        cdef hl7_meta_t *meta = m.meta
        cdef char *s = <char *> node_to_string(self._c_struct, meta, &length)
        return s.decode("UTF-8")
    
    def __str__(self) -> str:
        return self._c_to_string()
    
    def __iter__(self) -> Node:
        self._n = 0
        return self   
    
    def __next__(self) -> Node:
        #print(self._c_message_t.num_children)
        if self._n < self._c_struct.num_children:
            n = self._create_node(self._c_struct.children[self._n])
            self._n += 1
            return n

        raise StopIteration()
    
    def child_at(self, int pos) -> Node:
        """
        get the Nth child at pos. 
        
        Returns None if the child does not exist.

        pos starts at 0 in contrast to HL7 adressing.
        Example: 
        - `MSH-2` will be at pos 1
        - `MSH-2.1` will be at position 0
        
        :param int pos: position index starting at 0 or raises IndexError
        :rtype: Node
        """
        n = None

        # check bounds
        if self._c_struct.num_children == 0 or self._c_struct.num_children <= pos:
            raise IndexError("Child at " + str(pos) + " does not exist.")
        
        return self._create_node(self._c_struct.children[pos])

    # WTF: what do we need it for ?
    cdef int _c_pos(self):
        pass
        
    @property
    def pos_in_parent(self) -> int:
        """ index in the parents children list 
        
        This value is useful to directly address the `N`th 
        element after  the current `Node`.

        For example:

        ::

            # this is usually more efficient than looping 
            # through all siblings
            msh3 = msg.get("MSH-3")
            msh5 = msh3.parent.child_at(msh3.pos_in_parent + 2)

        :getter: get index in parent's children list
        :rtype: int position in `Node.parent.children` array"""
        return lib7.node_parent_child_pos(self._c_struct)
    
    @property
    def first_child(self) -> Node:
        """Get a reference to the first child of this Node's children
        
        convenience method for ``node.child_at(0)``

        :getter: returns first child or raises IndexError
        :rtype: Node
        """
        pos = 0
        if pos < self.num_children:
            return self._create_node(self._c_struct.children[pos])
        
        raise IndexError("Sibling " + str(pos) + " does not exist.")
        
    @property
    def last_child(self) -> Node:
        """Get a reference to the last child of this Node's children
        
        convenience method for ``node.child_at(node.num_children-1)``

        :getter: returns last child or raises IndexError
        :rtype: Node
        """
        pos = self.num_children - 1
        if pos > -1:
            return self._create_node(self._c_struct.children[pos])
        
        raise IndexError("Sibling " + str(pos) + " does not exist.")

    @property
    def previous_sibling(self) -> Node:
        """Get a reference to the  previous sibling node (to the left)
        
        :getter: returns sibling or raises IndexError
        :rtype: Node
        """
        pos = self.pos_in_parent-1
        
        if pos < 0:
            raise IndexError("Sibling " + str(pos) + " does not exist.")
        
        if pos < self.parent.num_children and pos > -1:
            return self._create_node(self._c_struct.parent.children[pos])
        
        raise IndexError("Sibling " + str(pos) + " does not exist.")
    
    @property
    def next_sibling(self) -> Node:
        """Get a reference to the  next sibling node (to the right)
        
        :getter: returns sibling or raises IndexError
        :rtype: Node
        """
        pos = self.pos_in_parent

        if pos < 0:
            raise IndexError("Sibling " + str(pos) + " does not exist.")
        
        pos += 1
        
        if pos < self.parent.num_children and pos > -1:
            return self._create_node(self._c_struct.parent.children[pos])
        
        raise IndexError("Sibling " + str(pos) + " does not exist.")

    @property 
    def first_sibling(self) -> Node:
        """Get a reference to the first sibling of this Node
        
        :getter: returns first sibling
        :rtype: Node
        """
        return self.parent.child_at(0)

    @property 
    def last_sibling(self) -> Node:
        """Get a reference to the last sibling of this Node
        
        :getter: returns last sibling
        :rtype: Node
        """
        return self.parent.child_at(self.parent.num_children-1)

    @property
    def num_siblings(self) -> int:
        """Number of siblings on this Node hirarchy level
        
        :getter: returns number of siblings
        :rtype: int
        """
        if self.parent == None:
            return None
        
        return self.parent.num_children

    @property
    def addr(self) -> str:
        """String representation of this Ndoes address
        
        See chapter `Understanding Addressing` for a detailed 
        description how addresses work. This method is costly and 
        should not be used exessively (for exampe for traversing).

        Use the internal node structure with __iter__ or sibling attributes
        to traverse the structure.

        This method is mainly intended to create user readable addresses 
        for error messages and `print()`

        :getter: returns a string representation of the Node's address
        :rtype: String
        """
        cdef hl7_addr_t *addr = lib7.addr_from_node(self._c_struct)
        
        if addr == NULL:
            raise Hl7StructureException("Failed to create address from node.")
        
        cdef char *addr_str = lib7.addr_to_string(addr)
        cdef Py_ssize_t length = strlen(addr_str)

        py_bytes_string = ""
        try:
            py_bytes_string = addr_str[:length].decode("UTF-8")  # Performs a copy of the data
        finally:
            free(addr_str)

        return py_bytes_string


cdef class Message(_AbstractNode):
    """This is the main class for parsing hl7 files
    
    It will contain a list of Segments (lines) of a hl7 file. It can 
    be iterated and will return top-level nodes of type SEGMENT.

    Every segment may contain 0-N children which are also iterable.

    Example::

        import lib7

        msg = lib7.open("path/to/some/file.hl7")

        # check what sort of message type this is, defined in MsH-9.1
        msh91 = None
        msh92 = None
        try:
            msh91 = msg.get("MSH-9.1")
            print(msh91) # should print the content of the FIELD
        except Hl7StructureException:
            print("MSH-9.1 Not found)
        
        try:
            msh92 = msh91.next_sibling
        except Hl7StructureException:
            print("MSH-9.2 Not found)
        
        root = msh92
        while root.parent:
            root = root.parent
        
        # we have now found the top most node which is of type MESSAGE
        # This node contains N segments in children
        print(root.type) # prints: Type.MESSAGE
        print("%r" % root) # prints: <MESSAGE (children: 2)>
        print(root.num_children) # prints: 2
        assert(root == msg) # same same but different (variable name)

        # every node in the structure is an iterator which will 
        # iterate over all child nodes.
        #
        # This is the preferred way, because it is faster than first 
        # fetching all nodes via `n.children` and then looping over them.
        for seg in root:
            print("%r" % seg) 
            # prints: <SEGMENT MSH(1) (children: 18), MSH>
            #         <SEGMENT PID(1) (children: 1), PID>
            
            print(seg.addr)
            # prints: MSH(1)
            #         PID(1)
        
        # or you may fetch a list of child ndoes like this:
        children = root.children

        # print the segment names
        for c in children:
            print(c.data) # prints: MSH, PID, ...

    See also:

    - :py:meth:`lib7.Message.get`: get a node by address
    - :py:meth:`lib7.Message.meta`: hl7 metadata such as separators, see: :py:class:`lib7.Meta`
    - :py:attr:`lib7.Message.children`: a list of segments in this `Message`
    - :py:attr:`lib7.Node.data`: the nodes data if it has no children ( `num_children` is 0)
    - :py:attr:`lib7.Node.addr`: string representation of the current node's address
    - :py:attr:`lib7.Node.parent`: the parent Node, `None` if there is no parent
    - :py:attr:`lib7.Node.children`: a list of child ndoes
    - :py:attr:`lib7.Node.next_sibling`: the next sibling of this ndoe
    - :py:attr:`lib7.Node.previous_sibling`: the last sibling of this node 
    - :py:attr:`lib7.Node.first_child`: gets the first child `Node`
    - :py:attr:`lib7.Node.last_child`: gets the last child `Node`
    - :py:meth:`lib7.Node.child_at`: get a specific child at position `N`
    - :py:attr:`lib7.Node.num_children`: how many child ndoes are there ?
    - :py:attr:`lib7.Node.num_siblings`: how many siblings are there ?
    - :py:attr:`lib7.Node.type`: returns the type :py:class:`lib7.Type`
    - :py:class:`lib7.Node`: every child of `Message` is a `Node`
    """
    cdef lib7.hl7_addr_t *_c_addr
    cdef lib7.message_t *_c_struct
    cdef FILE *_c_fd
    # count additional refreences created to _c_struct
    # free only when back to 0
    cdef int _c_additional_ref

    cdef Meta _c_meta

    @property
    def meta(self) -> Meta:
        """ Metadata of the HL7 message
        
        lets you read and set all the delimiters of the message. When a hl7 file
        is opened by :py:class:`Message` or :py:class:`Composer` the underlying
        C library is autodetecting all separators in the file.

        You may change these by setting different separators before writing 
        out a file. Access delimiters trough `self.meta.separator`.

        Example::

            msg = lib7.read("file.hl7") # will return a Message object
            msg.meta.separator.segment # should be "\\r" by default.

            # set to windows CRLF
            msg.meta.sparator.segment = "\\r\\n"

            msg.meta.sparator.crlf # is now set to `True`
            # indicating that we have a 2 byte segment separator

        see: :py:class:`MetaSeparator` for all properties in `self.meta.separator`

        :rtype: Meta
        """
        return self._c_meta

    @property
    def id(self) -> int:
        """Unique node ID
        
        Internally generated node id. This id is used to compare to nodes 
        if they are equal. This id is managed b the C Code and is read only.
        
        :getter: returns id
        :rtype: int"""
        return self._c_struct.id
    
    @property
    def type(self) -> Type:
        """Node type
        
        Depending on where a node is located in the hirarchy, it has 
        a different type. See also `Understanding Addressing`_

        :getter: node type
        :rtype: Type
        """
        return Type[Type(self._c_struct.type).name]
        #return self._c_struct.type
    
    @property
    def num_children(self) -> int:
        """number of children of this `Node`

        If num_children is 0, then this is typically a data node. Check the 
        `data` property for the content. This does not apply for 
        node_type SEGMENT where data always contains the segment name (no 
        matter how many children it has).

        `Node` provides and iterator to loop over all children.

        ::

            message = lib7.read(...)
            node = message.get("PID-9.1")
            for n in node:
                print node

        see also: :py:attr:`Node.children`, :py:func:`Node.__iter__`

        :getter: get number of children
        :rtype: int
        """
        return self._c_struct.num_children

    @property
    def children(self) -> list[Node]:
        """ get a list of child nodes 
        
        This property returns a list of child nodes. If you want to check
        or read child often :py:attr:`Node.__iter__` is to be preferred.

        `Node.__iter__` is faster than getting a full list of all child nodes
        when you know you might not need them all.

        :getter: get a list of all child nodes
        :rtype: list Nodes
        """
        children = []

        for i in range(0, self.num_children):
            children.append(self._create_node(self._c_struct.segments[i]))

        return children

    @property
    def parent(self) -> Node:
        """ get the parent node 
        
        Returns the parent :py:class:`Node` or :py:class:`Message`.

        :getter: depending on parent type, usually :py:class:`Node` is returned. however the top most parent is always :py:class:`Message`
        :rtype: Node or Message
        """
        return None

    @property
    def is_root(self) -> bool:
        """checks if this is the root node
        
        On the message level this is always true, :py:class:`Message` and 
        :py:class:`Composer` cannot have a parent.

        :rtype: Bool
        """
        return True

    def __cinit__(self):
        #print("Message.__cinit__()")
        self._c_additional_ref = 0
        self._c_addr = lib7.create_addr()
        self._c_struct = NULL
        self._c_fd = NULL
        self._c_init()
        self._c_meta = Meta()
    
    cdef pass_ref(self, lib7.message_t *msg):
        if self._c_struct != NULL:
            lib7.free_message_t(self._c_struct)
        
        self._c_additional_ref += 1
        self._c_struct = msg
        self._c_meta._c_set_ref(self._c_struct.meta)

    def __dealloc__(self):
        #print("Message.__dealloc__(self): " + str(self._c_additional_ref))

        # prevent multiple frees by checking if there are other objects 
        # using this objec'ts _c_struct
        if self._c_additional_ref > 0:
            self._c_additional_ref -= 1
            return

        #if self._c_addr != NULL:
        #    lib7.free_addr(self._c_addr)
        #    self._c_addr = NULL
        self.__free()

    def __init__(self, file=None):
        """
        Constructor

        if `file` is empty, an empty `Message`object is created.

        :param str file: optional file path to open and parse
        :rtype: Message
        """
        if file != None:
            self.open_file(file)
    
    def __str__(self) -> str:
        #unsigned char *message_to_string(message_t *message, hl7_meta_t *hl7_meta)
        cdef unsigned char *str = message_to_string(self._c_struct)
        ret = str.decode("UTF-8")
        free(str)
        return ret
    
    def __repr__(self) -> str:
        return "<"+ self.type.name + " (children: " + str(self._c_struct.num_children) + ")>"

    def __iter__(self):
        self._n = 0
        return self

    def __next__(self) -> Node:
        #print(self._c_struct.num_children)
        if self._n < self._c_struct.num_children:
            n = self._create_node(self._c_struct.segments[self._n])
            self._n += 1
            return n

        raise StopIteration()
    
    cdef Node _get(self, addr: str):
        """find a node by hl7 address """
        # FIXME: depending on the address, this method will only return 
        #        the first ocourance of a node
        #
        #        repetitions in field-lists or segments are not dealt with

        # int node_get_by_addr(message_t* message, hl7_addr_t *addr, node_t **node)

        # make sure addr is reset to it's original state
        #self._c_addr = reset_addr(self._c_addr)

        # create a intiialized native addr struct
        py_addr = addr.encode('UTF-8')
        cdef char* c_addr = py_addr
        cdef int r = set_addr_from_string(c_addr, &self._c_addr)
        #addr_dump(self._c_addr)
        if r != 0:
            raise Hl7AddressException(addr, r)

        cdef node_t* n = NULL# this node will hold our result
        r = node_get_by_addr(self._c_struct, self._c_addr, &n)

        # if r is 0, then we have not found a node
        if r != 0:
            raise Hl7StructureException("Failed to resolve node in structure: " + str(r))
        
        # if r is greater 0, then we get a node type as response
        # FIXME: is it wise to create a new instance of 
        #        node, or should we find the existing one?
        return self._create_node(n)
    
    def get(self, addr) -> Node:
        """ get a reference to a `Node` by a string address
        
        This is a conveniecne way to access Nodes in a HL7 Structure.
        See `Understanding Addressing` for details on the address syntax.

        Example::
            
            msg = lib7.read("file.hl7")
            msh9 = msg.get("MSH-9.1") # first FIELD in MSH-9
        
        :getter: returns node or raises Hl7StructureException if the `Node` does not exist
        :rtype: Node
        """
        return self._get(addr)
    """
    def type_to_string(self) -> str:
        " ""string representation of the current Node's type_t

        This getter returns MESSAGE always on the `Message` class.
        
        :getter: returns an uppercase name of the node type
        :rtype: String"" "
        return "MESSAGE"
    """
    
    def open_file(self, str file):
        """Open a HL7 file
        
        This Method will return a Message object which is iteratable. Iterating 
        over it will return all Segments (of type Node) which will contain 
        sub-nodes (if there are any) which are alos iteratable.

        :param str file: file name pointing to the hl7 file or raises IOError or Hl7ParseException
        :rtype: Message
        """
        self._open_file(file)

    cdef _open_file(self, str file):
        # if there is a structure allocated, release all reasources and 
        # free memory
        self.__free()

        cdef lib7.message_t *_c_struct = lib7.create_message_t(NULL)
        cdef FILE *_c_fd = NULL


        # convert file name into c_string
        py_byte_string = file.encode('UTF-8')
        cdef char* c_string = py_byte_string

        # initialize meta
        # FIXME: do we have to free the old meta structure here ?
        # open file
        _c_fd = hl7_open(c_string)
        if _c_fd == NULL:
            lib7.free_message_t(_c_struct)
            raise IOError("Failed to open file {}!".format(file))

        # check if this file has a bom;
        # this will move fd at the first byte after the BOM
        #detect_bom(_c_fd)
        cdef bom_t *bom = detect_bom(_c_fd)
        _c_struct.meta.bom = bom

        # parse file
        cdef int ret = hl7_decode(_c_fd, &_c_struct)
        
        if ret != 0:
            # cleanup
            #lib7.free_hl7_meta(_c_meta)
            lib7.hl7_close(_c_fd)
            lib7.free_message_t(_c_struct)
            #free(c_string)
            raise Hl7ParseException(ret)
        

        lib7.hl7_close(_c_fd)
        self._c_struct = _c_struct
        self._c_meta._c_set_ref(self._c_struct.meta)

    def dump(self):
        """Dump the structure to stdout.
        
        This method is primarely used for debugging and testing.
        """
        lib7.dump_structure(self._c_struct)
    
    def __free(self):
        #print("Message.__free(self)")
        if self._c_fd != NULL:
            #print("Message.__free _c_fd")
            lib7.hl7_close(self._c_fd)
            self._c_fd = NULL

        if self._c_struct != NULL:
            #print("Message.__free _c_struct")
            free_message_t(self._c_struct)
            self._c_struct = NULL
    
    def child_at(self, int pos) -> Node:
        """
        get the Nth child at pos. 
        
        Returns None if the child does not exist.

        pos starts at 0 in contrast to HL7 adressing.
        Example: 
        - `MSH-2` will be at pos 1
        - `MSH-2.1` will be at position 0
        
        :param int pos: position index starting at 0 or raises IndexError
        :rtype: Node
        """

        # check bounds
        if self._c_struct.num_children == 0 or self._c_struct.num_children <= pos:
            raise IndexError("Child at " + str(pos) + " does not exist.")
        
        return self._create_node(self._c_struct.segments[pos])

    @property
    def previous_sibling(self) -> Node:
        """ Message has no siblings
        
        This is here for compatibility reasons with `Node`

        will always raise IndexError
        """
        raise IndexError("Message has no siblings")
    
    @property
    def next_sibling(self) -> Node:
        """ Message has no siblings
        
        This is here for compatibility reasons with `Node`

        will always raise IndexError
        """
        raise IndexError("Message has no siblings")

    @property 
    def first_sibling(self) -> Node:
        """ Message has no siblings
        
        This is here for compatibility reasons with `Node`

        will always raise IndexError
        """
        raise IndexError("Message has no siblings")

    @property 
    def last_sibling(self) -> Node:
        """ Message has no siblings
        
        This is here for compatibility reasons with `Node`

        will always raise IndexError
        """
        raise IndexError("Message has no siblings")

    @property
    def num_siblings(self) -> Node:
        """ Message has no siblings
        
        This is here for compatibility reasons with `Node`

        :rtype: int always 0
        """
        return 0

cdef class Composer(Message):
    """Compose HL7 files
    
    With :py:class:`lib7.Composer` you get access to writing and creating hl7 files.
    because :py:class:`lib7.Composer` derives from :py:class:`lib7.Message`, 
    all the above methods described in `Reading HL7 Files`_ are applicable.

    ::

        import lib7

        # open an existing file for modifying
        msg = lib7.compose("path/to/some/file.hl7")

        # will create segment: "XYZ|^&mydata\\r"
        msg.set("XYZ(1)-1.2.3, "mydata") 

        # now write the changed data
        msg.write_file("path/to/some/other.hl7")

        # if you do bulk editing over many files, it is a good 
        # idea to free memory like so:
        del(msg)

        # create an empty message.
        msg = lib7.compose()

        # this message will already contain an MSH segment, always, 
        # it's really needed
        #
        # now you can start adding stuff
        msg.set("PID-3(1), "patientnumber")

        del(msg)

    See also:

    - :py:meth:`lib7.Composer.set`: set a node by address, it will be created if it doesn't exist
    - :py:meth:`lib7.Composer.write_file`: write the created or modified file
    - :py:class:`lib7.Message`: all the other methods inherited from `Message`
    - :py:class:`lib7.Node`: every child of `Message` is a `Node`

    """

    def __cinit__(self):
        #print("Composer.__cinit__()")
        #self._c_struct  = lib7.create_message_t(NULL)
        # set default meta behaviour to CR line ending
        #self._c_struct.meta.crlf = 0
        pass

    def __dealloc__(self):
        #print("Composer.__dealloc__(self)")
        # do only free C reousrces here that have been allocated by  composer
        #self.__free()
        pass
    
    def __init__(self, file = None):
        """
        Constructor

        if `file` is empty, an empty `Composer` object is created with a default `MSH` Segment.

        :param str file: optional file path to open and parse
        :rtype: Message
        """
        cdef unsigned char delim[2]; # = {self._c_meta.sep_field, 0}
        cdef int r = 0
        
        self._c_addr    = lib7.create_addr()
        #print("set from string")
        
        if file != None:
            super().__init__(file)
        
        else:
            self._c_struct  = lib7.create_message_t(NULL)
            #self.meta._c_meta = self._c_struct.meta
            self._c_meta._c_set_ref(self._c_struct.meta)

            # set default meta behaviour to CR line ending
            self._c_struct.meta.crlf = 0
            
            r = set_addr_from_string("MSH-1", &self._c_addr)

            #print("Composer.__init__()")
            delim[0] = self._c_struct.meta.sep_field
            delim[1] = 0
            #print("before node")
            node_set_by_addr(self._c_struct, self._c_addr, delim, 2)
            #print("after node")
            
            # encoding from python 3 is always UTF-8
            r = set_addr_from_string("MSH-18", &self._c_addr)
            node_set_by_addr(self._c_struct, self._c_addr, "UTF-8", 6)
    
    def set(self, addr: str, data: str, hl7_escape=False):
        """Set a data of a `Node` (create `Node` if it doesn't exist)

        FIXME: hl7_escape here does not make sense. Either decode while reading from file or while writing to file

        Parameters
        ----------
        addr : str
            HL7 `Node` address as String. see `Understanding Addressing`_ for details.

        data : str
            Raw data to write to the node. This data will be stored as bytearray a C `unsigned char *`

        hl7_escape : bool
            hl7 escape all characters such as (``\\n`` to ``\\.b\\`` etc.)

        """
        cdef int r = 0
        cdef unsigned char *c_data_escaped = NULL
        cdef int out_length = 0

        addr_byte_string = addr.encode('UTF-8')
        cdef char* c_addr = addr_byte_string
        r = set_addr_from_string(c_addr, &self._c_addr)
        if r != 0:
            raise Hl7AddressException(addr_byte_string, r)
        #print("address parsed")
        # handle input string
        data_byte_string = data.encode('UTF-8')
        cdef unsigned char* c_data = data_byte_string
        cdef int l = strlen(<const char*> c_data) + 1
        #print("data encoded")

        # hl7 escape ?
        if hl7_escape:
            #int hl7_escape(unsigned char *inp, unsigned char **outp, 
            #               int in_length, int *out_length, hl7_meta_t *meta);
            lib7.hl7_escape(c_data, &c_data_escaped, l, &out_length, self._c_struct.meta)
            r = node_set_by_addr(self._c_struct, self._c_addr, c_data_escaped, out_length)
            pass
        else:
            r = node_set_by_addr(self._c_struct, self._c_addr, c_data, l)
        
        if r != 0:
            raise Hl7SetNodeException(addr, r)

        # get rid of manually allocated memory
        if hl7_escape:
            free(c_data_escaped)
    
    def write_file(self, file: str):
        """Write current data to file

        If you would like to use different delimiters than the original file
        (in case you opened one) or the defaults, use `Message.meta` to 
        change them before writing the file to disk.

        Parameters
        ----------
        file : str
            File name pointing to the HL7 file

        """

        # convert file name into c_string
        py_file = file.encode('UTF-8')
        cdef char* c_file = py_file

        # write message
        len = message_to_file(self._c_struct, c_file)

        # check for errors
        if len < 0:
            raise IOError("Failed to write file {}, error code: {}".format(file), len)
        
        # cleanup
        #print(f"len: {len}")
        #free(c_file)


def read(file: str) -> Message:
    """Open HL7 file and return parsed Composer Object

    Parameters
    ----------
    file : str
        File name pointing to the HL7 file

    Returns
    -------
    Message
        The message object which provides access to all  :py:class:`Node`
    """
    
    msg: Message = Message(file)
    return msg

def search_file(file: str, search_term: str):
    """fast search of data in Nodes
    
    **NOTE:** this interface is royall broken and should not *yet* be used. It will probably segfault.
    """

    py_file = file.encode('UTF-8')
    cdef char *c_file = py_file

    py_search_term = search_term.encode('UTF-8')
    cdef unsigned char *c_search_term = py_search_term

    cdef flags_t *flags = lib7.create_flags_t()
    cdef search_res_t *res = create_search_res(c_search_term)

    print(search_term)
    cdef FILE *_c_fd = hl7_open(c_file)
    cdef int addr_l = 0
    cdef char *c_addr = "MSH-3"
    cdef hl7_addr_t **addr = parse_address(c_addr, &addr_l)
    print("Number of parsed addresses: " + str(addr_l))

    res.greedy = 1
    res.file = strdup(c_file)
    res.addr = addr
    res.addr_l = addr_l
    # FIXME: segfaults in _search_subnodes
    """
    cdef int ret = search_node(_c_fd, res)
    print("Return value: " + str(ret))
    """

def compose(file: str = None) -> Composer:
    """ compose an empty message (file=None) or parse a hl7 file
    
    This method returns a composer. The composer is able to 
    manipulate a message with self.set(...).

    if (file == None) then a boilerplate MSH segment is created.
    
    See also:

    - :py:meth:`lib7.Composer.set`: set a node by address, it will be created if it doesn't exist
    - :py:meth:`lib7.Composer.write_file`: write the created or modified file
    - :py:class:`lib7.Message`: all the other methods inherited from `Message`
    - :py:class:`lib7.Node`: every child of `Message` is a `Node`

    """
    return Composer(file)
