from setuptools import Extension, setup
from Cython.Build import cythonize
from Cython.Compiler import Options
import os, sys
sys.path.insert(0, 'src')
from version import __version__ as v

# this is only necessary when not using setuptools/distribute
#from sphinx.setup_command import BuildDoc
#cmdclass = {'build_sphinx': BuildDoc}

# https://opendreamkit.org/2017/06/09/CythonSphinx/
def isfunction(obj):
    return hasattr(type(obj), "__code__")

import inspect
inspect.isfunction = isfunction

def version():
    version = v
    if "-SNAPSHOT" in version:
        parts = version.split(".")
        version = parts[0] + "." + parts[1] + "."
        patchlevel = parts[2].replace("-SNAPSHOT", "")
        version += "dev" + patchlevel
    return version

"""
version = ""
with open("csource/version_parser.h", 'r') as f:
    for line in f:
        if line[0:5] == "const":
            kv = line.split("=")
            v = kv[1]
            # remove quotes and semicolon
            v = v.replace("\"", "")
            # setuptools is unhappy with -SNAPSHOT in version
            #v = v.replace("-SNAPSHOT", "")
            
            version = v.replace(";", "").strip()
            break
"""

with open("README.md", 'r') as f:
    long_description = f.read()

debug_symbols = False
DEBUG = os.getenv("DEBUG")
if DEBUG and DEBUG.lower == "true":
    debug_symbols = True
    print("building with gdb_symbols")

ext_modules = [
    Extension(
        "lib7",
        sources=["lib7.pyx"],
        include_dirs=["src"],
    )
]

# set compiler options
Options.docstrings = True

setup(
    name="lib7",
    version = version(),
    author="Simon Wunderlin",
    author_email="swunderlin@gmail.com",
    description="hl7 parser and composer library",
    long_description=long_description,
    long_description_content_type='text/markdown',
    url="https://gitlab.com/wunderlins/hl7parse/-/tree/7COMPOSE/",
    project_urls = {
        "Documentation": "http://spliffy.tx0.org/7view/doc/pylib7/",
        "Download": "http://spliffy.tx0.org/7view/"
    },
    ext_modules=cythonize(
        ext_modules, 
        language_level=3,
        gdb_debug=debug_symbols,
        # Configure optional Cython coverage.
        compiler_directives = { 
            #"language_level": sys.version_info[0],
            "embedsignature": True,
        }
    ),
    classifiers=[
        "Programming Language :: Python :: 3 :: HL7",
        "License :: OSI Approved :: LGPL License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    #cmdclass=cmdclass
)