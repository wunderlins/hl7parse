from libc.stdio cimport FILE

cdef extern from "bom.h":
    cdef struct bom_t:
        char *bom;
        int length;

cdef extern from "meta.h":
    
    cdef enum line_delimiter_t:
        # undefined, not found
        DELIM_NONE = 0,
        # Default HL7: 0x0D
        DELIM_CR,  
        # Unix: 0x0A
        DELIM_LF,  
        # Windows: 0x0D, 0x0A (must be other than all others ;)
        DELIM_CRLF 

    cdef struct hl7_meta_t: 
        # number of defined seperators
        int field_length; 
        # -1 (unknown), 0 or 1, need to know if we have 2 char message sep
        int crlf; 

        # message separator, defaults to LF
        char sep_message;
        # field separator, default: '|'
        char sep_field;
        # component separator, default: '^'
        char sep_comp;
        # field repetition separator, default: '~'
        char sep_rep;
        # escape character, default: '\'
        char sep_escape;
        # su component separator, default: '&'
        char sep_subcmp;

        # file encoding from MSH-18
        char *encoding;
        # hl7 version from MSH-12
        char *version;
        # message type from MSH-9.1
        char *type;
        # message sub type from MSH-9.2
        char *subtype;
        # bom sequence and length
        bom_t *bom;
    
cdef extern from "node.h":

    cdef enum node_type_t:
        MESSAGE     = 1
        SEGMENT     = 2
        FIELDLIST   = 4
        FIELD       = 8
        COMP        = 16
        SUBCOMP     = 32
        LEAF        = 64
    
    ctypedef struct raw_field_t: 
        # byte array containing the raw field character data including delimiters
        unsigned char *field;
        # array of delimiter characters
        unsigned char delim[1000];
        # array of positions of the above delimiting characters
        unsigned int  pos[1000];
        # length of the delimiter array
        unsigned int delim_l;
        size_t length;

    ctypedef struct node_t: 
        int type;
        int id;

        node_t *parent;
        node_t **children;

        int num_children;
        int _num_children_allocated;

        unsigned char *data;
        size_t length;
        int pos;

    ctypedef struct message_t: 
        int type;
        int id;
        
        # list of nodes
        node_t *parent;
        node_t **segments;

        int num_children;
        int _num_children_allocated;

        hl7_meta_t *meta;

cdef extern from "address.h":
    ctypedef struct hl7_addr_t:
        char* segment;
        int fieldlist;
        int field;
        int comp;
        int subcmp;
        int seg_count;

    ctypedef struct seg_count_t:
        unsigned int length;
        unsigned int _allocated;
        int* count;
        char** segments;


cdef extern from "buffer.h":
    ctypedef struct buf_t:
        int length
        int allocated
        unsigned char *buffer

cdef extern from "search.h":
    ctypedef struct flags_t:
        # verbose
        int verbose;
        # search term
        int search_term;
        # greedy
        int greedy;
        # json output
        int output_json;
        # xml output
        int output_xml;
        # csv output
        int output_csv;
        # address to search in
        int address;
        # search_term
        unsigned char *search_term_value;
        # address string
        char *address_value;
        # quiet, output values only 
        int quiet;
        # base64 decode values
        int decode64;
        # use output file instead of stdout
        int output_file;
        # output file name
        char* output_file_value;
        # output file handle
        FILE *output_fd;
        # case insensitive search
        int case_insensitive;

    cdef enum search_mode_t:
        # stupid, dumb case insensitive search over file
        SEARCH_SUBSTRING = 0
        # search only lines which start with segment name
        SEARCH_SEGMENT   = 1
        # parse segments and search in specific fields
        SEARCH_NODE      = 2

    ctypedef struct result_item_t:
        char *file;
        int line_num;
        # position of result in segment or line
        int pos;      
        # optional, for segment based searches
        hl7_addr_t *addr; 
        # field or result content
        char *str;
        # length of the data buffer confusingly named str
        int length;

    ctypedef struct search_res_t:
        char *file;
        hl7_addr_t **addr;
        int addr_l;
        int greedy;
        unsigned char *search_term;
        int length;
        result_item_t **items;

## implementation ##############################################################
cdef extern from "globals.c":
    const int loglevel

cdef extern from "logging.c":
    void logprint(const char *fmt, ...);
    const char *logtime();

cdef extern from "getline.c":
    ssize_t getdelim(char **lineptr, size_t *n, int delim, FILE *stream);

cdef extern from "base64.c":
    int hl7_64decode(char, size_t, unsigned char, size_t);

cdef extern from "bom.c":
    bom_t* detect_bom(FILE *fd)

cdef extern from "meta.c":
    hl7_meta_t* init_hl7_meta_t()
    void free_hl7_meta(hl7_meta_t *hl7_meta)
    int read_meta(hl7_meta_t *hl7_meta, FILE *fd)
    char *hl7_meta_string(hl7_meta_t* meta)

cdef extern from "util.c":
    void *memdup(void* src, size_t length)

cdef extern from "node_util.c":
    unsigned char *node_to_string(node_t *node, hl7_meta_t *meta, int *length)
    int message_to_file(message_t *message, char *filename)
    unsigned char *message_to_string(message_t *message)
    int node_get_by_addr(message_t* message, hl7_addr_t *addr, node_t **node)
    int node_set_string(node_t *node, char *data)
    int node_set_by_addr(message_t *message, hl7_addr_t *addr, unsigned char *value, int length)
    
cdef extern from "node.c":
    raw_field_t* create_raw_field_t()
    void free_raw_field(raw_field_t* raw_e)
    void *memdup(void* src, size_t length)
    void dump_structure(message_t *message)
    message_t* create_message_t(hl7_meta_t *meta)
    const char *node_type_to_string(node_type_t type)
    hl7_addr_t* addr_from_node(node_t *node)
    int node_parent_child_pos(node_t *node);

cdef extern from "decode.c":
    int hl7_decode(FILE* fd, message_t **message_p)
    message_t *decode(FILE* fd, hl7_meta_t *meta)
    FILE* hl7_open(char* filename)
    int hl7_close(FILE* fd)
    void free_message_t(message_t *message)

cdef extern from "buffer.c":
    buf_t *new_buf_t();
    void free_buf(buf_t *buffer);
    int append_buf(buf_t *buffer, int add, unsigned char* data);
    int append_bufc(buf_t *buffer, unsigned char c);

cdef extern from "encode.c":
    int hl7_escape(unsigned char *inp, unsigned char **outp, int in_length, int *out_length, hl7_meta_t *meta)
    int hl7_unescape(unsigned char *inp, unsigned char **outp, int in_length, int *out_length, hl7_meta_t *meta)

cdef extern from "address.c":
    hl7_addr_t *reset_addr(hl7_addr_t *a)
    hl7_addr_t* create_addr()
    hl7_addr_t* addr_from_string(char* str)
    char* addr_to_string(hl7_addr_t* addr)
    int set_addr_from_string(char* str, hl7_addr_t **ret_addr)
    void free_addr(hl7_addr_t* addr)
    void addr_dump(hl7_addr_t* a)

    hl7_addr_t* clone_addr(hl7_addr_t* addr)
    seg_count_t* create_seg_count()
    int get_seg_count(char* segment, seg_count_t* c)
    int add_seg_count(char* segment, seg_count_t* c)
    void free_seg_count(seg_count_t* c)

cdef extern from "search.c":
    flags_t *create_flags_t()
    search_res_t *create_search_res(unsigned char* search_term)
    int append_result(char *file, int line_num, int pos, hl7_addr_t *addr, search_res_t **res, char *str, int length)
    hl7_addr_t **parse_address(char *addrstr, int *length)
    #int search_file(char *filename, flags_t flags)
    int search_substring(FILE* fd, search_res_t *sr)
    int search_segment(FILE* fd, search_res_t *sr)
    int search_node(FILE* fd, search_res_t *sr)

cdef extern from "message_state.c":
    ctypedef struct message_state_t: 
        # fire progress callback every N bytes
        int progress_every

        # The total length of the parsed file
        size_t parsed_length

    # progress callback
    void (*cb_progress)(message_t *message, size_t total, size_t current);

    # start event, will be fired once when the parser starts
    void (*cb_start)(message_t *message);

    # finish callback. 
    void (*cb_end)(message_t *message, size_t max, size_t current, int exit_code);

    # segment parsed callback
    void (*cb_segment)(message_t *message, size_t num, char name[3]);

    # @brief initializes an message_state_t structure
    message_state_t *message_state_new();

    # @brief frees an messate_state_t structure
    void message_state_free(message_state_t *ms);
