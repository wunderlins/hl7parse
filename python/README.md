# lib7

This is the pythonized interface to the C HL7 library [lib7](https://gitlab.com/wunderlins/hl7parse).

- [Documentation][1]
- [Downloads](http://spliffy.tx0.org/7view/)
- [Source](https://gitlab.com/wunderlins/hl7parse/-/tree/master)

# interface definition

## Objects

- Message 
- Node (Segments and Nodes)
- Composer(Message)

The `Message` contains a parsed HL7 file, or is a container for Segments. 
`Composer` contains the same features as `Message` but can alter hl7
file content with `composer.set(...)`.

`Node` represents any node (including segments) in a hl7 message. It can have
children. If it has children, then data will always be None.

Details documentation can be found [online][1].

## Adressing

```
MSH|...||||||||ORU&data^R01~rep1

MSH(1)-9(1).2.1 <--- all optional address values
 ^  ^  ^ ^  ^ ^
 |  |  | |  | +---- SUBCOMP
 |  |  | |  +------ COMPONENT
 |  |  | +--------- FIELD (optimal or should it default to 1????) repetition
 |  |  +----------- FIELDLIST
 |  +-------------- SEGMENT (optional, defaults to 1) repetition
 +----------------- SEGMENT

MSH                                SEGMENT
   |   ||||||||                    FIELDLIST
    ...                    ~rep1   FIELD
               ORU     ^R01        COMPONENT
                  &data            SUBCMP


```

Addressing in HL7 (as it is used) is often ambigous. The 2 cases where tis 
is true is by addressing SEGMENTs and FIELDs.

As an example:

```
REP|0|f1~rep1|f2
REP|1|f21|f2~rep2
```

Here we see the 2 repetitions:
- a segment can occour multiple times in a hl7 message (typically `OBR`) or in this case `REP`
- a field (typically delimited by `|` can have multiple repetitions delimited by `~`

Often the value `f1` is address by `REP-2` but this would return `f1~rep1` from the first line
and `f21` from the 2nd line. The correct addres to find `f1` would be:

    REP(1)-2(1)

or to find `f21`

    REP(2)-2(1)
    
TODO: `Mesage.get(addr)` will always find the first SEGMENT and will return a 
pointer to the FIELDLIST. However it would make more sense to return the first 
field always and introduce a 2nd method `Message.getAll(addr)` which will then 
return a list of all possible nodes by ambigously written addresses.

# Installation

## install source package with pip

`make dist` will build a binary and source package found in the `dist/` folder. 
`make cython` will just build the shared object, located in this folder. 

### install as user

    pip install --user dist/hl7-1.0.2-SNAPSHOT.tar.gz

### uninstall as user

    pip uninstall -y "$(pip freeze --user | grep hl7)"

https://cython.readthedocs.io/en/latest/src/tutorial/index.html

## building from source

`make dep all`

The python module can be found in this folder called `lib7-py-version.so` or `lib7-py-version.dll` on windows.

In `dist` there will be 2 files located, 
- `hl7-*.whl` is a binary distribution fo this target architecture/platform
- `hl7-version.tar.gz` is the source package, that can be built and installed with `pip`

Additionally the html documentation will be created in:
- `doc/_build/html/`

## Links

- [Sphinx cross referencing](https://www.sphinx-doc.org/en/master/usage/restructuredtext/domains.html#cross-referencing-python-objects)
- [Sphinx markaup reference](https://docutils.sourceforge.io/docs/user/rst/quickref.html#substitution-references-and-definitions)

[1]: http://spliffy.tx0.org/lib7/python/