#!/usr/bin/env python

import unittest
import os, sys
sys.path.insert(0, "..")

import lib7

test_data = "" + \
    "MSH|^~\&|FDHL7|JOHNSON LABS||P1055|201007231634||ORU^R01|P1055-0000047907|P|2.3|1||NE|NE||UNICODE\r" + \
    "PID|10~11^20~21&210~2^\r"


class BaseTest(unittest.TestCase):
    """Testing top level hl7 methods"""
    msg = None
    file_path = None

    def setUp(self):
        """ executed before every test suite """

        # create data file
        if self.file_path == None:
            tmp = None
            if os.path.isdir("/tmp"):
                tmp = "/tmp"
            else:
                tmp = os.getenv("TEMP")
            
            # write test file
            self.file_path = tmp + "/test.hl7"
            with open(self.file_path, "w") as f:
                # Writing data to a file
                f.write(test_data)

        self.msg = lib7.read(self.file_path)

    def tearDown(self):
        """ run after every test suite """
        os.remove(self.file_path)
        del(self.msg)

class TestParse(BaseTest):
    def test_init(self):
        self.assertEqual(type(self.msg).__name__, "Message")
        self.assertEqual(self.msg.num_children, 2)
        self.assertRaises(IndexError, lambda: self.msg.next_sibling)
    
    def test_msh(self):
        t1 = self.msg.get("MSH-9.1")
        t2 = self.msg.get("MSH-9.2")
        del(t1)
        t1 = self.msg.get("MSH-9.1")
        self.assertEqual(str(t1), "ORU")
        self.assertEqual(t1.data, "ORU")
        self.assertEqual(str(t2), "R01")
        self.assertEqual(t2.data, "R01")

class TestAccess(BaseTest):
    def test_access_after_delete(self):
        t1 = self.msg.get("MSH-9.1")

        # FIXME: this will cause an segmentation fault
        #        becuase self.msg_c_struct is gone
        #del(self.msg)
        #print(t1)

        # try to acces Meta based data
        # try to access Message based data
        # try to access Node based data

class TestMeta(BaseTest):
    def test_read(self):
        meta = self.msg.meta
        def set_sep_message(v):
            meta.separator.segment = v
        self.assertRaises(ValueError, lambda: set_sep_message("ab"))
        self.assertEqual(False, meta.crlf)
        self.assertEqual("\r", meta.separator.segment)
        #print(meta.crlf)
        #print(meta.separator.message)


    def test_write(self):
        meta = self.msg.meta
        
        meta.separator.segment = "a"
        self.assertEqual("a", meta.separator.segment)

        meta.separator.field = "b"
        self.assertEqual("b", meta.separator.field)

        meta.separator.comp = "c"
        self.assertEqual("c", meta.separator.comp)

        meta.separator.rep = "d"
        self.assertEqual("d", meta.separator.rep)

        meta.separator.escape = "e"
        self.assertEqual("e", meta.separator.escape)

        meta.separator.subcmp = "f"
        self.assertEqual("f", meta.separator.subcmp)

class TestNode(BaseTest):

    ## trversal test to top
    def test_traverse(self):
        t1 = self.msg.get("MSH-9.1")
        p = t1.parent
        t11 = p.first_child
        self.assertEqual(t1, t11)
        t111 = p.child_at(0)
        self.assertEqual(t1, t111)
        last1 = p.last_child
        last2 = p.child_at(p.num_children-1)
        self.assertEqual(last1, last2)

    def test_multiple_message_ref(self):
        m = self.msg.get("MSH")
        m2 = m
        #print("type: " + m.type_to_string())
        p = m.parent
        p2 = p
        #print("type: " + p.type_to_string())
        self.assertEqual(p.id, p2.id)
        self.assertEqual(p.type, lib7.Type.MESSAGE)

    def test_traverse_top(self):
        t1 = self.msg.get("MSH-9.1")

        p = t1
        while p.parent != None:
            p = p.parent
        
        self.assertEqual(p.type, lib7.Type.MESSAGE)
        #print("%r" % p)
    
    ## traversal tests to bottom
    def test_traverse_to_msh911(self):
        m = self.msg.get("MSH")
        m9 = m.child_at(8) # msh-9, not sure if this addressing is helpful
        m91= m9.first_child.first_child # msh-9(1).1

        m91_2 = self.msg.get("MSH-9.1")
        self.assertEqual(m91, m91_2)

        # test index error on siblings
        self.assertRaises(IndexError, lambda: m91.next_sibling.next_sibling)
        self.assertRaises(IndexError, lambda: m91.previous_sibling)

        # test children via children array
        c = m9.first_child.children
        self.assertEqual(len(c), m9.first_child.num_children)

        # test children via __iter__
        count = 0
        for e in m9.first_child:
            count += 1
        self.assertEqual(count, m9.first_child.num_children)

if __name__ == '__main__':
    unittest.main()