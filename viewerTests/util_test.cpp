#include "util_test.h"
#include "node_util.h"
#include "../viewer/view_util.h"

utilTest::utilTest(QObject *parent) : QObject(parent) {}
utilTest::~utilTest() {}

void utilTest::initTestCase() {
    qInfo() << "Startup";
    QString dir = QCoreApplication::applicationDirPath().append("/../data");
    m_data_dir.setFileName(dir);
    //qInfo() << "Data Directory: " << m_data_dir.fileName();

    // setup parsed message
    QFile b64file(m_data_dir.fileName() + "/minimal_pid_CR_bas64_LF.hl7");
    if (!b64file.exists())
        QFAIL("Data directory not found.");
    
    // time to parse the file
    FILE *fd = fopen(b64file.fileName().toStdString().c_str(), "rb");
    hl7_meta_t *meta = init_hl7_meta_t();
    m_message = decode(fd, meta);
    fclose(fd);

    if (m_message == NULL)
        QFAIL("Failed to parse test file: minimal_pid_CR_bas64_LF.hl7");
}

void utilTest::cleanupTestCase() {
    //qInfo() << "Cleanup";
}


void utilTest::test_b64_field() {
    //qInfo() << "test_b64_field";

    // setup test data
    node_t *node_b64 = nullptr; // pointer to node with base64 data
    node_t *node_char = nullptr; // pointer to node with single digit number
    node_t *node_escaped = nullptr; // MSH-2 should always have an escape character 

    int ret = 0;
    QString str_addr("WT3-3");
    hl7_addr_t *addr = addr_from_string((char *) str_addr.toStdString().c_str());
    ret = node_get_by_addr(m_message, addr, &node_b64);

    if (ret != 0) {
        char msg[100] = {0};
        sprintf(msg, "Failed to fetch WT3-3 from: minimal_pid_CR_bas64_LF.hl7: %d", ret);
        QFAIL(msg);
    }

    addr->fieldlist = 2;
    ret = node_get_by_addr(m_message, addr, &node_char);

    if (ret != 0) {
        char msg[100] = {0};
        sprintf(msg, "Failed to fetch WT3-2 from: minimal_pid_CR_bas64_LF.hl7: %d", ret);
        QFAIL(msg);
    }

    QString str_addr2("MSH-2");
    addr = addr_from_string((char *) str_addr2.toStdString().c_str());
    ret = node_get_by_addr(m_message, addr, &node_escaped);
    if (ret != 0) {
        char msg[100] = {0};
        sprintf(msg, "Failed to fetch MSH-2 from: minimal_pid_CR_bas64_LF.hl7: %d", ret);
        QFAIL(msg);
    }

    // initialize stub, check defaults
    view_util b64(node_b64, m_message->meta);
    QVERIFY2(b64.get_chunk_size() == 500, "chunk_size is not 500");
    QVERIFY2(b64.get_stub().length() == 500, "stub length is not 500");

    view_util ch(node_char, m_message->meta);
    QVERIFY2(ch.get_chunk_size() == 500, "chunk_size is not 500");
    QVERIFY2(ch.get_stub().length() == 1, "stub length is not 1");

    view_util enc(node_escaped, m_message->meta);
    QVERIFY2(enc.get_stub().length() == 4, "stub length is not 4");

    // check if is b64 encoded
    QVERIFY2(b64.is_base64_encoded() == YES, "WT3-3 content should be base64 encoded");
    QVERIFY2(ch.is_base64_encoded() == NO, "WT3-2 content should not be base64 encoded");

    // check mime
    QString mime_64   = b64.get_mime();
    QString mime_char = ch.get_mime();
    //qInfo() << mime_64 << " " << mime_char;
    QVERIFY2(b64.get_stub_decoded().length() && b64.get_stub_decoded().at(0) != node_b64->data[0], "node_b64 is not base64 decoded");
    QVERIFY2(mime_64.compare("application/pdf") == 0, "mime type should be application/pdf");
    QVERIFY2(mime_char.compare("text/plain") == 0, "mime type should be text/plain");

    QVERIFY2(b64.get_file_suffix().compare("pdf") == 0, "file suffix should be pdf");
    QVERIFY2(ch.get_file_suffix().compare("txt") == 0, "file suffix should be txt");

    // check if hl7 encoded
    QVERIFY2(b64.is_hl7_encoded() == false, "b64 should not be hl7 encoded");
    QVERIFY2(ch.is_hl7_encoded() == false, "ch should not be hl7 encoded");
    QVERIFY2(enc.is_hl7_encoded() == true, "enc should be hl7 encoded");
}
