#ifndef UTI_TEST_H
#define UTI_TEST_H

#include <QtTest/QtTest>
#include <QString>
#include <QFile>
#include <QProcessEnvironment>
#include "decode.h"

class utilTest : public QObject
{
    Q_OBJECT
public:
    explicit utilTest(QObject *parent = nullptr);
    ~utilTest();
private:
    message_t *m_message = nullptr;
    QFile m_data_dir;

private slots:
    // predefined
    void initTestCase();
    void cleanupTestCase();

    void test_b64_field();

};


#endif // UTI_TEST_H