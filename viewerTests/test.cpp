// http://imaginativethinking.ca/qtest-101-writing-unittests-qt-application/

#include <QCoreApplication>
#include "util_test.h"

int main(int argc, char *argv[])
{
    // set application command line parameters
    /*
    const char *myargv[5];
    myargv[0] = "-c";
    myargv[1] = "-m=with-param";
    myargv[2] = "--fid=1234";
    myargv[3] = "--pid=5678";
    myargv[4] = "../../curl-7.76.1.zip";
    int myargc = 5;
    **/

    // initialize application with our args
    QCoreApplication app(argc, (char **) argv);
    utilTest util;

    // initialize test with command line args from main()
    QTest::qExec(&util, argc, argv);
}
