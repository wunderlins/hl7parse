# Simon's Code of Conduct

The goal of this project is to create the best possible solution for 
a technical problem, not politics.

If you are reading this because you believe certain groups should have 
special rights, then this project is not for you, go somewhere else!

Behave as if you were raised by decent human beings, use common sense. We
are all humans, every one has a bad day now and then. Saying no is not an 
insult.
