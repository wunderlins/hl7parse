# Delimiter 

## Delimiter types

https://docs.microsoft.com/en-us/biztalk/adapters-and-accelerators/accelerator-hl7/message-encodings

The original HL7 encoding defines five delimiters, which each message declares within the MSH segment. These indicate:

- Segments \<cr>
- Fields |
- Components ^
- Subcomponents &
- Repetition ~ (of a field, component, or subcomponent)

## Delimiter Definition


https://docs.microsoft.com/en-us/biztalk/adapters-and-accelerators/accelerator-hl7/message-delimiters

| Delimiter | Value | Usage |
|-----------|-------|-------|
| Segment terminator | \<cr\> 0x0D | A carriage return terminates a segment record. You cannot change this value.
| Field separator | \| | A pipe character separates two adjacent data fields within a segment. This character also separates the segment ID from the first data field in each segment. |
| Component separator |	^ |A hat character separates adjacent components of data fields where allowed by the HL7 standard. |
| Repetition separator | ~ | A tilde character separates multiple occurrences of components or subcomponents in a field where allowed by the HL7 standard. |
| Escape character| \ |You use an escape character with any field that conforms to an ST, TX, or FT data type, or with the data (fourth) component of the ED data type. If no escape characters exist in a message, you can omit this character. However, you must include it if you use subcomponents in the message. |
| Subcomponent separator | & | An ampersand character separates adjacent subcomponents of data fields where allowed by the HL7 standard. If there are no subcomponents, then you can omit this character. |

## Escape sequences

http://healthstandards.com/blog/2006/11/02/hl7-escape-sequences/

| Character | Description Conversion | 
| -------- | -- |
| \Cxxyy\ | Single-byte character set escape sequence with two hexadecimal values not converted |
| \E\ | Escape character converted to escape character (e.g., ‘\’) |
| \F\ | Field separator converted to field separator character (e.g., ‘|’) |
| \H\ | Start highlighting not converted |
| \Mxxyyzz\ | Multi-byte character set escape sequence with two or three hexadecimal values (zz is optional) not converted |
| \N\ | Normal text (end highlighting) not converted |
| \R\ | Repetition separator converted to repetition separator character (e.g., ‘~’) |
| \S\ | Component separator converted to component separator character (e.g., ‘^’) |
| \T\ | Subcomponent separator converted to subcomponent separator character (e.g., ‘&’) |
| \Xdd…\ | Hexadecimal data (dd must be hexadecimal characters) converted to the characters  dentified by each pair of digits |
| \Zdd…\ | Locally defined escape sequence not converted |

## Data Types Common Schemas

https://docs.microsoft.com/en-us/biztalk/adapters-and-accelerators/accelerator-hl7/data-types-common-schemas