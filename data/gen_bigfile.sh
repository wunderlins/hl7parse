#!/usr/bin/env bash

out=big_file.hl7
cat min.hl7 | tr "\n" "\r"> $out

b64=$(cat test64.txt)

for i in {1..1000}; do 
    echo $i; 
    echo -ne "B64|$i|$b64\r" >> $out
done

