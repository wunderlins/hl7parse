#!/usr/bin/env C
#include <stdio.h>
#include <string.h>

// bom definitions
static int const bom[15][7] = {
    {4, 0x84, 0x31, 0x95, 0x33}, // GB-18030
    {4, 0xDD, 0x73, 0x66, 0x73}, // UTF-EBCDIC
    {4, 0xFF, 0xFE, 0x00, 0x00}, // UTF-32 LE
    {4, 0x00, 0x00, 0xFF, 0xFF}, // UTF-32 BE

    {4, 0x2B, 0x2F, 0x76, 0x38}, // UTF-7
    {4, 0x2B, 0x2F, 0x76, 0x39}, // UTF-7
    {4, 0x2B, 0x2F, 0x76, 0x2B}, // UTF-7
    {4, 0x2B, 0x2F, 0x76, 0x2F}, // UTF-7

    {3, 0xFB, 0xEE, 0xFF}, // BOCU-1
    {3, 0x0E, 0xFE, 0xFF}, // SCSU
    {3, 0xF7, 0x64, 0x4C}, // UTF-1
    {3, 0xEF, 0xBB, 0xBF}, // UTF-8
    {2, 0xFE, 0xFF}, // UTF-16 BE
    {2, 0xFF, 0xFE}, // UTF-16 LE
};

// read hl7 file template
FILE *fd = fopen("minimal_lf.hl7", "rb");
char template[500] = {0};
fread(template, 1, 500, fd);
fclose(fd);

printf("template:\n%s\n", template);

// loop over all definitions
int i=0;
while(i < 15) {
    int ii=1;
    int l = bom[i][0];
    printf("%02d %02d ", i, bom[i][0]);

    // generate filename
    char filename[100] = {0};
    while (ii <= l) {
        printf("%02X", bom[i][ii]);
        char hex[3] = {0};
        sprintf(hex, "%02X", bom[i][ii]);
        strcat(filename, hex);
        ii++;
    }
    strcat(filename, "_minimal_lf.hl7");
    printf(" %s\n", filename);

    // open file and write to it
    fd = fopen(filename, "wb");
    ii = 1;
    while (ii <= l) {
        fprintf(fd, "%c", bom[i][ii]);
        ii++;
    }
    fprintf(fd, "%s", template);
    fclose(fd);

    i++;
}
