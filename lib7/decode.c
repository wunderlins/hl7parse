#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "bom.h"
#include "meta.h"
#include "node.h"
#include "logging.h"
#include "util.h"

#include "../generated/version_parser.h"
#include "../generated/version_git.h"

#ifdef __MINGW32__
    #include "strndup.h"
#endif

const char *version_parser() {
    return VERSION_PARSER;
}

void print_error(int e, char* additional) {
	if (additional == NULL)
		fprintf(stderr, "%d, %s\n", e, strerror(e));
	else 
		fprintf(stderr, "%d, %s, %s\n", e, strerror(e), additional);
}

unsigned char* extract_substr(int start, int length, unsigned char *buffer) {
    unsigned char *field = (unsigned char*) malloc(sizeof(char) * (length+1));
    memcpy(field, &buffer[start], length);
    field[length] = 0;
    log_trace("[%d,%d], '%s', '%s'\n", start, length, buffer, field);
    return field;
}

int parse_segment(FILE *fd, hl7_meta_t* meta, node_t **fieldlist_p, unsigned char **segment_name) {
    log_debug("parse_segment()");

    // field buffer
    unsigned char *field = NULL;

    // field counter
    int field_c = 0;

    // field length, this length is excluding escape characters
    int field_l = 0;

    // length of the input message buffer
    int i = 0; 

    // current and last characters
    char c = 0;

    // initial line buffer size. will be enlarged as needed
    int buf_size = 1000;

    // last delimiter, this is the last found delimiter of any kind
    char last_delimiter = 0;

    // return value
    int ret = 0;

    // allocate a buffer with 1000 bytes. This is to hold the MSH header 
    // and should be more than enough, J/K, we dynamically resize
    unsigned char *buffer = (unsigned char*) malloc(buf_size);
    if (buffer == NULL)
        return 1;
    buffer[0] = 0;
    
    // raw element container
    raw_field_t *raw_e = create_raw_field_t();
    if (raw_e == NULL)
        return 5;
    
    // we have always at least one FIELDLIST with one FIELD, even if the 
    // field is empty: ||
    node_t *fieldlist = create_node_t(FIELDLIST, NULL, 0, 0);
    if (fieldlist == NULL) return 6;
    ret = node_append(fieldlist_p, fieldlist);
    if (ret != 0)
        return 8;

    // reference to the message
    message_t *message = (message_t*) (*fieldlist_p)->parent;

    // setup progress callback
    int fire_progress_callback = 0;
    size_t file_pos = 0;
    if (message != NULL && message->type == MESSAGE && 
        message->state->cb_progress != NULL && 
        message->state->progress_every ) { // prevent division by 0
        fire_progress_callback = 1;
        file_pos = ftell(fd);
    }

    c = getc(fd);
    char next_c = 0;
    while (c != EOF) {

        // this is a safeguard for over long segment names, it shoulkdn't be 
        // longer than 3 bytes. we tolerate 5, but otherwise it is clearly a bug
        // and we exit
        if (field_c == 0 && i > 5) {
            return 12;
        }

        // fire progress callback ?
        if (fire_progress_callback && (file_pos+i) && // prevent division by 0
            ((file_pos+i) % message->state->progress_every) == 0) { 
            message->state->cb_progress(message, message->state->parsed_length, file_pos+i);
        }

        next_c = getc(fd);
        //log_trace("next_c: %c, c: %c", next_c, c);

        /**
         * @todo
         * we fail on empty lines, deal with them
         */
        
        // reallocate a larger buffer
        if (i == buf_size) {
            buf_size += buf_size;
            buffer = (unsigned char*) realloc(buffer, buf_size);

            if (buffer == NULL)
                return 1;
        }
        
        // append new character to buffer
        buffer[i] = c;
        field_l++;

        // set crlf if it is set to -1
        if (meta->crlf == -1 && (c == '\r' || c == '\n')) {
            if (next_c == '\n')
                meta->crlf = 1;
            else {
                meta->crlf = 0;
            }
            
            // remeber the message separator
            log_debug("Setting delim to \\%02X, crlf: %d", c, meta->crlf);
            meta->sep_message = c;
            
            buffer[i] = 0;
        }

        /**
         * ### automatic metadata detection
         * 
         * It is safest to first run read_meta() before calling parse_segment().
         * If you know that this is an `MSH` segment (and only then) it is 
         * safe to skip read_meta() prior to running parse_segment().
         * 
         * If read_meta() is not run, then we
         * we go in meta detection mode and expect the field separator at 
         * position 3 of this line.
         * 
         * Not sure if this is really a good idea? We assume that the segment
         * name is always 3 characters which seems to be true most of the 
         * time (always?).
         * 
         * You may override this by first manually detecting all separators and 
         * feeding parse_segment with a complete metadata structure.
         * 
         * @note
         * we only jock here if read_meta() has not been used
         */
        if (i == 3 && meta->crlf == -1) {
            /**
             * @bug
             * if crlf is -1 and the first 3 characters are not 
             * MSH, then we have to trust meta or stop here
             */
            meta->sep_field = c;
        }

        // append delimiter to raw field
        if (c == meta->sep_comp || c == meta->sep_subcmp) {
            raw_e->delim[raw_e->delim_l] = c;
            raw_e->pos[raw_e->delim_l]   = field_l-1;
            raw_e->delim_l++;

            if (raw_e->delim_l == MAX_FIELDS) {
                log_fatal("We have reached %d delimiters in a field, this is more than we "
                          "can handle. Increase MAX_FIELDS and recompile.", raw_e->delim_l);
                return 4;
            }
        }

        log_trace("c: %02X, next_c: %02X", c, next_c);
        
        // check if we have a sub delimiter
        if (c == meta->sep_rep || c == meta->sep_field || 
            c == meta->sep_message || next_c == EOF) {
            log_trace("field_c: %d", field_c);
            if (field_c == 0) {
                log_debug("==========> NEW SEGMENT <===========");
            }

            // extract the field data
            int start = i - field_l + 1;
            field = extract_substr(start, field_l-1, buffer);
            
            log_trace("%03d %02d, last_delimiter: %c, current_delimitmer: %c, '%s'", i, field_c, last_delimiter, c, field);

            // process sub componennts
            log_debug("==========> NEW NODE [%02d] <===========", field_c);
            if (field_c != 0) {
                raw_e->field = field;
                raw_e->length = field_l;
                node_t *n = process_node(raw_e, meta, start); 
                if (n == NULL)
                    return 7;
                // append the node to parent's children
                ret = node_append(&fieldlist, n);
                if (ret != 0) {
                    log_error("Failed to allocate memory for node: '%s'", field);
                    return 8;
                }

                // when the delimiter was a field delimiter, then create a new fieldlist
                // and move the fieldlist pointer to it
                if (c == meta->sep_field) {
                    fieldlist = create_node_t(FIELDLIST, NULL, 0, start+4);
                    if (fieldlist == NULL)
                        return 6;
                    node_append(fieldlist_p, fieldlist);
                    if (ret != 0)
                        return 8;

                }
            } else {
                // first element, copy to segment_name
                unsigned char *name = (unsigned char*) malloc(field_l);
                log_debug("Allocating %d for segment name", field_l);
                if (name == NULL)
                    return 9;
                memcpy(name, field, field_l);
                *segment_name = name;
                
                // not an MSH Segment, abort.
                if (meta->crlf == -1 && strcmp((char*) *segment_name, "MSH") != 0) {
                    // automatic delimiter detection failed, file must start with 'MSH'
                    return 10;
                }

                // MSH delimiter handling
                //
                // special treatement if this is an MSH element:
                // 1. detect and write delimiter to meta structure
                // 2. add fieldlist with sep_field as 1.1
                // 3. skip delimiters and add field with delimiter content as 2.1
                //printf("segment: %s\n", *segment_name);
                if (meta->crlf == -1 && strcmp((char*) *segment_name, "MSH") == 0) {
                    // fast fortward file poitner after MSH-2
                    meta->sep_field = c;
                    buffer[i++] = c;
                    unsigned char sep[6] = {
                        meta->sep_comp, meta->sep_rep, 
                        meta->sep_escape, meta->sep_subcmp,
                        0
                    };
                    int si = 0;

                    while (next_c != meta->sep_field) {

                        if (c == EOF || c == '\r' || c == '\n') {
                            // we have an error condition, end of 
                            // file/line reached without finding a 
                            // '|' at the the end of MSH-2.
                            return 11;
                        }

                        if (si == 0)
                            meta->sep_comp = next_c;
                        if (si == 1)
                            meta->sep_rep = next_c;
                        if (si == 2)
                            meta->sep_escape = next_c;
                        if (si == 3)
                            meta->sep_subcmp = next_c;

                        sep[si++] = next_c;
                        buffer[i++] = next_c;
                        
                        c = next_c;
                        next_c = getc(fd);
                    }
                    sep[si] = 0;
                    sep[si+1] = 0;
                    
                    // get rid of 2nd field separator
                    c = next_c;
                    next_c = getc(fd);

                    // add 2 fieldlists, MSH-1.1
                    unsigned char *s = malloc(2);
                    s[0] = meta->sep_field;
                    s[1] = 0;
                    node_t *n11 = create_node_t(FIELD, s, 2, 4);
                    free(s);
                    if (n11 == NULL)
                        return 6;
                    ret = node_append(&fieldlist, n11);
                    if (ret != 0) {
                        log_error("Failed to allocate memory for node: '%s', node_append return code %d", s, ret);
                        return 9;
                    }
                    
                    // and MSH-2.1 containing sep
                    fieldlist = create_node_t(FIELDLIST, NULL, 0, 5);
                    if (fieldlist == NULL)
                        return 6;
                    ret = node_append(fieldlist_p, fieldlist);
                    if (ret != 0) {
                        log_error("Failed to allocate memory for node: '%s', node_append return code %d", s, ret);
                        return 9;
                    }

                    field_c++;
                    node_t *n21 = create_node_t(FIELD, sep, si+1, 5);
                    if (n21 == NULL)
                        return 6;
                    ret = node_append(&fieldlist, n21);
                    if (ret != 0) {
                        log_error("Failed to allocate memory for node: '%s', node_append return code %d", s, ret);
                        return 9;
                    }
                    
                    fieldlist = create_node_t(FIELDLIST, NULL, 0, start+5+si);
                    if (fieldlist == NULL)
                        return 6;
                    
                    ret = node_append(fieldlist_p, fieldlist);
                    if (ret != 0) {
                        log_error("Failed to allocate memory for node: '%s', node_append return code %d", s, ret);
                        return 9;
                    }
                }
            }
            
            // reset delimiters
            if (field != NULL) {
                free(field);
            }
            field = NULL;
            raw_e->field = NULL;
            raw_e->delim_l = 0;
            field_l = 0;
            last_delimiter = c;
            field_c++;
        }

        /*
         * check if we have found a message seperator
         * 
         * If so, process the last field, the remeber the separator and 
         * return with success.
         */
        if (c == meta->sep_message || next_c == EOF) {
            /*
            // check the next character is '\n', then we have windows style
            // line endings (0x0D, 0x0A) which we need to tell the parser by 
            // setting the meta->crlf flag to 1. The parser can then react to 
            // double byte Segment delimiters
            if (meta->crlf == 1 && next_c == '\n') {
                log_debug("Skipping extra \\n");
                buffer[i] = next_c;
            } else { 
                log_debug("No extra \\n");
            }
            
            // we are done, success. return buffer and erro code 0
            buffer[i+1] = 0; // delimit string

            // some node debugging
            if (loglevel >= LEVEL_TRACE) {
                log_debug("segment->children: %d", (*fieldlist_p)->num_children);
                for(int cnt=0; cnt < (*fieldlist_p)->num_children; cnt++) {
                    log_trace("node: %d, %d %d, '%s'", (*fieldlist_p)->children[cnt]->type, 
                        (*fieldlist_p)->children[cnt]->num_children, 
                        (*fieldlist_p)->children[cnt]->length, 
                        (*fieldlist_p)->children[cnt]->data );
                }
            }
            
            // put last character back into file buffer
            //if (meta->crlf != 1)
            ungetc(next_c, fd);
            */
            // cleanup
            log_debug("done");

            if (c == meta->sep_message)
                log_debug("message separator set");
            if (next_c == EOF)
                log_debug("EOF reached");

            if (buffer != NULL)
                free(buffer);
                
            if (raw_e != NULL)
                free(raw_e);
            
            if (meta->crlf == 0)
                ungetc(next_c, fd);
            
            // populate meta data if we have just successfully parsed an MSH 
            // Field. update type, subtype and encoding
            if (strcmp((char*) *segment_name, "MSH") == 0) {
                //printf("type: %s\n", *segment_name);

                // check if we find a segment type in MSH-9.1
                if ((*fieldlist_p)->num_children > 8 && (*fieldlist_p)->children[8]->children[0]->num_children > 0) {
                    log_debug("Setting meta->type");
                    //printf("field data: %s\n", (*fieldlist_p)->children[8]->children[0]->children[0]->data);
                    meta->type = strdup((char*) (*fieldlist_p)->children[8]->children[0]->children[0]->data);
                    // check if we find a segment subtype in MSH-9.2
                    if ((*fieldlist_p)->children[8]->children[0]->num_children > 1) {
                        log_debug("Setting meta->subtype");
                        //printf("field data: %s\n", (*fieldlist_p)->children[8]->children[0]->children[1]->data);
                        meta->subtype = strdup((char*) (*fieldlist_p)->children[8]->children[0]->children[1]->data);
                    }
                }
                log_debug("types set");
                if ((*fieldlist_p)->num_children > 17 && (*fieldlist_p)->children[17]->num_children > 0) {
                    log_debug("Setting meta->encoding");
                    // check if we find a charcter set in MSH-18
                    if ((*fieldlist_p)->children[17]->children[0]->data != NULL)
                        meta->encoding = strdup((char*) (*fieldlist_p)->children[17]->children[0]->data);
                }
            }

            return 0;
        }
        
        c = next_c;
        i++;
    } // end character loop
    
    if (buffer != NULL)
        free(buffer);
        
    if (raw_e != NULL)
        free(raw_e);

    // a pretty rare edge case
    // we just hit EOF on the first fgetc(), this means nothing to do there
    //
    // remove the last fieldlist, it is empty
    if (c == EOF && i == 0) {
        if ((*fieldlist_p)->num_children) {
            (*fieldlist_p)->num_children--;
            free_node_t((*fieldlist_p)->children[(*fieldlist_p)->num_children]);
        }
        return 0;
    }

    log_warning("We should never reach this point. However, it seems as if we just did, didn't we? This means a software developer has royally screwed up. We can't handle this situation, the program is too stupid. Therefore we just die. Oh, jolly good.");
    return 3;
}

int hl7_decode(FILE* fd, message_t **message_p) {

    // get the remaning size of the file to parse. without bom this will be 
    // the size of the file in bytes.
    //
    // This is needed for the progress callback
    size_t file_length = 0;
    //log_debug("fd position: %d", ftell(fd));
    size_t fd_pos = ftell(fd);
    fseek(fd, 0, SEEK_END);
    file_length = ftell(fd);
    fseek(fd, fd_pos, SEEK_SET); // reset
    //log_debug("fd position: %d", ftell(fd));
    
    log_debug("hl7_walk()");
    //hl7_meta_t *meta = (*message_p)->meta;
    
    int ret = 0;

    // message_t must be created before calling hl7_decode
    if (*message_p == NULL) {
        return 1;
    }
    
    message_t *message = *message_p;
    message->state->parsed_length = file_length;

    // check if we have a registered start callback
    if (message->state->cb_start != NULL)
        message->state->cb_start(message); // run callback
    
    // if a progress callback is registered and the frequency of the callback 
    // is unconfigured, set it to 1%
    // 
    // this is suitable for large files. If you are parsing small files over 
    // slow file systems, you may lower this to make the progress callback 
    // fire more frequently. However, be aware that calling it too often on 
    // large files might slow down you application.
    if (message->state->cb_progress != NULL && message->state->progress_every == 0) {
        message->state->progress_every = file_length/100;

        // small files, less than 100 bytes will cause a devision by zero, fix this by 
        // setting progress_every to 1
        //
        // this should be caught by parse_segment()
        //if (!message->state->progress_every)
        //    message->state->progress_every = 1;
        //log_info("setting progress_every to 1%%: %d", message->state->progress_every);
    }

    // parse the MSH line and extract metadata
    node_t *fieldlist = create_node_t(SEGMENT, NULL, 0, 0);
    message_append(&message, fieldlist);
    if (fieldlist == NULL) {
        // free stuff
        free_node_t(fieldlist);
        fieldlist = NULL;
        message->num_children--;

        // check if we have a registered segment callback
        if (message->state->cb_end != NULL)
            message->state->cb_end(message, file_length, ftell(fd), 2); // run callback
        return 2;
    }

    unsigned char *segment_name;
    //ret = parse_msh(fd, meta, &fieldlist, &segment_name);
    ret = parse_segment(fd, message->meta, &fieldlist, &segment_name);
    if (ret != 0) {
        // free stuff
        free_node_t(fieldlist);
        fieldlist = NULL;
        message->num_children--;

        // check if we have a registered segment callback
        if (message->state->cb_end != NULL)
            message->state->cb_end(message, file_length, ftell(fd), ret); // run callback
        return ret;
    }

    // check if the first segement is an MSH segment, else return
    if (strcmp(segment_name, "MSH") != 0) {
        // free stuff
        free_node_t(fieldlist);
        fieldlist = NULL;
        message->num_children--;

        // check if we have a registered segment callback
        if (message->state->cb_end != NULL)
            message->state->cb_end(message, file_length, ftell(fd), 3); // run callback
        return 3;
    }

    fieldlist->data = (unsigned char*) strdup((char*) segment_name);
    fieldlist->length = strlen((char*) segment_name) + 1;
    //log_info("SEGMENT: %s parsed, %d elements", segment_name, fieldlist->num_children);
    
    // check if we have a registered segment callback
    if (message->state->cb_segment != NULL)
        message->state->cb_segment(message, 0, strdup(segment_name)); // run callback

    free(segment_name);

    // part 2: parse subsequent elements
    int l = 2;
    while (!feof(fd)) {
        log_debug("Line: %d", l);
        unsigned char *segment_name = NULL;
        node_t *fieldlist = create_node_t(SEGMENT, NULL, 0, 0);
        message_append(&message, fieldlist);
        ret = parse_segment(fd, message->meta, &fieldlist, &segment_name);
        if (ret != 0) {
            // check if we have a registered segment callback
            if (message->state->cb_end != NULL)
                message->state->cb_end(message, file_length, ftell(fd), ret+10); // run callback
            return ret+10;
        }
        
        // check if we have parsed an empty line
        if (segment_name == NULL || strcmp((char*) segment_name, "") == 0 || !segment_name) {
            log_trace("SEGMENT free by name");
            if (segment_name != NULL) {
                free(segment_name);
                segment_name = NULL;
            }
            log_trace("SEGMENT freeing fieldlist");
            free_node_t(fieldlist);
            message->num_children--;
            fieldlist = NULL;
            continue;
		}

        // skip empty messages
        if (fieldlist->num_children == 0) {
            log_trace("SEGMENT free by child count");
            if (segment_name != NULL) {
                free(segment_name);
                segment_name = NULL;
            }
            free_node_t(fieldlist);
            message->num_children--;
            segment_name = NULL;
            fieldlist = NULL;
            continue;
        }

        fieldlist->data = (unsigned char*) strdup((char*) segment_name);
        fieldlist->length = strlen((char*) segment_name) + 1;
        log_debug("SEGMENT(%d): %s parsed, %d elements", l, segment_name, fieldlist->num_children);
        // check if we have a registered segment callback
        if (message->state->cb_segment != NULL)
            message->state->cb_segment(message, l-1, strdup(segment_name)); // run callback
        
        l++;
        free(segment_name);
        segment_name = NULL;

    }
    
    *message_p = message;
    // check if we have a registered segment callback
    if (message->state->cb_end != NULL)
        message->state->cb_end(message, file_length, ftell(fd), 0); // run callback
    return 0;
}

message_t *decode(FILE* fd, hl7_meta_t *meta) {
    // check if this file has a bom;
    // this will move df at the first byte after the BOM
    meta->bom = detect_bom(fd); 
    
    message_t *message = create_message_t(meta);
    int ret = hl7_decode(fd, &message);
    if (ret != 0)
        return NULL;
        
    log_debug("done parsing, returning message");
    return message;
}

FILE* hl7_open(char* filename) {
    log_debug("Opening file %s", filename);
    
    // HL7 File descriptor
	FILE *fd;
	if ((fd = fopen(filename, "rb")) == NULL) {
		//print_error(errno, filename);
		return NULL;
	}
    
    return fd;
}

int hl7_close(FILE* fd) {
    return fclose(fd);
}
