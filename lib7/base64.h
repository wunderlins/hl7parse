/** \file
 * @brief hl7 base64 decoder and encoder. 
 * 
 * These methods are able to decode hl7 sequence \.br\ on a base64 stream
 */
// https://en.wikibooks.org/wiki/Algorithm_Implementation/Miscellaneous/Base64#C_2
#pragma once

#include <inttypes.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief decode base64 a buffer
 * 
 * @param in char array to decode
 * @param inLen length of input
 * @param out array holding output
 * @param outLen length of output
 * @return 0 on success, 1 invalid input
 */
int base64decode(char *in, size_t inLen, unsigned char *out, size_t *outLen);

/**
 * @brief decode base64 a buffer
 * 
 * skip `\.br\` sequences typically found in hl7 base64 encoded multiline strings.
 * 
 * @param in char array to decode
 * @param inLen length of input
 * @param out array holding output
 * @param outLen length of output
 * @return 0 on success, 1 invalid input, 2 invalid escape sequence
 */
int hl7_64decode(char *in, size_t inLen, unsigned char *out, size_t *outLen);

/**
 * @brief decode base64 buffer to a file
 * 
 * skip `\.br\` sequences typically found in hl7 base64 encoded multiline strings.
 * 
 * @param in char array to decode
 * @param inLen length of input
 * @param out_fd file handle to write result to
 * @return 0 on success, 1 invalid input, 2 invalid escape sequence
 */
int hl7_64decode_fd(char *in, size_t inLen, FILE *out_fd);

/**
 * @brief encode base64
 * 
 * @param data_buf char array to encode
 * @param dataLength length of input
 * @param result array holding output
 * @param resultSize length of output
 * @return 0 on success, invalid input
 */
int base64encode(const void* data_buf, size_t dataLength, char* result, size_t resultSize);

#ifdef __cplusplus
}
#endif