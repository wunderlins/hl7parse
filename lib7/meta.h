/** \file
 * @brief hl7 meta data structures for message_t
 * 
 * meta_t contains information in all hl7 separators of a file and the BOM.
 * 
 * read_meta() can detect the delimiters of a file pointed to by `*fd`.
 * 
 * find_line_delimiter() will seek the first `CR` or `LF` and will 
 * detect if `CRLF` is present. In case of `CRLF` the attribute `crlf` is 
 * set to 1 else 0 or -1 (initial value when no detection has happened yet).
 * 
 * https://docs.microsoft.com/en-us/biztalk/adapters-and-accelerators/accelerator-hl7/message-delimiters
 * 
 * | Delimiter | Value | Usage |
 * |-----------|-------|-------|
 * | Segment terminator | \<cr\> 0x0D | A carriage return terminates a segment record. You cannot change this value.
 * | Field separator | \| | A pipe character separates two adjacent data fields within a segment. This character also separates the segment ID from the first data field in each segment. |
 * | Component separator |	^ |A hat character separates adjacent components of data fields where allowed by the HL7 standard. |
 * | Repetition separator | ~ | A tilde character separates multiple occurrences of components or subcomponents in a field where allowed by the HL7 standard. |
 * | Escape character| \ |You use an escape character with any field that conforms to an ST, TX, or FT data type, or with the data (fourth) component of the ED data type. If no escape characters exist in a message, you can omit this character. However, you must include it if you use subcomponents in the message. |
 * | Subcomponent separator | & | An ampersand character separates adjacent subcomponents of data fields where allowed by the HL7 standard. If there are no subcomponents, then you can omit this character. |
 */
#pragma once 

#include <stdio.h>
#include "bom.h"

/**
 * @brief HL7 Seperator configuration
 * 
 * @see meta.h
 */
typedef struct hl7_meta_t {
    /** number of defined seperators */
    int field_length; 
    /** are messages delimited by one or two bytes?
     * - `-1` (unknown), initial state
     * - `0` delimiter is either `CR` or `LF`
     * - `1` delimiter is `CRLF`
     * 
     * NOTE: in case of `CRLF` `sep_message` will contain `CR`/`0x0D`
     */
    int crlf; 

    /** message separator, defaults to `LF`. in case of `CRLF` this one byte variable will be `CR`. use `hl7_meta_t->crlf` to check */
    char sep_message;
    /** field separator, default: `|` */
    char sep_field;
    /** component separator, default: `^` */
    char sep_comp;
    /** field repetition separator, default: `~` */
    char sep_rep;
    /** escape character, default: `\` */
    char sep_escape;
    /** sub component separator, default: `&` */
    char sep_subcmp;

    /** file encoding from `MSH-18` */
    char *encoding;
    /** hl7 version from `MSH-12` */
    char *version;
    /** message type from `MSH-9.1` */
    char *type;
    /** message sub type from `MSH-9.2` */
    char *subtype;
    /** bom bytes sequence and length */
    bom_t *bom;

} hl7_meta_t;

/**
 * @brief possible line endings
 */
typedef enum line_delimiter_t {
    /** undefined, not found */
    DELIM_NONE = 0,
    /** Default HL7: `0x0D` */
    DELIM_CR,  
    /** Unix: `0x0A` */
    DELIM_LF,  
    /** Windows: `0x0D`, `0x0A` (must be other than all others ;) */
    DELIM_CRLF 
} line_delimiter_t;

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief initialize the seperator data structure
 * 
 * @return initialized hl7_meta_t struct
 */
hl7_meta_t* init_hl7_meta_t(void);

/**
 * @brief generate printable string of the meta data
 * 
 * @param meta a hopefully populated meta data struct
 * @return formatted string
 */
char *hl7_meta_string(hl7_meta_t* meta);

/**
 * @brief free the data structure
 * 
 * make sure nothing points to the elements in this structure. All
 * elements in the structure will be freed (including `bom`).
 * 
 * @param hl7_meta the meta structure
 */
void free_hl7_meta(hl7_meta_t *hl7_meta);

/**
 * @brief find line delimiter
 * 
 * This method will forward an FILE pointer upon it finds the first 
 * `\n`, `\r` or `EOF`. The FILE pointer will be restored to it's 
 * original position.
 * 
 * @param fd file pointer at theb eginnign of a file 
 * @returns 0 if unknown.
 */
line_delimiter_t find_line_delimiter(FILE* fd);

/**
 * @brief read up until the 2nd field delimiter
 * 
 * read up to 8 bytes to find all hl7 delimiters. This method assumes 
 * that it is reading an MSH segment (file pointer must be positioned at 'M').
 * 
 * Do not use it on any other hl7 segment!
 * 
 * This metod will detect all separators if they are defined. If not, separators 
 * will be left at the default. 
 * 
 * The user must make sure to parse the right segment. fd must be 
 * pointing to the start character of an MSH or MSA segment.
 * 
 * The file pointer is reset at the end of this method and reset back to the 
 * byte after the BOM.
 * 
 * If no BOM detection has been done and meta->bom still is NULL, we'll 
 * run detect_bom() first. To prevent this, initialize the bom object 
 * with a length of 0.
 * 
 * @todo
 * check if file length, there might only be 5 characters in it or 
 * less. handle error condition.
 * 
 * @note
 * do not re-use hl7_meta for different files, always detect 
 * separators before parsing.
 * 
 * @note
 * this method fails if there is only one field separator, i.e. `MSH|` 
 * will return `1`. even tho it may or maynot be a valid hl7 file `MSH-2`
 * is not followed by `MSH-1` and therefore a reliable detection is 
 * not possible.
 * 
 * Return codes:
 * - `0`: success
 * - `1`: the field delimiter was not found after the delimiters
 * - `2`: premature file end
 * - `3`: file does not begin with `MSH`
 * 
 * @param hl7_meta pointer to an empty metadata structure
 * @param fd file pointer pointing to the beginning of MSH or MSA segment
 * @returns 0 on succes, error code otherwise
 */
int read_meta(hl7_meta_t *hl7_meta, FILE *fd);

#ifdef __cplusplus
}
#endif