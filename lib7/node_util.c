/**
 * Node manipulation utilities
 * 
 */

#include "node_util.h"

int node_append_child(node_t *parent, node_t* node) {
    return node_append(&parent, node);
}

node_t *node_insert_child_at(node_t *parent, node_t *child, int pos) {
    return NULL;
}

int node_remove(node_t *node) {
    return -1;
}

node_t *node_remove_child(node_t *parent, node_t *child, int pos) {
    return NULL;
}

node_t *node_create_n_append(node_t *parent) {

    // FIXME: this mus fail on node_type_t SUBCMP

    node_type_t child_type = (parent->type*2);
    node_t *n = create_node_t(child_type, NULL, 0, 0);

    if (n == NULL)
        return n;

    node_append(&parent, n);
    log_debug("num_children of parent: %d", parent->num_children);

    // fieldlist shall always have at least one child, otherwise 
    // the structure is invalid
    if (parent->type == SEGMENT)
        n = node_create_n_append(n);

    return n;
}

node_t *node_pad_children(node_t *node, int length) {

    node_t *n = NULL;

    // if the node has already some data set, we are not allowed 
    // to add children, abort
    //
    // segment is a special case, the segment's name ist stored in data
    if (node->type > SEGMENT && (node->length || node->data != NULL)) {
        return n;
    }

    // check if we have anything to do
    if (node->num_children >= length) {
        log_debug("Already enough allocated children");
        return node->children[length-1];
    }

    // reference to child node type
    node_type_t child_type = (node->type)*2;
    int pad_start = node->num_children;

    if (loglevel >= LEVEL_DEBUG) {
        const char *pt = node_type_to_string(node->type);
        const char *ct = node_type_to_string(child_type);
        log_debug("Parent type: %s, child_type: %s", pt, ct);
    }
    
    for (int i=pad_start; i<length; i++) {
        log_debug("adding %d to %s", i, node_type_to_string(node->type));
        n = node_create_n_append(node);
        if (n == NULL)
            return NULL;
    }

    return n;
}

node_t *node_create_empty(node_type_t type) {

    node_t *n = create_node_t(type, NULL, 0, 0);

    if (type != FIELDLIST)
        return n;
    

    return NULL;
}

static int _n2str_leaf(buf_t *buffer, node_t *node, hl7_meta_t *meta) {
    log_trace("%s %d", node_type_to_string(node->type), node->length);
    int ret = 0;
    if (node->data != NULL)
        ret = append_buf_str(buffer, node->length, node->data);
    return ret;
}

static int _n2str_cmp(buf_t *buffer, node_t *node, hl7_meta_t *meta) {
    log_trace("%s %d", node_type_to_string(node->type), node->length);
    
    unsigned char sep[2] = {meta->sep_comp, 0};
    if (node->type == COMP)
        sep[0] = meta->sep_subcmp;

    if (node->num_children == 0) {
        return _n2str_leaf(buffer, node, meta);
    } else {
        for (int i=0; i<node->num_children; i++) {
            int r = _n2str_cmp(buffer, node->children[i], meta);
            log_trace("ret: %d", r);
            if (r != 0)
                return r;
            if (i+1 < node->num_children)
                append_buf_str(buffer, 2, sep);
        }
    }

    return 0;
}

static int _n2str_field(buf_t *buffer, node_t *node, hl7_meta_t *meta) {
    log_trace("%s %d: %s", node_type_to_string(node->type), node->length, node->data);


    if (node->num_children == 0) {
        return _n2str_leaf(buffer, node, meta);
    } else {

        unsigned char sep[2] = {meta->sep_comp, 0};
        for (int i=0; i<node->num_children; i++) {
            int r = _n2str_cmp(buffer, node->children[i], meta);
            //log_debug("ret: %d", r);
            if (r != 0)
                return r;
            if (i+1 < node->num_children)
                append_buf_str(buffer, 2, sep);
        }
    }

    return 0;
}

static int _n2str_fieldlist(buf_t *buffer, node_t *node, hl7_meta_t *meta) {
    log_trace("%s %d", node_type_to_string(node->type), node->length);

    if (node->num_children == 0) {
        return _n2str_leaf(buffer, node, meta);
    } else {
        unsigned char sep[2] = {meta->sep_rep, 0};
        for (int i=0; i<node->num_children; i++) {
            int r = _n2str_field(buffer, node->children[i], meta);
            
            if (r != 0)
                return r;
            if (i+1 < node->num_children)
                append_buf_str(buffer, 2, sep);
        }
    }

    return 0;
}

static int _n2str_segment(buf_t *buffer, node_t *node, hl7_meta_t *meta) {
    log_trace("%s %d", node_type_to_string(node->type), node->length);

    unsigned char sep[2] = {meta->sep_field, 0};

    append_buf_str(buffer, node->length, node->data);

    if (node->num_children == 0) {
        return _n2str_leaf(buffer, node, meta);
    } else {

        append_buf_str(buffer, 2, sep);
        
        // skip msh 1 to prevent double delimiter
        int start = 0;
        if (strcmp("MSH", (char *) node->data) == 0) {
            start = 2;
            
            // msh 2 contains delimiters. we do not care what 
            // is stored in that node. We always need to create 
            // it from the meta definition
            unsigned char msh2[5] = {
                meta->sep_comp, 
                meta->sep_rep,
                meta->sep_escape,
                meta->sep_subcmp,
                0
            };

            append_buf_str(buffer, 5, msh2);
            append_buf_str(buffer, 2, sep);
        }

        for (int i=start; i<node->num_children; i++) {
            int r = _n2str_fieldlist(buffer, node->children[i], meta);
            //printf("%d, %d, '%s'\n", i, node->children[i]->num_children, buffer->buffer);
            if (r != 0)
                return r;
            
            if (i+1 < node->num_children)
                append_buf_str(buffer, 2, sep);
        }
        
    }

    return 0;
}

unsigned char *node_to_string(node_t *node, hl7_meta_t *meta, int *length) {
    log_trace("%s %d", node_type_to_string(node->type), node->length);

    int ret = 0;
    buf_t *buffer = new_buf_t();

    if (node->num_children == 0) {
        ret = _n2str_leaf(buffer, node, meta);
    } else if (node->type == SEGMENT) {
        ret = _n2str_segment(buffer, node, meta);
    } else if (node->type == FIELDLIST) {
        ret = _n2str_fieldlist(buffer, node, meta);
    } else if (node->type == FIELD) {
        ret = _n2str_field(buffer, node, meta);
    } else if (node->type == COMP) {
        ret = _n2str_cmp(buffer, node, meta);
    } else if (node->type == SUBCOMP) {
        ret = _n2str_cmp(buffer, node, meta);
    } else {
        log_error("Uknown node type: %d", node->type);
        return NULL;
    }

    if (ret != 0) {
        log_debug("last return code %d", ret);
        return NULL;
    }

    *length = buffer->length;
    unsigned char* buf = buffer->buffer;
    free(buffer);
    return buf;
}

int message_to_file(message_t *message, char *filename) {

    FILE *fd = fopen(filename, "wb");
    if (fd == NULL)
        return -1;

    int l = 0;
    int total_length = 0;
    int linesep_len = 1;
    unsigned char linesep[3] = {message->meta->sep_message, 0, 0};
    if (message->meta->crlf) {
        linesep[1] = 13; // add CR
        linesep[1] = 10; // add LF
        linesep_len = 2;
    }
    
    for(int i=0; i<message->num_children; i++) {
        unsigned char *str = node_to_string(message->segments[i], message->meta, &l);
        //printf("%s\n", (char*) str);
        if (str == NULL) {
            // FIXME: free stuff, close handle
            return -2;
        }
        
        // total length does not contain the \0 character
        fwrite((char*) str, 1, l, fd);
        fwrite((char*) linesep, 1, linesep_len, fd);
        free(str);
        total_length += l + linesep_len;
    }

    fclose(fd);

    return total_length;
}

unsigned char *message_to_string(message_t *message) {
    int l = 0;
    int total_length = 0;
    buf_t *buffer = new_buf_t();
    int linesep_len = 2;
    unsigned char linesep[3] = {message->meta->sep_message, 0, 0};
    if (message->meta->crlf) {
        linesep[1] = 13; // add CR
        linesep[1] = 10; // add LF
        linesep_len = 3;
    }
    
    for(int i=0; i<message->num_children; i++) {
        unsigned char *str = node_to_string(message->segments[i], message->meta, &l);
        if (str == NULL) {
            // FIXME: free stuff
            return NULL;
        }
        
        // total length does not contain the \0 character
        append_buf_str(buffer, l+1, str);
        append_buf_str(buffer, linesep_len, linesep);
        free(str);
        total_length += l + linesep_len;
    }

    unsigned char *ret = buffer->buffer;
    // free buffer control structure, but keep buffer->buffer and return it
    free(buffer); 
    return ret;
}

int node_set_data(node_t *node, unsigned char *data, int length) {
    // check if node already has data, if so, free it
    if (node->length && node->data != NULL) {
        free(node->data);
        node->data = NULL;
    }

    // in case of any errors, report zero length, just in case
    node->length = 0;

    // copy data and assign it to the node
    node->data = (unsigned char*) memdup(data, length);
    if (node->data == NULL)
        return -1;
    node->length = length;
    return 0;
}

int node_set_string(node_t *node, char *data) {
    int l = strlen(data) + 1; // inclice \0 in the length
    return node_set_data(node, (unsigned char*) data, l);
}

int node_get_by_addr(message_t* message, hl7_addr_t *addr, node_t **node) {
    node_t *segment = NULL;
    node_t *fieldlist = NULL;
    node_t *field = NULL;
    node_t *comp = NULL;
    node_t *subcmp = NULL;

    int seg_count = addr->seg_count;
    if (seg_count == -1)
        seg_count = 1;

    if (loglevel >= LEVEL_DEBUG) {
        addr_dump(addr);
    }
    
    int repeat = 0;
    for(int i=0; i<message->num_children; i++) {
        if (addr->segment[0] == message->segments[i]->data[0] &&
            addr->segment[1] == message->segments[i]->data[1] &&
            addr->segment[2] == message->segments[i]->data[2]) {
            
            // we want the first
            if (addr->seg_count == -1 || repeat+1 == addr->seg_count) {
                segment = message->segments[i];
                break;
            }

            repeat++;
        }
    }
    log_debug("repeat: %d", repeat);
    log_debug("segment: %d", segment);

    // we have not found the apropreate segment, 
    // therefore we abort
    if (segment == NULL) {
        return SEGMENT;
    }
    
    // if we searched for a segment, return
    if (addr->fieldlist == -1) {
        log_debug("Returning segment");
        *node = segment;
        return 0;
    }

    // check fieldlist
    log_debug("Searching for FIELDLIST");
    if (segment->num_children >= addr->fieldlist) {
        fieldlist = segment->children[addr->fieldlist-1];
    } else {
        return FIELDLIST;
    }

    // check field
    int afield = addr->field;
    if (afield == -1)
        afield = 1;
    log_debug("Searching for FIELD");
    if (fieldlist->num_children >= afield) {
        field = fieldlist->children[afield-1];
    } else {
        return FIELD;
    }

    // Optional parameters, must not be -1
    if (addr->comp != -1) {
        // check component
        log_debug("Searching for COMP");
        if (field->num_children >= addr->comp) {
            comp = field->children[addr->comp-1];
        } else {
            return COMP;
        }

        if (addr->subcmp != -1) {
            // check sub component
            log_debug("Searching for SUBCOMP");
            if (comp->num_children >= addr->subcmp) {
                subcmp = comp->children[addr->subcmp-1];
            } else {
                return SUBCOMP;
            }
        }
    }

    // all levels checked, we are successfull, set return value
    // we are setting a reference to the found node to reference
    if (subcmp != NULL)
        *node = subcmp;
    else if (comp != NULL)
        *node = comp;
    else if (field != NULL)
        *node = field;

    return 0;
}

int node_set_by_addr(message_t *message, hl7_addr_t *addr, unsigned char *value, int length) {

    int l = 0;

    if (loglevel >= LEVEL_DEBUG)
        addr_dump(addr);

    // check if the segment exists.
    node_t *segment = NULL;
    int seg_count = addr->seg_count;
    if (seg_count == -1)
        seg_count = 1;
    int repeat = 0;
    for(int i=0; i<message->num_children; i++) {
        if (addr->segment[0] == message->segments[i]->data[0] &&
            addr->segment[1] == message->segments[i]->data[1] &&
            addr->segment[2] == message->segments[i]->data[2]) {
            
            // we want the first
            if (seg_count == -1 || repeat+1 == seg_count) {
                segment = message->segments[i];
                break;
            }

            repeat++;
        }
    }
    log_debug("repeat: %d", repeat);

    // we have not found the apropreate segment, 
    // therefore we need to create it.
    if (segment == NULL) {
        for (int i=repeat; i<seg_count; i++) {
            // FIXME: Error handling
            node_t *n = create_node_t(SEGMENT, (unsigned char*) addr->segment, 4, 0);
            message_append(&message, n);
            segment = n;
        }
    }

    if (loglevel >= LEVEL_DEBUG) {
        hl7_meta_t* meta = init_hl7_meta_t();
        unsigned char *s = node_to_string(segment, meta, &l);
        log_debug("segment: %s, length: %d", s, l);
        free(s);
        free_hl7_meta(meta);
    }

    // now that we have a segment, start checking and padding
    // FIELDLIST
    log_trace("SEG num_children: %d", segment->num_children);
    node_t *fieldlist = node_pad_children(segment, addr->fieldlist);
    if (fieldlist == NULL)
        return -1;
    log_debug("num_children: %d", fieldlist->num_children);
    
    // do the same for fields, we always need one
    int afield = addr->field;
    if (afield == -1)
        afield = 1;
    node_t *field = node_pad_children(fieldlist, afield);
    if (field == NULL) {
        return -2;
    }

    // now we need to check if there are any sub elements in addr. if not
    // set data
    if (addr->comp == -1) {
        int ret = node_set_data(field, value, length);
        if (ret != 0)
            return -3;
        
        //dump_structure(message);
        return 0;
    }

    //printf("here\n");
    // if we have a component, add it
    if (field->length || field->data != NULL)
        return -21; // node has already data, we cannot add children
    
    node_t *comp = node_pad_children(field, addr->comp);
    if (comp == NULL) {
        return -4;
    }

    if (addr->subcmp == -1) {
        int ret = node_set_data(comp, value, length);
        if (ret != 0)
            return -5;
        
        //dump_structure(message);
        return 0;
    }

    // if we have a sub component, add it
    node_t *subcmp = node_pad_children(comp, addr->subcmp);
    if (subcmp == NULL)
        return -6;

    // check if parent already has data, then we cannot
    // add data to this child
    if (comp->length != 0 || comp->data != NULL) {
        return -22;
    }

    int ret = node_set_data(subcmp, value, length);
    if (ret != 0)
        return -7;
    //dump_structure(message);

    /*
    if (loglevel >= LEVEL_DEBUG) {
        hl7_meta_t* meta = init_hl7_meta_t();
        char data[] = "VALUE";
        node_set_data(segment->children[0]->children[0], "v1", 3);
        // node_set_data(segment->children[1]->children[0], "VALUE", 6);
        node_t *cmp = node_append_n_create(segment->children[1]->children[0]);
        node_set_data(cmp, "SUBVAL1", 8);
        cmp = node_append_n_create(segment->children[1]->children[0]);
        node_set_data(cmp, "SUBVAL2", 8);
        log_debug("subcmp type: %s", node_type_to_string(cmp->type));
        dump_structure(message);
        char *s = (char *) node_to_string(segment, meta, &l);
        log_debug("->SEGMENT: %s, length: %d", s, l);
        free(s);
        free_hl7_meta(meta);
    }
    */

    log_debug("end node_set_by_addr()");

    return 0;
}
