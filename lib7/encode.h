/** \file
 * @brief HL7 escape and unsescape functions
 * 
 * http://healthstandards.com/blog/2006/11/02/hl7-escape-sequences/
 * 
 * The following escape sequences are implemented. More may follow 
 * but for performance reasons we do not support all (yet?):
 * 
 * |     | Character | Description Conversion                                  |
 * |---- |---------- |-------------------------------------------------------- |
 * | ✓ | `\E\`       | Escape character converted to escape character (e.g., `\`) |
 * | ✓ | `\F\`       | Field separator converted to field separator character (e.g., pipe) |
 * | ✓ | `\R\`       | Repetition separator converted to repetition separator character (e.g., `~`) |
 * | ✓ | `\S\`       | Component separator converted to component separator character (e.g., `^`) |
 * | ✓ | `\T\`       | Subcomponent separator converted to subcomponent separator character (e.g., `&`) |
 * | ✓ | `\.br\`     | Begin new output line. Set the horizontal position to the current left margin and increment the vertical position by 1. |
 * | ✘ | `\.sp <n>\` | End current output line and skip `<number>` vertical spaces. `<number>` is a positive integer or absent. If `<n>` is absent, skip one space. The horizontal character position remains unchanged. Note that for purposes of compatibility with previous versions of HL7, `^\.sp\` is equivalent to `\.br\`. |
 * | ✘ | `\.fi\`     | Begin word wrap or fill mode. This is the default state. It can be changed to a no-wrap mode using the `\.nf\` command.                                            |
 * | ✘ | `\.nf\`     | Begin no-wrap mode. |
 * | ✘ | `\.in <n>\` | Indent `<number>` of spaces, where `<number>` is a positive or negative integer. This command cannot appear after the first printable character of a line. |
 * | ✘ | `\.ti <n>\` | Temporarily indent `<number>` of spaces where number is a positive or negative integer. This command cannot appear after the first printable character of a line. |
 * | ✘ | `\.sk <n>\` | Skip `<number>` spaces to the right.                      |
 * | ✘ | `\.ce\`     | End current output line and center the next line.       |
 * | ✘ | `\H\`       | Start highlighting not converted                        |
 * | ✘ | `\N\`       | Normal text (end highlighting) not converted            |
 * | ✘ | `\Xdd…\`    | Hexadecimal data (dd must be hexadecimal characters) converted to the characters identified by each pair of digits. |
 * | ✘ | `\Cxxyy\`   | Single-byte character set escape sequence with two hexadecimal values not converted |
 * | ✘ | `\Mxxyyzz\` | Multi-byte character set escape sequence with two or three hexadecimal values (zz is optional) not converted |
 * | ✘ | `\Zdd…\`    | Locally defined escape sequence not converted           |
 */

#include "buffer.h"
#include "meta.h"

#ifndef ESCAPE_H
#define ESCAPE_H

//unsigned char* hl7_escape(unsigned char *inp, int length, hl7_meta_t *meta);
int hl7_escape(unsigned char *inp, unsigned char **outp, int in_length, int *out_length, hl7_meta_t *meta);

//unsigned char* hl7_unescape(unsigned char *inp, int length, hl7_meta_t *meta);
int hl7_unescape(unsigned char *inp, unsigned char **outp, int in_length, int *out_length, hl7_meta_t *meta);

#endif // ESCAPE_H