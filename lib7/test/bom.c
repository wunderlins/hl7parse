#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <errno.h>
#include <string.h>
#include <ctype.h>
#ifdef __APPLE__
#include <sys/syslimits.h> // for PATH_MAX on OSX
#endif
#include "../bom.h"
#include "CUnit/Basic.h"
#include "../generated/test.h"

// this is the directory where we are looking for files
// passed in as argv
DIR* dir = NULL;
char *dirname = NULL;

void test_bom_success() {
    FILE *fd = NULL;
    
    printf("dirname: %s ", dirname);
    // find all relevant files, they end in "*.hl7"
    struct dirent *dp;
    dir = opendir(dirname);
    if ((dir = opendir(dirname)) == NULL) {
        fprintf(stderr, "Can't open %s\n", dirname);
        return;
    }

    char filename_fqd[PATH_MAX] = {0};
    while ((dp = readdir(dir)) != NULL) {
        if (strcmp(dp->d_name, "minimal_lf.hl7") == 0)
            continue; // skip template file
        if (strcmp(dp->d_name, "empty.hl7") == 0)
            continue; // skip empty file

        // file name needs to end in hl7
        char ending[5] = {0};
        memcpy(ending, &dp->d_name[strlen(dp->d_name)-4], 4);
        //printf("ending: %s\n", ending);
        if (strcmp(ending, ".hl7") != 0)
            continue;
        sprintf(filename_fqd, "%s/%s", dirname, dp->d_name);
        //printf("filename: %s\n", dp->d_name);

        // parse filename, extract first token up to '_', it contins the bom 
        // encoded in HEX.
        char *fn = dp->d_name;
        //printf("filename: %s\n", fn);
        int even = 2;
        unsigned char *bomc = malloc(5); // bomc = {0, 0, 0, 0, 0};
        int boml = 0;
        while (*fn != 0 && *fn != '_') {
            if (even == 0) {
                char hex[5] = {'0', 'x', tolower(*(fn-1)), tolower(*fn), 0};
                //int n = strtol(hex, NULL, 16);
                int n = 0;
                sscanf(hex, "%x", &n);
                //printf("%s %c %c %d %d %d\n", hex, *(fn-1), *fn, n, even, boml);
                bomc[boml] = n;
                boml++;
            }
            even = !even;
            fn++;
        }

        // open file, read com and compare
        fd = fopen(filename_fqd, "rb");
        CU_ASSERT_NOT_EQUAL_FATAL(fd, NULL);
        bom_t *bom = detect_bom(fd);

        //print_bom(bom);
        char *inp_str = bom_to_string(boml, bomc, UNKNOWN);
        char *bom_str = bom_to_string(bom->length, (unsigned char*) bom->bom, bom->endianness);
        if (strcmp(inp_str, bom_str) != 0)
            printf("%s\n%s %s\n", dp->d_name, inp_str, bom_str);
        
        // compare
        CU_ASSERT_EQUAL(bom->length, boml);
        CU_ASSERT_EQUAL(0, memcmp(bom->bom, bomc, boml));

        if (bom->bom != NULL)
            free(bom->bom);
        free(bom);
        fclose(fd);
        
    }

    closedir(dir);
}

void test_bom_error() {
    FILE *fd = NULL;
    bom_t *bom1 = NULL;
    bom_t *bom2 = NULL;

    // baseline, a valid file without bom
    char filename_fqd[PATH_MAX] = {0};
    sprintf(filename_fqd, "%s/minimal_lf.hl7", dirname);
    fd = fopen(filename_fqd, "rb");
    CU_ASSERT_NOT_EQUAL_FATAL(fd, NULL);
    bom1 = detect_bom(fd);
    //print_bom(bom1);

    CU_ASSERT_EQUAL(bom1->length, 0);
    //printf("endianness: %d\n", bom1->endianness);
    CU_ASSERT_EQUAL(bom1->endianness, UNKNOWN);

    if (bom1->bom != NULL)
        free(bom1->bom);
    free(bom1);
    fclose(fd);

    // try on an empty file
    sprintf(filename_fqd, "%s/empty.hl7", dirname);
    fd = fopen(filename_fqd, "rb");
    CU_ASSERT_NOT_EQUAL_FATAL(fd, NULL);
    bom2 = detect_bom(fd);
    //print_bom(bom2);

    CU_ASSERT_EQUAL(bom2->length, 0);
    CU_ASSERT_EQUAL(bom2->endianness, UNKNOWN);

    if (bom2->bom != NULL)
        free(bom2->bom);
    free(bom2);
    fclose(fd);
}

int main(int argc, char *argv[]) {
    // Initialize the CUnit test registry
    if (CUE_SUCCESS != CU_initialize_registry())
        return CU_get_error();

    // Sets the basic run mode, CU_BRM_VERBOSE will show maximum output of run details
    // Other choices are: CU_BRM_SILENT and CU_BRM_NORMAL
    CU_basic_set_mode(CU_BRM_VERBOSE);

    CU_pSuite pSuite = NULL;

    // Add a suite to the registry
    pSuite = CU_add_suite("bom", 0, 0);
    // Always check if add was successful
    if (NULL == pSuite) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // setup tests by looking for an existing base directory
    if(argc != 2) {
        //printf("Failed because of missing directory, pass it into test via argv[1]\n");
        // return 1;
        dirname = malloc(PATH_MAX);
        sprintf(dirname, "%s/bom", datadir);
    } else {
        dirname = argv[1];
    }
    printf("dirname: %s\n", dirname);

    // check if directory exists
    DIR* dir = opendir(dirname);
    if (dir) {
        /* Directory exists. */
        closedir(dir);
    } else if (ENOENT == errno) {
        /* Directory does not exist. */
        printf("Directory with tests could not be found: %s\n", dirname);
        return 2;
    } else {
        /* opendir() failed for some other reason. */
        printf("Directory with tests could not be opened: %s\n", dirname);
        return 2;
    }

    // test known bom cases
    if (NULL == CU_add_test(pSuite, "test_bom_success", test_bom_success)) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    if (NULL == CU_add_test(pSuite, "test_bom_error", test_bom_error)) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // Run the tests and show the run summary
    CU_basic_run_tests();
    return CU_get_number_of_tests_failed();
}