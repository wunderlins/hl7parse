#include <stdio.h>
#include <stdlib.h>
#include "../address.h"
#include "CUnit/Basic.h"

int test2addr(char *str, char *seg, int fieldlist, int field, int comp, int subcmp, int seg_count) {
    int ret = 0;
    hl7_addr_t *a = NULL;

    //printf("Testing: %s\n", str);
    a = addr_from_string(str);

    //fflush(stdout);
    if (fieldlist == -2) {
        if (a == NULL) ret = 0;
    } else {   
        if(strcmp(a->segment, seg) == 0 &&
           a->fieldlist == fieldlist &&
           a->field == field &&
           a->comp == comp &&
           a->subcmp == subcmp &&
           a->seg_count == seg_count)
            ret = 1;
        
        if (a != NULL) {
            if (a->segment != NULL) 
                free(a->segment);
            free(a);
        }
    }

    return ret;
}

int test2str(char* src) {
    int ret = 0;
    hl7_addr_t *a  = addr_from_string(src);
    if (a == NULL) return ret;
    char *s = addr_to_string(a);
    //addr_dump(a);
    //printf("test2str(%s) -> %s\n", src, s);
    if(strcmp(src, s) == 0) ret = 1;
    free(s);

    if (a != NULL) {
        if (a->segment != NULL) 
            free(a->segment);
        free(a);
    }

    return ret;
}

void test_fromstr_success() {
    // test from_string() method
    // expected formats
    CU_ASSERT_EQUAL(test2addr("PID-1", "PID", 1, -1, -1, -1, -1), 1);
    CU_ASSERT_EQUAL(test2addr("PID-1(2)", "PID", 1, 2, -1, -1, -1), 1);
    CU_ASSERT_EQUAL(test2addr("PID-1(2).3", "PID", 1, 2, 3, -1, -1), 1);
    CU_ASSERT_EQUAL(test2addr("PID-1(2).3.4", "PID", 1, 2, 3, 4, -1), 1);
    CU_ASSERT_EQUAL(test2addr("PID(2)", "PID", -1, -1, -1, -1, 2), 1);
    CU_ASSERT_EQUAL(test2addr("PID(9)-3(4).1.1", "PID", 3, 4, 1, 1, 9), 1);
}

void test_fromstr_error() {
    // error conditions
    CU_ASSERT_EQUAL(test2addr("", "PID", -2, -1, -1, -1, -1), 0);
    CU_ASSERT_EQUAL(test2addr("PID-", "PID", -2, -1, -1, -1, -1), 0);
    CU_ASSERT_EQUAL(test2addr("PID-(", "PID", -2, -1, -1, -1, -1), 0);
    CU_ASSERT_EQUAL(test2addr("PID-12)", "PID", -2, -1, -1, -1, -1), 0);
    CU_ASSERT_EQUAL(test2addr("PID-a)", "PID", -2, -1, -1, -1, -1), 0);
    CU_ASSERT_EQUAL(test2addr("PID-(1)", "PID", -2, -1, -1, -1, -1), 0);
}

// test all documented error conditions
void test_setaddr_error() {
    int ret = 0;
    hl7_addr_t *addr = create_addr();

    // test with repetitions
    CU_ASSERT_EQUAL(set_addr_from_string("", &addr), -1);
    CU_ASSERT_EQUAL(set_addr_from_string("PID", &addr), 0);
    CU_ASSERT_EQUAL(set_addr_from_string("PID(12)", &addr), 0);
    CU_ASSERT_EQUAL(set_addr_from_string("PID-", &addr), -3);
    CU_ASSERT_EQUAL(set_addr_from_string("PID(", &addr), -2);
    CU_ASSERT_EQUAL(set_addr_from_string("PID(1", &addr), -2);
    CU_ASSERT_EQUAL(set_addr_from_string("PID(12", &addr), -2);
    CU_ASSERT_EQUAL(set_addr_from_string("PID(12)-", &addr), -3);
    CU_ASSERT_EQUAL(set_addr_from_string("PID(12)-a", &addr), -4);
    CU_ASSERT_EQUAL(set_addr_from_string("PID(12)-5(", &addr), -7);
    CU_ASSERT_EQUAL(set_addr_from_string("PID(12)-5(a", &addr), -8);
    CU_ASSERT_EQUAL(set_addr_from_string("PID(12)-5(1", &addr), -9);
    CU_ASSERT_EQUAL(set_addr_from_string("PID(12)-5(1)", &addr), 0);
    CU_ASSERT_EQUAL(set_addr_from_string("PID(12)-5(1).", &addr), -10);
    CU_ASSERT_EQUAL(set_addr_from_string("PID(12)-5(1).55", &addr), 0);
    CU_ASSERT_EQUAL(set_addr_from_string("PID(12)-5(1).5-", &addr), -11);
    CU_ASSERT_EQUAL(set_addr_from_string("PID(12)-5(1).5.", &addr), -13);
    CU_ASSERT_EQUAL(set_addr_from_string("PID(12)-5(1).5.a", &addr), -14);
    CU_ASSERT_EQUAL(set_addr_from_string("PID(12)-5(1).5.2", &addr), 0);

    // test without repetition
    CU_ASSERT_EQUAL(set_addr_from_string("PID-5(", &addr), -7);
    CU_ASSERT_EQUAL(set_addr_from_string("PID-5.", &addr), -10);
    CU_ASSERT_EQUAL(set_addr_from_string("PID-5.6", &addr), 0);
    CU_ASSERT_EQUAL(set_addr_from_string("PID-5.6.a", &addr), -14);
    CU_ASSERT_EQUAL(set_addr_from_string("PID-5.6.2", &addr), 0);

    free_addr(addr);
}

void test_tostr_success() {
    // test to_string() method
    CU_ASSERT_EQUAL(test2str("PID"), 1);
    CU_ASSERT_EQUAL(test2str("PID-1"), 1);
    CU_ASSERT_EQUAL(test2str("PID-1.2"), 1);
    CU_ASSERT_EQUAL(test2str("PID-1.2.3"), 1);
    CU_ASSERT_EQUAL(test2str("PID-1(2)"), 1);
    CU_ASSERT_EQUAL(test2str("PID-1(2).3"), 1);
    CU_ASSERT_EQUAL(test2str("PID-1(2).3.4"), 1);
    CU_ASSERT_EQUAL(test2str("PID(3)-1(2).3.4"), 1);
}

void test_tostr_error() {
    // test to_string() method
    CU_ASSERT_EQUAL(test2str("PID-"), 0);
    CU_ASSERT_EQUAL(test2str("PID-("), 0);
    CU_ASSERT_EQUAL(test2str("PID-2a"), 0);
    CU_ASSERT_EQUAL(test2str("PID-2-a"), 0);
    CU_ASSERT_EQUAL(test2str("PID-2.a"), 0);
    CU_ASSERT_EQUAL(test2str("PID-2.(a)"), 0);
    CU_ASSERT_EQUAL(test2str("PID-2.1w"), 0);
}

void test_seg_count() {
    seg_count_t *c = create_seg_count();
    add_seg_count("MSH", c);
    add_seg_count("PID", c);
    add_seg_count("OBX", c);
    add_seg_count("OBX", c);
    //printf("number of segments: %d\n", c->length);
    CU_ASSERT_EQUAL(c->length, 3); // 3 because MSH, PID, OBX

    //int sc = get_seg_count("OBX", c);
    //printf("number of OBX Segments: %d\n", sc);
    CU_ASSERT_EQUAL(get_seg_count("OBX", c), 2);
    CU_ASSERT_EQUAL(get_seg_count("MSH", c), 1);
    CU_ASSERT_EQUAL(get_seg_count("PID", c), 1);
    CU_ASSERT_EQUAL(get_seg_count("XXX", c), 0);

    free_seg_count(c);
}

int main(int argc, char *argv[]) {
    // Initialize the CUnit test registry
    if (CUE_SUCCESS != CU_initialize_registry())
        return CU_get_error();

    // Sets the basic run mode, CU_BRM_VERBOSE will show maximum output of run details
    // Other choices are: CU_BRM_SILENT and CU_BRM_NORMAL
    CU_basic_set_mode(CU_BRM_VERBOSE);

    CU_pSuite pSuite = NULL;

    // Add a suite to the registry
    pSuite = CU_add_suite("address", 0, 0);
    // Always check if add was successful
    if (NULL == pSuite) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // test all error conditions of set_addr_from_string()
    if (NULL == CU_add_test(pSuite, "test_setaddr_error", test_setaddr_error)) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // real world use cases
    if (NULL == CU_add_test(pSuite, "test_fromstr_success", test_fromstr_success)) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    if (NULL == CU_add_test(pSuite, "test_fromstr_error", test_fromstr_error)) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    if (NULL == CU_add_test(pSuite, "test_tostr_success", test_tostr_success)) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    if (NULL == CU_add_test(pSuite, "test_tostr_error", test_tostr_error)) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    if (NULL == CU_add_test(pSuite, "test_seg_count", test_seg_count)) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // Run the tests and show the run summary
    CU_basic_run_tests();
    return CU_get_number_of_tests_failed();
}
