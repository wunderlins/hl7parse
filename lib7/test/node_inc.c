int ret = -1;
message_t *m = create_message_t(NULL);

// create nodes
node_t* null_segment   = create_node_t(SEGMENT, (unsigned char*) "SEG", 4, 0);
node_t* null_fieldlist = create_node_t(FIELDLIST, NULL, 0, 0);
node_t* null_field     = create_node_t(FIELD, NULL, 0, 0);
node_t* data_field     = create_node_t(FIELD, (unsigned char*) "DATA", 5, 0);

CU_ASSERT_NOT_EQUAL(null_segment, NULL);
CU_ASSERT_NOT_EQUAL(null_fieldlist, NULL);
CU_ASSERT_NOT_EQUAL(null_field, NULL);
CU_ASSERT_NOT_EQUAL(data_field, NULL);

// create hirarchy
ret = message_append(&m, null_segment);
CU_ASSERT_EQUAL(ret, 0);
ret = node_append(&null_segment, null_fieldlist);
CU_ASSERT_EQUAL(ret, 0);
ret = node_append(&null_fieldlist, null_field);
CU_ASSERT_EQUAL(ret, 0);
ret = node_append(&null_fieldlist, data_field);
CU_ASSERT_EQUAL(ret, 0);