#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#ifdef __APPLE__
#include <sys/syslimits.h> // for PATH_MAX on OSX
#endif
#include "../node.h"
#include "CUnit/Basic.h"
#include "../generated/test.h"


void test_node_init() {
    #include "node_inc.c"

    // check message and dependencies
    CU_ASSERT_EQUAL(m->num_children, 1);
    CU_ASSERT_EQUAL(m->type, MESSAGE);
    CU_ASSERT_EQUAL(m->parent, NULL);
    CU_ASSERT_EQUAL(m->_num_children_allocated, MESSAGE_PREALLOC_CHILDREN);
    
    // check segment
    CU_ASSERT_EQUAL(null_segment->num_children, 1);
    CU_ASSERT_EQUAL(null_segment->type, SEGMENT);
    CU_ASSERT_EQUAL((message_t*) null_segment->parent, m);
    CU_ASSERT_EQUAL(null_segment->length, 4);
    CU_ASSERT_EQUAL(null_segment->_num_children_allocated, NODE_PREALLOC_CHILDREN);
    CU_ASSERT_EQUAL(0, memcmp(null_segment->data, (unsigned char*) "SEG", null_segment->length-1));

    // check fieldlist
    CU_ASSERT_EQUAL(null_fieldlist->num_children, 2);
    CU_ASSERT_EQUAL(null_fieldlist->type, FIELDLIST);
    CU_ASSERT_EQUAL(null_fieldlist->parent, null_segment);
    CU_ASSERT_EQUAL(null_fieldlist->length, 0);
    CU_ASSERT_EQUAL(null_fieldlist->_num_children_allocated, NODE_PREALLOC_CHILDREN);
    CU_ASSERT_EQUAL(null_fieldlist->data, NULL);

    // check field 1+2
    CU_ASSERT_EQUAL(null_field->num_children, 0);
    CU_ASSERT_EQUAL(null_field->type, FIELD);
    CU_ASSERT_EQUAL(null_field->parent, null_fieldlist);
    CU_ASSERT_EQUAL(null_field->length, 0);
    CU_ASSERT_EQUAL(null_field->_num_children_allocated, NODE_PREALLOC_CHILDREN);
    CU_ASSERT_EQUAL(null_field->data, NULL);

    CU_ASSERT_EQUAL(data_field->parent, null_fieldlist);
    CU_ASSERT_EQUAL(data_field->length, 5);
    CU_ASSERT_EQUAL(0, memcmp(data_field->data, (unsigned char*) "DATA", data_field->length-1));

    CU_ASSERT_EQUAL(node_parent_child_pos(null_field), 0);
    CU_ASSERT_EQUAL(node_parent_child_pos(data_field), 1);

    // cleanup
    free_message_t(m);
}

void test_node_in_segment() {
    #include "node_inc.c"

    hl7_addr_t *addr = create_addr();
    //char *data = malloc(4);
    //memcpy(data, "SEG", 4);
    addr->segment = "SEG"; //data;
    addr->fieldlist = 1;
    addr->field = 1;
    node_t *found1 =  node_in_segment(null_segment, addr);
    CU_ASSERT_EQUAL(null_field, found1);

    hl7_addr_t *a1 = addr_from_node(found1);
    //printf("%s | %s\n", addr->segment, a1->segment);
    CU_ASSERT_EQUAL(0, strcmp(addr->segment, a1->segment));
    CU_ASSERT_EQUAL(addr->fieldlist, a1->fieldlist);
    CU_ASSERT_EQUAL(addr->field, a1->field);
    CU_ASSERT_EQUAL(addr->comp, a1->comp);
    CU_ASSERT_EQUAL(addr->subcmp, a1->subcmp);

    addr->field = 2;
    node_t *found2 =  node_in_segment(null_segment, addr);
    CU_ASSERT_EQUAL(data_field, found2);
    CU_ASSERT_EQUAL(0, memcmp(found2->data, (unsigned char*) "DATA", found2->length-1));

    // cleanup
    free_message_t(m);
}

int main(int argc, char *argv[]) {
    // Initialize the CUnit test registry
    if (CUE_SUCCESS != CU_initialize_registry())
        return CU_get_error();
    
    // Sets the basic run mode, CU_BRM_VERBOSE will show maximum output of run details
    // Other choices are: CU_BRM_SILENT and CU_BRM_NORMAL
    CU_basic_set_mode(CU_BRM_VERBOSE);

    CU_pSuite pSuite = NULL;

    // Add a suite to the registry
    pSuite = CU_add_suite("node", 0, 0);
    // Always check if add was successful
    if (NULL == pSuite) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // test known node cases
    if (NULL == CU_add_test(pSuite, "test_node_init", test_node_init)) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    if (NULL == CU_add_test(pSuite, "test_node_in_segment", test_node_in_segment)) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    

    // Run the tests and show the run summary
    CU_basic_run_tests();
    return CU_get_number_of_tests_failed();
}