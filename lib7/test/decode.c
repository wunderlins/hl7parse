#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <errno.h>
#include <string.h>
#include <ctype.h>
#ifdef __APPLE__
#include <sys/syslimits.h> // for PATH_MAX on OSX
#endif
#include "../decode.h"
#include "CUnit/Basic.h"
#include "../generated/test.h"

const int loglevel = LEVEL_ERROR;

char *dirname = NULL;
int files_length = 13;
char files[13][PATH_MAX] = {
    "MDM^T02_Beispiel.hl7",
    "min.hl7", 
    "minimal.hl7",
    "minimal_pid_CR_bas64_LF.hl7", 
    "minimal_pid_CR.hl7",
    "minimal_pid_CRLF.hl7", 
    "minimal_pid_CR_with_additional_LF.hl7", 
    "minimal_pid_LF_bogous_base64encoding.hl7", 
    "minimal_pid_LF.hl7", 
    "minimal_subcmp_CRLF.hl7", 
    "minimal_subcmp.hl7", 
    "minimal_subnodes.hl7", 
    // "missing_2nd_delim.hl7", -> exit code 11
    "segfault.hl7"
};

char files_error[2][PATH_MAX] = {
    "test.pdf",
    "error_too_long_segment.hl7"
};

static void cb_progress(message_t *message, size_t total, size_t current) {
    //printf("Progress: total: %d, current: %d\n", total, current);
    //printf("Parsing: %02d%%\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b", (current*100/total)+1);
    fflush(stdout);
}

static void cb_end(message_t *message, size_t max, size_t current, int exit_code) {
    // clear progress line in terminal
    //printf("\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b");
    if (exit_code != 0) {
        printf("Parser failed with exit code: %02d", exit_code);
    } else {
        printf("%02d", exit_code);
    }
}

int decode_file(char *file) {
    int ret = -1; 

    // HL7 File descriptor
	FILE *fd;
	if ((fd = fopen(file, "rb")) == NULL) {
		print_error(errno, file);
		return 1;
	}

    // get file size
    size_t fd_pos = ftell(fd);
    fseek(fd, 0, SEEK_END);
    size_t file_size = ftell(fd);
    fseek(fd, fd_pos, SEEK_SET); // reset

    // initialize separator data structure
    hl7_meta_t* hl7_meta = init_hl7_meta_t();

    // check if this file has a bom;
    // this will move df at the first byte after the BOM
    hl7_meta->bom = detect_bom(fd); 

    // prepare parser
    message_t *message = create_message_t(hl7_meta);

    // example of a progress callback, it will show on very large files
    message->state->cb_progress = cb_progress;
    // call progress every 5%
    message->state->progress_every = file_size / 20;
    // add a callback at the end of paring, which will clear the progress line
    message->state->cb_end = cb_end;

    // parse hl7 file
    ret = hl7_decode(fd, &message);

    if (ret != 0) {
        fprintf(stderr, "We failed to parse message at line %d, exiting with %d.\n", message->num_children, ret);
    }
    /*
    char* meta_string = hl7_meta_string(hl7_meta);
    log_info("%s", meta_string);
    free(meta_string);
    */
    //dump_structure(message);

    // cleanup
    if (message != NULL)
        free_message_t(message);
    fclose(fd);
    //free_hl7_meta(hl7_meta);

    return ret;
}

void test_decode_success() {
    printf("\n");
    for (int i=0; i<files_length; i++) {
        char filename_fqd[PATH_MAX] = {0};
        sprintf(filename_fqd, "%s/%s", dirname, files[i]);

        printf("Testing %s: ", filename_fqd);
        int ret = decode_file(filename_fqd);
        printf("\n");
        CU_ASSERT_EQUAL(ret, 0);
    }
}

void test_decode_error() {
    printf("\n");
    char filename_fqd[PATH_MAX] = {0};
    sprintf(filename_fqd, "%s/%s", dirname, "error_too_long_segment.hl7");

    printf("Testing %s: ", filename_fqd);
    int ret = decode_file(filename_fqd);
    printf("\n");
    CU_ASSERT_EQUAL(ret, 22);
}

int main(int argc, char *argv[]) {
    // Initialize the CUnit test registry
    if (CUE_SUCCESS != CU_initialize_registry())
        return CU_get_error();
    
    // Sets the basic run mode, CU_BRM_VERBOSE will show maximum output of run details
    // Other choices are: CU_BRM_SILENT and CU_BRM_NORMAL
    CU_basic_set_mode(CU_BRM_VERBOSE);

    CU_pSuite pSuite = NULL;

    // Add a suite to the registry
    pSuite = CU_add_suite("decode", 0, 0);
    // Always check if add was successful
    if (NULL == pSuite) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // setup tests by looking for an existing base directory
    if(argc != 2) {
        //printf("Failed because of missing directory, pass it into test via argv[1]\n");
        // return 1;
        dirname = malloc(PATH_MAX);
        sprintf(dirname, "%s", datadir);
    } else {
        dirname = argv[1];
    }
    printf("dirname: %s\n", dirname);

    // test known node cases
    if (NULL == CU_add_test(pSuite, "test_decode_success", test_decode_success)) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // test known node cases
    if (NULL == CU_add_test(pSuite, "test_decode_error", test_decode_error)) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // Run the tests and show the run summary
    CU_basic_run_tests();
    return CU_get_number_of_tests_failed();
}