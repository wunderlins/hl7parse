#include "../buffer.h"
#include "CUnit/Basic.h"

static const int buf_prealloc_size = 256;

void test_new() {
    buf_t *b = new_buf_t();
    CU_ASSERT_EQUAL(b->length, 0);
    CU_ASSERT_EQUAL(b->allocated, buf_prealloc_size);
    CU_ASSERT_EQUAL(b->buffer[0], 0);
}

void test_append_small() {
    buf_t *b = new_buf_t();
    int ret = append_buf_str(b, 13, "012345678912");
    CU_ASSERT_EQUAL(b->length, 12);
    CU_ASSERT_EQUAL(b->allocated, buf_prealloc_size);
    CU_ASSERT_EQUAL(strcmp(b->buffer, "012345678912"), 0);
}

void test_append_large() {
    buf_t *b = new_buf_t();
    int ret;
    for (int i=0; i<29; i++)
        ret = append_buf_str(b, 11, "0123456789");
    CU_ASSERT_EQUAL(b->length, 290);
    CU_ASSERT_EQUAL(b->allocated, buf_prealloc_size*2);
    CU_ASSERT_EQUAL(b->buffer[0], '0');
}

int main(int argc, char *argv[]) {
    // Initialize the CUnit test registry
    if (CUE_SUCCESS != CU_initialize_registry())
        return CU_get_error();

    // Sets the basic run mode, CU_BRM_VERBOSE will show maximum output of run details
    // Other choices are: CU_BRM_SILENT and CU_BRM_NORMAL
    CU_basic_set_mode(CU_BRM_VERBOSE);

    CU_pSuite pSuite = NULL;

    // Add a suite to the registry
    pSuite = CU_add_suite("buffer", 0, 0);
    // Always check if add was successful
    if (NULL == pSuite) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // Add the test to the suite
    if (NULL == CU_add_test(pSuite, "test_new", test_new)) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    if (NULL == CU_add_test(pSuite, "test_append_small", test_append_small)) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    if (NULL == CU_add_test(pSuite, "test_append_large", test_append_large)) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // Run the tests and show the run summary
    CU_basic_run_tests();
    return CU_get_number_of_tests_failed();
}
