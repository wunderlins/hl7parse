#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <errno.h>
#include <string.h>
#include <ctype.h>
#ifdef __APPLE__
#include <sys/syslimits.h> // for PATH_MAX on OSX
#endif
#include "../meta.h"
#include "CUnit/Basic.h"
#include "../generated/test.h"

// this is the directory where we are looking for files
// passed in as argv
DIR* dir = NULL;
char *dirname = NULL;

char *file_path(char* file) {
    char *f = malloc(PATH_MAX);
    sprintf(f, "%s/%s", dirname, file);
    return f;
}

// test creation of meta structure
void test_meta_init() {
    hl7_meta_t *meta = init_hl7_meta_t();

    printf("\n%s\n", hl7_meta_string(meta));
    CU_ASSERT_EQUAL(meta->sep_message, '\r');
    CU_ASSERT_EQUAL(meta->sep_field,   '|');
    CU_ASSERT_EQUAL(meta->sep_comp,    '^');
    CU_ASSERT_EQUAL(meta->sep_rep,     '~');
    CU_ASSERT_EQUAL(meta->sep_escape,  '\\');
    CU_ASSERT_EQUAL(meta->sep_subcmp,  '&');
    CU_ASSERT_EQUAL(meta->crlf,         -1);

    CU_ASSERT_EQUAL(meta->encoding,  NULL);
    CU_ASSERT_EQUAL(meta->version,   NULL);
    CU_ASSERT_EQUAL(meta->type,      NULL);
    CU_ASSERT_EQUAL(meta->subtype,   NULL);

    CU_ASSERT_EQUAL(meta->bom, NULL);
    /*
    CU_ASSERT_EQUAL(meta->bom->length, 0);
    CU_ASSERT_EQUAL(meta->bom->bom, NULL);
    CU_ASSERT_EQUAL(meta->bom->endianness, UNKNOWN);
    */

    free_hl7_meta(meta);
}

// test parsing of MSH-1 and MSH-2
void test_meta_read_delimiters() {
    int ret = 0;
    FILE *fd;
    hl7_meta_t *meta = init_hl7_meta_t();
    char *file = NULL;

    file = file_path("oru/R01.hl7");
    fd = fopen(file, "rb");
    CU_ASSERT_NOT_EQUAL_FATAL(fd, NULL);
    ret = read_meta(meta, fd);
    fclose(fd);

    CU_ASSERT_EQUAL(ret, 0);

    printf("\n%s\n%s\n", file, hl7_meta_string(meta));

    CU_ASSERT_EQUAL(meta->crlf,         -1);
    CU_ASSERT_EQUAL(meta->sep_message, '\r');
    CU_ASSERT_EQUAL(meta->sep_field,   '|');
    CU_ASSERT_EQUAL(meta->sep_comp,    '^');
    CU_ASSERT_EQUAL(meta->sep_rep,     '~');
    CU_ASSERT_EQUAL(meta->sep_escape,  '\\');
    CU_ASSERT_EQUAL(meta->sep_subcmp,  '&');

    CU_ASSERT_EQUAL(meta->encoding,  NULL);
    CU_ASSERT_EQUAL(meta->version,   NULL);
    CU_ASSERT_EQUAL(meta->type,      NULL);
    CU_ASSERT_EQUAL(meta->subtype,   NULL);

    CU_ASSERT_EQUAL(meta->bom->length, 0);
    CU_ASSERT_EQUAL(meta->bom->bom, NULL);
    CU_ASSERT_EQUAL(meta->bom->endianness, UNKNOWN);
    free_hl7_meta(meta);

    meta = init_hl7_meta_t();
    file = file_path("oru/R01-dollar.hl7");
    fd = fopen(file, "rb");
    CU_ASSERT_NOT_EQUAL_FATAL(fd, NULL);
    ret = read_meta(meta, fd);
    fclose(fd);

    CU_ASSERT_EQUAL(ret, 0);
    printf("\n%s\n%s\n", file, hl7_meta_string(meta));

    CU_ASSERT_EQUAL(meta->crlf,         -1);
    CU_ASSERT_EQUAL(meta->sep_message, '\r');
    CU_ASSERT_EQUAL(meta->sep_field,   '$');
    CU_ASSERT_EQUAL(meta->sep_comp,    '^');
    CU_ASSERT_EQUAL(meta->sep_rep,     '~');
    CU_ASSERT_EQUAL(meta->sep_escape,  '\\');
    CU_ASSERT_EQUAL(meta->sep_subcmp,  '&');

    CU_ASSERT_EQUAL(meta->encoding,  NULL);
    CU_ASSERT_EQUAL(meta->version,   NULL);
    CU_ASSERT_EQUAL(meta->type,      NULL);
    CU_ASSERT_EQUAL(meta->subtype,   NULL);

    CU_ASSERT_EQUAL(meta->bom->length, 0);
    CU_ASSERT_EQUAL(meta->bom->bom, NULL);
    CU_ASSERT_EQUAL(meta->bom->endianness, UNKNOWN);
    free_hl7_meta(meta);
}

// test what happens on empty files
void test_meta_read_empty() {
    int ret = 0;
    FILE *fd;
    hl7_meta_t *meta = init_hl7_meta_t();
    char *file = file_path("bom/empty.hl7");
    fd = fopen(file, "rb");
    CU_ASSERT_NOT_EQUAL_FATAL(fd, NULL);
    ret = read_meta(meta, fd);
    fclose(fd);

    CU_ASSERT_EQUAL(ret, 3);
    printf("\n%s\n%s\n", file, hl7_meta_string(meta));
    free_hl7_meta(meta);
}

// test what happens if MSH-2 is not ended with MSH-1
void test_meta_read_undelimited_msh2() {
    int ret = 0;
    FILE *fd;
    hl7_meta_t *meta = init_hl7_meta_t();
    char *file = file_path("missing_2nd_delim.hl7");
    fd = fopen(file, "rb");
    CU_ASSERT_NOT_EQUAL_FATAL(fd, NULL);
    ret = read_meta(meta, fd);
    fclose(fd);

    CU_ASSERT_EQUAL(ret, 1);
    printf("\n%s\n%s\n", file, hl7_meta_string(meta));
    free_hl7_meta(meta);
}

// test what happens on empty MSH-2 segments (default should be used)
void test_meta_read_empty_msh2() {
    int ret = 0;
    FILE *fd;
    hl7_meta_t *meta = init_hl7_meta_t();
    char *file = file_path("minimal_subcmp.hl7");
    fd = fopen(file, "rb");
    CU_ASSERT_NOT_EQUAL_FATAL(fd, NULL);
    ret = read_meta(meta, fd);
    fclose(fd);

    CU_ASSERT_EQUAL(ret, 0);
    printf("\n%s\n%s\n", file, hl7_meta_string(meta));

    CU_ASSERT_EQUAL(meta->crlf,         -1);
    CU_ASSERT_EQUAL(meta->sep_message, '\r');
    CU_ASSERT_EQUAL(meta->sep_field,   '|');
    CU_ASSERT_EQUAL(meta->sep_comp,    '^');
    CU_ASSERT_EQUAL(meta->sep_rep,     '~');
    CU_ASSERT_EQUAL(meta->sep_escape,  '\\');
    CU_ASSERT_EQUAL(meta->sep_subcmp,  '&');

    CU_ASSERT_EQUAL(meta->encoding,  NULL);
    CU_ASSERT_EQUAL(meta->version,   NULL);
    CU_ASSERT_EQUAL(meta->type,      NULL);
    CU_ASSERT_EQUAL(meta->subtype,   NULL);

    CU_ASSERT_EQUAL(meta->bom->length, 0);
    CU_ASSERT_EQUAL(meta->bom->bom, NULL);
    CU_ASSERT_EQUAL(meta->bom->endianness, UNKNOWN);
    free_hl7_meta(meta);
}

void test_meta_filedelimiter() {

    line_delimiter_t ret = -2;
    FILE *fd;
    hl7_meta_t *meta = init_hl7_meta_t();
    char *file = NULL;
    
    // test CR
    file = file_path("minimal_pid_CR.hl7");
    fd = fopen(file, "rb");
    CU_ASSERT_NOT_EQUAL_FATAL(fd, NULL);
    ret = find_line_delimiter(fd);
    fclose(fd);
    CU_ASSERT_EQUAL(ret, DELIM_CR);

    // test LF
    file = file_path("minimal_pid_LF.hl7");
    fd = fopen(file, "rb");
    CU_ASSERT_NOT_EQUAL_FATAL(fd, NULL);
    ret = find_line_delimiter(fd);
    fclose(fd);
    CU_ASSERT_EQUAL(ret, DELIM_LF);

    // test CRLF
    file = file_path("minimal_pid_CRLF.hl7");
    fd = fopen(file, "rb");
    CU_ASSERT_NOT_EQUAL_FATAL(fd, NULL);
    ret = find_line_delimiter(fd);
    fclose(fd);
    CU_ASSERT_EQUAL(ret, DELIM_CRLF);

}


int main(int argc, char *argv[]) {
    // Initialize the CUnit test registry
    if (CUE_SUCCESS != CU_initialize_registry())
        return CU_get_error();
    
    // Sets the basic run mode, CU_BRM_VERBOSE will show maximum output of run details
    // Other choices are: CU_BRM_SILENT and CU_BRM_NORMAL
    CU_basic_set_mode(CU_BRM_VERBOSE);

    CU_pSuite pSuite = NULL;

    // Add a suite to the registry
    pSuite = CU_add_suite("meta", 0, 0);
    // Always check if add was successful
    if (NULL == pSuite) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // setup tests by looking for an existing base directory
    if(argc != 2) {
        //printf("Failed because of missing directory, pass it into test via argv[1]\n");
        //return 1;
        dirname = datadir;
    } else {
        dirname = argv[1];
    }

    // check if directory exists
    DIR* dir = opendir(dirname);
    if (dir) {
        /* Directory exists. */
        closedir(dir);
    } else if (ENOENT == errno) {
        /* Directory does not exist. */
        printf("Directory with tests could not be found: %s\n", dirname);
        return 2;
    } else {
        /* opendir() failed for some other reason. */
        printf("Directory with tests could not be opened: %s\n", dirname);
        return 2;
    }

    // test known meta cases
    if (NULL == CU_add_test(pSuite, "test_meta_init", test_meta_init)) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    if (NULL == CU_add_test(pSuite, "test_meta_read_delimiters", test_meta_read_delimiters)) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    
    if (NULL == CU_add_test(pSuite, "test_meta_read_empty", test_meta_read_empty)) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    if (NULL == CU_add_test(pSuite, "test_meta_read_undelimited_msh2", test_meta_read_undelimited_msh2)) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    if (NULL == CU_add_test(pSuite, "test_meta_read_empty_msh2", test_meta_read_empty_msh2)) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    if (NULL == CU_add_test(pSuite, "test_meta_filedelimiter", test_meta_filedelimiter)) {
        CU_cleanup_registry();
        return CU_get_error();
    }


    // Run the tests and show the run summary
    CU_basic_run_tests();
    return CU_get_number_of_tests_failed();
}