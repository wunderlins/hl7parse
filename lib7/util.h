/** \file
 * @brief hl7 utilitiy functions
 */
#ifndef UTIL_H
#define UTIL_H

#include <stddef.h>
#include <string.h>

#include "node.h"

#ifdef _WIN32
    /** windows long int */
    #define SIZE_T_L "%lld"
#else
    /** unix long int */
    #define SIZE_T_L "%ld"
#endif

// ascii for window's CMD.EXE
#ifdef _WIN32
    /** console marker for node */
    #define MARKER_T "\xC3"
    /** console marker for leaf */
    #define MARKER_L "\xC0"
    /** console marker for none */
    #define MARKER_D "\xC4"
#else // UTF-8 for the rest
    /** console marker for node */
    #define MARKER_T "\u251C"
    /** console marker for leaf */
    #define MARKER_L "\u2514"
    /** console marker for none */
    #define MARKER_D "\u2500"
#endif

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief copy a chunck of memory
 * 
 * Will allocate memory for you, you are in charge to free it later.
 * 
 * @param src byte array to copy
 * @param length length of input array
 * @returns pointer to copy byte array 
 */
void *memdup(void* src, size_t length);

/**
 * @brief print a parsed HL7 structure
 * 
 * debug function, used by 7parse to display an ASCII tree of the file content.
 * 
 * @param message the message to print
 */
void dump_structure(message_t *message);

/**
 * @brief trim white space at the beginnign and end of a string
 * 
 * @note
 * all trailing bytes of whitespace are replaced with '\0' in the original 
 * string and the pointer is shifted at the beginning to the first non 
 * whitespace character. You will loos track of the original beginning of 
 * the memory chunk. Use with care (always use a copy of the original pointer).
 * 
 * @param[out] str to string
 * @returns pointer to the new beginning of the string
 */
char *trim(char *str);

/**
 * @brief escape a character in a string
 * 
 * This function will allcoate a new string with all occourances of
 * quote_char prepended with escape_char.
 * 
 * You must take care of freeing the result!
 * 
 * @param str string to escape
 * @param quote_char the character to find and escape
 * @param escape_char the character to prepend in front of quote char
 * @return char array with double quote backslash escaped
 */
char *escape(char *str, char *quote_char, char escape_char);

#ifdef __cplusplus
}
#endif

#endif // UTIL_H