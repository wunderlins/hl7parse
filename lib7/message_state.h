/** \file 
 * @brief private message structures
 * 
 * This file describes a structure that is used to keep the state
 * of the parser while parsing a hl7 file. It is primarely used for
 * registering callback functions.
 * 
 * If you want to analyze parts of the document while the parser is still 
 * running or you are using it in a multi-threaded environment, then there is 
 * the possibility to add callback functions. The following callback hooks 
 * are available:
 * - `void (*cb_progress)(message_t *message, size_t total, size_t current);`
 * - `void (*cb_start)(message_t *message)`
 * - `void (*cb_end)(message_t *message, size_t max, size_t current, int exit_code)`
 * - `void (*cb_segment)(message_t *message, size_t num, char name[3])`
 * 
 * Also, you can controll how often the `cb_progress()` callback is fired, 
 * default is every 1% of progress (does not fire on files smaller 
 * than 100 bytes).
 * 
 * You may register callbacks as follows:
 * 
 * ```C
 * #include <stdio.h>
 * #include "decode.h" // or lib7.h which is the combined header for lib7.so
 * 
 * static void cb_progress(message_t *message, size_t total, size_t current) {
 *     printf("Parsing: %02d%%\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b", (current*100/total)+1);
 *     fflush(stdout);
 * }
 * 
 * static void cb_end(message_t *message, size_t max, size_t current, int exit_code) {
 *     // clear progress line in terminal
 *     printf("\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b");
 *     if (exit_code != 0) {
 *         printf("Parser failed with exit code: %02d\n", exit_code);
 *     } 
 * }
 * 
 * int main() {
 *     char *filename "some_file.hl7";
 *     FILE *fd = fopen(filename, "rb");
 *     message_t root = create_message_t(NULL);
 * 
 *     // example of a progress callback, it will show on very large files
 *     size_t fd_pos = ftell(fd);
 *     fseek(fd, 0, SEEK_END);
 *     size_t file_size = ftell(fd);
 *     fseek(fd, fd_pos, SEEK_SET); // reset
 *     
 *     // register callback progress
 *     root->state->cb_progress = cb_progress;
 *     // call progress every 5%
 *     root->state->progress_every = file_size / 20;
 *     // add a callback at the end of paring, which will clear the progress line
 *     root->state->cb_end = cb_end;
 * 
 * 
 *     // start the parser
 *     int ret = hl7_decode(fd, &root);
 *   
 *     fclose(fd); // no need for fd anymore, all data in memory
 *     if (ret != 0) {
 *       // whoopsie
 *       free_message_t(root);
 *     }
 *   
 *     // do something with the data in root
 *   
 *     // cleanup
 *     free_message_t(root);
 * }
 * ```
 * 
 */
#ifndef MESSAGE_STATE_H
#define MESSAGE_STATE_H

#include "node.h"
typedef struct message_t message_t; // forward declaration
/**
 * @brief holds callbacks and associated variables
 */
typedef struct message_state_t {
    /**
     * @brief fire progress callback every N bytes
     * 
     * If this value is 0, then progress will be fired every at every 1% 
     * of progress.
     */
    int progress_every;

    /**
     * @brief The total length of the parsed file
     * 
     * This is roughly file size (minus the size of the bom if there is any).
     * `parsed_length` will be set by the parser before the parsing starts.
     */
    size_t parsed_length;

    /**
     * @brief progress callback
     * 
     * This callback will fire every N bytes and will provide the numebr of 
     * currently read bytes `current` and the number of total bytes to 
     * read `total`.
     * 
     * Be carefull how often you make this event fire, on very large files
     * (MDM with base64 embeedded data for example) this could be fired 
     * millions of times.
     */
    void (*cb_progress)(message_t *message, unsigned long total, unsigned long current);

    /** 
     * @brief start event, will be fired once when the parser starts
     */
    void (*cb_start)(message_t *message);

    /**
     * @brief finish callback. 
     * 
     * Will be fired when parsing finished and will provide
     * the numebr of bytes processd and the exit code of the parser.
     */
    void (*cb_end)(message_t *message, unsigned long max, unsigned long current, int exit_code);

    /**
     *@brief segment parsed callback
     * 
     * will be fired once a segment is parsed and will provide the segment 
     * number `num` (roughly equals line number, blank lines not counted) 
     * and the segment name `name`
     */
    void (*cb_segment)(message_t *message, unsigned long num, char name[3]);
} message_state_t;

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief initializes an message_state_t structure
 */
message_state_t *message_state_new();

/**
 * @brief frees an messate_state_t structure
 */
void message_state_free(message_state_t *ms);

/*
static size_t hl7_decode_cb_max, 
              hl7_decode_cb_current, 
              hl7_decode_cb_every = 0;

static void (*hl7_decode_cb_callback)(size_t max, size_t current);
static void hl7_decode_cb_reset() {
    hl7_decode_cb_max = 0;
    hl7_decode_cb_current = 0;
    hl7_decode_cb_every = 0;
}

static void hl7_decode_cb(size_t max, size_t current) {
    // global vars to track progress, be careful, this is not thread safe
    hl7_decode_cb_callback(hl7_decode_cb_max, hl7_decode_cb_current);
}

void hl7_decode_cb_register(void (*callback)(size_t max, size_t current), size_t max, size_t every) {
    hl7_decode_cb_reset();
    hl7_decode_cb_every    = every;
    hl7_decode_cb_max      = max;
    hl7_decode_cb_callback = callback;
}
*/

#ifdef __cplusplus
}
#endif

#endif // MESSAGE_STATE_H