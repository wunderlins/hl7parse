#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "bom.h"
#include "meta.h"
#include "decode.h"

int main() {
	int ret = 0;
	
	FILE *fd;
	hl7_meta_t* hl7_meta;

	fd = fopen("../data/test/msh_minimal.hl7", "rb");

	// initialize separator data structure
	hl7_meta = init_hl7_meta_t();
	
	// check if this file has a bom;
	// this will move df at the first byte after the BOM
	hl7_meta->bom = detect_bom(fd); 

	// no new line in file, premature end
    message_t *message = create_message_t(hl7_meta);
	ret = hl7_decode(fd, &message);
	printf("%s", hl7_meta_string(hl7_meta));
	printf("ret: %d\n", ret);
	assert(ret == 3); 
	fclose(fd);

	assert((hl7_meta->sep_message == '\r'));
	assert((hl7_meta->crlf == 0));
	assert((hl7_meta->sep_field == '|'));
	assert((hl7_meta->sep_comp == '^'));
	assert((hl7_meta->sep_rep == '~'));
	assert((hl7_meta->sep_escape == '\\'));
	assert((hl7_meta->sep_subcmp == '&'));
	
	assert((hl7_meta->encoding == NULL));
	assert((hl7_meta->version == NULL));
	assert((hl7_meta->type == NULL));
	assert((hl7_meta->subtype == NULL));
	free(hl7_meta);

	fd = fopen("../data/test/msh_minimal_sep.hl7", "rb");

	// initialize separator data structure
	hl7_meta = init_hl7_meta_t();
	
	// check if this file has a bom;
	// this will move df at the first byte after the BOM
	hl7_meta->bom = detect_bom(fd); 

	// no new line in file, premature end
 	free_message_t(message);
    message_t *message = create_message_t(hl7_meta);
	ret = hl7_decode(fd, &message);
	printf("%s", hl7_meta_string(hl7_meta));
	printf("ret: %d\n", ret);
	assert(ret == 0); 
	fclose(fd);

	assert((hl7_meta->sep_message == '\n'));
	assert((hl7_meta->crlf == 0));
	assert((hl7_meta->sep_field == '1'));
	assert((hl7_meta->sep_comp == '2'));
	assert((hl7_meta->sep_rep == '3'));
	assert((hl7_meta->sep_escape == '4'));
	assert((hl7_meta->sep_subcmp == '5'));
	
	assert((hl7_meta->encoding == NULL));
	assert((hl7_meta->version == NULL));
	assert((hl7_meta->type == NULL));
	assert((hl7_meta->subtype == NULL));
	//free(hl7_meta);
	
	free_message_t(message);
	
	return ret;
}