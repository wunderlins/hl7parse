#ifndef MESSAGE_H
#define MESSAGE_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "datatypes.h"

/**
 * Search for the first occourance of "\n" "\r" or "\r\n"
 * 
 * Returns the poistion of the first delimiter. Returns 0 if no delimiter was found.
 * 
 * @param fp file handle
 * @param delim returns the first found delimiter
 * @param msh returns the full msh line
 */
int find_delim_msg(FILE* fp, char **delim, char **msh);

int find_delim_seg(char* msh_line, el_sep_t* delims);

#endif