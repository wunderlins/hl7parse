#include <iostream>
#include <sstream>
#include <string>
#include <map>
#include "node.hpp"

using namespace std;

string nodeTypes[] = {
    "NodeList",
    "Node",

    "SubNodeList",
    "SubNode",

    "SubSubNodeList",
    "Leaf"
};

Node::Node(NodeType type, Node* parent): 
    type(type), parent(parent) {
}

Node* Node::append(NodeType type, Node* parent) {
    if (!parent) parent = this;
    Node* n = new Node(type, parent);
    n->seq = children.size();
    children.emplace(n->seq, n);
    return n;
}

void Node::append(Node* n) {
    n->seq = children.size();
    n->parent = this;
    children.emplace(n->seq, n);
}

auto Node::size()  { return children.size(); }
auto Node::begin() { return children.begin(); }
auto Node::end()   { return children.end(); }

string Node::get_type() { return nodeTypes[this->type]; }

string Node::address() {
    string r = to_string(this->seq);
    Node *n = this;
    while (n->parent && n->parent->type != NODELIST) {
        n = n->parent;
        r = to_string(n->seq) + "-" + r;
    }

    return r;
}

Leaf::Leaf(const char *data, Node* parent): Node(LEAF, parent) {
    this->data = data;
}

Leaf* Leaf::append(const char *data, Node* parent) {
    Leaf* n = new Leaf(data, parent);
    n->seq = children.size();
    children.emplace(n->seq, n);
    return n;
}

/* Overloading * operator on string */
string operator*(const string& s, unsigned int n) {
    stringstream out;
    while (n--)
        out << s;
    return out.str();
}

string operator*(unsigned int n, const string& s) { return s * n; }

void printTree(Node* node, int indent) {
    string tab = "\t";

    if (node->type == LEAF) {
        cout << (tab * indent) << node->seq << "(" << node->address() << "), " << node->get_type() << ": " << node->data << endl;
    } else {
        cout << (tab * indent) << node->seq << "(" << node->address() << "), " << node->get_type() << endl;
    }

    NodeList::iterator it = node->begin();
    for (it=node->begin(); it != node->end(); ++it) {
        printTree(it->second, indent+1);
    }

}
