#include "field.h"

hl7_field_t* hl7_field_create(char* buff, int length) {
  hl7_field_t *el = malloc(sizeof(hl7_field_t));
  el->value = (char*) malloc(sizeof(char) * (length + 1));
  memcpy(el->value, buff, (length + 1));
  el->length = length;

  return el;
}