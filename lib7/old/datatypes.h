#ifndef DATATYPES_H
#define DATATYPES_H

typedef struct {
	char *sep;
	int length;
}	el_sep_t;

struct hl7_field_tt {
	char *value;
	int length;
	int type;
	struct hl7_field_tt** children;
};
typedef struct hl7_field_tt hl7_field_t;

typedef struct {
	char name[4];
	char *raw;
	int raw_length;
	int length;
	hl7_field_t **fields;
} hl7_segment_t;

typedef struct {
	hl7_segment_t *segments;
	int length;
} hl7_message_t;


#endif
