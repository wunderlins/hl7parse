#ifndef FIELD_H
#define FIELD_H

#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "datatypes.h"

hl7_field_t* hl7_field_create(char* buff, int length);

#endif