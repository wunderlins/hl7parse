#include <stdio.h>
#include <errno.h>
#include "message.h"
#include "segment.h"
#include "field.h"

void print_error(int e, char* additional) {
	if (additional == NULL)
		fprintf(stderr, "%d, %s\n", e, strerror(e));
	else 
		fprintf(stderr, "%d, %s, %s\n", e, strerror(e), additional);
}

int main(int argc, char** argv) {
	int i = 0;

	if (argc != 2) {
		printf("Usage: parse <file name>\n");
		return 1;
	}
	
	FILE *fd;
	if ((fd = fopen(argv[1], "rb")) == NULL) {
		print_error(errno, argv[1]);
		return 1;
	}
	
	// do some prelimenary parsing of the MSG segment to detirmine how to 
	// handle the file. We want to know:
	// 
	// - segment delimiters
	// - field delimiters
	// - encoding
	// - message type
	// - hl7 version
	char *delim;
	char *msh;
	int ret = 0;
	
	// FIXME: add a struct for all delimiter information.
	//        then add this struct to the message data structure
	
	// get header information and line seperator
	ret = find_delim_msg(fd, &delim, &msh);
	printf("%d, '\\%02hhx\\%02hhx', %s\n", ret, delim[0], delim[1], msh);
	
	// get field separator
	el_sep_t delim_seg;
	ret = find_delim_seg(msh, &delim_seg);
	printf("delim_seg: %s\n", delim_seg.sep);
	
	// get encoding
	// TBD

	hl7_segment_t *msh_segment;
	hl7_segment_parse(msh, &msh_segment, &delim_seg);
	
	for(i = 0; i < msh_segment->length; i++) {
		printf("seg[%02d]: %s\n", i, msh_segment->fields[i]->value);
	}

	// free parsed data
	hl7_segment_free(msh_segment);

	// free local ressouces
	free(delim_seg.sep);
	//free(delim);
	free(msh);
	fclose(fd);
	return 0;
}
