#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>

#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <map>

#include <ext/stdio_filebuf.h>
#include <iostream>
#include <fstream>

#include "node.hpp"

using namespace std;

typedef struct {
	char *field;
    char *line;
    char c;
	int field_length = 0;
    int needs_trim = 0;
}	el_sep_t;

void print_error(int e, char* additional) {
	if (additional == NULL)
		fprintf(stderr, "%d, %s\n", e, strerror(e));
	else 
		fprintf(stderr, "%d, %s, %s\n", e, strerror(e), additional);
}

/**
 * Search for the first occourance of "\n" "\r" or "\r\n"
 * 
 * Returns the poistion of the first delimiter. Returns 0 if no delimiter was found.
 * 
 * @param fp file handle
 * @param delim returns the first found delimiter
 * @param msh returns the full msh line
 * @return 0 on error, length of line on success
 */
int find_delim_msg(FILE* fp, el_sep_t* delim, char **msh) {
	char msh_buffer[4096];
	
	// rewind
	long old_pos = ftell(fp);
	fseek(fp, 0, SEEK_SET);
	
	int  i = 0;
	char c = 0;
	while((c = getc(fp)) != EOF) {
		if (c == '\r' || c == '\n') {
			// figure out if we have single or double char delimiter (eg. \r\n)
			char c1 = getc(fp);
			if (c == '\r' && c1 == '\n') {
				//printf("rn\n");
				delim->line = (char*) malloc(sizeof(char) * 3);
				strcpy(delim->line, "\r\n");
			} else {
				//printf("r|n\n");
				delim->line = (char*) malloc(sizeof(char) * 2);
				(delim->line)[0] = c;
				(delim->line)[1] = 0;
			}
			
			int s = sizeof(char) * (i + 1);
			*msh = (char*) malloc(s);
			memcpy(*msh, msh_buffer, s);
			
            delim->c = delim->line[0];
            if (delim->line[1] == '\n') {
                delim->needs_trim = 1;
                delim->c = '\n';
            }

			return i;
		}
		
		msh_buffer[i++] = c;
		msh_buffer[i]   = 0;
	}

	
	// reset file pointer
	fseek(fp, old_pos, SEEK_SET);
	return 0;
}

/**
 * extract the field seperators from MSH-2
 *
 * @param msh_line raw msh message up until the segment delimiter
 * @param delims contains an array of delimiter characters and the length of the array
 * @return 0 on succes otherwise error code
 */
int find_delim_seg(char* msh_line, el_sep_t* delims) {
	if (msh_line[0] != 'M' || msh_line[1] != 'S' || (msh_line[2] != 'H' && msh_line[2] != 'A'))
		return 1;
	
	char d[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	int i = 3;
	int o = 0;
	char c = 0;
	int len = strlen(msh_line);
	char delim_char = msh_line[3];

    //cout << "len: " << len << ", delim_char: " << delim_char << endl;
	
	while (((c = msh_line[i]) != delim_char || i < 4) && i < len) {
        //cout << i << ": " << c;
		if (c == '\\') {
			i++;
			continue;
		}
		d[o++] = c;
		i++;

        //cout << endl;
	}
	
	delims->field_length = strlen(d);
	delims->field = (char*) malloc(sizeof(char) * (i-4+1));
	memcpy(delims->field, d, delims->field_length);
	
	return 0;
}

// https://rosettacode.org/wiki/Tokenize_a_string_with_escaping#C.2B.2B
vector<string> tokenize(string input, char seperator, char escape) {
	vector<string> output;
	string token = "";
 
	bool inEsc = false;
	for (char ch : input) {
		if (inEsc) {
			inEsc = false;
		}
		else if (ch == escape) {
			inEsc = true;
			continue;
		}
		else if (ch == seperator) {
			output.push_back(token);
			token = "";
			continue;
		}
		token += ch;
	}
	if (inEsc) {
		throw "Invalid terminal escape";
	}
 
	output.push_back(token);
	return output;
}


int main(int argc, char** argv) {

	if (argc != 2) {
		printf("Usage: parse <file name>\n");
		return 1;
	}
	
	FILE *fd;
	if ((fd = fopen(argv[1], "rb")) == NULL) {
		print_error(errno, argv[1]);
		return 1;
	}

	el_sep_t delim;
	char *msh;
	int ret = 0;
	
	// FIXME: add a struct for all delimiter information.
	//        then add this struct to the message data structure
	
	// get header information and line seperator
	ret = find_delim_msg(fd, &delim, &msh);
	printf("%d, '\\%02hhx\\%02hhx', %s\n", ret, delim.line[0], delim.line[1], msh);

	// get field separator
	ret = find_delim_seg(msh, &delim);
	printf("delim_seg: %s\n", delim.field);

    // hand our C file handle over to ifstream, this is ugly, replace 
    // previous functions with C++ implementations
    fseek(fd, 0, SEEK_SET);
    __gnu_cxx::stdio_filebuf<char> filebuf(fd, std::ios::in); // 1
    istream is(&filebuf); // 2
    
	map<int, Node*> segments;
	string line;
	int i = 0;

	while (getline(is, line, delim.c)) {
        // std::cout <<  line << endl;
		//cout << line.substr(0, 3) << endl; 

		// skip empty lines
		if (line == "")
			continue;


		// FIXME: this is not very optimized, the tokenizer has to loop over each character 6 times
		Node *nodelist = new Node(NODELIST);
        for (string nl : tokenize(line, delim.field[0], '\\')) { // |

			Node *node = new Node(NODE);
		    for (string n : tokenize(nl, delim.field[1], '\\')) { // ^
				
				Node *subnodelist = new Node(SUBNODELIST);
			    for (string sn : tokenize(n, delim.field[2], '\\')) { // ~

					Node *subnode = new Node(SUBNODE);
				    for (string sn1 : tokenize(sn, delim.field[3], '\\')) { // &

						Node *subsubnodelist = new Node(SUBSUBNODELIST);
						for (string sn2 : tokenize(sn1, delim.field[2], '\\')) { // ~
							cout << "--> " << sn2 << endl;
							char* data = (char*) malloc(sizeof(char) * (strlen(sn2.c_str()) + 1 ));
							strcpy(data, sn2.c_str());
						
							Leaf* l = new Leaf(data);
							subsubnodelist->append(l);
						}

						subnode->append(subsubnodelist);
					}

					subnodelist->append(subnode);

				}
				
				node->append(subnodelist);
			}

			nodelist->append(node);
        }
/*		
        for (string nl : tokenize(line, delim.field[0], '\\')) { // |

			Node *node = new Node(NODE);
		    for (string n : tokenize(nl, delim.field[2], '\\')) { // ~
				
				Node *subnodelist = new Node(SUBNODELIST);
			    for (string sn : tokenize(n, delim.field[1], '\\')) { // ^

					Node *subnode = new Node(SUBNODE);
				    for (string sn1 : tokenize(sn, delim.field[2], '\\')) { // ~

						Node *subsubnodelist = new Node(SUBSUBNODELIST);
						for (string sn2 : tokenize(sn, delim.field[3], '\\')) { // &
							cout << "--> " << sn2 << endl;
							char* data = (char*) malloc(sizeof(char) * (strlen(sn2.c_str()) + 1 ));
							strcpy(data, sn2.c_str());
						
							Leaf* l = new Leaf(data);
							subsubnodelist->append(l);
						}

						subnode->append(subsubnodelist);
					}

					subnodelist->append(subnode);

				}
				
				node->append(subnodelist);
			}

			nodelist->append(node);
        } */
		

        //cout << endl;
		//segments.insert({i++, nodelist});
		printTree(nodelist);
    }

	fclose(fd);
	return 0;
}

/*
int main() { 
    // create root node
    Node *message = new Node(NODELIST);

    // append children
    message->append(NODE);
    Node* n2 = message->append(NODE);

    // append child to child
    Node* sn2l = n2->append(SUBNODELIST);
	Node* sn2 = sn2l->append(SUBNODE);

    Node* ssn2l = sn2->append(SUBSUBNODELIST);
    Leaf* l1 = new Leaf("Data 1");
    Leaf* l2 = new Leaf("Data 2");
    ssn2l->append(l1);
    ssn2l->append(l2);

    printTree(message);

    return 0; 
}
*/