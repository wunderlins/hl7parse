#ifndef SEGMENT_H
#define SEGMENT_H

#include <stdlib.h>
#include <string.h>
#include "datatypes.h"
#include "field.h"

void hl7_segment_free(hl7_segment_t *seg);
int hl7_segment_parse(char *line, hl7_segment_t **segment, el_sep_t *delim_seg);

#endif