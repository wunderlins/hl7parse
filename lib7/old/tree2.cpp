#include <iostream>
#include <sstream>
#include <string>
#include <map>

using namespace std;

// Node types
enum NodeType {
    ANCHOR,
    ROOT,
    SEGMENT,
    NODELIST,
    LEAF
};

string nodeTypes[] = {
    "Anchor",
    "Root",
    "Segment",
    "NodeList",
    "Leaf"
};

class Leaf;

// Node class
class Node  { 
public:
    const char *data = nullptr;
    NodeType type;
    Node* parent;
    int seq = 0;
    std::map<int, Node*> children;

    Node(NodeType type, Node* parent = nullptr): 
        type(type), parent(parent) {
        
        // add a pseudo parent to ROOT so it has a parent and upward rtaversal does 
        // not fail because of missing parent
        if (type == ROOT) {
            Node* n = new Node(ANCHOR);
            this->parent = n;
        }
    }

    Node* append(NodeType type, Node* parent = nullptr) {
        if (!parent) parent = this;
        Node* n = new Node(type, parent);
        int seq = children.size();
        n->seq = children.size();
        children.emplace(n->seq, n);
        return n;
    }

    void append(Node* n) {
        n->seq = children.size();
        n->parent = this;
        children.emplace(n->seq, n);
    }

    auto size()  { return children.size(); }
    auto begin() { return children.begin(); }
    auto end()   { return children.end(); }

    string getType() { return nodeTypes[this->type]; }

    string address() {
        string r = to_string(this->seq);
        Node *n = this;
        while (n->parent->type != ANCHOR) {
            n = n->parent;
            r = to_string(n->seq) + "-" + r;
        }

        return r;
    }
};
typedef std::map<int, Node*> NodeList;

class Leaf : public Node {
public:
    Leaf(const char *data, Node* parent = nullptr) : 
    Node(LEAF, parent) {
        this->data = data;
    }

    Leaf* append(const char *data, Node* parent = nullptr) {
        Leaf* n = new Leaf(data, parent);
        n->seq = children.size();
        children.emplace(n->seq, n);
        return n;
    }
};

/* Overloading * operator */
string operator*(const string& s, unsigned int n) {
    stringstream out;
    while (n--)
        out << s;
    return out.str();
}

string operator*(unsigned int n, const string& s) { return s * n; }

void printTree(Node* node, int indent=0) {
    string tab = "\t";

    if (node->type == LEAF) {
        cout << (tab * indent) << node->seq << "(" << node->address() << "), " << node->getType() << ": " << node->data << endl;
    } else {
        cout << (tab * indent) << node->seq << "(" << node->address() << "), " << node->getType() << endl;
    }

    NodeList::iterator it = node->begin();
    for (it=node->begin(); it != node->end(); ++it) {
        printTree(it->second, indent+1);
    }

}
   
int main() { 
    // create root node
    Node *rootNode = new Node(ROOT);

    // append children
    Node* n1 = rootNode->append(SEGMENT);
    Node* n2 = rootNode->append(SEGMENT);

    // append child to child
    n2->append(NODELIST);
    Leaf* l1 = new Leaf("Data");
    n2->append(l1);

    printTree(rootNode);

    // cout << "l1 parent: " << l1->parent->type << endl;

    /*

    // output data
    NodeList::iterator it = rootNode->begin();
    std::cout << "Number of children: " << rootNode->size() << endl;
    for (it=rootNode->begin(); it != rootNode->end(); ++it)
        std::cout << it->first << " => " << it->second->getType() << ", children: " << it->second->size() << '\n';
    */
    return 0; 
}