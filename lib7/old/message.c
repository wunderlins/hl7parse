#include "message.h"

/**
 * Search for the first occourance of "\n" "\r" or "\r\n"
 * 
 * Returns the poistion of the first delimiter. Returns 0 if no delimiter was found.
 * 
 * @param fp file handle
 * @param delim returns the first found delimiter
 * @param msh returns the full msh line
 * @return 0 on error, length of line on success
 */
int find_delim_msg(FILE* fp, char **delim, char **msh) {
	char msh_buffer[4096];
	
	// rewind
	long old_pos = ftell(fp);
	fseek(fp, 0, SEEK_SET);
	
	int  i = 0;
	char c = 0;
	while((c = getc(fp)) != EOF) {
		if (c == '\r' || c == '\n') {
			// figure out if we have single or double char delimiter (eg. \r\n)
			char c1 = getc(fp);
			if (c == '\r' && c1 == '\n') {
				//printf("rn\n");
				*delim = (char*) malloc(sizeof(char) * 3);
				*delim = "\r\n";
			} else {
				//printf("r|n\n");
				*delim = (char*) malloc(sizeof(char) * 2);
				(*delim)[0] = c;
				(*delim)[1] = 0;
			}
			
			int s = sizeof(char) * (i + 1);
			*msh = (char*) malloc(s);
			memcpy(*msh, msh_buffer, s);
			
			return i;
		}
		
		msh_buffer[i++] = c;
		msh_buffer[i]   = 0;
	}
	
	// reset file pointer
	fseek(fp, old_pos, SEEK_SET);
	return 0;
}

/**
 * extract the field seperators from MSH-2
 *
 * @param msh_line raw msh message up until the segment delimiter
 * @param delims contains an array of delimiter characters and the length of the array
 * @return 0 on succes otherwise error code
 */
int find_delim_seg(char* msh_line, el_sep_t* delims) {
	if (msh_line[0] != 'M' || msh_line[1] != 'S' || (msh_line[2] != 'H' && msh_line[2] != 'A'))
		return 1;
	
	char d[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	int i = 3;
	int o = 0;
	char c = 0;
	int len = strlen(msh_line);
	char delim_char = msh_line[4];
	
	while (((c = msh_line[i]) != delim_char || i < 4) && i < len) {
		if (c == '\\') {
			i++;
			continue;
		}
		d[o++] = c;
		i++;
	}
	
	delims->length = strlen(d);
	delims->sep = (char*) malloc(sizeof(char) * (i-4+1));
	memcpy(delims->sep, d, delims->length);
	
	return 0;
}