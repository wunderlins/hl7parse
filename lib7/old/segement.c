#include "segment.h"

#define ELBUFFER_CHUNK_SIZE 1000

void hl7_segment_free(hl7_segment_t *seg) {
  free(seg->fields);
  free(seg->raw);
  free(seg);
}

int hl7_segment_parse(char *line, hl7_segment_t **segment, el_sep_t *delim_seg) {
  hl7_segment_t *seg = malloc(sizeof(hl7_segment_t));
  seg->raw_length = strlen(line) + 1;
  seg->raw = malloc(sizeof(char) * seg->raw_length);
  seg->fields = malloc(sizeof(hl7_field_t));
  seg->length = 0;
  memcpy(seg->raw, line, seg->raw_length);

  int i = 0;
  int elcount = 0;
  char c = 0;
  int elbuffer_i = 0;
  int elbuffer_size = ELBUFFER_CHUNK_SIZE;
  char *elbuffer = malloc(sizeof(char) * elbuffer_size);

  for(; i<seg->raw_length; i++) {
    c = seg->raw[i];
    if (c != delim_seg->sep[0] || // not a delimiter
        (c == delim_seg->sep[0] && seg->raw[i-1] == '\\')) {
      // buffer character
      elbuffer[elbuffer_i++] = c;
      elbuffer[elbuffer_i] = 0;
      continue;
    }

    // first element is segment name
    if (elcount == 0)
      memcpy(seg->name, elbuffer, elbuffer_i+1);

    // create and append new element
    hl7_field_t **fields = realloc(seg->fields, sizeof(hl7_field_t) * (elcount + 1));
    fields[elcount] = hl7_field_create(elbuffer, elbuffer_i); // el;
    seg->fields = fields;
    seg->length++;

    // reset
    elbuffer[0] = 0;
    elbuffer_i = 0;
    elcount++;

  }

  // return the pointer of the newly allocated segment if we succeed
  *segment = seg;
  free(elbuffer);

  return 0;
}