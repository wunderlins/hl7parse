#pragma once

#include <iostream>
#include <sstream>
#include <string>
#include <map>

using namespace std;

// Node types
enum NodeType {
    NODELIST,        // Elements in a segment, delimited by '|'
    NODE,            // actual node, seq 0 is the first element and is always present. 
                     // if '~' is used in NODELIST then there is more than element present (repetition)

    SUBNODELIST,     // list of sub nodes, delimited by '^'
    SUBNODE,         // sub nodes delimited by '~', one element is always present

    SUBSUBNODELIST,  // sub sub nodes, delimited by '&'
    LEAF             // actual data element. more than one can be present if '~' is used
};

// Node class
class Node  { 
public:
    const char* data = nullptr;
    NodeType type;
    Node* parent;
    int seq = 0;
    std::map<int, Node*> children;

    Node(NodeType type, Node* parent = nullptr);

    Node* append(NodeType type, Node* parent = nullptr);

    void append(Node* n);

    auto size();
    auto begin();
    auto end();
    string get_type();
    string address();

    
};
typedef std::map<int, Node*> NodeList;

class Leaf : public Node {
public:
    Leaf(const char *data, Node* parent = nullptr);
    Leaf* append(const char *data, Node* parent);
};

/*
class Message : public Node {
public:
    string delim_segment = "\r";
    string delim_field   = "|^~&";
    string encoding      = "UTF-8";
    string hl7_type      = "";
    string hl7_subtype   = "";

    Message(): 
    Node(MESSAGE, nullptr) {}
};
*/

void printTree(Node* node, int indent=0);