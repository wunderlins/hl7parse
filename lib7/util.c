#include <stddef.h>
#include <string.h>
#include <stdlib.h>

#include "node.h"
#include "util.h"

void *memdup(void* src, size_t length) {
    void *dst = malloc((int) length);
    memcpy(dst, src, length);
    return dst;
}

char *trim(char* str) {
	char *ps = str; // temp pointer
	// trim leading white space
	while (*ps == ' ' || *ps == '\t' || *ps == '\r' || *ps == '\n') ps++;

	// trim leading white space
	size_t l = strlen(ps);
	char *pe = ps + l - 1;
	while (*pe == ' ' || *pe == '\t' || *pe == '\r' || *pe == '\n') {
		*pe = 0;
		pe--;
	}
	//printf("l: %d, buffer: %s\n", l, ps);
	char *buffer = malloc(sizeof(char) * (l+1));
	if (buffer == NULL)
		return NULL;

	strncpy(buffer, ps, l+1);

	return buffer;
}

void dump_structure(message_t *message) {
    // debug the result
    printf("Number of messages: %d\n", message->num_children);
    for (int i = 0; i < message->num_children; i++) {
        printf("\n%s: %d elements\n", message->segments[i]->data, message->segments[i]->num_children);
        
        for (int ii = 0; ii < message->segments[i]->num_children; ii++) {
            node_t *el = message->segments[i]->children[ii];
            //log_notice("\n# %02d %d", ii, el->num_children);

            // segment identifier
            unsigned char *element = message->segments[i]->data;
            for (int iii = 0; iii < el->num_children; iii++) {
                node_t *e = message->segments[i]->children[ii]->children[iii];
                if (e->num_children) {
                    printf("%s-%2d(%d)      [length: %d]\n", element, ii+1, iii+1, e->num_children);
                } else {
                    if (e->length > 50) {
                        unsigned char *data = (unsigned char*) memdup(e->data, 47);
                        data[46] = 0;
                        printf("%s-%2d(%d)      {"SIZE_T_L", %d} '%s'\n", element, ii+1, iii+1, e->length, e->pos, data);
                        free(data);
                    } else {
                        printf("%s-%2d(%d)      {"SIZE_T_L", %d} '%s'\n", element, ii+1, iii+1, e->length, e->pos, e->data);
                    }
                }

                for (int iiii = 0; iiii < e->num_children; iiii++) {
                    node_t *c = message->segments[i]->children[ii]->children[iii]->children[iiii];
                    char *marker1 = MARKER_T;
                    if (iiii+1 == e->num_children)
                        marker1 = MARKER_L;
                    if (c->num_children) {
                        printf(MARKER_L""MARKER_D""MARKER_D" %2d(%d).%d    [length: %d]\n", ii+1, iii+1, iiii+1, c->num_children);
                    } else {
                        if (c->length > 50) {
                            unsigned char *data = (unsigned char*) memdup(c->data, 47);
                            data[46] = 0;
                            printf("%s"MARKER_D""MARKER_D" %2d(%d).%d    {"SIZE_T_L", %d} '%s...'\n", marker1, ii+1, iii+1, iiii+1, c->length, c->pos, data);
                            free(data);
                        } else {
                            printf("%s"MARKER_D""MARKER_D" %2d(%d).%d    {"SIZE_T_L", %d} '%s'\n", marker1, ii+1, iii+1, iiii+1, c->length, c->pos, c->data);
                        }
                    }


                    for (int iiiii = 0; iiiii < c->num_children; iiiii++) {
                        node_t *s = message->segments[i]->children[ii]->children[iii]->children[iiii]->children[iiiii];
                        char *marker2 = MARKER_T;
                        if (iiiii+1 == c->num_children)
                            marker2 = MARKER_L;
                        if (s->num_children) {
                            printf(" %s"MARKER_D" %2d(%d).%d.%-2d [length: %d]\n", marker2, ii+1, iii+1, iiii+1, iiiii+1, s->num_children);
                        } else {
                            if (s->length > 50) {
                                unsigned char *data = (unsigned char*) memdup(c->data, 47);
                                data[46] = 0;
                                printf(" %s"MARKER_D" %2d(%d).%d.%-2d {"SIZE_T_L", %d} '%s...'\n", marker2, ii+1, iii+1, iiii+1, iiiii+1, s->length, s->pos, (unsigned char*) data);
                                free(data);
                            } else {
                                printf(" %s"MARKER_D" %2d(%d).%d.%-2d {"SIZE_T_L", %d} '%s'\n", marker2, ii+1, iii+1, iiii+1, iiiii+1, s->length, s->pos, s->data);
                            }
                        }

                    }

                }

            }

        }

    }
}

char *escape(char *str, char *quote_char, char escape_char) {
    int extra_alloc = 100;
    int spare = extra_alloc;
    int i = 0;
    int o = 0;
    int in_len = strlen(str);
    int out_len = in_len + extra_alloc;
    char *result = malloc(out_len + 1);
    //result[i++] = quote_char;

    int qlen = strlen(quote_char);

    for (i=0; i<in_len; i++) {
        // escape ?
        if (qlen == 1 && str[i] == quote_char[0]) {
            result[o++] = escape_char;
            spare--;
        } else {
            for(int ii=0; ii<qlen; ii++) {
                if (str[i] == quote_char[ii]) {
                    result[o++] = escape_char;
                    spare--;
                    break;
                }
            }
        }
        // add char to result
        result[o++] = str[i];

        // check if we need to realloc a larger output buffer
        if (spare == 1) {
            out_len += extra_alloc;
            result = realloc(result, out_len);
            spare = extra_alloc;
        }
    }
    result[o] = 0;
    //result[o] = 0;

    return result;
}
