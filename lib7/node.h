/** \file
 * @brief Main datastructures for HL7 nodes and messages
 * This file contains the main parser data structures and methods to read and parse hl7 files.
 *
 * All api functions defined here are used by the parser to create a 
 * node_t structure. For advanced node capabilities (for example editing)
 * see node_util.h experiemntal functions.
 */

#ifndef NODE_H
#define NODE_H

#include <stdio.h>
#include <stdlib.h>
#include "meta.h"
#include "address.h"
#include "message_state.h"
#include "logging.h"

/** @brief number of children to pre-allocate when there is not room in children anymore */
#define NODE_PREALLOC_CHILDREN 5

/** @brief number of children to pre-allocate when there is not room in children anymore */
#define MESSAGE_PREALLOC_CHILDREN 10

/** @brief maximum number of temporary elements to allocate in the parser buffer */
#define MAX_FIELDS 1000

/**
 * @brief structure to track delmiter fields in a fieldset
 * 
 * This is a private data structure for `parse_segment()`. It 
 * keeps track of all delimiters in a segment while looping 
 * over it byte by byte. `process_node()` the uses it to calculate 
 * bounds and structure of the message while extracting the 
 * data and structure.
 * 
 * This is a parser internal structure used to keep track of delimiters.
 * 
 * @note 
 * do not use outside of `parse_segment()`
 * 
 * @bug
 * move into parser, this is not being used by the node facilities at all.
 */
typedef struct raw_field_t {
    /** byte array containing the raw field character data including delimiters */
    unsigned char *field;
    /** array of delimiter characters  */
    unsigned char delim[MAX_FIELDS];
    /** array of positions of the above delimiting characters  */
    unsigned int  pos[MAX_FIELDS];
    /** length of the delimiter array  */
    unsigned int delim_l;
    /** length of the field's data */
    size_t length;
} raw_field_t;

/**
 * @brief Node types
 */
typedef enum node_type_t {
    /** root node, holding all segments */
    MESSAGE     = 1, 
    /** segment node, this is one lien in an hl7 file */
    SEGMENT     = 2, 
    /** fieldlist is everything btween | and |, respecting repetition characters ~ */
    FIELDLIST   = 4, 
    /** this is the actual field content, if there is a repetition delimiter ~ multiple of these fields are present, otherwise always one */
    FIELD       = 8, 
    /** the component delimited by ^ */
    COMP        = 16, 
    /** the sub componentent, delimited by & */
    SUBCOMP     = 32, 
    /** unused */
    LEAF        = 64 
} node_type_t;

/**
 * @brief primary storage type of a delimited element
 * 
 * Every hl7 element (regardless of place in the hirarchy) is represented 
 * by a node_t. The node_t.type defines where in the hirarchy the node is. Every 
 * node_t has a `parent` node.
 *
 * The top level node of a parsed HL7 message is always of type message_t. The 
 * Hirarchy looks like this:
 * - message_t this is the top level node
 *   - `node_t.type` == `SEGMENT` this is the segment node
 *     - `node_t.type` == `FIELDLIST` array of elements between `|`, always one, may have multiple children delimited by `~`
 *       - `node_t.type` == `FIELD`  array of fields delimited by `~`, always at least one
 *         - `node_t.type` == `COMP` optional components delimited by `^`
 *           - `node_t.type` == `SUBCOMP` optional sub components delimited by `&`
 *
 * The delimiters shown above are the standard delimiters. The HL7 file may 
 * define different delimiters which are read and accounted for by the parser.
 *
 * Every node has potential children indicated by node_t.num_children 
 * (0 means none) and has a parent (special case is message_t which is 
 * always the root of the structure and is the partent of node_type_t SEGMENT).
 * 
 * This also means, that some node types (node_t.type) need to be treated specially.
 * - node_type_t.SUBCOMP never has children
 * - node_type_t.SEGMENT always has a parent of type message_t, you need to cast it when accessing it
 * - node_type_t.SEGMENT the data property always contains the segment's name
 * 
 * ### creating a node
 *
 * To create a valid node_t you need to know it's data and length.
 * 
 * Example:
 * ```C
 * char *data = "abc";
 * int length = 4; // must include \0
 * node_t *n = create_node_t(FIELD, data, length, 0);
 *
 * // you may also create an empty node like this
 * node_t *n = create_node_t(FIELD, NULL, 0, 0);
 * ```
 * 
 * node_t takes owenship of the pointer pointing to `data`. be aware of the 
 * fact that when you free a node then all children and it's data will get freed.
 *
 * Example:
 * ```C
 * char *data = "abc";
 * int length = 4; // must include \0
 * node_t *n = create_node_t(FIELD, data, length, 0);
 * 
 * // free it
 * free_node_t(n);
 * // NOTE: data points to NULL now
 * ``` 
 * 
 * ### using the api
 * 
 * You should use the api to manage relationships between children and 
 * parents. this will make sure you don't get any dangling nodes in your 
 * structure.
 * 
 * Example:
 * 
 * ```C
 * int ret = -1;
 * message_t *root = create_message_t(NULL); // will create a message and meta with default delimiters
 * node_t *n = create_node(SEGMENT, "PID", 4, 0);
 * ret = message_append(&root, n);
 * if (ret != 0) {
 *   // whoopsie, something went wrong
 *   free_node_t(n);
 *   free_message_t(root);
 *   return;
 * }
 * 
 * // create PID-1(1) segment
 * node_t *pid1  = create_node(FIELDLIST, NULL, 0 , 0);
 * node_t *pid11 = create_node(FIELD, "DATA", 5 , 0);
 * ret = node_append(&n, n1);
 * ret = node_append(&n1, n11);
 * 
 * // from node_util.h
 * char *str = messate_to_string(root);
 * printf("%s\n", str);
 * free(str);
 * free_message(root); // frees all children
 * 
 * ```
 *
 * ### checking for children
 *
 * ```C
 * if (node->num_children > 0)
 *   // we have some children
 * ```
 *
 * ### getting the parent
 * message_t has a very similar structure (lacking data but having file metadata).
 * when accessing the parent of a `SEGMENT`, then the parent should be casted 
 * into `message_t*`.
 * 
 * ```C
 * if (node->type == SEGMENT)
 *   message_t* parent = (message_t*) node->parent;
 * else
 *   node_t* parent = node->parent;
 * ```
 */
typedef struct node_t {
    /** the type of the node */
    node_type_t type;
    /** unique id of the node */
    int id;

    /** pointer to parent node, should never be NULL */
    struct node_t *parent;
    /** array of child nodes */
    struct node_t **children;

    /** number of elements in children */
    int num_children;
    /** number of allocated elements in children */
    int _num_children_allocated;

    /** byte array of raw data, should be NULL if there is no data and "\0" when empty */
    unsigned char *data;
    /** number of bytes in data */
    size_t length;
    /** element position from the beginning of the segment in bytes, this might go away */
    int pos;
} node_t;

/**
 * @brief hl7 message container
 * 
 * This struct holds data about the hl7 file (delimiter) in `messgae_t.meta` as
 * well as a tree structure of nodes, stored in message_t.segments.
 * 
 * This is your main structure returned by the parser.
 * 
 * Example:
 * 
 * ```C
 * #include <stdio.h>
 * #include "decode.h" // or lib7.h which is the combined header for lib7.so
 * 
 * char *filename "some_file.hl7";
 * FILE *fd = fopen(filename, "rb");
 * message_t root = create_message_t(NULL);
 * int ret = hl7_decode(fd, &root);
 * fclose(fd); // no need for fd anymore, all data in memory
 * if (ret != 0) {
 *   // whoopsie
 *   free_message_t(root);
 * }
 * 
 * // do something with the data in root
 * 
 * // cleanup
 * free_message_t(root);
 * ```
 * 
 */
typedef struct message_state_t message_state_t; // forward declaration
typedef struct message_t {
    /** the type of the node */
    node_type_t type;
    /** unique id of the node */
    int id;
    
    /** pointer to parent node, should never be NULL */
    struct node_t *parent;
    /** number of allocated elements in children */
    struct node_t **segments;

    /** number of elements in children */
    int num_children;
    /** number of allocated elements in children */
    int _num_children_allocated;

    /** metadata containing delimiters and bom */
    hl7_meta_t *meta;

    /** callback functions for parser */
    message_state_t *state;
} message_t;

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief create raw fied structure
 * 
 * @returns raw_field_t
 */
raw_field_t* create_raw_field_t(void);

/** 
 * @brief free raw fiel structure
 */
void free_raw_field(raw_field_t* raw_e);

/**
 * @brief create a new node
 * 
 * you must make sure to properly free the node @see free_node_t(). You should 
 * add a pointer to the parent after creation like this: 
 * `my_node->parent = another_node;`
 * 
 * if you just plan to append as a child, @see node_append() and relationship 
 * with `parent` is taken care of.
 * 
 * @param type the node type
 * @param data byte array of data
 * @param length length including `\0` delimiting byte if there is any
 * @param pos position of element in line (segment). the position marks the beginnign delimiter
 * @returns initialized node
 */
node_t* create_node_t(node_type_t type, unsigned char *data, size_t length, int pos);

/**
 * @brief  cleanup all memory of a node
 * 
 * @note
 * be careful, if you have passed in pointers to node data or elements, 
 * they will be cleaned (node_t.data will be freed).
 * 
 * All children and children's children are freed.
 * 
 * @param node the node and it's children to be cleaned
 */
void free_node_t(node_t *node);

/**
 * @brief append a child node
 * 
 * Memory is allocated if _num_children_allocated is too small to hold an 
 * additional pointer in the children array. You must make sure to 
 * properly allocate memory for the child itself, use create_node_t() for that.
 * 
 * @param parent the parent node to be associated, may be null (this is reserved for the root node)
 * @param node the node to append
 * @return 0 on success, 1 if realloc() fails, 2 if parent is missing
 */
int node_append(node_t** parent, node_t *node) ;

/**
 * @brief find the position in parent's children struct
 * 
 * This is useful to find next/previous siblings.
 * 
 * @returns pos in child array, `-1` if there is no parent and `-2` if the node is not found in parents children
 */
int node_parent_child_pos(node_t *node);

/**
 * Sub component parser
 * 
 * This is the sub component prser that takes care of braking down all
 * elements in a field delimited by `^~&`.
 *
 * creates a node structure for components and subcomponents.
 * don't forget to set the parent after creation. This is an internal 
 * function of the parsers and is not useful anywhere else.
 * 
 * @todo
 * move to decode.c
 * 
 * @param raw_e raw field elements
 * @param meta hl7 meta data, containing delimiter
 * @param start_pos the position of the first delimiter beginning data of thes field in the segment
 * @returns node structure
 */
node_t *process_node(raw_field_t* raw_e, hl7_meta_t *meta, int start_pos);

/**
 * @brief dump raw_e structure to stdout
 * 
 * @note
 * this is only useful for debugging insede the parser
 * 
 * @todo
 * move to decode.c
 * 
 * @param raw_e the node's delimiter information
 */
void disply_raw_node(raw_field_t* raw_e);

/**
 * @brief check if a node with given addres exists
 * 
 * @param segment node tree to search (must be of type segment)
 * @param addr the address to look up
 * @returns the node if found, NULL otherwise
 */
node_t *node_in_segment(node_t *segment, hl7_addr_t *addr);

/*
 * denormalize node structure into string
 * 
 * @param node node and children to turn into string representation
 * @param meta hl7 metadata for delimiters
 * @param length returns the length of the allocated buffer
 * @returns allocated buffer
 */
//char* node_to_string(node_t *node, hl7_meta_t* meta, int *length);

/**
 * @brief string representation of node_type_t
 * 
 * @param type type
 * @returns string name of type
 */
const char *node_type_to_string(node_type_t type);

/**
 * @brief initialize an empty messagte_t struct
 * 
 * This function wil lsetup your message_t. If the first param is `NULL` then
 * default values for message_t.meta and message_t.meta.bom are set 
 * according to init_hl7_meta_t().
 * 
 * @see init_hl7_meta_t()
 * @param meta or NULL
 * @returns message_t with default values
 */
message_t* create_message_t(hl7_meta_t *meta);

/**
 * @brief free message_t an all it's child objects
 * 
 * This method will free messgate_t.meta and messagte_t.segments (recoursively).
 * 
 * @note
 * all pointer pointg to nodes within this message will be invalid after this 
 * function has been executed, be careful.
 * 
 * @param message the message to free
 */
void free_message_t(message_t *message);

/**
 * @brief append a segment to the message
 * 
 * This method will dynamically allocate more memory for message_t.segments
 * and store the number of allocated items in messgae_t._num_children_allocated.
 * 
 * @param parent typically the root node, a message_t object
 * @param node the segment to append to message
 * @returns 0 on success, 1 if no memory could be allocated
 */
int message_append(message_t **parent, node_t *node);

/**
 * @brief generate an addr from any node in a message
 * 
 * traverse up until message is reached. The structure must have a message type
 * as top parent or it will return `NULL`.
 * 
 * while traversing up through parents, if type is `MESSAGE` and parent 
 * is `NULL`, we have successfully reached the top and can produce a result.
 * 
 * @param node the starting point from where to traverse up
 * @returns hl7_addr_t address structure.
 */
hl7_addr_t* addr_from_node(node_t *node);

#ifdef __cplusplus
}
#endif

#endif // end NODE_H