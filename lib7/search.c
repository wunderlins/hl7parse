#ifndef _GNU_SOURCE
#   define _GNU_SOURCE
#endif

#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include "logging.h"
#include "meta.h"
#include "decode.h"
#include "address.h"
#include "node_util.h"
#include "node.h"
#include "search.h"
#include "base64.h"
#include "../generated/version_search.h"

#ifdef __MINGW32__
    #include <Shlwapi.h>
    char * strcasestr(const char * s1, const char * s2) {
        return StrStrIA(s1, s2);
    }
#endif

const char *version_search() {
    return VERSION_SEARCH;
}

static char* (*fn_find)(const char*, const char*);

flags_t *create_flags_t() {
    flags_t *search_flags = malloc(sizeof(flags_t));
    search_flags->verbose           = 0; ///- v: verbose
    search_flags->search_term       = 0; ///- s: search term
    search_flags->greedy            = 1; ///- n: greedy
    search_flags->output_json       = 0; ///- o=json: json output
    search_flags->output_xml        = 0; ///- o=xml: xml output
    search_flags->output_csv        = 0; ///- o=csv: csv output
    search_flags->address           = 0; ///- a: address to search in
    search_flags->search_term_value = NULL; ///- search_term (with s)
    search_flags->address_value     = NULL; ///- address string (with a)
    search_flags->quiet             = 0; ///- q: quiet, only for console output, displays values only
    search_flags->decode64          = 0; ///- d: base64 decode values
    search_flags->output_file       = 0; ///- f: output file
    search_flags->output_file_value = NULL; ///- output_file_value, name of the output file
    search_flags->output_fd         = NULL; ///- FILE* output_file handle,
    search_flags->case_insensitive  = 0;    ///- i: case insensitive search

    return search_flags;
}

hl7_addr_t **parse_address(char *addrstr, int *length) {
    log_debug("Parsing: '%s'", addrstr);
    char *pch;
    int space[100] = {-1};
    int i = 0;
    pch = strchr(addrstr, ' ');

    // only one address
    if (pch == NULL) {
        *length = 1;
        hl7_addr_t **ret = (hl7_addr_t **) malloc(sizeof(hl7_addr_t) * 2);
        ret[0] = addr_from_string(addrstr);
        ret[1] = NULL;

        if (ret[0] == NULL) {
            fprintf(stderr, "Invalid address: '%s'\n", addrstr);
            return NULL;
        }

        return ret;
    }

    // multiple addresses
    while (pch != NULL) {
        log_debug("space found at %d", pch - addrstr + 1);
        space[i++] = pch - addrstr + 1;
        pch = strchr(pch + 1,' ');
    }
    space[i] = strlen(addrstr)+1;

    int last = 0;
    int len  = 0;
    hl7_addr_t *addr[100] = {NULL};
    int addr_l = 0;
    for (int ii=0; ii <= i; ii++) {

        // skip all entries that are shorter than 3 characters
        len = space[ii] - last;
        if (len < 4) {
            log_debug("Too short: %d (start: %d)", len, space[ii]);
            last = space[ii];
            continue;
        }

        // extract data
        char *adr = malloc(len);
        memcpy(adr, addrstr+last, len);
        adr[len-1] = 0;
        log_debug("pos(%d): length: %d, %d-%d, '%s'", ii, len, last, space[ii], adr);
        hl7_addr_t *a = addr_from_string(adr);
        if (a == NULL) {
            fprintf(stderr, "Invalid address: '%s'\n", adr);
        }
        addr[addr_l++] = a;
        free(adr);

        last = space[ii];
        log_debug("last: %d", last);
    }

    hl7_addr_t **ret = (hl7_addr_t **) malloc(sizeof(hl7_addr_t) * (addr_l * 1));
    for (i=0; i<addr_l; i++) {
        ret[i] = addr[i];
    }
    *length = addr_l;
    log_debug("length: %d, i: %d", *length, i);
    ret[i] = NULL; // sentinel delimiter

    return ret;
}

void free_addr_arr(hl7_addr_t **addr) {
    for (int i=0; addr[i] != NULL; i++) {
        log_trace("Freeing addr: %d %s", i, addr[i]->segment);
        free_addr(addr[i]); 
    }
    free(addr);
}

search_res_t *create_search_res(unsigned char* search_term) {
    search_res_t *sr = malloc(sizeof(search_res_t));
    if (sr == NULL) return NULL;
    sr->items = NULL;
    sr->length = 0;
    sr->greedy = 0;
    sr->file = NULL;
    sr->addr = NULL;
    sr->addr_l = 0;
    if (search_term != NULL) {
        int l = strlen((char*) search_term) + 1;
        sr->search_term = malloc(l);
        memcpy(sr->search_term, search_term, l);
    } else {
        sr->search_term = NULL;
        sr->length = 0;
    }

    log_debug("search_term length: %d", sr->length);

    return sr;
}

void free_search_res(search_res_t *sr) {
    for(int i=0; i< sr->length; i++) {
        //log_warning("Freeing search res: %d", i);
        //printf("freeing addr\n");
        if (sr->items[i]->addr != NULL)
            free_addr(sr->items[i]->addr);
        //printf("freeing file\n");
        if (sr->items[i]->file != NULL)
            free(sr->items[i]->file);
        //printf("freeing items\n");
        if (sr->items[i]->str != NULL)
            free(sr->items[i]->str);
        //printf("freeing result item\n");
        free(sr->items[i]);
    }
    //printf("freeing result item array\n");
    free(sr->items);

    if (sr->addr != NULL)
        free_addr_arr(sr->addr);
    
    //printf("freeing search term\n");
    if (sr->search_term != NULL)
        free(sr->search_term);
    //printf("freeing file\n");
    if (sr->file != NULL)
        free(sr->file);

    free(sr);
}

/*
 * append result to result container
 */
static int append_result(char *file, int line_num, int pos, hl7_addr_t *addr, 
                  search_res_t **res, char *str, int length) {

    result_item_t *ri = malloc(sizeof(result_item_t));
    if (ri == NULL)
        return -1;
    
    ri->file = strdup(file);

    ri->line_num = line_num;
    ri->pos = pos;
    ri->addr = addr;

    ri->str = NULL;
    if (str != NULL)
        ri->str = strdup(str);

    // realloc items
    (*res)->length += 1;
    (*res)->items   = realloc((*res)->items, sizeof(result_item_t*) * (*res)->length);
    (*res)->items[(*res)->length - 1] = ri;

    ri->length = length;

    return 0;
}

int search_substring(FILE* fd, search_res_t *sr) {
    fprintf(stderr, "Search mode SEARCH_SUBSTRING not implemented\n");
    return 0;
}

int search_segment(FILE* fd, search_res_t *sr) {
    int i = 0;
    int ret = 0;
    int relevant = 0;
    seg_count_t *segc = create_seg_count();

    // get bom
    hl7_meta_t *meta = init_hl7_meta_t();
    meta->bom = detect_bom(fd);

    // read meta
    ret = read_meta(meta, fd);
    if (ret != 0)
        return 31;

    // find line delimiter
    line_delimiter_t delim = find_line_delimiter(fd);
    meta->crlf = 0;
    if (delim == DELIM_CRLF) {
        meta->sep_message = '\r';
        meta->crlf = 1;
    } else if (delim == DELIM_CR) {
        meta->sep_message = '\r';
    } else {
        meta->sep_message = '\n';
    }

    // this should typically be run against an MSH line, so we do expect
    // at least one line delimiter before EOF, otherwise the HL7 file makes
    // no sense!
    if (delim == DELIM_NONE)
        return 32;

    // rewind file pointer
    fseek(fd, meta->bom->length, SEEK_SET);

    log_debug("line delimiter: \\%02X", meta->sep_message);
    log_debug("crlf: %d", meta->crlf);

    // start looping over lines
    size_t buffsize = 1000;
    char *lineptr = malloc(buffsize);
    ssize_t getret = 0;
    if (lineptr == NULL) {
        log_warning("Failed to allocate line buffer memory *lineptr");
        return 30;
    }

    int line_num = -1;
    while (1) {

        // was this the last line ?
        if (getret == -1) {
            // we are done
            log_trace("reached EOF");
            break;
        }

        line_num++;
        log_trace("searching in %s: %d", sr->file, line_num);
        getret = getdelim(&lineptr, &buffsize, meta->sep_message, fd);

        // lineptr contains the delimiter as las character, get rid of it
        if (getret) {
            lineptr[getret-1] = 0;
        }

        // invalid line, we expect at least a segment name
        if (getret < 4) {
            log_trace("Line shorter than 4: %d", getret);
            if (meta->crlf)
                getc(fd); // get rid of windows \LF
            continue;
        }

        // extract segment name
        char seg[4] = {0, 0, 0, 0};
        memcpy(seg, lineptr, 3);

        // check repetition number
        int rep = add_seg_count(seg, segc);

        // check if we are interested in this segment ?
        i = 0;
        relevant = 0;
        while (sr->addr[i] != NULL) {
            log_trace("Comparing '%s', '%s'", seg, sr->addr[i]->segment);

            if (strcmp(sr->addr[i]->segment, seg) == 0) {
                if (sr->addr[i]->seg_count != -1) {
                    if (rep == sr->addr[i]->seg_count) {
                        relevant = 1;
                        log_debug("Found match '%s' on line %d", seg, line_num);
                        break;
                    }
                } else {
                    relevant = 1;
                    log_debug("Found match '%s' on line %d", seg, line_num);
                    break;
                }
            }
            i++;
        }

        // if we have a relevant segment, start search for substrings
        if (relevant) {
            
            if (sr->search_term != NULL) {
                log_debug("substring search: %s", sr->search_term);
                char *start = lineptr;
                char *found = lineptr;
                int len = strlen((char*) sr->search_term);

                while ((found = (*fn_find)(found, (char*) sr->search_term)) != NULL) {
                    int pos = found - start;

                    hl7_addr_t *a = create_addr();
                    a->segment = strdup(seg);
                    a->seg_count = rep;
                    append_result(sr->file, line_num, pos, a, &sr, lineptr, getret-1);
                    log_trace("found at line %d, pos %d: '%s'", line_num, pos, found);
                    found = found + len;

                    // find only the first result if we use non-greedy search
                    if (sr->greedy == 0) {
                        getret = -1; // force outer loop to quit
                        break;
                    }
                }
            } else {
                log_debug("while line");
                hl7_addr_t *a = create_addr();
                a->segment = strdup(seg);
                a->seg_count = rep;
                append_result(sr->file, line_num, 0, a, &sr, lineptr, getret-1);
                
                // search only the first line if we use non-greedy search
                if (sr->greedy == 0)
                    break;
            }
        }

        // get rid of windows \LF
        if (meta->crlf)
            getc(fd);
    }

    free_seg_count(segc);
    free(lineptr);
    return 0;
}


/*
 * Search specific node
 * 
 * This method wil search recouriveli in fieldlists, if the search address
 * does not define it (fieldlist = -1). 
 * 
 * All possible sub-nodes are concatenated into a string and then search 
 * will report the position of all findings.
 * 
 * if sr->greedy is set to 0, then the search will return after the first find.
 * Otherwise all possible occourances are returned.
 * 
 * 
 * @param n parent node to search in
 * @param sr result
 * @param meta hl7 meta data
 * @param fa search address
 * @returns 0 on success
 */
static int _search_subnodes(node_t *n, search_res_t *sr, hl7_meta_t *meta, hl7_addr_t *fa, int line_num) {
    int r = 0;
    int length = 0;
    // replace node_to_string
    char *nodestr = (char *) node_to_string(n, meta, &length);
    log_debug("Node(%d): %d, '%s', search length: %d, node_to-str->length: %d\n", fa->field, n->num_children, nodestr, sr->length, length);
    //printf("node->data: '%s'\n", n->data);

    // string search
    if (sr->search_term != NULL) {
        log_debug("search_subnodes, mode: substring search");
        char *start = nodestr;
        char *found = nodestr;
        int len = strlen((char*) sr->search_term);

        while ((found = (*fn_find)(found, (char*) sr->search_term)) != NULL) {
            int pos = found - start;
            hl7_addr_t *ra = clone_addr(fa);

            r = append_result(sr->file, line_num, pos, ra, &sr, nodestr, length);
            if (r != 0)
                return r;
            log_trace("found at line %d, pos %d: '%s'", line_num, pos, found);
            found = found + len;
            //free_addr(ra);

            // find only the first result if we use non-greedy search
            if (sr->greedy == 0)
                break;
        }
    } else {
        log_debug("search_subnodes, mode: return nodes");
        // just return contents
        hl7_addr_t *ra = clone_addr(fa);
        //printf("'%s'\n", nodestr);
        r = append_result(sr->file, line_num, n->pos, ra, &sr, nodestr, length);
        if (r != 0)
            return r;
        // free_addr(ra);
    }
    //if (fa != NULL)
    //    free_addr(fa);
    free(nodestr);
    return 0;
}

int search_subnodes(node_t *fieldlist, search_res_t *sr, int line_num, 
                    hl7_meta_t *meta, int segment_rep) {
    int adri = 0;
    node_t *n;

    while (sr->addr[adri] != NULL) {
        //addr_dump(sr->addr[adri]);
        log_debug("Comparing '%s', '%s'", fieldlist->data, sr->addr[adri]->segment);
        if (strcmp(sr->addr[adri]->segment, (char *) fieldlist->data) == 0 && 
            (segment_rep == -1 || sr->addr[adri]->seg_count == segment_rep)) {
            log_debug("Searching for '%s' in %s: %d", sr->search_term, sr->addr[adri]->segment, adri);
            
            // this is our search spec
            hl7_addr_t *a = sr->addr[adri];
            if (loglevel >= LEVEL_DEBUG)
                addr_dump(a);

            int r = 0;
            // get all repetions and search over string
            if (a->field == -1) {
                hl7_addr_t *fa = clone_addr(a);
                fa->comp = -1;
                fa->subcmp = -1;

                fa->field = 1;
                while((n = node_in_segment(fieldlist, fa)) != NULL) {
                    hl7_addr_t *a2 = clone_addr(a);
                    a2->field = fa->field;
                    node_t *n1 = node_in_segment(fieldlist, a2);
                    if (n1 != NULL) {
                        r = _search_subnodes(n1, sr, meta, a2, line_num);
                        if (r != 0)
                            return 1;
                        
                        if (sr->greedy == 0 && sr->length) {
                            free_addr(a2);
                            return 0;
                        }

                    } else {
                        ; // addr_dump(a2);
                    }
                    fa->field++;
                    free_addr(a2);
                }
                free_addr(fa);
            } else {
                log_debug("Single node search");
                n = node_in_segment(fieldlist, a);
                if (n == NULL) {
                    log_debug("Node not found");
                } else {
                    r = _search_subnodes(n, sr, meta, a, line_num);
                    if (r != 0)
                        return 1;

                    if (sr->greedy == 0 && sr->length) {
                        return 0;
                    }
                }
            }
        }
        adri++;
    }    
    return 0;
}

int search_node(FILE* fd, search_res_t *sr) {
    int i = 0;
    int ret = 0;
    char c = 0;
    int relevant = 0;
    int line_num = 0;
    seg_count_t *segc = create_seg_count();

    // get bom
    hl7_meta_t *meta = init_hl7_meta_t();
    meta->bom = detect_bom(fd);
    
    // for a generic approach, we use the 2nd method.
    //log_trace("before read_meta: ftell(fd): %d", ftell(fd));
    ret = read_meta(meta, fd);
    if (ret != 0) {
        free_seg_count(segc);
        return 41;
    }
    //log_trace("after read_meta: ftell(fd): %d", ftell(fd));

    // parse lines
    while (1) {
        // get first 3 chars of line
        char seg[4] = {0, 0, 0, 0};
        //log_debug("searching for segment");
        i = 0;
        for (; i<3; i++) {
            c = getc(fd);
            //if (c == '\n') c = getc(fd); // skip preliminary newlines
                
            if (c == EOF) {
                log_debug("End of file while searching segment");
                break;
            }
            seg[i] = c;
            log_debug("%02X %02X %02X", seg[0], seg[1], seg[2]);
        }

        if (c == EOF)
            break;

        // rewind
        //log_trace("Rewinding: %d", (i * -1));
        fseek(fd, (i * -1), SEEK_CUR);
        //log_trace("after segment detection: ftell(fd): %d", ftell(fd));
        // FIXME: we have a problem with CRLF, first char in segment missing
        log_debug("Segment: %s, line: %d", seg, line_num+1);

        // check repetition number
        int rep = add_seg_count(seg, segc);

        // check if we are interested in this segment (addr->segment == seg)?
        i = 0;
        relevant = 0;
        while (sr->addr[i] != NULL) {
            log_trace("Comparing '%s', '%s'", seg, sr->addr[i]->segment);

            if (strcmp(sr->addr[i]->segment, seg) == 0) {
                if (sr->addr[i]->seg_count != -1) {
                    if (rep == sr->addr[i]->seg_count) {
                        relevant = 2;
                        log_debug("Found match '%s' on line %d", seg, line_num);
                        break;
                    }
                } else {
                    relevant = 1;
                    log_debug("Found match '%s' on line %d", seg, line_num);
                    break;
                }
            }
            i++;
        }

        // we want to parse this line?
        if (relevant > 0) {
            node_t *fieldlist = create_node_t(SEGMENT, NULL, 0, 0);
            char *segment_name;
            log_debug("crlf: %d", meta->crlf);
            //ret = parse_msh(fd, meta, &fieldlist, &segment_name);
            ret = parse_segment(fd, meta, &fieldlist, (unsigned char**) &segment_name);
            if (ret != 0) {
                free_seg_count(segc);
                return 42;
            }
            fieldlist->data = (unsigned char*) segment_name;
            fieldlist->length = strlen((char*) segment_name);
            log_debug("crlf: %d, message sep: %02X", meta->crlf, meta->sep_message);

            // do we have to check repetition of segment ?
            int segment_repetition = -1;
            if (relevant == 2)
                segment_repetition = rep;
            
            ret = search_subnodes(fieldlist, sr, line_num, meta, segment_repetition);
            if (ret != 0) {
                free_seg_count(segc);
                return 43;
            }

            // done, release memory
            free_node_t(fieldlist);

            // if non greedy, return
            if (sr->greedy == 0 && sr->length) {
                free_seg_count(segc);
                free_hl7_meta(meta);
                return 0;
            }

            // WTF: parse_segment should handle CRLF, somehow we get still
            //      an LF stuck in fd stream, why ?
            char chr = 0;
            if (meta->crlf == 1)
                chr = fgetc(fd);
            log_debug("file handle at position %d, %02X", ftell(fd), chr);
            //if (chr != '\n')
            //    ungetc(chr, fd);
            line_num++;
            continue;
        }

        // forward to next line
        //log_trace("%d Forwarding to next line break, %d", line_num, ftell(fd));
        while((c = getc(fd)) != EOF) {
            if (c == '\r' || c == '\n') {
                log_trace("next line, crlf: %d, pos: %d", meta->crlf, ftell(fd));
                if (meta->crlf == 0) {
                    break;
                
                } else if (meta->crlf == 1) {
                    c = getc(fd);
                    break;
                } else {

                    // we don't know crlf yet
                    unsigned char c1 = getc(fd);
                    log_debug("CRLF detection c: %02X, c1: %02X, meta: %02X", c, c1, meta->sep_message);
                    if (c == '\r' && c1 == '\n') {
                        log_debug("have CRLF");
                        meta->crlf = 1;
                    } else {
                        log_debug("no CRLF");
                        meta->crlf = 0;
                        meta->sep_message = c;
                        if ((char) c1 != EOF)
                            ungetc(c1, fd);
                        else
                            c = c1;
                    }
                }
                break;
            }
        }

        line_num++;
        if (c == EOF)
            break;
    }

    // rewind, fseek(fd, -3, SEEK_SET)
        // parse segment
        // run sub element search on segment
    // else
        // fetch line 
        // discard line
        // handle crlf
    
    // cleanup
    log_debug("Done");
    free_seg_count(segc);
    free_hl7_meta(meta);

    return 0;
}

int search_file(char *filename, flags_t flags) {
    int i = 0;
    int ret = 0;

    // check if the file exists and is readable
    FILE* fd = hl7_open(filename);
    if (fd == NULL)
        return 11;

    // check what addresses we have to search
    int addr_l = 0;
    hl7_addr_t **addr = parse_address(flags.address_value, &addr_l);
    log_debug("addr_l: %d", addr_l);

    if (addr == NULL) {
        return 13;
    }

    fn_find = &strstr;
    if (flags.case_insensitive == 1)
        fn_find = &strcasestr;

    // check for faulty adress definitions and
    // determine search mode
    search_mode_t mode = SEARCH_SUBSTRING;
    for (i=0; i < addr_l; i++) {

        if (addr[i] == NULL) {
            //fprintf(stderr, "invalid address definition at position %d\n", i+1);
            free_addr_arr(addr);
            addr = NULL;
            return 13;
        }

        if (mode == SEARCH_SUBSTRING && addr[i]->segment != NULL) {
            mode = SEARCH_SEGMENT;
        }
        
        if (addr[i]->field != -1 || addr[i]->fieldlist != -1 || 
            addr[i]->comp != -1 || addr[i]->subcmp != -1)
            mode = SEARCH_NODE;
        
        //addr_dump(addr[i]);
    }

    // initialize separator data structure
    hl7_meta_t* hl7_meta = init_hl7_meta_t();

    // check if this file has a bom;
    // this will move df at the first byte after the BOM
    hl7_meta->bom = detect_bom(fd); 

    // find delimiters
    ret = read_meta(hl7_meta, fd);
    if (ret != 0) {
        free_addr_arr(addr);
        addr = NULL;
        free_hl7_meta(hl7_meta);
        hl7_meta = NULL;
        return 12;
    }

    // search mode:
    log_debug("searchterm: '%s'", flags.search_term_value);
    search_res_t *sr = create_search_res(flags.search_term_value);
    sr->greedy = flags.greedy;
    sr->file = strdup(filename);
    sr->addr = addr;
    sr->addr_l = addr_l;
    //log_debug("addr.search_term: '%s'", sr->search_term);
    
    ret = 0;
    switch(mode) {
        // file
        case SEARCH_SUBSTRING:
            log_debug("search mode: SEARCH_SUBSTRING");
            ret = search_substring(fd, sr);
            break;
        
        // segment
        case SEARCH_SEGMENT:
            log_debug("search mode: SEARCH_SEGMENT");
            ret = search_segment(fd, sr);
            break;
        
        // field/comp/subcom
        case SEARCH_NODE:
            log_debug("search mode: SEARCH_NODE");
            ret = search_node(fd, sr);
            break;
        
        default:
            log_warning("search mode: unknown");
            fprintf(stderr, "Unknown search mode %d\n", mode);
            ret = 14;
    }

    if (flags.verbose)
        printf("Result length: %d\n", sr->length);
    
    for (i=0; i<sr->length; i++) {
        result_item_t *item = sr->items[i];
        
        if (flags.output_json)
            output_json(item, flags, (i+1==sr->length) ? 1 : 0);
        else if(flags.output_xml)
            output_xml(item, flags);
        else if(flags.output_csv)
            output_csv(item, flags);
        else
            output_string(item, flags);
    }

    // free addr array
    fclose(fd);
    free_search_res(sr); // FIXME: when run from mingw we fail to free sr, why ?
    //free_addr_arr(addr);
    free_hl7_meta(hl7_meta);

    return ret;
}

void output_string(result_item_t *item, flags_t flags) {
    char *addr_str = NULL;
    if (flags.quiet) {
        if (flags.decode64) {
            size_t in_len = strlen(item->str);
            size_t out_len = in_len * 2.2;
            unsigned char *out = malloc(out_len);
            int ret = hl7_64decode(item->str, in_len, out, &out_len);
            if (ret != 0) {
                fprintf(stderr, "Invalid character in base64 data, error: %d\n", ret);
                exit(5);
            }
            int i=0;
            while(i < out_len)
                fputc(out[i++], flags.output_fd);
            free(out);
        } else {
            fprintf(flags.output_fd, "%s\n", item->str);
        }
        return;
    }

    fprintf(flags.output_fd, "%s: line: %d, pos: %d", item->file, item->line_num+1, item->pos);
    if (item->addr != NULL) {
        addr_str = addr_to_string(item->addr);
        fprintf(flags.output_fd, ", %s", addr_str);
        //fflush(flags.output_fd);
    }
    if (item->str != NULL) {
        fprintf(flags.output_fd, ": %s", item->str);
    }
    fprintf(flags.output_fd, "\n");

    // FIXME: failing to free allocated string on windows
    // if (addr_str != NULL)
    //     free(addr_str);
}

void print_json_value(result_item_t *item, flags_t flags) {
    int i = 0;
    int input_length = item->length;
    int output_length = (int) input_length * 1.1; // FIXME: add 10%, this is prone to blow up when som silly guy provides a string with """"""""

    char *out = malloc(output_length);
    int out_i = 0;
    for (i=0; i<input_length; i++) {

        // make sure buffer is large enough for the next 2 characters
        if (output_length < out_i+3) {
            int new_l = (int) output_length * 1.1;
            out = realloc(out, new_l);
            output_length = new_l;
        }

        // get character
        char c = item->str[i];

        // escape '"'
        if (c == '"')
            out[out_i++] = '\\';

        // copy character
        out[out_i++] = item->str[i];
    }
    out[out_i] = 0;

    fprintf(flags.output_fd, "%s", out);
    free(out);
}

void print_xml_value(result_item_t *item, flags_t flags) {
    int i=0;
    char c = 0;
    while(i < item->length) {
        c = item->str[i];
        if (c == '&')
            fprintf(flags.output_fd, "&amp;");
        else if (c == '<')
            fprintf(flags.output_fd, "&lt;");
        else if (c == '>')
            fprintf(flags.output_fd, "&gt;");
        else if (c == '"')
            fprintf(flags.output_fd, "&quot;");
        else if (c == '\'')
            fprintf(flags.output_fd, "&apos;");
        else
            fprintf(flags.output_fd, "%c", c);
        i++;
    }
}

void output_json(result_item_t *item, flags_t flags, int last) {
    fprintf(flags.output_fd, "{\"file\":\"%s\",\"line\":\"%d\",\"pos\":\"%d\"", item->file, item->line_num+1, item->pos);
    if (item->addr != NULL) {
        char *addr_str = addr_to_string(item->addr);
        fprintf(flags.output_fd, ",\"addr\":\"%s\"", addr_str);
        //free(addr_str);
    }

    if (item->str != NULL) {
        fprintf(flags.output_fd, ",\"data\":\"");
        print_json_value(item, flags);
        fprintf(flags.output_fd, "\"");
    }
    //if (last)
    //    fprintf(flags.output_fd, "}\n");
    //else
        fprintf(flags.output_fd, "},\n");
}

void output_xml(result_item_t *item, flags_t flags) {
    fprintf(flags.output_fd, "\t<result file=\"%s\" line=\"%d\" pos=\"%d\"", item->file, item->line_num+1, item->pos);
    if (item->addr != NULL) {
        char *addr_str = addr_to_string(item->addr);
        fprintf(flags.output_fd, " addr=\"%s\"", addr_str);
        free(addr_str);

    }
    if (item->str != NULL) {
        fprintf(flags.output_fd, ">");
        print_xml_value(item, flags);
        fprintf(flags.output_fd, "</result>\n");
    } else {
        fprintf(flags.output_fd, "/>\n");
    }
}

void output_csv(result_item_t *item, flags_t flags) {
    //printf("file: '%s'\n", item->file);
    fprintf(flags.output_fd, "\"%s\";\"%d\";\"%d\"", item->file, item->line_num+1, item->pos);
    if (item->addr != NULL) {
        char *addr_str = addr_to_string(item->addr);
        fprintf(flags.output_fd, ";\"%s\"", addr_str);
        //free(addr_str);
    }
    if (item->str != NULL) {
        fprintf(flags.output_fd, ";\"");
        print_json_value(item, flags);
        fprintf(flags.output_fd, "\"");
    }
    fprintf(flags.output_fd, "\n");
}
