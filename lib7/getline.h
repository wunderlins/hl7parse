/**
 * @brief windows glib compatibility function
 * 
 * getdelim(), getline() - read a delimited record from stream, ersatz implementation
 *
 * For more details, see: http://pubs.opengroup.org/onlinepubs/9699919799/functions/getline.html
 */
#ifndef GETLINE_H
#define GETLINE_H

#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif

extern ssize_t getdelim(char **lineptr, size_t *n, int delim, FILE *stream);

extern ssize_t getline(char **lineptr, size_t *n, FILE *stream);

#ifdef __cplusplus
}
#endif


#endif /* GETLINE_H */

