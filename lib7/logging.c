#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <unistd.h>

#include "logging.h"

// int loglevel = LEVEL_WARNING;

#ifndef PRODUCTION
void logprint(const char *fmt, ...) {
	va_list args;
	va_start(args, fmt);
	vfprintf(stderr, fmt, args);
	fflush(stderr);
}

// logging functions
const char *logtime(void) {
	static char buf[20];

	struct timeval tv;
	gettimeofday(&tv, NULL);
	strftime(buf, sizeof(buf), "%Y-%m-%d %H:%M:%S", localtime((const time_t*) &tv.tv_sec));

	return buf;
}
#else
void logprint(const char *fmt, ...) {;}
const char *logtime(void) {return NULL;}
#endif