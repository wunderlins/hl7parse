/** \file
 * @brief ini file parser
 * 
 * 2020, Simon Wunderlin
 */

#ifndef _INI_H_
#define _INI_H_

/**
 * @brief pre-allocate items 
 * 
 * we are dynamically pre-allocating child items so we don't have
 * to call realloc() on every item added. If you have large structures
 * with many items settin this to a higher value might benefit 
 * performance (less realloc calls).
 * 
 * default: 10
 */
#define ALLOC_NUM_ITEMS 10

/**
 * @brief parser positions
 * This structure holds the last known position of element boundaries
 */
typedef struct {
	/** last section start position */
	size_t section_start;
	/** last section end position */
	size_t section_end;
	/** last item start position */
	size_t item_start;
	/** last item end position */
	size_t item_end;
	/** position of last equal sign (k/v delimiter) */
	size_t item_equal;
	/** start pos of last comment */
	size_t item_comment;
} last_pos_t;

/**
 * @brief key value pair of an ini file, delimited by `=`
 * This structure defines a key/value pair of an ini file
 */
typedef struct {
	/** start position in file */
	int start; 
	/** end position in file */
	int end;   
	/** key */
	char* name;
	/** value */
	char* value; 
} ini_item_t;

/**
 * @brief a section, that contains N ini_item_t
 * this structure holds a section with N items 
 */
typedef struct {
	/** name of the section, this is that part in brackets `[` and `]` */
	char* name;
	/** number of ini_items_t in this section */
	int length;  
	/** max allocated items */
	int size;
	/** array holding the items (k/v pairs) */
	ini_item_t **items;
} ini_section_t;

/**
 * @brief container holding ections
 * Top level strcture, holds N sections.
 */
typedef struct {	
	/** number of sections */
	int length;
	/** max allocated space in sections */
	int size;
	/** the array holding sections */
	ini_section_t **sections;
} ini_section_list_t;

/**
 * @brief constructor 
 * 
 * PArses an ini file. make sure `fp` points to the beginning of the file 
 * before invoking.
 * 
 * @param fp file pointer
 * @returns section list
 */
ini_section_list_t *ini_parse(FILE *fp);

/**
 * @brief free memory
 * 
 * @param ini parsed structure
 */
void ini_free(ini_section_list_t *ini);

/**
 * @brief find sections and keys in ini structure
 * 
 * @param ini structure to search in
 * @param section_name section to search for
 * @returns pointer to section or NULL if  was not found
 */
ini_section_t *ini_find_section(ini_section_list_t *ini, 
                                const char* section_name);
/**
 * @brief search for key in section
 * 
 * @param s section to search in
 * @param key name of the key to search for
 * @returns reference to the key/value pair or NULL if not found
 */
ini_item_t *ini_find_key(ini_section_t *s, const char* key);

/**
 * @brief get value of key in section
 * 
 * get a value for a specific item in a section. returns NULL if not found
 * 
 * @param ini section list to search in
 * @param section name of the section to search in
 * @param key key to search for in section
 * @returns value if the key is found in section otherwise `NULL`
 */
char *ini_get_value(ini_section_list_t *ini,
                const char* section, const char* key);

#endif // _INI_H_