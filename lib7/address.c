#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include <ctype.h>
#include "address.h"

hl7_addr_t *reset_addr(hl7_addr_t *a) {
    a->segment = NULL;
    a->fieldlist = -1;
    a->field = -1;
    a->comp = -1;
    a->subcmp = -1;
    a->seg_count = -1;
    return a;
}

hl7_addr_t *create_addr() {
    hl7_addr_t *a = malloc(sizeof(hl7_addr_t));
    return reset_addr(a);
}

void free_addr(hl7_addr_t *addr) {
    if (addr == NULL) return;
    //printf("addr %p, '%s'\n", addr->segment, addr->segment);
    if (addr->segment != NULL) 
        free(addr->segment);
    free(addr); 
    addr = NULL;
}

int set_addr_from_string(char* str, hl7_addr_t **ret_addr) {
    *ret_addr = reset_addr(*ret_addr);
    
    int i = 0;
    int len = strlen(str);
    if (len == 0)
        return -1;
    
    hl7_addr_t *a = *ret_addr;
    char *end = str + len;

    // find the segment delimiter
    char *seg = str;
    int seg_pos = 0;
    for (; seg_pos<len; seg_pos++, seg++) {
        if (*seg == '-' || *seg == '(' || *seg == 0) break;
    }
    
    // copy segment
    char *segment = malloc(seg_pos+1);
    memcpy(segment, str, seg_pos);
    segment[seg_pos] = 0;
    a->segment = segment;
    //printf("segment: '%s'\n", segment);

    //printf("%d, %d\n", seg_pos, len);
    if (seg_pos == len)
        return 0;

    char *addr = str + seg_pos;
    //printf("segment: '%s', rest: '%s'\n", segment, addr);

    // check if we have a repetition
    char *p = addr;
    if (*p == '(') {
        (void) *p++;
        //printf("str %s, p: %c\n", str, *p);
        if (p == end) return -2; // end of string
        char rep[10] = {0};
        for(i=0; p < end; i++, p++) {
            //printf("digit: '%c'\n", (char) *p);
            if (isdigit(*p) != 0) {
                rep[i] = *p;
                continue;
            }
            //printf("rep %s\n", rep);

            // convert numeric char array to integer
            a->seg_count = atoi(rep);
            //printf("seg_count: %d, rep: '%s'\n", a->seg_count, rep);
            if (*p != ')')
                return -2;
            
            (void) *p++;
            if (p == end) // we are done
                return 0;
            
            // we are done, rest of the buffer is processed later
            break;
        }

        // check if we are done already
        if (p == end) {
            // no closing bracket?
            if (a->seg_count == -1)
                return -2;
            return 0;
        }
    }

    // skip the dash
    if (p+1 < end && *p == '-') { 
        (void) *p++; 
        //printf("%s\n", "skipping dash");
    } else {
        //printf("%s\n", "we have an invalid address that ends in '-'");
        return -3;
    }

    //printf("p: '%c'\n", (char) *p);

    // if the next character is not a digit, something went wrong
    if (isdigit(*p) == 0) {
        //free(a->segment);
        //free(a);
        return -4;
    }
    
    // find first occourance of  '.' or '('
    char buff[10] = {0};
    for(i=0; p < end; i++, p++) {
        if (*p == '(' || *p == '.')
            break;
        if (isdigit(*p) == 0) {
            //free(a->segment);
            //free(a);
            return -5;
        }
        buff[i] = *p;
    }
    buff[i] = 0;
    int field = p-addr; 
    if (!field) {
        //free(a->segment);
        //free(a);
        return -6;
    }
    a->fieldlist = atoi(buff);

    char *start = p;

    // check if we have to parse field or comp
    if (p < end && *p == '(') {
        p++;
        if (p == end) { // invalid field definition
            //free(a->segment);
            //free(a);
            return -7;
        }

        i = 0;
        for(; p < end; i++, p++) {
            if (*p == ')')
                break;
            if (isdigit(*p) == 0) {
                //free(a->segment);
                //free(a);
                return -8;
            }
            buff[i] = *p;
        }
        buff[i] = 0;
        int fieldlist = p-start; 
        if (!fieldlist) {
            //free(a->segment);
            //free(a);
            return -9;
        }
        a->field = atoi(buff);
        
        // get rid of ')'
        if (*p != ')') return -9;
        p++; // FIXME: this is dangerous, we might overrun the buffer

        //printf("next char: %lld, end: %lld\n", p, end);
    }

    if (p < end) {
        p++; // skip '.' ? -- dangerous

        if (p == end) { // invalid field definition
            //free(a->segment);
            //free(a);
            return -10;
        }

        start = p;
        i = 0;
        for(; p < end; i++, p++) {
            if (*p == '.')
                break;
            if (isdigit(*p) == 0) {
                //free(a->segment);
                //free(a);
                return -11;
            }
            buff[i] = *p;
        }
        buff[i] = 0;
        int comp = p-start; 
        if (!comp) {
            //free(a->segment);
            //free(a);
            return -12;
        }
        a->comp = atoi(buff);
    }

    if (p < end) {
        p++; // skip '.', dangerous

        if (p == end) { // invalid field definition
            //free(a->segment);
            //free(a);
            return -13;
        }

        start = p;
        i = 0;
        for(; p < end; i++, p++) {
            //printf("%c ", *p);
            if (*p == '.')
                break;
            if (isdigit(*p) == 0) {
                //free(a->segment);
                //free(a);
                return -14;
            }
            buff[i] = *p;
        }
        buff[i] = 0;
        int subcmp = p-start; 
        if (!subcmp) {
            //free(a->segment);
            //free(a);
            return -15;
        }
        a->subcmp = atoi(buff);
    }

    return 0;
}

hl7_addr_t *addr_from_string(char* str) {
    hl7_addr_t *a = create_addr();
    int ret = set_addr_from_string(str, &a);
    if (ret != 0) {
        free_addr(a);
        return NULL;
    }
    return a;
}

char *addr_to_string(hl7_addr_t *addr) {
    char out[100] = {0};
    char buf[20] = {0};

    // segment + '-'
    if (addr->segment != NULL)
        strcpy(out, addr->segment);
    else
        strcpy(out, "   ");
    
    if (addr->seg_count > -1) {
        strcat(out, "(");
        sprintf(buf, "%d", addr->seg_count);
        strcat(out, buf);
        strcat(out, ")");
    }
    
    if (addr->fieldlist > -1) {
        strcat(out, "-");
        sprintf(buf, "%d", addr->fieldlist);
        strcat(out, buf);
    }

    if (addr->field > -1) {
        strcat(out, "(");
        sprintf(buf, "%d", addr->field);
        strcat(out, buf);
        strcat(out, ")");
    }

    if (addr->comp > -1) {
        strcat(out, ".");
        sprintf(buf, "%d", addr->comp);
        strcat(out, buf);
    }

    if (addr->subcmp > -1) {
        strcat(out, ".");
        sprintf(buf, "%d", addr->subcmp);
        strcat(out, buf);
    }

    //return out;
    char *ret = malloc(strlen(out)+1);
    strcpy(ret, out);
    return ret;
}

void addr_dump(hl7_addr_t *a) {
    if (a == NULL) {
        printf("segment, uniinitialized\n");
        return;
    }
        
    if (a->segment != NULL)
        printf("segment:   '%s'\n", a->segment);
    else
        printf("segment:   '(null)'\n");
    printf("seg_count: %d\n", a->seg_count);
    printf("fieldlist: %d\n", a->fieldlist);
    printf("field:     %d\n", a->field);
    printf("comp:      %d\n", a->comp);
    printf("subcmp:    %d\n", a->subcmp);
}

hl7_addr_t *clone_addr(hl7_addr_t* addr) {
    hl7_addr_t *fa = create_addr();
    fa->segment = strdup(addr->segment);
    fa->fieldlist = addr->fieldlist;
    fa->field = addr->field;
    fa->comp = addr->comp;
    fa->subcmp = addr->subcmp;
    fa->seg_count = addr->seg_count;
    return fa;
}

seg_count_t *create_seg_count() {
    seg_count_t *c = malloc(sizeof(seg_count_t));
    c->length = 0;
    c->_allocated = 2;
    c->count = malloc(sizeof(int) * (c->_allocated));
    c->segments = malloc(sizeof(char*) * (c->_allocated));
    //printf("c: %p, segments: %p, count: %p\n", c, c->segments, c->count);

    return c;
}

void free_seg_count(seg_count_t *c) {
    //printf("c: %p, segments: %p, count: %p\n", c, c->segments, c->count);
    free(c->count);
    for(int i=0; i<c->length; i++)
        free(c->segments[i]);
    free(c->segments);
    free(c);
}

int add_seg_count(char *segment, seg_count_t *c) {
    int i = 0;
    int pos = -1;

    // find poisition of element
    for(i=0; i<c->length; i++) {
        // check if segment matches
        if (c->segments[i][0] == segment[0] && 
            c->segments[i][1] == segment[1] && 
            c->segments[i][2] == segment[2]) {
            // we have a match in segment
            pos = i;
            break;
        }
    }

    // update
    if (pos != -1) {
        c->count[pos]++;
        return c->count[pos];
    }

    // insert
    if (c->length+1 == c->_allocated) { // enlarge container
        c->_allocated = c->_allocated * 2;
        c->count = realloc(c->count, sizeof(int) * (c->_allocated));
        c->segments = realloc(c->segments, sizeof(char*) * (c->_allocated));
        //printf("segments: %p, count: %p\n", c->segments, c->count);
    }
    c->segments[c->length] = strdup(segment);
    c->count[c->length] = 1;
    c->length++;

    return 1;
}

int get_seg_count(char *segment, seg_count_t *c) {
    int count = 0;

    // find poisition of element
    for(int i=0; i<c->length; i++) {
        // check if segment matches
        if (c->segments[i][0] == segment[0] && 
            c->segments[i][1] == segment[1] && 
            c->segments[i][2] == segment[2]) {
            // we have a match in segment
            count = c->count[i];
            break;
        }
    }

    return count;
}
