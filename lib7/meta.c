#include "bom.h"
#include "meta.h"
#include "logging.h"

hl7_meta_t* init_hl7_meta_t() {
    hl7_meta_t* sep = (hl7_meta_t*) malloc(sizeof(hl7_meta_t));
    sep->field_length = 0;
    sep->crlf = -1; // undefined, 1=true, 0=false

    sep->sep_message = '\r';
    sep->sep_field   = '|';
    sep->sep_comp    = '^';
    sep->sep_rep     = '~';
    sep->sep_escape  = '\\';
    sep->sep_subcmp  = '&';

    sep->encoding    = NULL;
    sep->version     = NULL;
    sep->type        = NULL;
    sep->subtype     = NULL;
    sep->bom         = NULL;

    /*
    sep->bom = (bom_t*) malloc(sizeof(bom_t*));
    sep->bom->length = 0;
    sep->bom->bom    = NULL;
    sep->bom->endianness = UNKNOWN;
    */

    return sep;
}

char *hl7_meta_string(hl7_meta_t* meta) {
    // bom string
    char bom[50] = {0};
    char sub_buf[5];
    int i=0;
    if (meta->bom) {
        for (; i < meta->bom->length; i++) {
            sprintf(sub_buf, "\\%02hhX", meta->bom->bom[i]);
            strcat(bom, sub_buf);
        }
    }
    bom[i] = 0;

    int l = 508 + strlen(bom); 
    if (meta->encoding) l += strlen(meta->encoding);
    if (meta->version)  l += strlen(meta->version);
    if (meta->type)     l += strlen(meta->type); 
    if (meta->subtype)  l += strlen(meta->subtype); 

    char *buf = (char*) malloc(1000);

    sprintf(buf, "Message Separator: \\%02hhX\n"
                 "Using CRLF:        %d\n"
                 "BOM:               %s\n"
                 "Field separators:  %c %c %c %c %c (%d)\n"
                 "HL7 Encoding:      %s\n"
                 "HL7 version:       %s\n"
                 "HL7 type:          %s\n"
                 "HL7 sub-type:      %s\n",
    
    meta->sep_message,
    meta->crlf, 
    bom, 
    meta->sep_field,
    meta->sep_comp,
    meta->sep_rep,
    (meta->sep_escape) ? meta->sep_escape : ' ',
    (meta->sep_subcmp) ? meta->sep_subcmp : ' ',
    meta->field_length,

    meta->encoding,
    meta->version,
    meta->type,
    meta->subtype
    );

    return buf;
}

void free_hl7_meta(hl7_meta_t *hl7_meta) {
    if (hl7_meta == NULL)
        return;
    
    if (hl7_meta->bom != NULL && hl7_meta->bom->bom != NULL) {
        free(hl7_meta->bom->bom);
        hl7_meta->bom->bom = NULL;
    }
    if (hl7_meta->bom != NULL) free(hl7_meta->bom);
    hl7_meta->bom = NULL;

    if (hl7_meta->encoding != NULL) free(hl7_meta->encoding);
    if (hl7_meta->version != NULL)  free(hl7_meta->version);
    if (hl7_meta->type != NULL)     free(hl7_meta->type);
    if (hl7_meta->subtype != NULL)  free(hl7_meta->subtype);
    if (hl7_meta != NULL)           free(hl7_meta);
    hl7_meta = NULL;
}


line_delimiter_t find_line_delimiter(FILE* fd) {
    int old_pos = ftell(fd);
    line_delimiter_t ret = DELIM_NONE;

    // find line delimiter
    char c = 0;
    log_debug("Searching for line delimiter, starting at: %d", old_pos);
    while ((c = getc(fd)) != EOF) {
        if (c == '\n' || c == '\r') {
            char sep = c;
            c = getc(fd);

            if (sep == '\r' && c == '\n')
                ret = DELIM_CRLF;
            else if (sep == '\n')
                ret = DELIM_LF;
            else
                ret = DELIM_CR;
            
            break;
        }
    }

    // reset file pointer
    fseek(fd, old_pos, SEEK_SET);
    return ret;
}

int read_meta(hl7_meta_t *hl7_meta, FILE *fd) {
    int i = 0;
    unsigned char c = 0;

    // if the bom object is NULL, do a bom detection first
    if (hl7_meta->bom == NULL) {
        hl7_meta->bom = detect_bom(fd);
    }

    // M
    c = fgetc(fd); if ((char) c == EOF) { fseek(fd, hl7_meta->bom->length, SEEK_SET); return 3; }
    // S
    c = fgetc(fd); if ((char) c == EOF) { fseek(fd, hl7_meta->bom->length, SEEK_SET); return 3; }
    // H|A
    c = fgetc(fd); if ((char) c == EOF) { fseek(fd, hl7_meta->bom->length, SEEK_SET); return 3; }
    i = 3;

    // field separator
    hl7_meta->sep_field = fgetc(fd); i++;
    if ((char) c == EOF) { fseek(fd, hl7_meta->bom->length, SEEK_SET); return 2; }
    
    // check component separator
    c = fgetc(fd); i++;
    if ((char) c == EOF) { fseek(fd, hl7_meta->bom->length, SEEK_SET); return 2; }
    if (c == hl7_meta->sep_field) {
        fseek(fd, hl7_meta->bom->length, SEEK_SET);
        return 0;
    }
    hl7_meta->sep_comp = c;

    // check repetition separator
    c = fgetc(fd); i++;
    if ((char) c == EOF) { fseek(fd, hl7_meta->bom->length, SEEK_SET); return 2; }
    if (c == hl7_meta->sep_field) {
        fseek(fd, hl7_meta->bom->length, SEEK_SET);
        return 0;
    }
    hl7_meta->sep_rep = c;

    // check escape separator
    c = fgetc(fd); i++;
    if ((char) c == EOF) { fseek(fd, hl7_meta->bom->length, SEEK_SET); return 2; }
    if (c == hl7_meta->sep_field) {
        fseek(fd, hl7_meta->bom->length, SEEK_SET);
        return 0;
    }
    hl7_meta->sep_escape = c;

    // check sub component
    c = fgetc(fd); i++;
    if ((char) c == EOF) { fseek(fd, hl7_meta->bom->length, SEEK_SET); return 2; }
    if (c == hl7_meta->sep_field) {
        fseek(fd, hl7_meta->bom->length, SEEK_SET);
        return 0;
    }
    hl7_meta->sep_subcmp = c;

    // next character must be a field separator, or something went badly wrong
    c = fgetc(fd); i++;
    if ((char) c == EOF) { fseek(fd, hl7_meta->bom->length, SEEK_SET); return 2; }
    if (c != hl7_meta->sep_field) {
        fseek(fd, hl7_meta->bom->length, SEEK_SET);
        return 1;
    }

    // rewind file poitner to the end of BOM
    fseek(fd, hl7_meta->bom->length, SEEK_SET);

    return 0;
}