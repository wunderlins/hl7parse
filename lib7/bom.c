#include "bom.h"

char * bom_to_string(int length, unsigned char *bom, bom_endianness_t endianness) {
    // 4: 2B 2F 76 39 NONE 
    char *out = malloc(21); // {'\0'};
    char l[5] = {0};
    sprintf(l, "%d ", (unsigned char) length);
    strcat(out, l);
    for(int i=0; i<length; i++) {
        char s[4] = "   ";
        sprintf(s, "%02X ", (unsigned char) bom[i]);
        //printf("--> '%s'\n", s);
        strcat(out, s);
    }

    /*
    if (endianness == LITTLE)
        strcat(out, "LE  ");
    else if (endianness == BIG)
        strcat(out, "LE  ");
    else if (endianness == UNKNOWN)
        strcat(out, "    ");
    else if (endianness == SIGNATURE)
        strcat(out, "IR  ");
    else
        strcat(out, "NONE\n");
    // printf("%s\n", out);
    */
    return out;
}

void print_bom(bom_t* bom) {
    char *str = bom_to_string(bom->length, bom->bom, bom->endianness);
    printf("%s\n", str);
}

bom_t* detect_bom(FILE *fd) {
    // get first char from file
    //unsigned char c = getc(fd);
    unsigned char bom[5];
    bom_t *ret = (bom_t*) malloc(sizeof(bom_t));
    ret->bom = NULL;
    ret->length = 0;
    ret->endianness = UNKNOWN;
    
    // read the first 5 bytes
    size_t fdpos = ftell(fd);
    int li=0;
    for (; li<5 && !feof(fd); li++) {
        bom[li] = fgetc(fd);
    }
    li--;
    
    // reset file pointer
    fseek(fd, fdpos, SEEK_SET);
    unsigned char c = bom[0];

    // check if we have a bom
    if (li > 0 &&
        (c == 0xEF || c == 0xFE || c == 0xFF || c == 0 || c == 0x2B || 
         c == 0xF7 || c == 0xDD || c == 0x0E || c == 0xFB || c == 0x84)) {

        ret->endianness = SIGNATURE;

        // utf 7 can use up to 5 bytes
        // FIXME: this is never reached when `0x2B 0x2F 0x76` is already detected above
        //bom[4] = getc(fd);
        if (li > 3 && bom[0] == 0x2B && bom[1] == 0x2F && bom[2] == 0x76 && 
            (bom[3] == 0x38 || bom[3] == 0x39 || bom[3] == 0x2B || bom[3] == 0x2F)) {
            ret->length = 4;
            ret->bom = (char*) malloc(ret->length);
            memcpy(ret->bom, bom, ret->length);
            fseek(fd, fdpos+ret->length, SEEK_SET);
            ret->endianness = SIGNATURE;
            return ret;
        }

        // 4 bytes, are we done yet ?
        //bom[3] = getc(fd);
        if (li > 3 && (
            (bom[0] == 0x00 && bom[1] == 0x00 && bom[2] == 0xFF && bom[3] == 0xFF) ||
            (bom[0] == 0xFF && bom[1] == 0xFE && bom[2] == 0x00 && bom[3] == 0x00) ||
            (bom[0] == 0xDD && bom[1] == 0x73 && bom[2] == 0x66 && bom[3] == 0x73) ||
            (bom[0] == 0x84 && bom[1] == 0x31 && bom[2] == 0x95 && bom[3] == 0x33))) {
            ret->length = 4;
            ret->bom = (char*) malloc(ret->length);
            memcpy(ret->bom, bom, ret->length);
            fseek(fd, fdpos+ret->length, SEEK_SET);
            ret->endianness = LITTLE;
            if (bom[0] == 0x00 && bom[1] == 0x00 && bom[2])
                ret->endianness = BIG;
            return ret;
        }

        // 3 bytes, are we done yet ?
        //bom[2] = getc(fd);
        ret->length++;
        if (li > 2 && (
            (bom[0] == 0xEF && bom[1] == 0xBB && bom[2] == 0xBF) ||
            (bom[0] == 0xF7 && bom[1] == 0x64 && bom[2] == 0x4C) ||
            (bom[0] == 0x0E && bom[1] == 0xFE && bom[2] == 0xFF) ||
            (bom[0] == 0xFB && bom[1] == 0xEE && bom[2] == 0xFF))) {
            ret->length = 3;
            ret->bom = (char*) malloc(ret->length);
            memcpy(ret->bom, bom, ret->length);
            fseek(fd, fdpos+ret->length, SEEK_SET);
            ret->endianness = LITTLE;
            return ret;
        }

        // there are always 2 characters.
        ret->length++;
        // are we done yet ?
        if (li > 1 && (
            (bom[0] == 0xFF && bom[1] == 0xFE) ||
            (bom[0] == 0xFE && bom[1] == 0xFF))) {
            ret->length = 2;
            ret->bom = (char*) malloc(ret->length);
            memcpy(ret->bom, bom, ret->length);
            fseek(fd, fdpos+ret->length, SEEK_SET);
            ret->endianness = LITTLE;
            if (bom[0] == 0xFE)
                ret->endianness = BIG;
            return ret;
        }

        ret->endianness = UNKNOWN;
    }

    // reset file pointer if there was no bom
    fseek(fd, 0, SEEK_SET);
    return ret;
}