/** \file
 * @brief hl7 address structures and utilities
 * 
 * Parse address strings into hl7_addr_t and vice versa. All string addresses 
 * start with `1` for the first element. This is an end-user interface. All
 * internal addresses in hl7_addr_t start with `0` whereis `-1` 
 * means `undefined`.
 * 
 * @note
 * All elements in hl7_addr_t are intialized to `-1`. If a address string 
 * is parsed with set_addr_from_string, all elements which remain at `-1`
 * have not been specified in the string (thus are unset).
 * 
 * ### HL7 adressing
 * 
 * String addresses are written i nthe following format, they are translated
 * into an efficient struct hl7_addr_t:
 * 
 * ```
 * SEG(3)-1(2).3.4
 *  ^  ^  ^ ^  ^ ^
 *  |  |  | |  | +-- optional: hl7_addr_t.subcmp     (delimited by &)
 *  |  |  | |  +---- optional: hl7_addr_t.comp       (delimited by ^)
 *  |  |  | +------- optional: hl7_addr_t.field      (delimited by ~)
 *  |  |  +--------- optional: hl7_addr_t.fieldlist  (delimited by |)
 *  |  +------------ optional: hl7_addr_t.seg_count  (segement repetition)
 *  +--------------- required: hl7_addr_t.segment    (delimited by \r)
 * ```
 * 
 */

#ifndef ADDRESS_H
#define ADDRESS_H

/**
 * @brief hl7 element address
 * 
 * All addresses start with `0`. `-1` means undefined (not specified when a 
 * string was converted to hl7_addr_t).
 * 
 * Some obvious defaults must be taken into account by the user. For 
 * example, if `field` is `-1`, then it always should be treated as `0`, 
 * because there is no element with an undefined number of fields (repetitions),
 * if not specified, the first element hast to be used.
 * 
 * Same for `seg_count`, if it is not specified (and therefore equals `-1`) 
 * the first index `0` is meant.
 */
typedef struct hl7_addr_t {
    /** segment name, typically 3 byte upper case letters */
    char* segment;
    /** the data between the hl7_meta_t.sep_field (typically `|`) */
    int fieldlist;
    /** the data between the hl7_meta_t.sep_rep (typically `~`), fields can have repetitions. typically these are omited in addresses. */
    int field;
    /** the data between the hl7_meta_t.sep_comp (typically `^`) */
    int comp;
    /** the data between the hl7_meta_t.sep_subcmp (typically `&`) */
    int subcmp;
    /** the address of the n-th segment in a file. If you have 3 `oBX` segments and you want the 2nd, then use `OBX(2)` to specifically address the second */
    int seg_count;
} hl7_addr_t;

/**
 * @brief keep track of the number of the same segments in a message_t
 * 
 * This struct keeps track of the number of segments with the same name. This is
 * important when search certain elements, to make sure we do not overrun 
 * buffers. 
 * 
 * All memory is allocated dynamically, `_allocated` keeps track of the length
 * of allocated space in `count` and `segments`. `length` keeps track of the 
 * used elements.
 * 
 * The count of a `segment` can be foudn at the same index in `count`.
 */
typedef struct seg_count {
    /** number of elements stored in count and segments */
    unsigned int length;
    /** size of allocated elements in count and segments */
    unsigned int _allocated;
    /** array of segment names */
    char** segments;
    /** array of numbers of segments */
    int* count;
} seg_count_t;


#ifdef __cplusplus
extern "C" {
#endif


/**
 * @brief reset to default values
 *  
 * set all values ot `-1` and segment to `NULL`.
 * 
 * @param addr address struct to reset
 */
hl7_addr_t *reset_addr(hl7_addr_t *addr);

/**
 * @brief create address structure
 * 
 * malloc() an hl7_addr_t struct and set all values to default values (reset_addr())
 * 
 * @returns empty address struct
 */
hl7_addr_t* create_addr();

/**
 * @brief parse string adresss
 * 
 * shorthand for set_addr_from_string() which also allocates hl7_addr_t.
 * 
 * @see set_addr_from_string()
 * @param str string address, see create_addr for format
 * @returns parsed address as structure, NULL on error
 */
hl7_addr_t* addr_from_string(char* str);

/**
 * @brief set address by string
 * 
 * Parses a string address, and returns ret_addr srtuct containing 
 * the string definition. ret_addr will first be reset.
 * 
 * returns `0` on succes and the following error codes otherwise:
 * - ` -1`: string length 0
 * - ` -2`: segment count not ended by `)`
 * - ` -3`: field address must start with `-`
 * - ` -4`: field address is not a digit
 * - ` -5`: expected `(` or `.` after field
 * - ` -6`: invalid field address, must at least set SEG-N
 * - ` -7`: field repetition `(` must be followed by digit
 * - ` -8`: field repetition may only contain digits between `(` and `)`
 * - ` -9`: premature end in field repetition (fieldlist)
 * - `-10`: preamture end, expecting component address after `.`
 * - `-11`: value error, only digits allowed in component address after `.`
 * - `-12`: premature end, comp address is empty
 * - `-13`: invalid character followed after comp
 * - `-14`: '.' for subcomp must be followed by an address, end of string reached
 * - `-15`: premature end, subcomp address is empty
 * 
 * @param str string address
 * @param ret_addr address struct which will contain the string address
 * @returns 0 on success, error code otherwise
 */
int set_addr_from_string(char* str, hl7_addr_t **ret_addr);

/**
 * @brief create a string representation of the address
 * 
 * Allocates memory for a string and rturns the string representation of addr.
 * 
 * @param addr our address
 * @returns string representation of address
 */
char* addr_to_string(hl7_addr_t* addr);

/**
 * @brief free hl7_addr_t struct
 * @param addr address to free
 */
void free_addr(hl7_addr_t* addr);

/**
 * @brief dispaly address
 * 
 * Debug method, will printf() the content of hl7_addr_t
 * 
 * @param addr address to dump
 */
void addr_dump(hl7_addr_t* addr);

/**
 * @brief clone an address
 * 
 * You must make sure to completely deallocate the newly created address, @see free_add().
 * 
 * @param addr address to clone
 * @returns copy of address
 */
hl7_addr_t* clone_addr(hl7_addr_t* addr);

/**
 * @brief Create segment struct
 * 
 * @returns seg_count_t struct with allocated memory
 */
seg_count_t* create_seg_count();

/**
 * @brief free segment count struct
 * 
 * @param segc strcut to clear
 */
void free_seg_count(seg_count_t* segc);

/**
 * @brief increment count for segment
 * 
 * This method dynamically allocates more memory for segment and count if required
 * 
 * @note
 * segments must be 3 char long strings. no bounds checking is done
 * 
 * @param segment string segment
 * @param segc segment struct
 * @returns new count for provided segment
 */
int add_seg_count(char* segment, seg_count_t* segc);

/**
 * @brief get count for segment name
 * 
 * get the count for a specific segment
 * 
 * @param segment string name of the segment
 * @param segc segment struct
 * @returns count for segment, 0 if not found
 */
int get_seg_count(char* segment, seg_count_t* segc);

#ifdef __cplusplus
}
#endif

#endif // ADDRESS_H