#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include "logging.h"
#include "meta.h"
#include "decode.h"
#include "node.h"
#include "address.h"
#include "search.h"
#include "../generated/usage_search.h"

const int loglevel = LEVEL_WARNING;

void dump_flags(flags_t f) {
    printf("command line arguments parsed:\n");
    printf("> verbose:    %d\n", f.verbose);
    printf("> search:     %d = '%s'\n", f.search_term, f.search_term_value);
    printf("> greedy:     %d\n", f.greedy);
    printf("> json:       %d\n", f.output_json);
    printf("> xml:        %d\n", f.output_xml);
    printf("> csv:        %d\n", f.output_csv);
    printf("> address:    %d '%s'\n", f.address, f.address_value);
}

/**
 * 
 * Exit values:
 *  - 1: argument error
 *  - 2: empty search term
 *  - 3: -q cannot be used with -o=nnn
 *  - 4: failed to open output file -f=filename
 *  - 5: invalid character in base64 data
 *  ...
 *  - 11: failed to open file
 *  - 12: failed to parse meta data @see read_meta()
 *  - 13: invalid address @see parse_address()
 *  - 14: invalid/unknown search mode
 *  ...
 *  - 30: failed to allocate memory
 *  - 31: failed to detect delimiters 
 *  - 32: no line delimiter found, we do not accept one line files
 */
int main(int argc, char *argv[], char *envp[]) {
    
    // initialize command line defaults
    /*
    flags_t search_flags = {
        0, // v: verbose
        0, // s: search term
        1, // n: greedy
        0, // o=json: json output
        0, // o=xml: xml output
        0, // o=csv: csv output
        0, // a: address to search in
        NULL, // search_term (with s)
        NULL, // address string (with a)
        0, // q: quiet, only for console output, displays values only
        0, // d: base64 decode values
        0, // f: output file
        NULL, // output_file_value, name of the output file
        NULL, // FILE* output_file handle,
        0     // i: case insensitive search
    };
    */
   flags_t *search_flags = create_flags_t();

    int index;
    int c;
    char *opt = NULL;

    // read command line options
    while ((c = getopt (argc, argv, "v:s:a:hno:qdf:i")) != -1) {
        switch (c) {
            case 'v':
                opt = optarg;
                if (*opt == '=')
                    opt++;
                search_flags->verbose = atoi(opt);
                break;
            case 's':
                search_flags->search_term = 1;
                opt = optarg;
                if (*opt == '=')
                    opt++;
                search_flags->search_term_value = (unsigned char*) opt;
                break;
            case 'a':
                search_flags->address = 1;
                opt = optarg;
                if (*opt == '=')
                    opt++;
                search_flags->address_value = opt;
                break;
            case 'n':
                search_flags->greedy = 0;
                break;
            case 'q':
                search_flags->quiet = 1;
                break;
            case 'd':
                search_flags->decode64 = 1;
                break;
            case 'i':
                search_flags->case_insensitive = 1;
                break;
            case 'f':
                opt = optarg;
                if (*opt == '=')
                    opt++;
                search_flags->output_file = 1;
                search_flags->output_file_value = opt;
            case 'o':
                opt = optarg;
                if (*opt == '=')
                    opt++;
                if (strcmp(opt, "csv") == 0)
                    search_flags->output_csv = 1;
                if (strcmp(opt, "xml") == 0)
                    search_flags->output_xml = 1;
                if (strcmp(opt, "json") == 0)
                    search_flags->output_json = 1;
                log_debug("Output Format: %s", opt);
                break;
            case '?':
                if (optopt == 'v' || optopt == 's' || optopt == 'a' || optopt == 'f') {
                    fprintf (stderr, "Option -%c requires an argument.\n", optopt);
                } else if (isprint(optopt)) {
                    fprintf (stderr, "Unknown option `-%c'.\n", optopt);
                    printf("%s", search_usage);
                } else {
                    fprintf (stderr, "Unknown option character `\\x%x'.\n", optopt);
                }
                return 1;
            case 'h':
                printf("%s", search_usage);
                exit(0);
            default:
                printf("%s", search_usage);
                abort();
        }
    }

    // debug flags
    if (loglevel >= LEVEL_DEBUG)
        dump_flags(*search_flags);

    // at least we do need a search term
    if (search_flags->search_term == 0 && search_flags->address == 0) {
        fprintf(stderr, "Missing search term or Address.\n");
        free(search_flags);
        return 1;
    }

    if (search_flags->search_term && (search_flags->search_term_value == NULL || 
        strlen((char*) search_flags->search_term_value) == 0)) {
        fprintf(stderr, "Search term cannot be empty.\n");
        free(search_flags);
        return 2;
    }

    // defaults
    if (search_flags->address == 0) {
        search_flags->address_value = "";
    }

    if ((search_flags->output_csv || search_flags->output_json || 
         search_flags->output_xml) && search_flags->quiet) {
        fprintf(stderr, "-q only works on console output, not possible with -o=json|csv|xml.\n");
        free(search_flags);
        return 3;
    }

    // check if there is an output file defined
    if (search_flags->output_file == 1) {
        // try to open a file handle
        search_flags->output_fd = fopen(search_flags->output_file_value, "wb");
        if (search_flags->output_fd == NULL) {
            fprintf(stderr, "Failed to open output file: '%s'\n", search_flags->output_file_value);
            free(search_flags);
            return 4;
        }
    } else {
        search_flags->output_fd = stdout;
    }

    // loop over files
    int ret = 0;
    if (search_flags->output_csv)
        fprintf(search_flags->output_fd, "file;line;position;address;data\n");
    if (search_flags->output_json)
        fprintf(search_flags->output_fd, "[\n");
    if (search_flags->output_xml) {
        fprintf(search_flags->output_fd, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
        fprintf(search_flags->output_fd, "<results xmlns=\"http://wunderlin.net/7parse\">\n");
    }
    
    for (index = optind; index < argc; index++) {
        if (search_flags->verbose)
            fprintf(search_flags->output_fd, "Searching file %s\n", argv[index]);
        
        // TODO: check if file is a regular file or a directory
        ret = search_file(argv[index], *search_flags);
        // TODO: resolve files in directroy and loop over regular files

        if (ret != 0) {
            fprintf(stderr, "Failed to parse %s, error code %d\n", argv[index], ret);
        }
    }

    if (search_flags->output_json)
        fprintf(search_flags->output_fd, "]\n");
    if (search_flags->output_xml)
        fprintf(search_flags->output_fd, "</results>\n");
    if (search_flags->output_file == 1)
        fclose(search_flags->output_fd);

    free(search_flags);

    return 0;
}
