/** \file
 * @brief experimental hl7 editing methods
 * 
 * Node manipulation utilities
 * 
 * @note
 * this is work in progress and by no means complete nor stable
 */
#ifndef NODE_UTIL_H
#define NODE_UTIL_H

/**
 * 
 * 
 */
#include "util.h"
#include "address.h"
#include "node.h"
#include "buffer.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief append child
 * 
 * @see node_append()
 * @param parent the parent node to append a child to
 * @param node the ndoe to append to parent
 * @returns 0 on success or error code otherwise
 */
int node_append_child(node_t *parent, node_t* node);

/**
 * @brief unimplemented: set a child at the sold position
 * 
 * This function set a child at `pos`.
 * 
 * We have cases to handle
 * - num_children-1 < pos: pad children with empty nodes first, return NULL
 * - num_children-1 >= pos: replace existing node and return pointer to old node
 * 
 * @todo
 * unimplemented!
 * 
 * @todo
 * The caller must take care to deallocate the replaced node. This is a bit 
 * dangerous, maybe it should be done here.
 * 
 * @param parent the parent node to append a child to
 * @param child the ndoe to append to parent
 * @param pos insert at this position
 * @returns new node
 */
node_t *node_insert_child_at(node_t *parent, node_t *child, int pos);

/**
 * @brief unimplemented: remove a node
 * 
 * this method will check parent and remove the reference from
 * the children array, moving all elements after the removed one 1 
 * slot to the front.
 * 
 * All child nodes (if any) are also deleted.
 * 
 * @todo
 * unimplemented!
 * 
 * @note
 * define a struct, where poitners to empty nodes are kept. so we 
 * can detect if a user tries to access it later. 
 * remove data, keep structure.
 * 
 * @see node_remove_child()
 * @param node to remove from tree
 */
int node_remove(node_t *node);

/**
 * @brief unimplemented: remove a child from the children array
 * 
 * find node, move all subsequent children one slot to the left,
 * adjust num_children and return a reference to the removed 
 * node for de-allocation or further use.
 * 
 * @todo
 * unimplemented!
 * 
 * @param parent the parent node to append a child to
 * @param child the ndoe to append to parent
 * @param pos insert at this position
 */
node_t *node_remove_child(node_t *parent, node_t *child, int pos);

/**
 * @brief create and append and empty child node
 * 
 * Special treatement for `FIELDLIST`, a `FIELD` node is always appended
 * and returned.
 * 
 * @param parent the parent node to append a child to
 * @returns new node
 */
node_t *node_create_n_append(node_t *parent);

/**
 * @brief create empty child nodes until count is reached
 * 
 * pad the children array until count is reached. care must be taken with 
 * node type `NODELIST` which should always get a single empty child in
 * the node list iself.
 * 
 * @see node_create_n_append()
 * @param parent parent node
 * @param count number to pad children to
 * @returns last child added
 */
node_t *node_pad_children(node_t *parent, int count);

/**
 * @brief create an empty node
 * 
 * for type `FIELDIST`, always create at least one field child node.
 * 
 * @note
 * returns `NULL` on `FIELDLIST` which seems a bit strange.
 * 
 * @deprecated
 * this interface makes no sense, it's a duplicate of create_node_t() 
 * with a better name
 * 
 * @param type node type to create
 * @returns new node
 */
node_t *node_create_empty(node_type_t type);

/**
 * @brief convert node structure into string
 * 
 * This method concatenates a whole tree into a string. It can be used 
 * anywhere in the tree structure from `SEGMENT` downwards.
 * 
 * If you want to use specific delimiters, make sure to create a custom meta_t
 * structure. Otherwise (using default delimiter as of HL7 specification)
 * just provice a default created with init_hl7_meta_t().
 * 
 * The length of the string is returned in the pointer of parameter 3 `length`,
 * however, the return string os `\0` delimited.
 * 
 * @param node root node
 * @param meta delimiter definitions
 * @param length returns the length of the output string
 * @returns string representation of node tree
 */
unsigned char *node_to_string(node_t *node, hl7_meta_t *meta, int *length);

/**
 * @brief write contents of message to file
 * 
 * same as `message_to_string()` but writing to a file directly.
 * 
 * This message returns the bytes writte on sucess or a 
 * negative integer on error.
 * 
 * Error codes:
 * - `-1`: failed to open file for writing
 * - `-2`: failed to convert segment to string
 * 
 * @see message_to_string()
 * @param message input to convert to string and write to file
 * @param filename string to file name where result shall be written
 * @returns 0 on succes, error otherwise
 */
int message_to_file(message_t *message, char *filename);

/**
 * @brief convert message structure to hl7 file
 * 
 * returns a string containing all segments covnerted to string, 
 * separated by delimiters defined in meta.
 * 
 * If you want to use different line or field separators, you may change these 
 * in meta before passing it into this method.
 * 
 * This method will return a string buffer on success, or NULL on error.
 * 
 * @param message input to convert to string 
 * @returns NULL on error else array buffer containing the hl7 file content
 */
unsigned char *message_to_string(message_t *message);

/**
 * @brief update date of a node
 * 
 * Checks and frees already associated data.
 * 
 * Then allocates memory for the data and assoiates it with the node, 
 * length is udpated (length is the actual length, if you are setting a 
 * string, you have to count the `\0` as well).
 * 
 * @param node 
 * @param data array buffer to set data of node to
 * @param length of the array buffer
 * @returns 0 on succes, -1 when memory could not be allocated
 */
int node_set_data(node_t *node, unsigned char *data, int length);

/**
 * @brief set string data to node
 * 
 * convenience function for `node_set_data()`. This method takes care of 
 * tracking the data length. It will copy all data up until the 
 * first `\0` character).
 * 
 * @param node 
 * @param data
 * @returns 0 on succes, -1 when memory could not be allocated
 */
int node_set_string(node_t *node, char *data);

/**
 * @brief find a node by address
 * 
 * The address of the found node will be set in the 3rd parameter. If the node
 * could not be found, the return value will currespond to the node level
 * where we could not find anything.
 * 
 * Let's say, the segment does not exist, then we return `SEGMENT`. If
 * the fieldlist is shorter than addr->fieldlist, then we return 
 * `FIELDLIST`, etc.
 * 
 * Return codes: if the node cannot be found, the return code will correspond 
 * to the node type where we faild to traverse.
 * 
 * @param message root node
 * @param addr address to search for
 * @param node this pointer contains the result node or NULL if not found
 * @returns If found, we return 0 and *node will point to the found node
 */
int node_get_by_addr(message_t* message, hl7_addr_t *addr, node_t **node);

/**
 * @brief set a value by address
 * 
 * This method creates a node structure if it does not exist.
 * 
 * Then it sets the value to the provided value.
 * 
 * If the node already existed, the method will 
 * destroy (deallocate) the old node and replace it with 
 * the new one.
 * 
 * return codes:
 * - `1`: Failed to allocate fieldlist 
 * - `2`: Failed to allocate field
 * - `3`: Failed to set data on field
 * - `4`: Failed to pad children on field (Comp)
 * - `5`: Failed to set data on comp
 * - `6`: Failed to pad children on field (subcmp)
 * - `7`: Failed to set data on subcomp
 * - `21`: tryed to add a component to a field that already has data
 * - `22`: tryed to add a sub-component to a component that already has data
 * 
 * @param message root node to traverse to
 * @param addr address of the new node
 * @param value array of the new node's content
 * @param length of value
 * @returns 0 on success, error code otherwise
 */
int node_set_by_addr(message_t *message, hl7_addr_t *addr, unsigned char *value, int length);

#ifdef __cplusplus
}
#endif

#endif // NODE_UTIL_H
