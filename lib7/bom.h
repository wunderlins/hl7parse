/** \file
 * @brief find unicode bom
 * 
 * When parsing an HL7 file, the opened file pointer should be at the 
 * beginning of data (typically just at the beginning of `MSH`).
 * 
 * If the file contains a unicode BOM, and the file pointer points at the 
 * beginning  of the file, the parser will fail. Therefore we first must skip 
 * the BOM bytes.
 * 
 * This is a crude method of detecting if the file has a BOM. Alternatively
 * you may deploy you own method and just skip ahead until you know the file 
 * pointer is at the first character of data (at the beginning of `MSH`) 
 * before parsing the file.
 * 
 * ### how it's done:
 * 
 * we try to detect known BOM patterns and then place the pointer just after it.
 * known patterns:
 * 
 * 2 Bytes
 * - UTF-16 LE `0xFF 0xFE`
 * - UTF-16 BE `0xFE 0xFF`
 * 
 * 3 Bytes
 * - UTF-8 `0xEF 0xBB 0xBF`
 * - UTF-1 `0xF7 0x64 0x4C`
 * - SCSU `0x0E 0xFE 0xFF`
 * - BOCU-1 `0xFB 0xEE 0xFF`
 * 
 * 4 Bytes
 * - UTF-7 `0x2B 0x2F 0x76` // Followed by 38, 39, 2B, or 2F (ASCII 8, 9, + or /), depending on what the next character is.
 * - UTF-32 BE `0x00 0x00 0xFF 0xFF`
 * - UTF-32 LE `0xFF 0xFE 0x00 0x00`
 * - UTF-EBCDIC `0xDD 0x73 0x66 0x73`
 * - GB-18030 `0x84 0x31 0x95 0x33`
 * 
 * ## usage
 * ```C
 * #include "bom.h"
 * 
 * FILE *fd = fopen(some/file, 'rb');
 * rewind(fd); // make sure the file pointer is at the beginning of the file
 * bom_t* detect_bom(fd);
 * 
 * ```
 */
#pragma once 

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * @brief endianness detected in bom
 */
typedef enum {
    /** undetected */
    UNKNOWN,
    /** little endian */
    LITTLE,
    /** big endian */
    BIG,
    /** smaller than 16 bit, it doesn't matter */
    SIGNATURE
} bom_endianness_t;

/**
 * @brief Byte Order MArk (BOM) information of a file.
 * This struct is created by detect_bom()
 */
typedef struct bom_t {
    /** contanis the raw bytes of the bom */
    char *bom;
    /** the length of the bom */
    int length;
    /** endianness */
    bom_endianness_t endianness;
} bom_t;

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief hex representation of the bom
 * 
 * @param length lenght of input buffer
 * @param bom byte array with the bom
 * @param endianness endianness to display
 * @returns printable string
 */
char * bom_to_string(int length, unsigned char *bom, bom_endianness_t endianness);

/**
 * @brief debug function to print bom
 * 
 * @param bom
 */
void print_bom(bom_t* bom);

/**
 * @brief check if the file has a bom
 * 
 * if there is a bom, it will be copied to `bom->bom`. The file pointer will be 
 * set to the first character after the bom.
 * 
 * To check if a bom has been detected, bom->length is greater than 0. Length 
 * represents the number of bytes bom->bom contains.
 * 
 * @note
 * The file pointer must be at the beginning of the file or this will fail. 
 * Either run detect_bom() right after opening a file or rewind before using.
 * 
 * @see https://en.wikipedia.org/wiki/Byte_order_mark
 * @param fd file handle to read data from
 * @return bom_t bom bytes are stored in `bom->bom`, length is indicated by `bom->length`
 */
bom_t* detect_bom(FILE *fd);

#ifdef __cplusplus
}
#endif