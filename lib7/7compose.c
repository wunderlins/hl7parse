#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "logging.h"
#include "bom.h"
#include "meta.h"
#include "node.h"
#include "decode.h"
#include "util.h"
#include "address.h"
#include "node_util.h"

// tests
const int loglevel = LEVEL_DEBUG;
int main(int argc, char *argv[]) {
    int ret = 0;
    
    // initialize separator data structure
    hl7_meta_t* meta = init_hl7_meta_t();

    bom_t *bom = (bom_t*) malloc(sizeof(bom_t));
    bom->bom = NULL;
    bom->length = 0;
    meta->bom = bom; 

    // create an empty message
    message_t *message = create_message_t(meta);
    log_debug("new messages, number of childre: %d", message->num_children);

    /*
    // keep track of the count of segments by name.
    // if a message has 2 OBX elements, then 
    // get_seg_count("OBX") will return 2. 
    // get_seg_count("ABC") will return 0 if there is none 
    // in message.
    seg_count_t *segments = create_seg_count();
    */

    // part one: start creating entries
    // 1. create a segment "SEG"
    // 2. create a node at SEG-5, type: FIELD
    // 3. set the value of a FIELD at SEG-4
    // 4. set a value at SEG-3(3).3.3

    // set default structure, MSH segment
    hl7_addr_t *a_msh = addr_from_string("MSH-2");
    unsigned char *value_msh2 = (unsigned char*) "ABCD";
    ret = node_set_by_addr(message, a_msh, value_msh2, 5);
    if (ret != 0) {
        char *addrstr = addr_to_string(a_msh);
        log_debug("Failed to add node, error code %d, addr: %s", ret, addrstr);
        free(addrstr);
    }
    
    // part two: by address
    // create address for "SG2(2)-3(2).1"
    hl7_addr_t *a1 = addr_from_string("SG2(2)-3(2).1");
    // create whole node structure and set value to "VALUE"
    unsigned char *value = (unsigned char*) "VALUE";
    ret = node_set_by_addr(message, a1, value, 6);
    if (ret != 0) {
        log_debug("Failed to add node, error code %d, addr: %s", ret, addr_to_string(a1));
    }

    hl7_addr_t *a2 = addr_from_string("SG2(2)-2(1).1");
    ret = node_set_by_addr(message, a2, value, 6);
    if (ret != 0) {
        log_debug("Failed to add node, error code %d, addr: %s", ret, addr_to_string(a2));
    }

    // overwrite value of first element
    hl7_addr_t *a3 = addr_from_string("SG2(2)-3(2).1");
    int r = set_addr_from_string("SG2(2)-3(2).1", &a1);
    unsigned char *value2 = (unsigned char*) "VALUE2";
    ret = node_set_by_addr(message, a1, value2, 7);
    if (ret != 0) {
        log_debug("Failed to add node, error code %d, addr: %s", ret, addr_to_string(a3));
    }

    // set data for field 1 in sg2(2)
    // this is dangerous, don't do this if you are not 100% sure 
    // what you are doing!
    //
    // use node_set_by_addr(...) instead!
    node_t *n = message->segments[2]->children[0]->children[0];
    unsigned char *direct = (unsigned char*) "DIRECT INSERT";
    node_set_data(n, direct, 14);

    // part 3: dump structure
    // 1. dump_ndoes()
    dump_structure(message);

    // check if we can resolve the address of this node
    hl7_addr_t *a_from_node = addr_from_node(n);
    log_debug("generated address:");
    addr_dump(a_from_node);

    // 2. node_to_string()
    unsigned char *hl7_str = message_to_string(message);
    printf("%s", hl7_str);
    free(hl7_str);

    // do some funny stuff with delimiters
    //
    // control output of message generation via meta. 
    // You may set field separators. also, encoding 
    // should be taken into account and converted (not yet implemented)

    // line delimiter
    meta->crlf = 1;
    meta->sep_message = 13;

    // field separators
    meta->sep_field  = '!';
    meta->sep_comp   = '*';
    meta->sep_subcmp = '$';

    hl7_str = message_to_string(message);
    printf("%s", hl7_str);
    free(hl7_str);

    // error condition, sg2(2)-1 already has data, adding 
    // comp data should not be allowed
    log_debug("%s", "adding invalid node");
    hl7_addr_t *a4 = addr_from_string("SG2(2)-1.1");
    unsigned char *value3 = (unsigned char*) "INVALID INSERT";
    ret = node_set_by_addr(message, a4, value3, 15);
    if (ret != 0) {
        log_debug("Failed to add node, error code %d, addr: %s", ret, addr_to_string(a3));
    }

    // find nodes, test node_get_by_addr
    node_t *found_node = NULL;
    ret = node_get_by_addr(message, a3, &found_node);
    if (found_node != NULL) {
        log_debug("found ret: %d, data: '%s'", ret, (char*) found_node->data);
    } else {
        log_debug("not found ret: %d, no data, node not found", ret);
    }

    // non existing addr
    hl7_addr_t *saddr = addr_from_string("SG2(3)-3(2).1");
    ret = node_get_by_addr(message, saddr, &found_node);
    if (ret == 0) {
        log_debug("found ret: %d, data: '%s'", ret, found_node->data);
    } else {
        char *addrstr = (char*) addr_to_string(saddr);
        log_debug("not found %s ret: %d, no data, node not found", addrstr, ret);
        free(addrstr); addrstr = NULL;
    }

    saddr->seg_count = 2;
    saddr->fieldlist = 4;
    ret = node_get_by_addr(message, saddr, &found_node);
    if (ret == 0) {
        log_debug("found ret: %d, data: '%s'", ret, found_node->data);
    } else {
        char *addrstr = (char*) addr_to_string(saddr);
        log_debug("not found %s ret: %d, no data, node not found", addrstr, ret);
        free(addrstr); addrstr = NULL;
    }

    saddr->fieldlist = 3;
    saddr->field = 3;
    ret = node_get_by_addr(message, saddr, &found_node);
    if (ret == 0) {
        log_debug("found ret: %d, data: '%s'", ret, found_node->data);
    } else {
        char *addrstr = (char*) addr_to_string(saddr);
        log_debug("not found %s ret: %d, no data, node not found", addrstr, ret);
        free(addrstr); addrstr = NULL;
    }

    saddr->field = 2;
    saddr->comp = 2;
    ret = node_get_by_addr(message, saddr, &found_node);
    if (ret == 0) {
        log_debug("found ret: %d, data: '%s'", ret, found_node->data);
    } else {
        char *addrstr = (char*) addr_to_string(saddr);
        log_debug("not found %s ret: %d, no data, node not found", addrstr, ret);
        free(addrstr); addrstr = NULL;
    }

    saddr->comp = 1;
    saddr->subcmp = 1;
    ret = node_get_by_addr(message, saddr, &found_node);
    if (ret == 0) {
        log_debug("found ret: %d, data: '%s'", ret, found_node->data);
    } else {
        char *addrstr = (char*) addr_to_string(saddr);
        log_debug("not found %s ret: %d, no data, node not found", addrstr, ret);
        free(addrstr); addrstr = NULL;
    }
    free_addr(saddr); saddr = NULL;

    // cleanup
    free_message_t(message); message = NULL;
    //free_hl7_meta(meta); meta = NULL;
    free_addr(a1); a1 = NULL;
    free_addr(a2); a2 = NULL;
    free_addr(a3); a3 = NULL;
    //free_seg_count(segments);
    
    
    //test new node_to_string2

    // HL7 File descriptor
    /*
	FILE *fd;
	if ((fd = fopen(argv[1], "rb")) == NULL) {
		print_error(errno, argv[1]);
		return 1;
	}
    */


    return 0;
}
