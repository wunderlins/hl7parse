:: configure the field to extract
set FIELD=OBX-5.5
:: full path to the reader
set READER="C:\Program Files (x86)\Adobe\Acrobat Reader DC\Reader\AcroRd32.exe"

:: run
set TMPFILE=%TEMP%\hl7.pdf
set batdir=%~dp0
%batdir%\..\build\7search.exe -f "%TMPFILE%" -d -q -a %FIELD% %1
%READER% "%TMPFILE%"
del "%TMPFILE%"