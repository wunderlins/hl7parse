/** \file
 * @brief logging functioins and macros
 * 
 * if the macro PRODUCTION is set, all log functions will output nothing.
 */
#pragma once

/** log elvel fatal */
#define LEVEL_FATAL     1
/** log elvel critical */
#define LEVEL_CRITICAL  2
/** log elvel error */
#define LEVEL_ERROR     3
/** log elvel warning */
#define LEVEL_WARNING   4
/** log elvel notice */
#define LEVEL_NOTICE    5
/** log elvel info */
#define LEVEL_INFO      6
/** log elvel debugging */
#define LEVEL_DEBUG     7
/** log elvel trace */
#define LEVEL_TRACE     8

#ifndef PRODUCTION
	/**
	 * @brief log fatal
	 * @param[in] fmt format
	 * @param[in] ... arguments
	 */
	#define log_fatal(fmt, ...) \
		logprint("%s %s:%d " fmt "\n", \
		logtime(), __FUNCTION__, __LINE__, ##__VA_ARGS__)
	/**
	 * @brief log critical
	 * @param[in] fmt format
	 * @param[in] ... arguments
	 */
	#define log_critical(fmt, ...) \
		logprint("%s %s:%d " fmt "\n", \
		logtime(), __FUNCTION__, __LINE__, ##__VA_ARGS__)
	/**
	 * @brief log error
	 * @param[in] fmt format
	 * @param[in] ... arguments
	 */
	#define log_error(fmt, ...) \
		logprint("%s %s:%d " fmt "\n", \
		logtime(), __FUNCTION__, __LINE__, ##__VA_ARGS__)
		
	/**
	 * @brief log warning
	 * @param[in] fmt format
	 * @param[in] ... arguments
	 */
	#define log_warning(fmt, ...)  if (loglevel >= LEVEL_WARNING)  \
		logprint("%s %s:%d " fmt "\n", logtime(), __FUNCTION__, \
		__LINE__, ##__VA_ARGS__)
	/**
	 * @brief log notice
	 * @param[in] fmt format
	 * @param[in] ... arguments
	 */
	#define log_notice(fmt, ...)  if (loglevel >= LEVEL_NOTICE)  \
		logprint("%s %s:%d " fmt "\n", logtime(), __FUNCTION__, \
		__LINE__, ##__VA_ARGS__)
	/**
	 * @brief log info
	 * @param[in] fmt format
	 * @param[in] ... arguments
	 */
	#define log_info(fmt, ...)  if (loglevel >= LEVEL_INFO)  \
		logprint("%s %s:%d " fmt "\n", logtime(), __FUNCTION__, \
		__LINE__, ##__VA_ARGS__)
	/**
	 * @brief log debug
	 * @param[in] fmt format
	 * @param[in] ... arguments
	 */
	#define log_debug(fmt, ...) if (loglevel >= LEVEL_DEBUG) \
		logprint("%s %s:%d " fmt "\n", logtime(), __FUNCTION__, \
		__LINE__, ##__VA_ARGS__)
	/**
	 * @brief log trace
	 * @param[in] fmt format
	 * @param[in] ... arguments
	 */
	#define log_trace(fmt, ...) if (loglevel >= LEVEL_TRACE) \
		logprint("%s %s:%d " fmt "\n", logtime(), __FUNCTION__, \
		__LINE__, ##__VA_ARGS__)

#else

	#define log_fatal(fmt, ...) ;
	#define log_critical(fmt, ...) ;
	#define log_error(fmt, ...) ;
	#define log_warning(fmt, ...) ;
	#define log_notice(fmt, ...) ;
	#define log_info(fmt, ...) ;
	#define log_debug(fmt, ...) ;
	#define log_trace(fmt, ...) ;

#endif


#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief global log level
 * 
 * must be declared in main program
 */
extern const int loglevel;

/**
 * @brief log function
 * 
 * This method is a drop in replacement for printf()
 * 
 * @see sprintf()
 * @param fmt format according to sprintf()
 */
void logprint(const char *fmt, ...);

/**
 * @brief create timestamp
 * 
 * @returns current time in "%Y-%m-%d %H:%M:%S" 
 */
const char *logtime(void);

#ifdef __cplusplus
}
#endif