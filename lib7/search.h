/** \file
 * @brief hl7 search utilities
 */
#ifndef SEARCH_H
#define SEARCH_H

#include "address.h"
#include "meta.h"

#ifdef _WIN32
    #include "getline.h"
#endif

/**
 * @brief search version information
 */
const char *version_search();

/**
 * @brief command line parameters
 * 
 * this structure holds the command line parameters.
 */
typedef struct flags_t {
    /** verbose */
    int verbose;
    /** search term */
    int search_term;
    /** greedy */
    int greedy;
    /** json output */
    int output_json;
    /** xml output */
    int output_xml;
    /** csv output */
    int output_csv;
    /** address to search in */
    int address;
    /** search_term */
    unsigned char *search_term_value;
    /** address string */
    char *address_value;
    /** quiet, output values only */
    int quiet;
    /** base64 decode values */
    int decode64;
    /** use output file instead of stdout */
    int output_file;
    /** output file name */
    char* output_file_value;
    /** output file handle */
    FILE *output_fd;
    /** case insensitive search */
    int case_insensitive;
} flags_t;

/**
 * @brief search modes
 */
typedef enum search_mode_t {
    /** stupid, dumb case insensitive search over file */
    SEARCH_SUBSTRING = 0, 

    /** search only lines which start with segment name */
    SEARCH_SEGMENT   = 1,

    /** parse segments and search in specific fields */
    SEARCH_NODE      = 2
} search_mode_t;

/**
 * @brief search result
 * 
 * pos is the position in the line (SEARCH_SEGMENT/SEARCH_SUBSTRING) or
 * position in the segment (SEARCH_NODE)
 */
typedef struct result_item_t {
    /** file name */
    char *file;
    /** result line number */
    int line_num;
    /** position of result in segment or line */
    int pos;      
    /** optional, for segment based searches */
    hl7_addr_t *addr; 
    /** field or result content */
    char *str;
    /** length of the data buffer confusingly named str */
    int length;
} result_item_t;

/**
 * @brief holds 0-N result items
 * 
 * Holds relevant search parameters and result items. This is used as buffer 
 * during search over various files.
 */
typedef struct search_res_t {
    /** file name */
    char *file;
    /** array of addresses to search */
    hl7_addr_t **addr;
    /** number of addresses in addr */
    int addr_l;
    /** search until first result in file (greedy==0) or keep going (greedy==1) */
    int greedy;
    /** search string */
    unsigned char *search_term;
    /** number of result items? */
    int length;
    /** result item array */
    result_item_t **items;
} search_res_t;

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief create default values for flags_t
 */
flags_t *create_flags_t();

/**
 * @brief initiaizes a result container
 * 
 * @param search_term string to search for
 * @returns an empty result contrainer
 */
search_res_t *create_search_res(unsigned char* search_term);

/**
 * @brief frees all data associated with search result
 * 
 * make sure to allocate data for every item.
 * 
 * @param[out] sr search result struct
 */
void free_search_res(search_res_t *sr);

/*
 * @brief append result to result container
int append_result(char *file, int line_num, int pos, hl7_addr_t *addr, search_res_t **res, char *str, int length);
 */

/**
 * @brief parse address string
 * 
 * split address strings by spaces.
 * 
 * The array is terminated by a sentinel.
 * 
 * @param addrstr address string to parse
 * @param length length of string
 * @returns array of addresses
 */
hl7_addr_t **parse_address(char *addrstr, int *length);

/**
 * @brief free address array
 * 
 * will free al lelements and the array itself.
 * 
 * @param[out] addr address array
 */
void free_addr_arr(hl7_addr_t **addr);

/**
 * @brief search for files
 * 
 * Return codes:
 * - 0: success
 * - 11: failed to open file
 * - 12: failed to parse meta data @see read_meta()
 * - 13: invalid address @see parse_address()
 * - 14: invalid/unknown search mode
 * - 2*: general error in SEARCH_SUBSTRING, @see search_substring()
 * - 3*: general error in SEARCH_SEGMENT, @see search_segment()
 * - 4*: general error in SEARCH_NODE, @see search_node()
 * 
 * @param filename file to search
 * @param flags search configuration
 * @returns 0 on success, error code otherwise
 */
int search_file(char *filename, flags_t flags);

/**
 * @brief search substring
 * @todo
 * not implemented
 * 
 * @param fd file to search in, seeking from the current position
 * @param[out] sr search result container
 * @returns 0 on success, 20-29 on error
 */
int search_substring(FILE* fd, search_res_t *sr);

/**
 * @brief this search variant is a line based search
 * 
 * We fetch every line, compare the segment name with the ones which are 
 * sought after. If the segment name matches with one in the addr definitions
 * we do a substring search. 
 * 
 * If the greedy option is set, we are searching for all occourances of 
 * sr->search_term. If not, the function returns upon the first match or
 * when the end of file is reached.
 * 
 * Results are stored in sr. sr->length holds the number of results, 
 * sr->items is an array of search results.
 * 
 * The user must take care to properly deallocate sr after use. 
 * @see free_search_res()
 * 
 * Return Codes:
 *  - 30: failed to allocate memory
 *  - 31: failed to detect delimiters 
 *  - 32: no line delimiter found, we do not accept one line files
 * 
 * @param fd file to search in, seeking from the current position
 * @param[out] sr search result container
 * @returns 0 on success, 30-39 on error
 */
int search_segment(FILE* fd, search_res_t *sr);

/**
 * search sub nodes
 * 
 * concatenates all sub nodes of an address and searches for substring in
 * sr->search_term. if sr->search_term is NULL, then the concatenated line is 
 * returned as new sr->items.
 * 
 * Return Codes:
 * - 1: failed to search sub_nodes @see _search_subnodes()
 * 
 * @param fieldlist the base node to search in
 * @param[out] sr pointer to search result
 * @param line_num needed to add positional information to sr 
 * @param meta the current hl7 file meta data
 * @param segment_rep segment repetition, all == -1
 * @returns int 0 on success
 */
int search_subnodes(node_t *fieldlist, search_res_t *sr, int line_num, 
                    hl7_meta_t *meta, int segment_rep);

/**
 * @brief parse line and find in sub elements
 * 
 * Error codes:
 * - 41: failed to read metadata @see read_meta()
 * - 42: failed to parse segment
 * - 43: failed to search subnodes @see search_subnodes()
 *  
 * 
 * @param fd file to search in, seeking from the current position
 * @param[out] sr search result container
 * @returns 0 on success, 40-49 on error
 */
int search_node(FILE* fd, search_res_t *sr);

/**
 * @brief JSON value, entities escaped
 * 
 * Escape all `"` characters with `\\` + `"` inside the string.
 * 
 * @param item item containing item->str with data and item->length 
 * @param flags command line search flags
 * @returns json string value
 */
void print_json_value(result_item_t *item, flags_t flags);

/**
 * @brief XML value, entities escaped
 * 
 * make sure to escape all vital xml entities and wrap a tag around the value.
 * 
 * | Special character | gets replaced by | escaped form |
 * |------------------ |----------------- |------------- |
 * | Ampersand         | `&amp;`          | `&`          |
 * | Less-than         | `&lt;`           | `<`          |
 * | Greater-than      | `&gt;`           | `>`          |
 * | Quotes            | `&quot;`         | `"`          |
 * | Apostrophe        | `&apos;`         | `'`          |
 * 
 * @param item iem containing item->str with data and item->length 
 * @param flags command line search flags
 * @returns xml tag as string
 */
void print_xml_value(result_item_t *item, flags_t flags);

/**
 * @brief print a result item as json
 * 
 * @param item the item to print
 * @param flags command line search flags
 * @param last 1 if this is the last record, so we can ommit the the comma after the object
 */
void output_json(result_item_t *item, flags_t flags, int last);

/**
 * @brief print a result item as xml
 * 
 * @param item the item to print
 * @param flags command line search flags
 */
void output_xml(result_item_t *item, flags_t flags);

/**
 * @brief print a result item as csv
 * 
 * @param item the item to print
 * @param flags command line search flags
 */
void output_csv(result_item_t *item, flags_t flags);

/**
 * @brief printf a result
 * 
 * @param item the result item to print
 * @param flags command line arguments
 */
void output_string(result_item_t *item, flags_t flags);

#ifdef __cplusplus
}
#endif

#endif // SEARCH_H