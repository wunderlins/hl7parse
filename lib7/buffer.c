/**
 * dynamically growing byte array
 */
#include "buffer.h"
#include "logging.h"

static const int buf_prealloc_size = 256;

buf_t *new_buf_t() {
    buf_t *buffer = (buf_t*) malloc(sizeof(buf_t));
    buffer->buffer = malloc(buf_prealloc_size);
    buffer->buffer[0] = 0;
    buffer->allocated = buf_prealloc_size;
    buffer->length = 0;

    return buffer;
}

void free_buf(buf_t *buffer) {
    if (buffer->buffer != NULL) {
        free(buffer->buffer);
        buffer->buffer = NULL; // prevent double frees
    }

    if (buffer != NULL) {
        free(buffer);
        buffer = NULL; // prevent double frees
    }
}

static inline int _realloc_buf(buf_t *buffer, int add) {
    if (buffer->length + add < buffer->allocated) {
        ; // pass, we have enough room
    } else
    
    // preallocate small chunk and copy
    if (buffer->length + add < (buffer->allocated + buf_prealloc_size)) {
        buffer->buffer = (unsigned char*) realloc(buffer->buffer, buffer->allocated + buf_prealloc_size);
        if (buffer->buffer == NULL)
            return 1;
        buffer->allocated = buffer->allocated + buf_prealloc_size;
    } else { // preallocate large chunk and copy
        int newlen = buffer->allocated + buf_prealloc_size + add;
        buffer->buffer = (unsigned char*) realloc(buffer->buffer, newlen);
        if (buffer->buffer == NULL)
            return 1;
        buffer->allocated = newlen;
    }

    return 0;
}

/**
 * @bug
 * questionable interface design
 * 
 * input trats data as byte array and requires an absolute length. internally 
 * we set a '\0' char to the last byte, potentially loosing data.
 * 
 * on the other hand append_bufc() doesn't care, how should it.
 */
int append_buf_str(buf_t *buffer, int size, unsigned char* data) {
    log_trace("length %d, size %d, allocated %d, buffer '%s', data '%s'", buffer->length, size, buffer->allocated, buffer->buffer, data);

    int ret = _realloc_buf(buffer, size);
    if (ret != 0)
        return ret;

    // copy data
    if (size) {
        log_trace("ADDING DATA: %p %p %s", buffer->buffer, buffer->buffer + buffer->length, data);
        memcpy(buffer->buffer + buffer->length, data, size);
        log_trace("%s", buffer->buffer);

        buffer->length += size-1;

        // make sure sring is NULL Terminated
        buffer->buffer[buffer->length] = 0;
    }

    return 0;
}

int append_bufc(buf_t *buffer, unsigned char c) {
    log_trace("length %d, add %d, allocated %d, buffer '%s', data '%c'", buffer->length, 1, buffer->allocated, buffer->buffer, c);

    int ret = _realloc_buf(buffer, 1);
    if (ret != 0)
        return ret;

    log_trace("ADDING CHAR: %p %p '%c'", buffer->buffer, buffer->buffer + buffer->length, c);
    memcpy(buffer->buffer + buffer->length, &c, 1);
    log_trace("%s", buffer->buffer);
    buffer->length += 1;

    // make sure sring is NULL Terminated
    buffer->buffer[buffer->length] = 0;

    return 0;
}
