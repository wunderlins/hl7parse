/** \file
 * @brief generic growing buffer library
 */
#ifndef BUFFER_H
#define BUFFER_H
#include <stdlib.h>
#include <string.h>

/**
 * @brief dynamically growing generic bytearray
 */
typedef struct buf {
    /** length of the content */
    int length;
    /** length of the allocated memory (will dynamically be raised if too small) */
    int allocated;
    /** byte array containg the data */
    unsigned char *buffer;
} buf_t;

/**
 * @brief allocate initial memory
 */
buf_t *new_buf_t();

/**
 * @brief release all memory of buffer
 * 
 * @param buffer
 */
void free_buf(buf_t *buffer);

/**
 * @brief add data to buffer
 *
 * we excpect buffer always to be at least 1 byte long and 
 * terminated by `\0`.
 * 
 * The output buffer will always be `\0` terminated.
 * 
 * to prevent constant reallocs() on small chunks, the allocated size
 * `buf_t.allocated` is doubled when more memory is required.
 * 
 * If a large chunk is added that is larger than `buf_t.allocated * 2` then 
 * the current size + the size of the new chunk is calculated and reallocated.
 * 
 * @param buffer the buffer to work on
 * @param size size of the new chunk
 * @param data byte array to papend
 * @returns 0 on success, error code otherwise
 */
int append_buf_str(buf_t *buffer, int size, unsigned char* data);

/**
 * @brief append a character
 * 
 * slightly faster than `append_buf_str(...)` which takes a C character as input.
 * 
 * @param buffer the buffer to work on
 * @param c the character to add
 * @returns 0 on success, error code otherwise
 */
int append_bufc(buf_t *buffer, unsigned char c);

#endif // BUFFER_H