/** 
 * @brief extract embedded PDF from MDM message and open it in pdf viewer
 */
#ifdef __MINGW32__
    #include <windows.h>
#endif

#include <limits.h>
#include <stdlib.h>
#include <sys/types.h>

#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "logging.h"
#include "meta.h"
#include "decode.h"
#include "node.h"
#include "address.h"
#include "search.h"
#include "base64.h"
#include "util.h"
#include "ini.h"
#include "../generated/usage_pdf.h"

/* Create a realpath replacement macro for when compiling under mingw
 * Based upon https://stackoverflow.com/questions/45124869/cross-platform-alternative-to-this-realpath-definition
 */
#ifdef WIN32
    #define realpath(N,R) _fullpath((R),(N),PATH_MAX)
#endif

const int loglevel = LEVEL_WARNING;
char *appdir = NULL;

#ifdef PRODUCTION
    void usage() {
        // create temp file with usage content
        pid_t pid = getpid();
        char *tmpdir = getenv("TEMP");
        if (tmpdir == NULL)
            tmpdir = "/tmp";
        char *tmpfile = malloc(strlen(tmpdir) + 50);
        sprintf(tmpfile, "%s/%d_7pdf_usage.txt", tmpdir, (int) pid);
        log_info("tmp: %s", tmpfile);

        // write usage to temp file
        FILE* fd = fopen(tmpfile, "wb");
        fputs(pdf_usage, fd);
        fclose(fd);

        // path to dialog exe
        char *dialog = malloc(strlen(appdir) + 20);
        strcpy(dialog, appdir);
        strcat(dialog, "/dialog");

        // execute
        execlp(dialog, dialog, "\"Usage\"", "-f",  tmpfile, (char*) NULL);

        exit(1);
    }

    void error(int exitcode, char *message) {

        // FIXME: find system specific dialog app

        char *dialog = malloc(strlen(appdir) + 20);
        strcpy(dialog, appdir);
        strcat(dialog, "/dialog");
       
        //char *msg = malloc(strlen(message) + 3);
        //sprintf(msg, "\"%s\"", message);

        // windows cmd.exe handles quoting differently. use ^
        // as escape character. make sure to escape:
        // < | > ^, use caret ^ as escape character
        //
        // because variable expnsion takes place, we need to deal with 
        // %%, however the scape character für % is %, so %%
        // 
        // delayed variable expansion changes the rules, make sure that it is 
        // not used: https://www.robvanderwoude.com/escapechars.php

        /*
        char *msg_tmp = escape(message, "\"<>|", '^');
        char *msg_tmp2 = escape(msg_tmp, "%", '%');
        char *msg = malloc(strlen(msg_tmp2) + 3);
        sprintf(msg, "\"%s\"", msg_tmp2);
        printf("%s", msg);
        */

        execlp(dialog, dialog, "\"Error\"", message, (char*) NULL);
        exit(exitcode);
    }
#else
    void usage() {
        printf("Usage: 7pdf <hl7_file> [element address] [viewer.exe]\n");
        exit(1);
    }

    void error(int exitcode, char *message) {
        // TODO: allow escape to accept more than one escape_character
        char *msg_tmp = escape(message, "\"<>|", '^');
        char *msg_tmp2 = escape(msg_tmp, "%", '%');
        char *msg = malloc(strlen(msg_tmp2) + 3);
        sprintf(msg, "\"%s\"", msg_tmp2);
        fprintf(stderr, "%s", msg);
        exit(exitcode);
    }
#endif

// initialize command line defaults
flags_t search_flags = {
    0, // v: verbose
    0, // s: search term
    1, // n: greedy
    0, // o=json: json output
    0, // o=xml: xml output
    0, // o=csv: csv output
    0, // a: address to search in
    NULL, // search_term (with s)
    NULL, // address string (with a)
    1, // q: quiet, only for console output, displays values only
    1, // d: base64 decode values
    0, // f: output file
    NULL, // output_file_value, name of the output file
    NULL, // FILE* output_file handle
};

#ifndef __MINGW32__
// FIXME: this does not work on OSX!
char *program_path() {
    char *path = malloc(PATH_MAX);
    if (path != NULL) {
        if (readlink("/proc/self/exe", path, PATH_MAX) == -1) {
            free(path);
            path = NULL;
        }
    }
    return path;
}
#endif

int main(int argc, char **argv) {

    int ret = -1;
    int i = 0;
    FILE *fd;
    ini_section_list_t *ini = NULL;
    

    // resolve working directory
#ifdef __MINGW32__
    appdir = strdup(argv[0]);
    ret = 1;
#else
    //printf("realpath, %s\n", argv[0]);
    appdir = malloc(PATH_MAX);
    ret = (realpath(program_path(), appdir) != NULL);
    //log_info("appdir: %s", appdir);
#endif

    
    // remove the file part
    i = strlen(appdir)-1;
    //printf("appdir: '%s'\n", appdir);
    while (appdir[i] != '\\' && appdir[i] != '/')
        appdir[i--] = 0;
    // printf("%s\n", appdir);

    if (argc < 2) {
        usage();
    }

    char *inifile = malloc(PATH_MAX);
    strcpy(inifile, appdir);
    strcat(inifile, "7pdf.ini");
    
    // check if the file exists
    if( access( inifile, F_OK|R_OK ) != 0 ) {
        // for linux, check ../etc/7pdf.ini
        strcpy(inifile, appdir);
        strcat(inifile, "../etc/7pdf.ini");
        log_debug("inifile: '%s'", inifile);
        inifile = realpath(inifile, NULL);
        log_debug("inifile: '%s'", inifile);
        
        if( access( inifile, F_OK|R_OK ) != 0 ) {
            // file does not exists
            error(3, "7pdf.ini not found, Aborting\n");
        }
    } 
    log_info("inifile: %s", inifile);
    
    // read ini file
    FILE *fd_ini = fopen(inifile, "rb");
    if (fd_ini == NULL) {
        fprintf(stderr, "Failed to open ini file %s\n", inifile);
    }
    ini = ini_parse(fd_ini);
    fclose(fd_ini);

    // get a section, and then an item
    ini_section_t *sect = ini_find_section(ini, "7pdf");
    log_debug("section length: %d", sect->length);
    ini_item_t *address = ini_find_key(sect, "address");
    ini_item_t *viewer  = ini_find_key(sect, "viewer");
    log_debug("Viewer: %s", viewer);
    log_debug("Addr:   %s", address);
    
    // copy relevant parts to local variables
    char *str_address = strdup(address->value);
    char *str_viewer  = strdup(viewer->value);

    // address from cmd ?
    if (argc > 2) {
        if(strcmp(argv[2], "") != 0 && strcmp(argv[2], "\"\""))
            str_address = argv[2];
    }

    // viewer from cmd ?
    if (argc > 3) {
        str_viewer = argv[3];
    }
    
    // free ini parser
    ini_free(ini);
    
    log_debug("ini address: %s", str_address);
    log_debug("ini viewer: %s", str_viewer);

    // check what addresses we have to search
    int addr_l = 0;
    hl7_addr_t **addr = parse_address(str_address, &addr_l);
    if (loglevel >= LEVEL_DEBUG)
        addr_dump(addr[0]);
    
    // check if we can see the hl7 file
    char *hl7_file = argv[1];
    if( access(hl7_file, F_OK ) == -1 ) {
        // file does not exists or is not readable
        char *msg = malloc(60 + strlen(hl7_file));
        sprintf(msg, "%s not readable, Aborting\n", hl7_file);
        error(4, msg);
    }
    
    // check if the file exists and is readable
    fd = hl7_open(hl7_file);
    if (fd == NULL) {
        char *msg = malloc(60 + strlen(hl7_file));
        sprintf(msg, "Failed to open %s\n", hl7_file);
        error(11, msg);
    }

    // initialize separator data structure
    hl7_meta_t* hl7_meta = init_hl7_meta_t();

    // check if this file has a bom;
    // this will move df at the first byte after the BOM
    hl7_meta->bom = detect_bom(fd); 

    // find delimiters
    ret = read_meta(hl7_meta, fd);
    if (ret != 0) {
        free_hl7_meta(hl7_meta);
        hl7_meta = NULL;

        error(12, "Parser: failed to find delimiters");
    }
    
    line_delimiter_t d = find_line_delimiter(fd);
    hl7_meta->crlf = 0;
    hl7_meta->sep_message = d;
    if (d == DELIM_CRLF) {
        hl7_meta->crlf = 1;
        hl7_meta->sep_message = '\r';
    } else if (d == DELIM_CR)
        hl7_meta->sep_message = '\r';
    else 
        hl7_meta->sep_message = '\n';
    
    log_debug("%s", hl7_meta_string(hl7_meta));

    // search mode:
    search_res_t *sr = create_search_res(NULL);
    sr->greedy = search_flags.greedy;
    sr->file = strdup(hl7_file);
    sr->addr = addr;
    sr->addr_l = addr_l;
    ret = search_node(fd, sr);
    fclose(fd);
    
    
    // check if we have a result, there should be exactly one:
    for (i=0; i<sr->length; i++) {
        log_debug("Result %d: line: %d, pos: %d", i, sr->items[i]->line_num, sr->items[i]->pos);
    }
    
    // abort if we didn't find any result
    if (sr->length != 1) {
        fprintf(stderr, "did not find element %s\n", str_address);

        char *msg = malloc(60 + strlen(str_address));
        sprintf(msg, "did not find element %s\n", str_address);
        error(5, msg);
    }

    // open result temp file
    pid_t pid = getpid();
    char *tmpdir = getenv("TEMP");
    if (tmpdir == NULL)
        tmpdir = "/tmp";
    char *tmpfile = malloc(strlen(tmpdir) + 50);
    sprintf(tmpfile, "%s/%d.pdf", tmpdir, (int) pid);
    log_info("tmp: %s", tmpfile);
    
    // base64 decode data and write it to dst file
    fd = fopen(tmpfile, "wb");
    if (fd == NULL) {
        fprintf(stderr, "Failed to open temp file: %s\n", tmpfile);

        char *msg = malloc(60 + strlen(tmpfile));
        sprintf(msg, "Failed to open temp file: %s\n", tmpfile);
        error(6, msg);
    }
    
    result_item_t *item = sr->items[0];
    size_t in_len = strlen(item->str); // FIXME: item->length should know the length in bytes already
    //size_t out_len = in_len * 2.2;
    //unsigned char *out = malloc(out_len);
    ret = hl7_64decode_fd(item->str, in_len, fd);
    if (ret != 0) {
        fprintf(stderr, "Invalid character in base64 data, error: %d\n", ret);

        char *msg = malloc(60);
        sprintf(msg, "Invalid character in base64 data, error: %d\n", ret);
        error(7, msg);
    }
    //i=0;
    //while(i < out_len)
    //    fputc(out[i++], fd);
    //free(out);
    fclose(fd);
    
    // free search result, not needed anymore
    free_search_res(sr);
    
    // open viewer
    char *cmd = malloc(strlen(tmpfile) + strlen(str_viewer) + 5);
    sprintf(cmd, "%s %s", str_viewer, tmpfile);

    // FIXME: for windows, don't use system
    //  execl("c:\\winnt\\system32\\notepad.exe", 0);
    // or: execlp("notepad.exe", 0);

    log_debug("%s, %s, %s\n", str_viewer, str_viewer, tmpfile);

#ifndef __MINGW32__
    execl(str_viewer, str_viewer, tmpfile, (char*) NULL);
#else
    STARTUPINFO si;
    PROCESS_INFORMATION pi;

    ZeroMemory( &si, sizeof(si) );
    si.cb = sizeof(si);
    ZeroMemory( &pi, sizeof(pi) );

    // Start the child process. 
    if( !CreateProcess( NULL,   // No module name (use command line)
        cmd,        // Command line
        NULL,           // Process handle not inheritable
        NULL,           // Thread handle not inheritable
        FALSE,          // Set handle inheritance to FALSE
        0,              // No creation flags
        NULL,           // Use parent's environment block
        NULL,           // Use parent's starting directory 
        &si,            // Pointer to STARTUPINFO structure
        &pi )           // Pointer to PROCESS_INFORMATION structure
    ) {
        char *err = malloc(60);
        sprintf(err, "CreateProcess failed (%ld).", GetLastError());
        error(14, err);
    }

    // Wait until child process exits.
    WaitForSingleObject( pi.hProcess, INFINITE );

    // Close process and thread handles. 
    CloseHandle( pi.hProcess );
    CloseHandle( pi.hThread );
   
#endif

    // cleanup
    // prevent race condition
    sleep(5);
    unlink(tmpfile);
    free_hl7_meta(hl7_meta);
    return 0;
}
