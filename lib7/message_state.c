#include "message_state.h"

message_state_t *message_state_new() {
    message_state_t *ms = malloc(sizeof(message_state_t));
    ms->progress_every = 0;
    ms->parsed_length  = 0;
    ms->cb_start       = NULL;
    ms->cb_progress    = NULL;
    ms->cb_segment     = NULL;
    ms->cb_end         = NULL;

    return ms;
}

void message_state_free(message_state_t *ms) {
    if (ms != NULL)
        free(ms);
    ms = NULL;
}