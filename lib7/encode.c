#include "logging.h"
#include "encode.h"

/**
 * escape hl7 string
 * 
 * input length does contain the \0 delimiter
 */
int hl7_escape(unsigned char *inp, unsigned char **outp, int in_length, int *out_length, hl7_meta_t *meta) {
    //printf("length %d, '%s'\n", length, inp);
    buf_t *buf = new_buf_t();

    int i = 0;
    int l = 0;
    int r = 0;
    unsigned char charbuf[9] = {0};

    // loop over input buffer, skip \0
    for (i = 0; i < in_length-1; i++) {
        unsigned char *p = inp + i;

        log_trace("%c %d", *p);
        if (*p == meta->sep_message) { // LF
            charbuf[0] = meta->sep_escape;
            charbuf[1] = '.';
            charbuf[2] = 'b';
            charbuf[3] = 'r';
            charbuf[4] = meta->sep_escape;
            charbuf[5] = 0;
            l = 6;
        } else if (*p == meta->sep_escape) { // backslash
            charbuf[0] = meta->sep_escape;
            charbuf[1] = 'E';
            charbuf[2] = meta->sep_escape;
            charbuf[3] = 0;
            l = 4;
        } else if (*p == meta->sep_field) { // |
            charbuf[0] = meta->sep_escape;
            charbuf[1] = 'F';
            charbuf[2] = meta->sep_escape;
            charbuf[3] = 0;
            l = 4;
        } else if (*p == meta->sep_rep) { // ~
            charbuf[0] = meta->sep_escape;
            charbuf[1] = 'R';
            charbuf[2] = meta->sep_escape;
            charbuf[3] = 0;
            l = 4;
        } else if (*p == meta->sep_comp) { // ^
            charbuf[0] = meta->sep_escape;
            charbuf[1] = 'S';
            charbuf[2] = meta->sep_escape;
            charbuf[3] = 0;
            l = 4;
        } else if (*p == meta->sep_subcmp) { // &
            charbuf[0] = meta->sep_escape;
            charbuf[1] = 'T';
            charbuf[2] = meta->sep_escape;
            charbuf[3] = 0;
            l = 4;
        } else {
            // no match, append single character
            r = append_bufc(buf, *p);
            if (r != 0)
                return r;
            continue;
        }

        // match, append string
        r = append_buf_str(buf, l, charbuf);
        if (r != 0)
            return r;
    }
    
    *outp = buf->buffer;
    *out_length = buf->length + 1;
    free(buf);

    return 0;
}

/**
 * unescape hl7 string
 * 
 * input length does contain the \0 delimiter
 */
int hl7_unescape(unsigned char *inp, unsigned char **outp, int in_length, int *out_length, hl7_meta_t *meta) {
    unsigned char *buffer = malloc(in_length+1);

    int i = 0;
    int ii = 0;
    int o = 0;
    char tmpbuf[4] = {0};

    // loop over input until length is reached
    for (i = 0; i < in_length; i++) {
        unsigned char *p = inp + i;

        log_trace("%c %d", *p);
        if (*p == meta->sep_escape) { // CR
            // maximum length to expect is 5 bytes
            // check the next 5 bytes if we find another escape char
            ii = 1;
            int handled = 0;
            while (i + ii < in_length && ii < 6) {
                if (*(p + ii) == meta->sep_escape) {
                    log_trace("ii: %c, ii: %d, i: %d", *(p + ii), ii, i);
                    // found another escape sequence, figure out 
                    // what to unencode
                    if (tmpbuf[0] == 'E') { // backspace
                        buffer[o++] = meta->sep_escape;
                        i += 2;
                        handled = 1;
                        break;
                    } else if (tmpbuf[0] == 'F') { // |
                        buffer[o++] = meta->sep_field;
                        i += 2;
                        handled = 1;
                        break;
                    } else if (tmpbuf[0] == 'R') { // ~
                        buffer[o++] = meta->sep_rep;
                        i += 2;
                        handled = 1;
                        break;
                    } else if (tmpbuf[0] == 'S') { // ^
                        buffer[o++] = meta->sep_comp;
                        i += 2;
                        handled = 1;
                        break;
                    } else if (tmpbuf[0] == 'T') { // &
                        buffer[o++] = meta->sep_subcmp;
                        i += 2;
                        handled = 1;
                        break;
                    } if (tmpbuf[0] == '.' &&
                          tmpbuf[1] == 'b' &&
                          tmpbuf[2] == 'r') { // CR
                        buffer[o++] = meta->sep_message;
                        i += 4;
                        handled = 1;
                        break;
                    }

                    // ok we don't know this sequence, write tmpbuf
                    // to buffer and fast-forward i to the end of 
                    // the current buffer
                    buffer[o++] = meta->sep_escape;
                    for (int fi=0; fi<ii-1; fi++) {
                        buffer[o++] = tmpbuf[fi];
                    }
                    buffer[o++] = meta->sep_escape;
                    handled = 1;
                    i += ii; // fast-forward
                    break;
                } else {
                    // remeber skipped character
                    tmpbuf[ii-1] = *(p + ii);
                }

                ii++;
            }

            if (handled == 0)
                buffer[o++] = *p;
        } else {
            // no match, append single character
            buffer[o++] = *p;
        }
    }
    buffer[o] = 0; // terminate string

    *outp = buffer;
    *out_length = o + 1;

    return 0;
}
