/* 
 * ini file parser
 * 
 * 2020, Simon Wunderlin
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "ini.h"
#include "util.h"
#include "logging.h"

int resize_section_list(ini_section_list_t *s) {
	size_t l = s->length + ALLOC_NUM_ITEMS;
	ini_section_t **sections = realloc(s->sections, sizeof(ini_section_t) * l);

	if (sections == NULL) // failed to allocate memmory
		return -1;
	//free(s->sections);
	s->sections = sections;

	s->size = l;
	return 0;
}

int resize_items(ini_section_t *s) {
	size_t l = s->length + ALLOC_NUM_ITEMS;
	ini_item_t **items = (ini_item_t **) realloc(s->items, sizeof(ini_item_t) * l);

	if (items == NULL) // failed to allocate memmory
		return -1;

	//free(s->items);
	s->items = items;

	s->size = l;
	return 0;
}

char *read_chunk(FILE *fp, size_t start, size_t end) {
	log_debug("read_chunk(%d, %d, fd pos: %ld)", start, end, ftell(fp));

	// remeber old file pointer position
	long old_pos = ftell(fp);
	long length = end - start;

	// resposition file pointer to the beginning of string
	fseek(fp, start, SEEK_SET);
	char *buffer = malloc(sizeof(char) * (length + 1));
	if (buffer == NULL)
		return NULL;
	size_t l = fread(buffer, 1, length, fp);
	buffer[l] = 0; // terminate string

	// reset file pointer
	fseek(fp, old_pos, SEEK_SET);

	log_debug("fd pos: %ld", ftell(fp));

	// return string
	return buffer;
}

char *ini_get_value(ini_section_list_t *ini,
                const char* section, const char* key) {
	ini_section_t *sect = ini_find_section(ini, section);
	if (sect == NULL)
    return NULL;

  ini_item_t *item = ini_find_key(sect, key);
  if (item == NULL) 
    return NULL;

  return item->value;
}

ini_section_t *ini_parse_section(FILE *fp, char c, size_t pos, last_pos_t *last_pos) {

	// valid data
	if (last_pos->section_start == 0 && last_pos->section_end == 0) {
		return NULL;
	}

	char *buffer = read_chunk(fp, last_pos->section_start+1, last_pos->section_end);
	log_debug("section start: %d, section end: %d: buffer: '%s', ftell(): %d", last_pos->section_start, last_pos->section_end, buffer, ftell(fp));
	ini_section_t *section;
	section = malloc(sizeof(ini_section_t));
	if (section == NULL) {
		return NULL;
	}

	//log_debug("%p %s", section, buffer);
	section->items  = malloc(sizeof(ini_item_t));
	section->name   = buffer;
	section->length = 0;
	section->size   = 0;
	resize_items(section);
	log_debug("%p %s, ftell(): %d", section, section->name, ftell(fp));

	// reset
	last_pos->section_start = 0;
	last_pos->section_end   = 0;

	return section;
}

ini_item_t *ini_parse_item(FILE *fp, char c, size_t i, 
                           ini_section_list_t *s, last_pos_t *last_pos) {

	char *name  = NULL;
	char *value = NULL;
	int section_pos = s->length -1;

	ini_item_t *item;
	item = malloc(sizeof(ini_item_t));
	name = read_chunk(fp, last_pos->item_start, last_pos->item_equal);

	log_debug("name: '%s', start: %d, equal: %d, end: %d", name, last_pos->item_start, last_pos->item_equal, last_pos->item_end);

	if (last_pos->item_comment > last_pos->item_equal && 
			last_pos->item_comment < last_pos->item_end)
		value = read_chunk(fp, last_pos->item_equal+1, last_pos->item_comment);
	else
		value = read_chunk(fp, last_pos->item_equal+1, last_pos->item_end);

	// if the first character of name is a ';'
	// disregard this line
	if (name[0] == ';') {
		free(name);
		free(value);
		free(item);
		return NULL;
	}

	item->name  = trim(name);  
	item->value = trim(value); 
	free(name);
	free(value);
	item->start = last_pos->item_start;
	item->end   = last_pos->item_end;
	
	// allocate more mem for items
	if (s->sections[section_pos]->length-1 == s->sections[section_pos]->size)
		resize_items(s->sections[section_pos]);

	int item_pos = s->sections[section_pos]->length;
	s->sections[section_pos]->items[item_pos] = item;
	s->sections[section_pos]->length = s->sections[section_pos]->length + 1;

	return item;
}

ini_section_list_t *ini_parse(FILE *fp) {
	char c = 0;
	char last_c = 0;
	size_t i;
	ini_section_list_t *s;
	s = malloc(sizeof(ini_section_list_t));
	if (s == NULL)
		return NULL;
	s->size = 1;
	s->length = 0;
	s->sections = malloc(sizeof(ini_section_t));
	resize_section_list(s);
	int in_section = 0;

	// file length
	fseek(fp, 0, SEEK_END);
	long eof = ftell(fp);
	// make sure we start at the beginning of the file
	rewind(fp); 

	last_pos_t p = {0, 0, 0, 0, 0, 0};
	last_pos_t *last_pos = &p; 

	// loop char by char over file
	for (i = 0; feof(fp) == 0; i++) {
		c = getc(fp);
		log_debug("%c, ftell(): %d", c, (unsigned char) ftell(fp));

		// find out what to do acoording to the character
		switch (c) {
			case '[':
				log_debug("%c", c);
				if (last_c == '\n' || last_c == '\r' || s->length == 0) {
					last_pos->section_start = i;
					last_pos->section_end   = 0;
					last_c = c;
					in_section = 1;
					continue;
				}
				break;

			case ']':
				log_debug("%c", c);
				if (in_section == 1) {
					// allocate more mem for items
					if (s->length-1 == s->size) {
						int ret = resize_section_list(s);
						if (ret == -1) {
							log_error("Failed to resize section");
							return NULL;
						}
					}
					
					last_pos->section_end   = i;
					s->sections[s->length] = ini_parse_section(fp, c, i, last_pos);
					log_debug("section name: '%s', ftell(): %d", s->sections[s->length]->name, ftell(fp));
					s->length ++;
					last_c = c;
					
					last_pos->section_start = 0;
					last_pos->section_end   = 0;
					in_section = 0;
					continue;
				}
				break;
		}

		// are we currently waiting for a section end ?
		if (in_section == 1) {
			last_c = c;
			continue;
		}
		
		// if we got this far, we are dealing with items
		if (((c == '\r' || c == '\n') && last_pos->item_start) ||
		      i+1 == eof) {
			last_pos->item_end     = i;
			if (i+1 == eof)
			last_pos->item_end     = i+1;

			int valid = 1;
			// handle incomplete situations
			if (last_pos->item_equal == 0)
				valid = 0; // no key/value delimiter found

			if (last_pos->item_start+1 == last_pos->item_end)
				valid = 0; // empty line

			// no section yet, skip
			int section_pos = s->length -1;
			if (section_pos < 0)
				valid = 0;
			
			// read name and value
			if (valid)
				ini_parse_item(fp, c, i, s, last_pos);

			// reset item
			last_pos->item_start   = i+1;
			last_pos->item_end     = 0;
			last_pos->item_equal   = 0;
			last_pos->item_comment = 0;

			last_c = c;
			continue;
		}

		if ((c == '\r' || c == '\n') && !last_pos->item_start) {
			log_debug("item: newline");
			last_pos->item_start = i+1;
		}

		// remeber first equal sign
		if (c == '=' && last_pos->item_equal == 0) {
			log_debug("item: equal");
			last_pos->item_equal = i;
		}

		// deal with comments
		if (c == ';' && last_pos->item_comment == 0) {
			log_debug("item: comment");
			last_pos->item_comment = i;
		}
		
		last_c = c;
	}
	log_debug("end, last_c = %c", last_c);
	return s;
}

void ini_free(ini_section_list_t *ini) {
	int i, ii;
	for (i=0; i<ini->length; i++) {

		for(ii=0; ii < ini->sections[i]->length; ii++) {
			free(ini->sections[i]->items[ii]->name);
			free(ini->sections[i]->items[ii]->value);
			free(ini->sections[i]->items[ii]);
		}

		free(ini->sections[i]->name);
		free(ini->sections[i]->items);
		free(ini->sections[i]);
	}
	free(ini->sections);
	free(ini);
}

ini_section_t *ini_find_section(ini_section_list_t *ini, 
                                const char* section_name) {
	int i;
	for (i=0; i<ini->length; i++) {														
		if(strcmp(ini->sections[i]->name, section_name) == 0)
			return ini->sections[i];
	}
	return NULL;
}

ini_item_t *ini_find_key(ini_section_t *s, const char* key) {
	int i = 0;
	for(i=0; i < s->length; i++) {
		if (strcmp(s->items[i]->name, key) == 0)
			return s->items[i];
	}
	return NULL;
}
