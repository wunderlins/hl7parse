#include "node.h"

raw_field_t* create_raw_field_t() {
    raw_field_t *e = (raw_field_t*) malloc(sizeof(raw_field_t));
    if (e == NULL)
        return NULL;
    
    e->delim_l = 0;
    e->length = 0;
    e->field = NULL;
    return e;
};

void free_raw_field(raw_field_t* raw_e) {
    if (raw_e->field != NULL) {
        free(raw_e->field);
        raw_e->field = NULL;
    }
    free(raw_e);
    raw_e = NULL;
}

void disply_raw_node(raw_field_t* raw_e) {
    for (int i = 0; i < raw_e->delim_l; i++) {
        log_trace("%03d%c ", raw_e->pos[i], raw_e->delim[i]);
    }
}

static const char *node_type_names[] = {
    "", // 0 == NONE
    "MESSAGE", //     = 1,
    "SEGMENT", //     = 2,
    "FIELDLIST", //   = 4,
    "FIELD", //       = 8,
    "COMP", //        = 16,
    "SUBCOMP", //     = 32,
    "LEAF" //         = 64
};

const char *node_type_to_string(node_type_t type) {
    if (type == 0)
        return node_type_names[0];

    if (type == 1)
        return node_type_names[1];

    if (type == 2)
        return node_type_names[2];

    if (type == 4)
        return node_type_names[3];

    if (type == 8)
        return node_type_names[4];

    if (type == 16)
        return node_type_names[5];

    if (type == 32)
        return node_type_names[6];

    if (type == 64)
        return node_type_names[7];

    log_error("invalid node type: %d", type);
    return "";
}

int node_parent_child_pos(node_t *node) {
    node_t* parent = node->parent;
    if (parent == NULL)
        return -1;
    
    for (int i=0; i<parent->num_children; i++) {
        if (parent->children[i]->id == node->id) 
            return i;
    }

    return -2;
}

node_t *process_node(raw_field_t* raw_e, hl7_meta_t *meta, int start_pos) {
    log_debug("'%s'", raw_e->field);

    if (loglevel >= LEVEL_DEBUG) {
        raw_e->delim[raw_e->delim_l] = 0;
        log_debug("delimiter: %d '%s'", raw_e->delim_l, raw_e->delim);
    }

    // 1. no sub delimiter
    if (raw_e->delim_l == 0) {
        //unsigned char *data = (unsigned char*) malloc(raw_e->length);
        //memcpy(data, raw_e->field, raw_e->length);
        //data[raw_e->length-1] = 0;
        log_debug("single field: %02d | '%s'", raw_e->length, raw_e->field);
        return create_node_t(FIELD, raw_e->field, raw_e->length, start_pos);
    }

    // 2. split by component (^) and sumcomponent (&)

    // remeber the current level, we have COMP, SUBCOMP
    node_type_t level = COMP;

    int ret = 0;

    // remeber the previsous delimiter, we start with a '|'
    char last_delim = meta->sep_comp;

    // reference node
    node_t *n = create_node_t(FIELD, NULL, 0, start_pos);
    if (n == NULL)
        return NULL;
    node_t *parent = n;

    // length of the current element's data
    int l = 0;
    // sub element counter
    int i = 0;
    // start pos
    int start = 0;
    for (i = 0; i < raw_e->delim_l; i++) {
        //if (i > 0) start = raw_e->pos[i-1] + 1;
        if (i > 0) start = raw_e->pos[i-1] + 1;
        l = raw_e->pos[i] - start;

        char next_delim = 0;
        if (i < raw_e->delim_l)
            next_delim = raw_e->delim[i];

        // new sub level
        int first_subcmp = 0;
        /*
        if (last_delim == '|' && next_delim == '&') {
            printf("Error in pos: %d\n", (start_pos + start));
        }
        */
        if ((level == COMP && last_delim == meta->sep_subcmp) || // look back
            (next_delim != 0 && level == COMP && next_delim == meta->sep_subcmp) // look forward
            ) {
                
            if (next_delim != 0 && level == COMP && next_delim == meta->sep_subcmp) {
                first_subcmp = 1;
            }
            node_t *c = create_node_t(COMP, NULL, 0, start_pos + start);
            if (c == NULL)
                return NULL;

            ret = node_append(&parent, c);
            if (ret != 0)
                return NULL;

            parent = c;
            level = SUBCOMP;
        }

        // traverse up
        if (level == SUBCOMP && last_delim == meta->sep_comp && first_subcmp == 0) {
            parent = parent->parent;
            level = COMP;
        }

        // create node
        unsigned char *data = (unsigned char*) malloc(l+1);
        if (data == NULL)
            return NULL;
        memcpy(data, raw_e->field+start, l);
        data[l] = 0;
        node_t *c = create_node_t(level, data, l+1, start_pos + start);
        if (c == NULL)
            return NULL;
        node_append(&parent, c);
        free(data);
        data = NULL;
        if (ret != 0)
            return NULL;

        if (level == COMP) {
            log_debug("--> [%d/COMP] l: %d, start: %d, delim: %c", i, l, start, raw_e->delim[i]);
        } else {
            log_debug("--> [%d/SCMP] l: %d, start: %d, delim: %c", i, l, start, raw_e->delim[i]);
        }

        last_delim = raw_e->delim[i];
    }

    // last element
    start = raw_e->pos[i-1] + 1;
    log_debug("start: %d, pos[i-1]: %d, raw_e->length: %d", start, raw_e->pos[i-1], raw_e->length);
    l = raw_e->length - start -1; // total length includes \0, substract

    /*
    unsigned char *data = (unsigned char*) malloc(l+1);
    memcpy(data, raw_e->field+start, l);
    data[l] = 0;
    log_debug("+data: '%s'", data);
    */

    // new sub level
    if (level == COMP && last_delim == meta->sep_subcmp) {
        node_t *c = create_node_t(COMP, NULL, 0, start_pos);
        if (c == NULL)
            return NULL;
        node_append(&parent, c);
        if (ret != 0)
            return NULL;
        parent = c;
        level = SUBCOMP;
    }

    // traverse up
    if (level == SUBCOMP && last_delim == meta->sep_comp) {
        parent = parent->parent;
        level = COMP;
    }

    // create node
    //node_t *c = new_node_t(level, data, l+1);
    node_t *c = create_node_t(level, raw_e->field+start, l+1, start_pos);
    if (c == NULL)
        return NULL;
    node_append(&parent, c);
    //free(data);
    if (ret != 0)
        return NULL;
    
    if (level == COMP) {
        log_debug("+-> [%d/COMP] l: %d, start: %d, delim: %c", i, l, start, '|');
    } else {
        log_debug("+-> [%d/SCMP] l: %d, start: %d, delim: %c", i, l, start, '|');
    }


    // return created node
    return n;
}

int _node_get_counter() {
    static int node_counter = 0;
    return node_counter++;
}

node_t* create_node_t(node_type_t type, unsigned char *data, size_t length, int pos) {

    node_t *n = (node_t*) malloc(sizeof(node_t));
    if (n == NULL)
        return NULL;

    n->type = type;
    if (length) {
        n->data = (unsigned char*) malloc(length);
        memcpy(n->data, data, length);
    } else {
        n->data = NULL;
    }
    n->length = length;
    n->pos = pos;
    n->parent = NULL;

    // allocate space for children
    n->children = (node_t**) malloc(sizeof(node_t*) * NODE_PREALLOC_CHILDREN);
    n->num_children = 0;
    n->_num_children_allocated = NODE_PREALLOC_CHILDREN;

    n->id = _node_get_counter();
    log_trace("alloc %d, pos: %d, length: %d, type: %s", n->id, n->pos, n->length, node_type_to_string(n->type));
    return n;
}

void free_node_t(node_t *node) {
    int l = node->num_children;
    // free all children, no more slavary, thee-heee, i've done my part!
    for (int i=node->num_children-1; i > -1; i--) {
        if (node->children[i]->data != NULL) {
        	log_trace("FREEING node(%d): [%d] %d '%s'", i, node->children[i]->type, i, node->children[i]->data);
        } else {
        	log_trace("FREEING node(%d): [%d] %d '%s'", i, node->children[i]->type, i, "(null)");
        }
        free_node_t(node->children[i]);
        node->children[i] = NULL;
    }
    node->num_children = 0;

    // free the current node
    if (node->data != NULL) {
    	free(node->data);
        node->data = NULL;
    }
    free(node->children);
    node->children = NULL;
    int pid = -1;
    if (node->parent != NULL)
        pid = node->parent->id;
    log_trace("free %d (%d), parent: %d", node->id, l, pid);
    free(node);
    node = NULL;
}

int node_append(node_t** parent, node_t *node) {

    if (*parent == NULL && node->type > SEGMENT) {
        log_warning("Adding children to node without parent");
        return 2;
    }

    if (node->data != NULL) {
      log_debug("append: %d: '%s'", (*parent)->num_children, node->data);
    } else {
      log_debug("append: %d: '%s'", (*parent)->num_children, "(null)");
    }

    if ((*parent)->_num_children_allocated == (*parent)->num_children) {
        (*parent)->_num_children_allocated = (*parent)->_num_children_allocated * 2;
        node_t** new_children = (node_t**) realloc((*parent)->children, sizeof(node_t*) * (*parent)->_num_children_allocated);
        if (new_children == NULL)
            return 1;
        /*
        // update the parent of all children
        for(int i=0; i<(*parent)->num_children; i++) {
            new_children[i]->parent = *parent;
            //printf("ppid %d\n", (*parent)->id);
        }
        */
       
        log_trace("(malloc) new_parent->num_children: %d", (*new_children)->num_children);
        (*parent)->children = new_children;
    }
    
    (*parent)->children[(*parent)->num_children] = node;
    (*parent)->num_children++;
    node->parent = *parent;
    log_trace("appending %d -> %d (pid), children: %d", node->id, node->parent->id, (*parent)->num_children);
    return 0;
}

message_t* create_message_t(hl7_meta_t *meta) {
    message_t *m = (message_t*) malloc(sizeof(message_t));

    m->type = MESSAGE;
    m->segments = (node_t**) malloc(sizeof(node_t*) * MESSAGE_PREALLOC_CHILDREN);
    m->num_children = 0;
    m->_num_children_allocated = 10;
    m->id = _node_get_counter();
    m->parent = NULL;

    // initialize meta data struct
    if (meta == NULL)
        m->meta = init_hl7_meta_t();
    else
        m->meta = meta;

    m->state = message_state_new();

    log_trace("alloc %d, type: %s", m->id, node_type_to_string(m->type));
    return m;
}

void free_message_t(message_t *message) {
    if (message == NULL)
        return;
    
    // free all children, no more slavary, thee-heee, i've done my part!
    for (int i=message->num_children-1; i > -1; i--) {
        log_trace("FREEING message: [%d] %d", message->segments[i]->type, i);
        free_node_t(message->segments[i]);
        message->segments[i] = NULL;
    }
    message->num_children = 0;
    
    // free meta if there is any
    if (message->meta != NULL) {
        free_hl7_meta(message->meta);
        message->meta = NULL;
    }
    
    // free the current node
    free(message->segments);
    message->segments = NULL;
    free(message);
    message = NULL;

}

int message_append(message_t **parent, node_t *node) {
    log_debug("append: %d: '%s'", (*parent)->num_children, node->data);
    if ((*parent)->_num_children_allocated == (*parent)->num_children) {
        (*parent)->_num_children_allocated = (*parent)->_num_children_allocated * 2;
        node_t** new_children = (node_t**) realloc((*parent)->segments, sizeof(node_t*) * (*parent)->_num_children_allocated);
        if (new_children == NULL)
            return 1;

        log_trace("(malloc) new_parent->num_children: %d", (*new_children)->num_children);
        (*parent)->segments = new_children;
    }

    (*parent)->segments[(*parent)->num_children] = node;
    (*parent)->num_children++;

    node->parent = (node_t*) *parent;

    return 0;
}

node_t *node_in_segment(node_t *segment, hl7_addr_t *addr) {
    if (segment->type != SEGMENT) {
        log_debug("Wrong type: %s", node_type_to_string(segment->type));
        return NULL;
    }

    // on segment level, check if segment matches addr
    if (strcmp((char*) segment->data, addr->segment) != 0) {
        log_debug("Segment name '%s' and '%s' do not match", segment->data, addr->segment);
        return NULL;
    }
    
    // validate if we have something useful to search
    if (addr->fieldlist == -1) {
        log_debug("no field specified");
        return NULL;
    }

    node_t *n = NULL;

    // find node on fieldlist level, we always have at least 1
    int field = 1;
    if (addr->field != -1)
        field = addr->field;

    // check if the number of fields in node is big enough
    if (segment->num_children >= addr->fieldlist) {
        n = segment->children[addr->fieldlist-1];
    } else {
        log_debug("fieldlist not found %d", addr->fieldlist);
        return NULL;
    }
    
    if (n->num_children >= field) {
        log_debug("field %d-%d, type: %d", addr->field, addr->field, n->type);
        n = n->children[field-1];
        log_debug("%d", n->num_children);
    } else {
        log_debug("fieldlist has only %d children, wanted %d", n->num_children, field);
        return NULL;
    }

    if (addr->comp > -1) {
        if (n->num_children >= addr->comp) {
            n = n->children[addr->comp-1];
        } else {
            if (loglevel >= LEVEL_DEBUG)
                addr_dump(addr);
            log_debug("comp not found %d children, wanted %d, pos: %d", n->num_children, addr->comp, n->pos);
            return NULL;
        }
    }

    if (addr->subcmp > -1) {
        if (n->num_children >= addr->subcmp) {
            n = n->children[addr->subcmp-1];
        } else {
            log_debug("subcmp not found %d children, wanted %d", n->num_children, addr->subcmp);
            return NULL;
        }
    }

    return n;
}

hl7_addr_t* addr_from_node(node_t *node) {

    hl7_addr_t tmpa = (hl7_addr_t) {
        .segment = NULL,
        .fieldlist = -1,
        .field = -1,
        .comp = -1,
        .subcmp = -1,
        .seg_count = -1
    };

    // at which level do we start to traverse upwards ?
    node_t *current_node = node;

    int i=0;
    while (current_node->type > SEGMENT) {
        // we are in subcmp, comp, field or fieldlist.
        // do the loopy-loop over children and find position of the 
        // current element while updating tmpa

        // no parent, no valid structure. only message is allowed to have 
        // a NULL parent since it's the root node.
        if (current_node->parent == NULL)
            return NULL;

        int found = 0;
        for(i=0; i < current_node->parent->num_children; i++) {
            if (current_node->parent->children[i] == current_node) {
                found = 1;
                if (current_node->type == SUBCOMP)
                    tmpa.subcmp = i+1;
                else if (current_node->type == COMP)
                    tmpa.comp = i+1;
                else if (current_node->type == FIELD)
                    tmpa.field = i+1;
                else if (current_node->type == FIELDLIST)
                    tmpa.fieldlist = i+1;
                else {
                    // we found a type that does not belong here (below SEGMENT)
                    // so the structure is borked, abandon ship immediately!
                    return NULL;
                }

                current_node = current_node->parent;
                break;
            }
        }
        
        // if we have not found the node in the parent's children, then
        // the structure is borked and some children are inproperly
        // linked, we have to abort.
        if (found == 0)
            return NULL;
    }

    // if we got this far, we know the structure of the segment and 
    // all it's sub components that are associated with it.
    //
    // make sure our parent is a valid MESSAGE
    if (current_node->parent == NULL || current_node->parent->type != MESSAGE)
        return NULL;
    
    // last thing to do is find out if the segment itself is used more than
    // once in this message and update seg_count.
    //
    // current_node should now point to a segment
    tmpa.segment = strdup((char*) current_node->data);

    int seg_count = 0;
    int found_segment = 0;
    for(i=0; i < current_node->parent->num_children; i++) {
        node_t *seg = current_node->parent->children[i];

        if (seg == current_node) {
            tmpa.seg_count = seg_count+1;
            found_segment = 1;
            break;
        }

        if (strcmp((char*) seg->data, (char*) current_node->data) == 0)
            seg_count++;
    }

    // if we have not found the segment in the message, structure is not 
    // valid, abort
    if (found_segment == 0) 
        return NULL;

    // ok, finally we have it. allocate memory on the heap for an address
    // and return it
    hl7_addr_t *ret = create_addr();

    // copy temp data from stack to heap
    *ret = tmpa;

    return ret;
}